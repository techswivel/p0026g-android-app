package com.techswivel.qthemusic

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val topic = "testing_pre_56_4_142_1660921098_wod"
        if (topic.contains("_wod", true)) {
            println("contain wod")
            val newtopic = topic.dropLast(4)
            assertEquals("testing_pre_56_4_142_1660921098", newtopic)
        } else if (topic.contains("_wd", true)) {
            println("contain wd")
            val newtopic = topic.dropLast(3)
            assertEquals("testing_pre_56_4_142_1660921098", newtopic)
        } else {
            assertEquals("testing_pre_56_4_142_1660921098", "testing_pre_56_4_142_1660921098_wod")
            println("no match")
        }
    }

    @Test
    fun regexTest() {
        val regesString =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\" \"@_{}*!-/`,§±~;\\[\\]:?<>'\"\\\\|()#\$%^&+=+])(?=\\S+).{8,}\$"
        val regex = (regesString)
        val p = Pattern.compile(regex)
        val m: Matcher = p.matcher("TechSwivel $001")
        assertEquals(true, m.matches())
    }
}