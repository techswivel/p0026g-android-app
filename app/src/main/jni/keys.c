#include <jni.h>

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_source_remote_retrofit_DataInterceptor_getStagingApiKey(JNIEnv *env,jobject instance) {

    return (*env)->NewStringUTF(env, "eb366ea0-8479-4cfb-9b2a-d4780b75d9ae");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_source_remote_retrofit_DataInterceptor_getDevelopmentApiKey(
        JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "eb366ea0-8479-4cfb-9b2a-d4780b75d9ae");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_source_remote_retrofit_DataInterceptor_getAcceptanceApiKey(
        JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "4871083f-30d4-494e-8934-3b06acb0fdeb");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_source_remote_retrofit_DataInterceptor_getProductionApiKey(
        JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "d4864608-2426-410d-bff7-6c599bb7df83");
}
JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_application_QTheMusicApplication_getGoogleClientIdStaging(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "74605093159-funodu4lv47i9u57h2meqahlcan5elo3.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_application_QTheMusicApplication_getGoogleClientIdDevelopment(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "74605093159-funodu4lv47i9u57h2meqahlcan5elo3.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_application_QTheMusicApplication_getGoogleClientIdAcceptance(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "345540049924-j2pgaphe6hhoios3oag2g7ktpdo1crrb.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_application_QTheMusicApplication_getGoogleClientIdProduction(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "716441253502-tetnmv0sg1nnvkkr715bagro7equ0ubh.apps.googleusercontent.com");
}


JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getRemoteGoogleClientIdStaging(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "74605093159-funodu4lv47i9u57h2meqahlcan5elo3.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientSecretStaging(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "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");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientRedirectUriStaging(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "https://qthemusic-staging.firebaseapp.com/__/auth/handler");
}


JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getRemoteGoogleClientIdDevelopment(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "74605093159-funodu4lv47i9u57h2meqahlcan5elo3.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientSecretDevelopment(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "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");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientRedirectUriDevelopment(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "https://qthemusic-staging.firebaseapp.com/__/auth/handler");
}


JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getRemoteGoogleClientIdAcceptance(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "345540049924-j2pgaphe6hhoios3oag2g7ktpdo1crrb.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientSecretAcceptance(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "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");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientRedirectUriAcceptance(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "https://qthemusic-acceptance.firebaseapp.com/__/auth/handler");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getRemoteGoogleClientIdProduction(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "716441253502-tetnmv0sg1nnvkkr715bagro7equ0ubh.apps.googleusercontent.com");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientSecretProduction(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "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");
}

JNIEXPORT jstring JNICALL
Java_com_techswivel_qthemusic_dataManager_RemoteDataManager_getGoogleClientRedirectUriProduction(
        JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env,
                                "https://qthemusic-production.firebaseapp.com/__/auth/handler");
}
