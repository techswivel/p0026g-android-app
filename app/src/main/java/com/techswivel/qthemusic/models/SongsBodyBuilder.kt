package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.customData.enums.SongType
import java.io.Serializable

class SongsBodyBuilder : Serializable {
    var albumId: Int? = null
    var playListId: Int? = null
    var type: SongType? = null
    var categoryId: Int? = null
    var artistId: Int? = null

    companion object {
        fun build(builder: SongsBodyBuilder) = SongsBodyModel(
            builder.albumId,
            builder.playListId,
            builder.type,
            builder.categoryId,
            builder.artistId
        )
    }
}