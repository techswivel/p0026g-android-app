package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Category

data class Interests(
    @Expose
    @SerializedName("interests")
    val interests: List<Category?>
)