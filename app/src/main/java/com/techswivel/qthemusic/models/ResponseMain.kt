package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseMain(
    @SerializedName("response")
    @Expose
    var response: ResponseModel?
)

