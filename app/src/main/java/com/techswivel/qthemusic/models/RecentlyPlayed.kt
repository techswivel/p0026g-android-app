package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song

data class RecentlyPlayed(
    @Expose
    @SerializedName("albums")
    val albums: List<Album>,
    @Expose
    @SerializedName("artists")
    val artists: List<Artist>,
    @Expose
    @SerializedName("songs")
    val songs: List<Song>
)