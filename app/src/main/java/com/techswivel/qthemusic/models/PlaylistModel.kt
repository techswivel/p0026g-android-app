package com.techswivel.qthemusic.models

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.room.Ignore
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.BR
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlaylistModel(
    @Expose
    @SerializedName("playListId")
    var playListId: Int?,
    @Expose
    @SerializedName("title")
    var title: String?,
    @Expose
    @SerializedName("totalSongs")
    var totalSongs: Int?,
    @Expose
    @SerializedName("playListTitle")
    var playListTitle: String?,
) : BaseObservable(), Parcelable {

    @Ignore
    private var mSelectedButtonVisibility: ObservableField<Int>? = null

    constructor() : this(null, null, null, null) {

    }

    @Bindable
    fun getSelectedButtonVisibility(): ObservableField<Int>? {
        return mSelectedButtonVisibility
    }


    fun setDownloadButtonVisibility(tickVisibility: ObservableField<Int>) {
        this.mSelectedButtonVisibility = tickVisibility
        notifyPropertyChanged(BR.selectedButtonVisibility)
    }
}
