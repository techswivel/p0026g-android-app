package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.SongType

data class NextPlaySong(
    @Expose
    @SerializedName("currentSongId")
    var currentSongId: Int?,
    @Expose
    @SerializedName("categoryId")
    var categoryId: Int?,
    @Expose
    @SerializedName("albumId")
    var albumId: Int?,
    @Expose
    @SerializedName("artistId")
    var artistId: Int?,
    @Expose
    @SerializedName("playListId")
    var playListId: Int?,
    @Expose
    @SerializedName("playedFrom")
    var playedFrom: SongType?
)
