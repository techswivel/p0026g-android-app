package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Song


data class BuyingHistory(
    @Expose
    @SerializedName("itemType")
    val itemType: String,
    @Expose
    @SerializedName("title")
    val songTitle: String,
    @Expose
    @SerializedName("songCoverImageUrl")
    val songCoverImageUrl: String,
    @Expose
    @SerializedName("duration")
    val songDuration: String,
    @Expose
    @SerializedName("price")
    var songPrice: String?,
    @Expose
    @SerializedName("timeOfPurchased")
    val timeOfPurchased: Long,
    @Expose
    @SerializedName("typeOfTransection")
    val typeOfTransection: String,
    @Expose
    @SerializedName("cardPrefix")
    val cardPrefix: Int,
    @Expose
    @SerializedName("sku")
    val sku: String,
    @Expose
    @SerializedName("song")
    val song: List<Song>?,
)