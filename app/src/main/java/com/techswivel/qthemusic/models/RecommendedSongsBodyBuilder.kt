package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.customData.enums.SongType
import java.io.Serializable

class RecommendedSongsBodyBuilder : Serializable {
    var artistId: Int? = null
    var categoryId: Int? = null
    var requestType: SongType? = null
    var dataType: RecommendedSongsType? = null

    companion object {
        fun build(builder: RecommendedSongsBodyBuilder) = RecommendedSongsBodyModel(
            builder.artistId,
            builder.categoryId,
            builder.requestType,
            builder.dataType
        )
    }
}