package com.techswivel.qthemusic.models


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Meta(
    @SerializedName("count")
    @Expose
    var count: Int?,
    @SerializedName("currentPage")
    @Expose
    var currentPage: Int?,
    @SerializedName("perPage")
    @Expose
    var perPage: Int?,
    @SerializedName("total")
    @Expose
    var total: Int?,
    @SerializedName("totalPages")
    @Expose
    var totalPages: Int?
) : Parcelable {
    constructor() : this(null, null, null, null, null)

    fun isEmpty(): Boolean {
        return count == null && currentPage == null && perPage == null && total == null && totalPages == null
    }
}