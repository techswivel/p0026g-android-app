package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ArtistModel(
    @Expose
    @SerializedName("artistId")
    var artistId: Int?,
    @Expose
    @SerializedName("isFollowed")
    var isFollowed: Boolean?,
)
