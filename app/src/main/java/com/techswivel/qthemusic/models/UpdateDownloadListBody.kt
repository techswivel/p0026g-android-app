package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.ActionType

data class UpdateDownloadListBody(
    @SerializedName("songId")
    @Expose
    var songId: Int?,
    @SerializedName("type")
    @Expose
    var type: ActionType?,
)
