package com.techswivel.qthemusic.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Song
import kotlinx.parcelize.Parcelize

@Parcelize
data class ArtistDetails(
    @Expose
    @SerializedName("aboutArtist")
    val aboutArtist: String?,
    @Expose
    @SerializedName("albums")
    val albums: List<Album>?,
    @Expose
    @SerializedName("artistCoverImageUrl")
    val artistCoverImageUrl: String?,
    @Expose
    @SerializedName("artistName")
    val artistName: String?,
    @Expose
    @SerializedName("isPreOrderAvailble")
    val isPreOrderAvailble: Boolean? = false,
    @Expose
    @SerializedName("isPrimumArtist")
    val isPrimumArtist: Boolean?,
    @Expose
    @SerializedName("songs")
    val songs: List<Song>?,
    @Expose
    @SerializedName("totalAlbums")
    val totalAlbums: Int?,
    @Expose
    @SerializedName("totalSongs")
    val totalSongs: Int?,
    @Expose
    @SerializedName("isUserFollowed")
    val isUserFollowed: Boolean?
) : Parcelable