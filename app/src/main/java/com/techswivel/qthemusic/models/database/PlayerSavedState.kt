package com.techswivel.qthemusic.models.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.techswivel.qthemusic.customData.enums.SongType

@Entity(tableName = "PlayerSavedState")
data class PlayerSavedState(
    @PrimaryKey
    var currentSongId: Int,
    var categoryId: Int?,
    var albumId: Int?,
    var artistId: Int?,
    var playListId: Int?,
    var playedFrom: SongType?,
    var songsList: MutableList<Song>?,
    var currentSongModel: Song?,
    var currentAlbumModel: Album?
)
