package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SyncHistoryModel(
    @Expose
    @SerializedName("isListeningHistory")
    var isListeningHistory: Boolean?,
    @Expose
    @SerializedName("isRecentlyPlayed")
    var isRecentlyPlayed: Boolean?,
    @Expose
    @SerializedName("listeningHistory")
    var listeningHistory: List<ListeningHistory>?,
    @Expose
    @SerializedName("recentlyPlayed")
    var recentlyPlayed: List<ListeningHistory>?
)