package com.techswivel.qthemusic.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.ResponseType
import com.techswivel.qthemusic.customData.enums.SongStatus
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchedSongs(
    @Expose
    @SerializedName("albumArtist")
    val albumArtist: String?,
    @Expose
    @SerializedName("albumCoverImageUrl")
    val albumCoverImageUrl: String?,
    @Expose
    @SerializedName("albumId")
    val albumId: Int?,
    @Expose
    @SerializedName("albumName")
    val albumName: String?,
    @Expose
    @SerializedName("albumStatus")
    val albumStatus: AlbumStatus?,
    @Expose
    @SerializedName("albumTitle")
    val albumTitle: String?,
    @Expose
    @SerializedName("price")
    val albumPrice: Float?,
    @Expose
    @SerializedName("sku")
    val sku: String?,
    @Expose
    @SerializedName("artist")
    val artist: String?,
    @Expose
    @SerializedName("artistCoverImageUrl")
    val artistCoverImageUrl: String?,
    @Expose
    @SerializedName("artistId")
    val artistId: Int?,
    @Expose
    @SerializedName("artistName")
    val artistName: String?,
    @Expose
    @SerializedName("categoryId")
    val categoryId: Int?,
    @Expose
    @SerializedName("coverImageUrl")
    val coverImageUrl: String?,
    @Expose
    @SerializedName("isAddedToPlayList")
    val isAddedToPlayList: Boolean?,
    @Expose
    @SerializedName("isFavourit")
    val isFavourit: Boolean?,
    @Expose
    @SerializedName("lyrics")
    val lyrics: String?,
    @Expose
    @SerializedName("numberOfSongs")
    val numberOfSongs: Int?,
    @Expose
    @SerializedName("playListId")
    val playListId: Int?,
    @Expose
    @SerializedName("songAudioUrl")
    val songAudioUrl: String?,
    @Expose
    @SerializedName("songDuration")
    val songDuration: String?,
    @Expose
    @SerializedName("songId")
    val songId: Int?,
    @Expose
    @SerializedName("songStatus")
    val songStatus: SongStatus?,
    @Expose
    @SerializedName("songTitle")
    val songTitle: String?,
    @Expose
    @SerializedName("songVideoUrl")
    val songVideoUrl: String?,
    @Expose
    @SerializedName("type")
    val type: ResponseType?
) : Parcelable {
    constructor(
        artistId: Int?,
        artistName: String?,
        type: ResponseType?,
        artistCoverImageUrl: String?
    ) :
            this(
                null, null, null, null, null, null, null, null,
                null, artistCoverImageUrl, artistId, artistName, null, null, false, false, null,
                null, null, null, null, null, null, null, null, type
            )


    constructor(
        albumId: Int?,
        albumTitle: String?,
        albumArtist: String?,
        type: ResponseType?,
        albumCoverImageUrl: String?,
        albumStatus: AlbumStatus?,
        numberOfSongs: Int?
    ) :
            this(
                albumArtist, albumCoverImageUrl, albumId, null, albumStatus, albumTitle, null, null,
                null, null, null, null, null, null, false, false, null,
                numberOfSongs, null, null, null, null, null, null, null, type
            )

    constructor(
        songId: Int?,
        albumId: Int?,
        isAddedToPlayList: Boolean?,
        playListId: Int?,
        artistId: Int?,
        categoryId: Int?,
        type: ResponseType?,
        isFavourit: Boolean?,
        songTitle: String?,
        coverImageUrl: String?,
        songDuration: String?,
        songStatus: SongStatus?,
        songAudioUrl: String?,
        songVideoUrl: String?,
        artist: String?,
        lyrics: String?,
        albumName: String?
    ) :
            this(
                null,
                null,
                albumId,
                albumName,
                null,
                null,
                null,
                null,
                artist,
                null,
                artistId,
                null,
                categoryId,
                coverImageUrl,
                isAddedToPlayList,
                isFavourit,
                lyrics,
                null,
                playListId,
                songAudioUrl,
                songDuration,
                songId,
                songStatus,
                songTitle,
                songVideoUrl,
                type
            )


}