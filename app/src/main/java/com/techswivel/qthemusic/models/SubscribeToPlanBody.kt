package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.ItemType
import com.techswivel.qthemusic.customData.enums.PurchaseType
import java.io.Serializable

data class SubscribeToPlanBody(
    @SerializedName("purchaseType")
    @Expose
    var purchaseType: PurchaseType?,
    @SerializedName("purchaseToken")
    @Expose
    var purchaseToken: String?,
    @SerializedName("itemType")
    @Expose
    var itemType: ItemType?,
    @SerializedName("planId")
    @Expose
    var planId: Int?,
    @SerializedName("songId")
    @Expose
    var songId: Int?,
    @SerializedName("albumId")
    @Expose
    var albumId: Int?,
    @SerializedName("preOrderId")
    @Expose
    var preOrderId: Int?,
    @SerializedName("cardId")
    @Expose
    var cardId: Int?,
    @SerializedName("paidAmount")
    @Expose
    var paidAmount: Float?,
    @SerializedName("preOrderSongId")
    @Expose
    var preOrderSongId: Int?,
    @SerializedName("preOrderAlbumId")
    @Expose
    var preOrderAlbumId: Int?,
) : Serializable
