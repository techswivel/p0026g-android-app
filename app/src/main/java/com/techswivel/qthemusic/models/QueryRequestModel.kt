package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class QueryRequestModel(
    @Expose
    @SerializedName("queryString")
    var queryString: String?,
    @Expose
    @SerializedName("languageId")
    var languageId: Int?

)
