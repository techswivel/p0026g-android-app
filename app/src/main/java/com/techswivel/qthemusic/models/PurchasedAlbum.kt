package com.techswivel.qthemusic.models


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Song
import kotlinx.parcelize.Parcelize

@Parcelize
data class PurchasedAlbum(
    @SerializedName("albumCoverImageURL")
    @Expose
    var albumCoverImageURL: String?,
    @SerializedName("albumTitle")
    @Expose
    var albumTitle: String?,
    @SerializedName("price")
    @Expose
    var price: Float?,
    @SerializedName("songs")
    @Expose
    var songs: List<Song>?,
    @SerializedName("timeOfPurchased")
    @Expose
    var timeOfPurchased: String?,
    @SerializedName("totalSongs")
    @Expose
    var totalSongs: Int?
) : Parcelable {
    override fun toString(): String {
        return "PurchasedAlbum(albumCoverImageURL=$albumCoverImageURL, albumTitle=$albumTitle, price=$price, songs=$songs, timeOfPurchased=$timeOfPurchased, totalSongs=$totalSongs)"
    }
}