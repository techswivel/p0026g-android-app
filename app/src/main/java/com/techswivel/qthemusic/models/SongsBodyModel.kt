package com.techswivel.qthemusic.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.SongType

data class SongsBodyModel(
    @Expose
    @SerializedName("albumId")
    var albumId: Int?,
    @Expose
    @SerializedName("playListId")
    var playListId: Int?,
    @Expose
    @SerializedName("type")
    var type: SongType?,
    @SerializedName("categoryId")
    @Expose
    var categoryId: Int?,
    @SerializedName("artistId")
    @Expose
    var artistId: Int?,
)