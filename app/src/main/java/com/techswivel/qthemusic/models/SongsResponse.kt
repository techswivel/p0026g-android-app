package com.techswivel.qthemusic.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.models.database.Song

data class SongsResponse(
    @Expose
    @SerializedName("albumStatus")
    var albumStatus: AlbumStatus?,
    @Expose
    @SerializedName("songs")
    var songs: List<Song>?
)