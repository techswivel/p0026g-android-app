package com.techswivel.qthemusic.models.database


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Album")
data class Album(
    @Expose
    @SerializedName("albumCoverImageUrl")
    @ColumnInfo(name = "albumCoverImageUrl")
    var albumCoverImageUrl: String?,
    @Expose
    @SerializedName("albumId")
    @PrimaryKey
    @ColumnInfo(name = "albumId")
    var albumId: Int?,
    @SerializedName("albumStatus")
    @ColumnInfo(name = "albumStatus")
    @Expose
    var albumStatus: AlbumStatus?,
    @Expose
    @SerializedName("albumTitle")
    @ColumnInfo(name = "albumTitle")
    var albumTitle: String?,
    @Expose
    @SerializedName("albumArtist")
    @ColumnInfo(name = "albumArtist")
    var albumArtist: String?,
    @Expose
    @SerializedName("type")
    @ColumnInfo(name = "type")
    var type: RecommendedSongsType?,
    @Expose
    @SerializedName("numberOfSongs")
    @ColumnInfo(name = "numberOfSongs")
    var numberOfSongs: Int?,
    @SerializedName("price")
    @Expose
    var price: String?,
    @SerializedName("sku")
    @Expose
    var sku: String?,
    @Expose
    @SerializedName("recentPlay")
    @ColumnInfo(name = "recentPlay")
    var recentPlay: Long?,
    @Expose
    @ColumnInfo(name = "albumIsRecentlyPlayed")
    @SerializedName("albumIsRecentlyPlayed")
    var albumIsRecentlyPlayed: Boolean?
) : Parcelable {

    constructor(
        albumCoverImageUrl: String?,
        albumId: Int?,
        albumStatus: AlbumStatus?,
        albumTitle: String?,
        albumArtist: String?,
        type: RecommendedSongsType?,
        numberOfSongs: Int?,
        price: String?,
        sku: String?
    )
            : this(
        albumCoverImageUrl,
        albumId,
        albumStatus,
        albumTitle,
        albumArtist,
        type,
        numberOfSongs,
        price,
        sku,
        null,
        null
    )

    constructor() : this(null, null, null, null, null, null, null, null, null)

}