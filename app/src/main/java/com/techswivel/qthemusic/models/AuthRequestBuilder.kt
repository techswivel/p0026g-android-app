package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.customData.enums.LoginType
import com.techswivel.qthemusic.customData.enums.OtpType
import com.techswivel.qthemusic.customData.enums.SocialSites
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.Serializable

class AuthRequestBuilder : Serializable {
    var email: String? = null
    var name: String? = null
    var dob: Int? = null
    var gender: String? = null
    var completeAddress: String? = null
    var city: String? = null
    var state: String? = null
    var country: String? = null
    var zipCode: Int? = null
    var profile: MultipartBody.Part? = null
    var socialId: String? = null
    var password: String? = null
    var loginType: LoginType? = null
    var socialSite: SocialSites? = null
    var fcmToken: String? = null
    var accessToken: String? = null
    var deviceIdentifier: String? = null
    var otpType: OtpType? = null
    var phoneNumber: String? = null
    var otp: Int? = null
    var deviceName: String? = null
    var avatar: String? = null

    var mProfile: MultipartBody.Part? = null
    var mName: okhttp3.RequestBody? = null
    var mEmail: okhttp3.RequestBody? = null
    var mDob: Int? = null
    var mGender: okhttp3.RequestBody? = null
    var mPassword: okhttp3.RequestBody? = null
    var mFcmToken: okhttp3.RequestBody? = null
    var mDeviceIdentifier: okhttp3.RequestBody? = null
    var mCompleteAddress: okhttp3.RequestBody? = null
    var mCity: okhttp3.RequestBody? = null
    var mState: okhttp3.RequestBody? = null
    var mCountry: okhttp3.RequestBody? = null
    var mZipCode: Int? = null
    var mSocailId: okhttp3.RequestBody? = null
    var mSocialSite: okhttp3.RequestBody? = null
    var mIsEnableNotification: Boolean? = null
    var mIsArtistUpdateEnable: Boolean? = null
    var mPhoneNumber: okhttp3.RequestBody? = null
    var mOtp: Int? = null


    fun mProfile(mProfile: MultipartBody.Part?) = apply { this.mProfile = mProfile }
    fun mName(mName: RequestBody?) = apply { this.mName = mName }
    fun mEmail(mEmail: RequestBody?) = apply { this.mEmail = mEmail }
    fun mDob(mDob: Int?) = apply { this.mDob = mDob }
    fun mGender(mGender: RequestBody?) = apply { this.mGender = mGender }
    fun mPassword(mPassword: RequestBody?) = apply { this.mPassword = mPassword }
    fun mFcmToken(mFcmToken: RequestBody?) = apply { this.mFcmToken = mFcmToken }
    fun mDeviceIdentifier(mDeviceIdentifier: RequestBody?) =
        apply { this.mDeviceIdentifier = mDeviceIdentifier }

    fun mCompleteAddress(mCompleteAddress: RequestBody?) =
        apply { this.mCompleteAddress = mCompleteAddress }

    fun mCity(mCity: RequestBody?) = apply { this.mCity = mCity }
    fun mState(mState: RequestBody?) = apply { this.mState = mState }
    fun mCountry(mCountry: RequestBody?) = apply { this.mCountry = mCountry }
    fun mSocailId(mSocailId: RequestBody?) = apply { this.mSocailId = mSocailId }
    fun mSocialSite(mSocialSite: RequestBody?) = apply { this.mSocialSite = mSocialSite }
    fun mIsEnableNotification(mIsEnableNotification: Boolean?) =
        apply { this.mIsEnableNotification = mIsEnableNotification }

    fun mIsArtistUpdateEnable(mIsArtistUpdateEnable: Boolean?) =
        apply { this.mIsArtistUpdateEnable = mIsArtistUpdateEnable }

    fun mPhoneNumber(mPhone: RequestBody?) = apply { this.mPhoneNumber = mPhoneNumber }
    fun mOtp(mOtp: Int?) = apply { this.mOtp = mOtp }


    fun productId(password: String?) = apply { this.password = password }
    fun email(email: String?) = apply { this.email = email }
    fun name(name: String?) = apply { this.name = name }
    fun dob(dob: Int?) = apply { this.dob = dob }
    fun completeAddress(address: String?) = apply { this.completeAddress = address }
    fun gender(gender: String?) = apply { this.gender = gender }
    fun city(city: String?) = apply { this.city = city }
    fun state(state: String?) = apply { this.state = state }
    fun country(country: String?) = apply { this.country = country }
    fun zipCode(zipCode: Int?) = apply { this.zipCode = zipCode }
    fun socialId(socialId: String?) = apply { this.socialId = socialId }
    fun profile(profile: MultipartBody.Part?) = apply { this.profile = profile }
    fun loginType(loginType: LoginType?) = apply { this.loginType = loginType }
    fun socialSite(socialSite: SocialSites?) = apply { this.socialSite = socialSite }
    fun fcmToken(fcmToken: String?) = apply { this.fcmToken = fcmToken }
    fun accessToken(accessToken: String?) = apply { this.accessToken = accessToken }
    fun deviceIdentifier(deviceIdentifier: String?) =
        apply { this.deviceIdentifier = deviceIdentifier }

    fun otpType(otpType: OtpType?) = apply { this.otpType = otpType }
    fun phoneNumber(phoneNumber: String?) = apply { this.phoneNumber = phoneNumber }
    fun otp(otpType: String?) = apply { this.otp = otp }
    fun deviceName(deviceName: String?) = apply { this.deviceName = deviceName }
    fun avatar(avatar: String?) = apply { this.avatar = avatar }

    override fun toString(): String {
        return super.toString()
    }

    companion object {

        fun builder(builder: AuthRequestBuilder) = AuthRequestModel(
            builder.name,
            builder.email,
            builder.dob,
            builder.gender,
            builder.completeAddress,
            builder.city,
            builder.state,
            builder.country,
            builder.zipCode,
            builder.profile,
            builder.socialId,
            builder.password,
            builder.loginType,
            builder.socialSite,
            builder.fcmToken,
            builder.accessToken,
            builder.deviceIdentifier,
            builder.otpType,
            builder.phoneNumber,
            builder.otp,
            builder.deviceName,
            builder.avatar
        )
    }
}