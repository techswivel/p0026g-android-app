package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Song
import java.io.Serializable

data class AuthModel(
    @Expose
    @SerializedName("name")
    var name: String?,
    @SerializedName("email")
    @Expose
    var email: String?,
    @Expose
    @SerializedName("avatar")
    var avatar: String?,
    @Expose
    @SerializedName("jwt")
    var jwt: String?,
    @Expose
    @SerializedName("dOb")
    var dOB: Long?,
    @Expose
    @SerializedName("phoneNumber")
    var phoneNumber: String?,
    @Expose
    @SerializedName("gender")
    var gender: String?,
    @Expose
    @SerializedName("isInterestsSet")
    var isInterestsSet: Boolean?,
    @Expose
    @SerializedName("address")
    var address: Address?,
    @Expose
    @SerializedName("subscription")
    var subscription: SubscriptionPlans?,
    @Expose
    @SerializedName("notification")
    var notification: Notification?,
    @Expose
    @SerializedName("socialId")
    var socialId: String?,
    @Expose
    @SerializedName("purchasedsongs")
    val purchasedsongs: List<Song>?,
    @Expose
    @SerializedName("purchasedAlbums")
    val purchasedAlbums: List<Album>?,
) : Serializable