package com.techswivel.qthemusic.models.database

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.ActionType

class UpdatePlayListModel(
    @Expose
    @SerializedName("songId")
    var songId: Int? = null,
    @Expose
    @SerializedName("type")
    var type: ActionType? = null,
    @Expose
    @SerializedName("playListId")
    var playListId: Int? = null
)