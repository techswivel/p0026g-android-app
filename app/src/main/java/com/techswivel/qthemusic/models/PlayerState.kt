package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.helper.PlayerHelper
import com.techswivel.qthemusic.models.database.Song

data class PlayerState(
    val currentSong: Song? = null,
    val isPlaying: Boolean = false,
    val isNotificationRemoved: Boolean = false,
    val playerHelper: PlayerHelper? = null
)
