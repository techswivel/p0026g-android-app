package com.techswivel.qthemusic.models

import androidx.databinding.BaseObservable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.LoginType
import com.techswivel.qthemusic.customData.enums.OtpType
import com.techswivel.qthemusic.customData.enums.SocialSites
import okhttp3.MultipartBody
import java.io.Serializable

class AuthRequestModel(
    @Expose
    @SerializedName("name")
    var name: String?,
    @Expose
    @SerializedName("email")
    var email: String?,
    @Expose
    @SerializedName("dOb")
    var dob: Int?,
    @Expose
    @SerializedName("gender")
    var gender: String?,
    @Expose
    @SerializedName("completeAddress")
    var completeAddress: String?,
    @Expose
    @SerializedName("city")
    var city: String?,
    @Expose
    @SerializedName("state")
    var state: String?,
    @Expose
    @SerializedName("country")
    var country: String?,
    @Expose
    @SerializedName("zipCode")
    var zipCode: Int?,
    @Expose
    @SerializedName("profile")
    var profile: MultipartBody.Part?,
    @Expose
    @SerializedName("socialId")
    var socialId: String?,
    @Expose
    @SerializedName("password")
    var password: String?,
    @Expose
    @SerializedName("loginType")
    var loginType: LoginType?,
    @Expose
    @SerializedName("socialSite")
    var socialSite: SocialSites?,
    @Expose
    @SerializedName("fcmToken")
    var fcmToken: String?,
    @Expose
    @SerializedName("accessToken")
    var accessToken: String?,
    @Expose
    @SerializedName("deviceIdentifier")
    var deviceIdentifier: String?,
    @Expose
    @SerializedName("otpType")
    var otpType: OtpType?,
    @Expose
    @SerializedName("phoneNumber")
    var phoneNumber: String?,
    @Expose
    @SerializedName("otp")
    var otp: Int?,
    @Expose
    @SerializedName("deviceName")
    var deviceName: String?,
    @Expose
    @SerializedName("avatar")
    var avatar: String?
) : BaseObservable(), Serializable {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )

}