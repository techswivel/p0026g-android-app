package com.techswivel.qthemusic.models.database

import androidx.room.Embedded
import androidx.room.Relation

data class SongsArtistAndAlbum(
    @Embedded
    val song: Song,
    @Relation(parentColumn = "artistId", entityColumn = "artistId")
    val artist: Artist?,
    @Relation(parentColumn = "albumId", entityColumn = "albumId")
    val album: Album?
)
