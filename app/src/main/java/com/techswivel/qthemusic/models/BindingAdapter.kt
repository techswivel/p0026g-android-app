package com.techswivel.qthemusic.models

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_USER_PLAN_ID
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.SwipeRevealLayout
import com.techswivel.qthemusic.utils.Utilities
import com.techswivel.qthemusic.utils.roundToTwoDecimal
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object BindingAdapter {

    private const val TAG = "BindingAdapter"

    @JvmStatic
    @BindingAdapter("setTextToTextView")
    fun setTextToTextView(textView: TextView, text: String?) {
        textView.text = text
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setNomberOfSong")
    fun setNomberOfSong(textView: TextView, noOfSongs: Int?) {
        if (noOfSongs == null) {
            textView.text = "0 songs"
        } else {
            textView.text = "$noOfSongs songs"
        }
    }

    @JvmStatic
    @BindingAdapter("closeDeleteView")
    fun closeDeleteView(view: SwipeRevealLayout, model: Any) {
        view.close(true)
    }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    @JvmStatic
    @BindingAdapter("setSongDuration")
    fun setSongDuration(textView: TextView, song_duration: String?) {
        if (song_duration != null) {
            val duration = Utilities.formatSongDuration(song_duration.toLong())
            textView.text = duration
        }
    }


    @SuppressLint("SetTextI18n", "DefaultLocale")
    @JvmStatic
    @BindingAdapter("setSongTypeAndDuration")
    fun setSongTypeAndDuration(textView: TextView, song: Song) {
        var songType: String? = null
        if (song != null) {
            if (song.songVideoUrl != null) {
                songType = "Video"
            } else {
                songType = "Audio"
            }
        }

        textView.text = "$songType - ${song.songDuration}"
    }


    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setPlan")
    fun setPlan(textView: TextView, subscription: SubscriptionPlans?) {
        textView.text =
            "$" + subscription?.planPrice?.roundToTwoDecimal() + " / " + subscription?.planDuration
    }


    @JvmStatic
    @BindingAdapter("setDate")
    fun setDate(textView: TextView?, dateInMillis: Long) {

        if (dateInMillis.toInt() != 0) {
            val dateObj = dateInMillis.let { dateInMillis ->
                Date(dateInMillis * 1000)
            }
            textView?.text = DateFormat.getDateInstance(DateFormat.LONG).format(dateObj)
        } else {
            textView?.text = textView?.context?.getString(R.string.not_added)
        }
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    @JvmStatic
    @BindingAdapter("setPreOrderDate")
    fun setPreOrderDate(textView: TextView?, dateInMillis: Long) {
        Log.d(TAG, " preOrder long is $dateInMillis")
        if (dateInMillis.toInt() != 0) {
            try {
                val netDate = Date(dateInMillis * 1000)
                Log.d(TAG, "netDate is $netDate")
                val mCalendar = Calendar.getInstance()
                mCalendar.time = netDate
                val mYear = mCalendar.get(Calendar.YEAR)
                val mMonth = mCalendar.get(Calendar.MONTH)
                val date = mCalendar.get(Calendar.DATE)
                val mMonthStringText = Utilities.getMonths(mMonth.plus(1))
                textView?.text = "$date $mMonthStringText $mYear"
                Log.d(TAG, "date in tv is $date $mMonthStringText $mYear")

            } catch (e: Exception) {
                Log.d("TAG", "${e.message}")
            }
        } else {
            textView?.text = textView?.context?.getString(R.string.not_added)
        }
    }

    @JvmStatic
    @BindingAdapter("setSongListAdapter")
    fun setSongListAdapter(
        pRecyclerView: RecyclerView,
        purchasedHistoryAlbum: BuyingHistory
    ) {
        val songListAdapter =
            RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                    return R.layout.sub_item_buying_history
                }

                override fun onNoDataFound() {

                }
            }, purchasedHistoryAlbum.song as MutableList<Any>)
        pRecyclerView.adapter = songListAdapter
        pRecyclerView.setHasFixedSize(true)
        songListAdapter.notifyDataSetChanged()
    }

    @JvmStatic
    @BindingAdapter("setDateOfBirth")
    fun setDateOfBirth(textView: TextView, text: String?) {
        textView.text = text
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setAlbumPrice")
    fun setAlbumPrice(button: AppCompatButton, album: Album?) {
        if (album != null) {
            button.text =
                "Pay $" + album.price?.roundToTwoDecimal() + " Now"
        }
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setPriceOnTextView")
    fun setPriceOnTextView(textView: TextView, price: String?) {
        textView.text = "$ ".plus(price?.roundToTwoDecimal())
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setSongPrice")
    fun setSongPrice(button: AppCompatButton, song: Song?) {
        if (song?.songPrice != null) {
            button.text =
                "Pay $" + song.songPrice?.roundToTwoDecimal() + " Now"
        } else {
            button.text =
                "Pay $ No Value Now"
        }
    }


    @SuppressLint("SimpleDateFormat")
    @JvmStatic
    @BindingAdapter("setDateTime")
    fun setDateTime(textView: TextView, text: Long) {
        val mdate = Date(text * 1000)
        val mCalendar = Calendar.getInstance()
        mCalendar.time = mdate
        val mYear = mCalendar.get(Calendar.YEAR)
        val mMonth = mCalendar.get(Calendar.MONTH)
        val date = mCalendar.get(Calendar.DATE)
        val mMonthStringText = Utilities.getMonths(mMonth.plus(1))
        val currentTime = SimpleDateFormat("HH:mm aa", Locale.getDefault()).format(mdate)
        val format = "$date $mMonthStringText $mYear - $currentTime"
        textView.text = format.format(date)
    }

    @JvmStatic
    @BindingAdapter("setImageViewImage")
    fun setImageViewImage(pImageView: ImageView, image: String?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val circularProgressDrawable = CircularProgressDrawable(pImageView.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()
            val requestOptions = RequestOptions()
            requestOptions.placeholder(circularProgressDrawable)
            requestOptions.error(R.drawable.no_image_palce_holder)
            Glide.with(pImageView.context).setDefaultRequestOptions(requestOptions).load(image)
                .into(pImageView)
        }
    }

    @JvmStatic
    @BindingAdapter("setImageViewImageForProfile")
    fun setImageViewImageForProfile(pImageView: ImageView, image: String?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val circularProgressDrawable = CircularProgressDrawable(pImageView.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.setColorFilter(
                ContextCompat.getColor(QTheMusicApplication.getContext(), R.color.color_white),
                PorterDuff.Mode.SRC_IN
            )
            circularProgressDrawable.start()
            val requestOptions = RequestOptions()
            requestOptions.placeholder(circularProgressDrawable)
            requestOptions.error(R.drawable.no_image_palce_holder)
            Glide.with(pImageView.context).setDefaultRequestOptions(requestOptions).load(image)
                .into(pImageView)
        }
    }


    @JvmStatic
    @BindingAdapter("setSongTime")
    fun setSongTime(pTextView: TextView, duration: String) {
        val minutes = (duration.toInt() % 3600) / 60
        val seconds = duration.toInt() % 60

        pTextView.text = String.format("%02d:%02d", minutes, seconds);
    }

    @JvmStatic
    @BindingAdapter("setBuyPlanVisibility")
    fun setBuyPlanVisibility(pButton: AppCompatButton, song: Song?) {
        val subscription = PrefUtils.getInt(QTheMusicApplication.getContext(), KEY_USER_PLAN_ID)
        if (subscription == 0) {
            pButton.visibility = View.VISIBLE
        } else {
            pButton.visibility = View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("setBuyPlanVisibility")
    fun setBuyPlanVisibility(textView: TextView, song: Song?) {
        val subscription = PrefUtils.getInt(QTheMusicApplication.getContext(), KEY_USER_PLAN_ID)
        if (subscription == 0) {
            textView.visibility = View.VISIBLE
        } else {
            textView.visibility = View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("setBuyPlanVisibility")
    fun setBuyPlanVisibility(imageView: ImageView?, song: Song?) {
        val subscription = PrefUtils.getInt(QTheMusicApplication.getContext(), KEY_USER_PLAN_ID)
        if (subscription == 0) {
            imageView?.visibility = View.VISIBLE
        } else {
            imageView?.visibility = View.GONE
        }
    }


    @JvmStatic
    @BindingAdapter("setImageWithBlur")
    fun setImageWithBlur(pImageView: ImageView, image: String) {
        Glide.with(pImageView.context).load(image)
            .override(20, 20)
            .into(pImageView)
    }

}