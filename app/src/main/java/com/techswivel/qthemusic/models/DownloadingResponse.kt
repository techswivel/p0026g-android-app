package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import java.io.Serializable

class DownloadingResponse private constructor(
    val status: DownloadingStatus,
    var progress: Long?,
    val message: String?,
    val translationID: Int
) : Serializable {
    companion object {
        fun failed(message: String, translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.FAILED, null, message, translationID)
        }

        fun rollBack(message: String, translationID: Int): DownloadingResponse {
            return DownloadingResponse(
                DownloadingStatus.UN_CONFIGURABLE,
                null,
                message,
                translationID
            )
        }

        fun paused(translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.PAUSED, null, null, translationID)
        }

        fun pending(translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.PENDING, null, null, translationID)
        }

        fun running(progress: Long?, translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.RUNNING, progress, null, translationID)
        }

        fun success(translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.SUCCESS, null, null, translationID)
        }

        fun cancel(translationID: Int): DownloadingResponse {
            return DownloadingResponse(DownloadingStatus.CANCEL, null, null, translationID)
        }
    }
}