package com.techswivel.qthemusic.models


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.customData.enums.SongType
import kotlinx.parcelize.Parcelize

@Parcelize
data class RecommendedSongsBodyModel(
    @SerializedName("artistId")
    @Expose
    var artistId: Int?,
    @SerializedName("categoryId")
    @Expose
    var categoryId: Int?,
    @SerializedName("requestType")
    @Expose
    var requestType: SongType?,
    @SerializedName("dataType")
    @Expose
    var dataType: RecommendedSongsType?
) : Parcelable