package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SubscriptionPlans(
    @SerializedName("planId")
    @Expose
    var planId: Int?,
    @SerializedName("planTitle")
    @Expose
    var planTitle: String?,
    @SerializedName("planPrice")
    @Expose
    var planPrice: String?,
    @SerializedName("planDuration")
    @Expose
    var planDuration: String?,
    @SerializedName("planTopic")
    @Expose
    var planTopic: String?,
    @SerializedName("sku")
    @Expose
    var sku: String?
) : Serializable {
    constructor() : this(null, null, null, null, null, null)
}
