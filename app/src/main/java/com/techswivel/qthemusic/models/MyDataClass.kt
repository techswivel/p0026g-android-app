package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.models.database.Song

class MyDataClass(
    @Expose
    @SerializedName("auth")
    val authModel: AuthModel?,
    @Expose
    @SerializedName("category")
    var category: List<Category>?,
    @Expose
    @SerializedName("playlists")
    val playlistModel: List<PlaylistModel>?,
    @Expose
    @SerializedName("playlist")
    val playlistModelResponse: PlaylistModel?,
    @Expose
    @SerializedName("songs")
    val songList: List<Song>?,
    @Expose
    @SerializedName("artist")
    val artistList: List<Artist>?,
    @Expose
    @SerializedName("albums")
    val albumList: List<Album>?,
    @Expose
    @SerializedName("Languages")
    val Languages: List<Language>?,
    @Expose
    @SerializedName("searchResults")
    val searchedSongs: List<SearchedSongs>?,
    @Expose
    @SerializedName("totalAmount")
    val totalAmount: Double?,
    @Expose
    @SerializedName("buyingHistory")
    val buyingHistory: List<BuyingHistory>?,
    @Expose
    @SerializedName("preOrderData")
    val preOrderData: List<PreOrderData>?,
    @Expose
    @SerializedName("purchasedPreOrder")
    val purchasedPreOrder: PreOrderData?,
    @Expose
    @SerializedName("artistDetails")
    val artistDetails: ArtistDetails?,
    @SerializedName("totalSongs")
    @Expose
    var totalSongs: Int?,
    @Expose
    @SerializedName("recentlyPlayed")
    val recentlyPlayed: RecentlyPlayed?,
    @Expose
    @SerializedName("listeningHistory")
    val listeningHistory: RecentlyPlayed?,
    @Expose
    @SerializedName("plans")
    val subscriptionPlans: List<SubscriptionPlans>?,
    @SerializedName("purchasedAlbum")
    @Expose
    var purchasedAlbum: PurchasedAlbum?,
    @SerializedName("purchasedSong")
    @Expose
    var purchasedSong: Song?,
    @SerializedName("purchasedSubscription")
    @Expose
    var purchasedSubscription: SubscriptionPlans?
) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}