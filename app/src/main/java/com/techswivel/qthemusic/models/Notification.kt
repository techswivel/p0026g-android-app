package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Notification(
    @Expose
    @SerializedName("isNotificationEnabled")
    var isNotificationEnabled: Boolean?,
    @Expose
    @SerializedName("isArtistUpdateEnabled")
    var isArtistUpdateEnabled: Boolean?
) : Serializable