package com.techswivel.qthemusic.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.PreOrderType
import kotlinx.parcelize.Parcelize

@Parcelize
data class PreOrderData(
    @Expose
    @SerializedName("isPurchesed")
    val isPurchesed: Boolean?,
    @Expose
    @SerializedName("preOrderCoverImageUrl")
    val preOrderCoverImageUrl: String?,
    @Expose
    @SerializedName("preOrderId")
    val preOrderId: Int?,
    @Expose
    @SerializedName("preOrderTitle")
    val preOrderTitle: String?,
    @Expose
    @SerializedName("preOrderType")
    val preOrderType: PreOrderType?,
    @Expose
    @SerializedName("preOrderedCount")
    val preOrderedCount: Int?,
    @Expose
    @SerializedName("price")
    val price: Double?,
    @Expose
    @SerializedName("releaseDate")
    val releaseDate: Long?,
    @Expose
    @SerializedName("totalPreOrders")
    val totalPreOrders: Int?,
    @SerializedName("sku")
    @Expose
    var sku: String?,
    @Expose
    @SerializedName("totalnumberOfSongs")
    val totalnumberOfSongs: Int?,
    @Expose
    @SerializedName("totalPurchesed")
    val totalPurchesed: Int?,

    ) : Parcelable