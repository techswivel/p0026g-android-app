package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Address(
    @Expose
    @SerializedName("completeAddress")
    var completeAddress: String?,
    @Expose
    @SerializedName("city")
    var city: String?,
    @Expose
    @SerializedName("state")
    var state: String?,
    @Expose
    @SerializedName("country")
    var country: String?,
    @Expose
    @SerializedName("zipCode")
    var zipCode: Int?
) : Serializable
