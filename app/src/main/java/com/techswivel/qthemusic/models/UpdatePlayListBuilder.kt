package com.techswivel.qthemusic.models

import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.models.database.UpdatePlayListModel
import java.io.Serializable

class UpdatePlayListBuilder : Serializable {
    var songId: Int? = null
    var type: ActionType? = null
    var playListId: Int? = null

    companion object {
        fun builder(updatePlayListBuilder: UpdatePlayListBuilder) = UpdatePlayListModel(
            updatePlayListBuilder.songId,
            updatePlayListBuilder.type,
            updatePlayListBuilder.playListId
        )
    }
}