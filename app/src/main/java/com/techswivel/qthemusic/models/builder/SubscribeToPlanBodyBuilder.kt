package com.techswivel.qthemusic.models.builder


import com.techswivel.qthemusic.customData.enums.ItemType
import com.techswivel.qthemusic.customData.enums.PurchaseType
import com.techswivel.qthemusic.models.SubscribeToPlanBody
import com.techswivel.qthemusic.models.database.FailedPurchased

class SubscribeToPlanBodyBuilder {
    var purchaseType: PurchaseType? = null
    var purchaseToken: String? = null
    var itemType: ItemType? = null
    var planId: Int? = null
    var songId: Int? = null
    var albumId: Int? = null
    var cardId: Int? = null
    var paidAmount: Float? = null
    var preOrderId: Int? = null
    var subscriptionPlan: FailedPurchased? = null
    var preOrderSongId: Int? = null
    var preOrderAlbumId: Int? = null

    companion object {
        fun build(builder: SubscribeToPlanBodyBuilder) = SubscribeToPlanBody(
            builder.purchaseType,
            builder.purchaseToken,
            builder.itemType,
            builder.planId,
            builder.songId,
            builder.albumId,
            builder.preOrderId,
            builder.cardId,
            builder.paidAmount,
            builder.preOrderSongId,
            builder.preOrderAlbumId
        )
    }
}