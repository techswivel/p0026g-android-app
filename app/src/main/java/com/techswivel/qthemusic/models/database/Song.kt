package com.techswivel.qthemusic.models.database


import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.BR
import com.techswivel.qthemusic.customData.enums.SongStatus
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
@Parcelize
@Entity(tableName = "Song")
data class Song(
    @SerializedName("albumId")
    @ColumnInfo(name = "albumId")
    @Expose
    var albumId: Int?,
    @ColumnInfo(name = "albumName")
    @SerializedName("albumName")
    @Expose
    var albumName: String?,
    @ColumnInfo(name = "artist")
    @SerializedName("artist")
    @Expose
    var artist: String?,
    @ColumnInfo(name = "artistId")
    @SerializedName("artistId")
    @Expose
    var artistId: Int?,
    @ColumnInfo(name = "categoryId")
    @SerializedName("categoryId")
    @Expose
    var categoryId: Int?,
    @ColumnInfo(name = "coverImageUrl")
    @SerializedName("coverImageUrl")
    @Expose
    var coverImageUrl: String?,
    @ColumnInfo(name = "isAddedToPlayList")
    @SerializedName("isAddedToPlayList")
    @Expose
    var isAddedToPlayList: Boolean?,
    @ColumnInfo(name = "isFavourite")
    @SerializedName("isFavourite")
    @Expose
    var isFavourite: Boolean?,
    @ColumnInfo(name = "lyrics")
    @SerializedName("lyrics")
    @Expose
    var lyrics: String?,
    @ColumnInfo(name = "playListId")
    @SerializedName("playListId")
    @Expose
    var playListId: Int?,
    @ColumnInfo(name = "songAudioUrl")
    @SerializedName("songAudioUrl")
    @Expose
    var songAudioUrl: String?,
    @ColumnInfo(name = "audiofileName")
    @SerializedName("audiofileName")
    @Expose
    var audiofileName: String?,
    @ColumnInfo(name = "videofileName")
    @SerializedName("videofileName")
    @Expose
    var videofileName: String?,
    @ColumnInfo(name = "songDuration")
    @SerializedName("songDuration")
    @Expose
    val songDuration: String?,
    @ColumnInfo(name = "songPrice")
    @SerializedName("price")
    @Expose
    var songPrice: String?,
    @SerializedName("songId")
    @PrimaryKey
    @ColumnInfo(name = "songId")
    @Expose
    var songId: Int?,
    @ColumnInfo(name = "songStatus")
    @SerializedName("songStatus")
    @Expose
    var songStatus: SongStatus?,
    @ColumnInfo(name = "songTitle")
    @SerializedName("songTitle")
    @Expose
    var songTitle: String?,
    @ColumnInfo(name = "songVideoUrl")
    @SerializedName("songVideoUrl")
    @Expose
    var songVideoUrl: String?,
    @ColumnInfo(name = "localPath")
    @SerializedName("localPath")
    var localPath: String?,
    @Expose
    @ColumnInfo(name = "recentPlay")
    @SerializedName("recentPlay")
    var recentPlay: Long?,
    @Expose
    @ColumnInfo(name = "songIsRecentlyPlayed")
    @SerializedName("songIsRecentlyPlayed")
    var songIsRecentlyPlayed: Boolean?,
    @ColumnInfo(name = "sku")
    @SerializedName("sku")
    @Expose
    var sku: String?,

) : BaseObservable(), Parcelable {

    @IgnoredOnParcel
    @Ignore
    private var progressVisibilityState: ObservableField<Boolean>? = null

    @Bindable
    fun getProgressVisibilityState(): ObservableField<Boolean>? {
        return progressVisibilityState
    }

    fun setProgressVisibilityState(tickVisibility: ObservableField<Boolean>) {
        this.progressVisibilityState = tickVisibility
        notifyPropertyChanged(BR.selectedViewBackground)
    }

    @IgnoredOnParcel
    @Ignore
    private var progressVisibilityPercentage: ObservableField<Int>? = null

    @Bindable
    fun getProgressVisibilityPercentage(): ObservableField<Int>? {
        return progressVisibilityPercentage
    }

    fun setProgressVisibilityPercentage(percentage: ObservableField<Int>) {
        this.progressVisibilityPercentage = percentage
        notifyPropertyChanged(BR.selectedViewBackground)
    }

    @IgnoredOnParcel
    @Ignore
    private var progressVisibilityPercentageString: ObservableField<String>? = null

    @Bindable
    fun getProgressVisibilityPercentageString(): ObservableField<String>? {
        return progressVisibilityPercentageString
    }

    fun setProgressVisibilityPercentageString(percentage: ObservableField<String>) {
        this.progressVisibilityPercentageString = percentage
        notifyPropertyChanged(BR.selectedViewBackground)
    }

    override fun toString(): String {
        return "Song(albumId=$albumId, albumName=$albumName, artist=$artist, artistId=$artistId, categoryId=$categoryId, coverImageUrl=$coverImageUrl, isAddedToPlayList=$isAddedToPlayList, isFavourite=$isFavourite, lyrics=$lyrics, playListId=$playListId, songAudioUrl=$songAudioUrl, audiofileName=$audiofileName, videofileName=$videofileName, songDuration=$songDuration, songPrice=$songPrice, songId=$songId, songStatus=$songStatus, songTitle=$songTitle, songVideoUrl=$songVideoUrl, localPath=$localPath, recentPlay=$recentPlay, songIsRecentlyPlayed=$songIsRecentlyPlayed, sku=$sku, progressVisibilityState=$progressVisibilityState, progressVisibilityPercentage=$progressVisibilityPercentage, progressVisibilityPercentageString=$progressVisibilityPercentageString)"
    }

    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ) {

    }

    constructor(
        albumId: Int?,
        albumName: String?,
        artist: String?,
        artistId: Int?,
        categoryId: Int?,
        coverImageUrl: String?,
        isAddedToPlayList: Boolean?,
        isFavourit: Boolean?,
        lyrics: String?,
        playListId: Int?,
        songAudioUrl: String?,
        audiofileName: String?,
        videofileName: String?,
        songDuration: String?,
        songPrice: String?,
        songId: Int?,
        songStatus: SongStatus?,
        songTitle: String?,
        songVideoUrl: String?,

        ) : this(
        albumId,
        albumName,
        artist,
        artistId,
        categoryId,
        coverImageUrl,
        isAddedToPlayList,
        isFavourit,
        lyrics,
        playListId,
        songAudioUrl,
        audiofileName,
        videofileName,
        songDuration,
        songPrice,
        songId,
        songStatus,
        songTitle,
        songVideoUrl,
        null,
        null,
        null,
        null
    ) {

    }
    constructor(
        albumId: Int?,
        albumName: String?,
        artist: String?,
        artistId: Int?,
        categoryId: Int?,
        coverImageUrl: String?,
        isAddedToPlayList: Boolean?,
        isFavourit: Boolean?,
        lyrics: String?,
        playListId: Int?,
        songAudioUrl: String?,
        audiofileName: String?,
        songDuration: String?,
        songPrice: String?,
        songId: Int?,
        songStatus: SongStatus?,
        songTitle: String?,
        songVideoUrl: String?
    ) : this(
        albumId,
        albumName,
        artist,
        artistId,
        categoryId,
        coverImageUrl,
        isAddedToPlayList,
        isFavourit,
        lyrics,
        playListId,
        songAudioUrl,
        audiofileName,
        null,
        songDuration,
        songPrice,
        songId,
        songStatus,
        songTitle,
        songVideoUrl,
        null,
        null,
        null,
        null
    ) {

    }


}