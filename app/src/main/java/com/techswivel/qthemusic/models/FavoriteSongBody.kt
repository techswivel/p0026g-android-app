package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FavoriteSongBody(
    @SerializedName("songId")
    @Expose
    var songId: Int?,
    @SerializedName("isFavourite")
    @Expose
    var isFavourite: Boolean?,
)
