package com.techswivel.qthemusic.models.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.techswivel.qthemusic.customData.enums.ItemType

@Entity(tableName = "FailedPurchased")
data class FailedPurchased(
    @PrimaryKey
    @ColumnInfo(name = "itemId")
    var id: Int?,
    @ColumnInfo(name = "isAcknowledge")
    var isAcknowledge: Boolean?,
    @ColumnInfo(name = "isFromLogin")
    var isFromLogin: Boolean?,
    @ColumnInfo(name = "itemType")
    var itemType: ItemType?,
    @ColumnInfo(name = "sku")
    var sku: String?
) {
    constructor() : this(null, false, false, null, null)

    override fun toString(): String {
        return "FailedPurchased(id=$id, isAcknowledge=$isAcknowledge, isFromLogin=$isFromLogin, itemType=$itemType, sku=$sku)"
    }


}