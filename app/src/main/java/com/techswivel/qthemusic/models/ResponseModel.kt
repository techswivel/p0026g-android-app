package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @SerializedName("status")
    @Expose
    var status: Boolean?,
    @SerializedName("message")
    @Expose
    var message: String?,
    @SerializedName("data")
    @Expose
    var data: MyDataClass,
    @SerializedName("meta")
    @Expose
    var meta: Meta?,
)