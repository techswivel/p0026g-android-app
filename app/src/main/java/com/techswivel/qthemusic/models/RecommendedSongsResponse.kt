package com.techswivel.qthemusic.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song

data class RecommendedSongsResponse(
    @SerializedName("albums")
    @Expose
    var albums: List<Album>?,
    @SerializedName("artist")
    @Expose
    var artist: List<Artist>?,
    @SerializedName("songs")
    @Expose
    var songs: List<Song>?,
    @SerializedName("totalSongs")
    @Expose
    var totalSongs: Int?
)