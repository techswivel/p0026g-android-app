package com.techswivel.qthemusic.models.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.source.local.database.converter.DownloadStatusConverter
import com.techswivel.qthemusic.source.local.database.converter.SongConverter

@Entity(tableName = "downloadIds")
data class DownloadIds(
    @PrimaryKey
    @ColumnInfo(name = "downloadId")
    @Expose
    var downloadId: Long?,
    @ColumnInfo(name = "fileName")
    @Expose
    var fileName: String?,
    @ColumnInfo(name = "reason")
    @Expose
    var reason: String? = "No Reason Provided",
    @SerializedName("fileId")
    @ColumnInfo(name = "fileId")
    @Expose
    val translationId: Int,
    @SerializedName("downloadStatus")
    @ColumnInfo(name = "downloadStatus")
    @Expose
    @TypeConverters(DownloadStatusConverter::class)
    val downloadStatus: DownloadingStatus,
    @SerializedName("song")
    @ColumnInfo(name = "song")
    @Expose
    @TypeConverters(SongConverter::class)
    var mSong: Song?,
    @ColumnInfo(name = "type")
    val type: String,
)