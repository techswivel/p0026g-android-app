package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GoogleResponseModel {
    @Expose
    @SerializedName("access_token")
    var accessToken: String? = null

    @Expose
    @SerializedName("token_type")
    var tokenType: String? = null

    @Expose
    @SerializedName("expires_in")
    var expiresIn: Int? = null

    @Expose
    @SerializedName("refresh_token")
    var refreshToken: String? = null

}