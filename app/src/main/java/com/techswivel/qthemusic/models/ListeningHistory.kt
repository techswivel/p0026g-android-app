package com.techswivel.qthemusic.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "ListeningHistory")
data class ListeningHistory(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0,
    @Expose
    @ColumnInfo(name = "albumId")
    @SerializedName("albumId")
    var albumId: Int?,
    @Expose
    @ColumnInfo(name = "artistId")
    @SerializedName("artistId")
    var artistId: Int?,
    @Expose
    @ColumnInfo(name = "songId")
    @SerializedName("songId")
    var songId: Int?
) : Parcelable