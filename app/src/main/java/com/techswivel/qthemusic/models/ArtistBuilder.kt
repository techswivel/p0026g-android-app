package com.techswivel.qthemusic.models

import java.io.Serializable

class ArtistBuilder : Serializable {
    var artistId: Int? = null
    var isFollowed: Boolean? = null

    companion object {
        fun build(artistBuilder: ArtistBuilder) = ArtistModel(
            artistBuilder.artistId,
            artistBuilder.isFollowed
        )
    }
}