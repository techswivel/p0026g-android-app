package com.techswivel.qthemusic.models.builder


import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.models.Meta
import com.techswivel.qthemusic.models.NextPlaySong
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.PlayerSavedState
import com.techswivel.qthemusic.models.database.Song

class NextPlaySongBuilder {
    var currentSongId: Int? = null
    var categoryId: Int? = null
    var albumId: Int? = null
    var artistId: Int? = null
    var playListId: Int? = null
    var playedFrom: SongType? = null
    var songsList: MutableList<Song>? = null
    var currentSongModel: Song? = null
    var currentAlbumModel: Album? = null
    var apiMeta: Meta? = null


    fun currentSongId(currentSongId: Int?) = apply { this.currentSongId = currentSongId }
    fun categoryId(categoryId: Int?) = apply { this.categoryId = categoryId }
    fun albumId(albumId: Int?) = apply { this.albumId = albumId }
    fun artistId(artistId: Int?) = apply { this.artistId = artistId }
    fun playListId(playListId: Int?) = apply { this.playListId = playListId }
    fun playedFrom(playedFrom: SongType?) = apply { this.playedFrom = playedFrom }
    fun songsList(songsList: MutableList<Song>?) = apply { this.songsList = songsList }
    fun currentSongModel(currentSongModel: Song?) =
        apply { this.currentSongModel = currentSongModel }

    fun currentAlbumModel(currentAlbumModel: Album?) =
        apply { this.currentAlbumModel = currentAlbumModel }

    fun apiMeta(apiMeta: Meta?) = apply { this.apiMeta = apiMeta }

    fun toSavedState(): PlayerSavedState {
        return PlayerSavedState(
            currentSongId ?: 0,
            categoryId,
            albumId,
            artistId,
            playListId,
            playedFrom,
            songsList,
            currentSongModel,
            currentAlbumModel
        )
    }

    companion object {
        fun build(builder: NextPlaySongBuilder) = NextPlaySong(
            builder.currentSongId,
            builder.categoryId,
            builder.albumId,
            builder.artistId,
            builder.playListId,
            builder.playedFrom
        )
    }
}