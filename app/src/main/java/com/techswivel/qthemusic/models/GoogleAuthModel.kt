package com.techswivel.qthemusic.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GoogleAuthModel(
    @Expose
    @SerializedName("code")
    var serverAuthCode: String,
    @Expose
    @SerializedName("grant_type")
    var grantType: String,
    @Expose
    @SerializedName("client_id")
    var clientId: String?,
    @Expose
    @SerializedName("client_secret")
    var clientSecretId: String?,
    @Expose
    @SerializedName("redirect_uri")
    var redirectURL: String?
)