package com.techswivel.qthemusic.models.database


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Category")
data class Category(
    @Expose
    @SerializedName("categoryId")
    @PrimaryKey
    @ColumnInfo(name = "categoryId")
    var categoryId: Int?,
    @Expose
    @SerializedName("totalSongs")
    @ColumnInfo(name = "totalSongs")
    var totalSongs: Int?,
    @Expose
    @SerializedName("totalAlbums")
    @ColumnInfo(name = "totalAlbums")
    var totalAlbums: Int?,
    @Expose
    @SerializedName("categoryImageUrl")
    @ColumnInfo(name = "categoryImageUrl")
    var categoryImageUrl: String?,
    @Expose
    @SerializedName("categoryTitle")
    @ColumnInfo(name = "categoryTitle")
    var categoryTitle: String?,
    @Expose
    @SerializedName("isSelected")
    @ColumnInfo(name = "isSelected")
    var isSelected: Boolean?
) : Parcelable