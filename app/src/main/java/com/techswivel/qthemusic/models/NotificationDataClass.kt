package com.example.example

import com.google.gson.annotations.SerializedName
import com.techswivel.qthemusic.models.PurchasedAlbum


data class NotificationDataClass(
    @SerializedName("title") var title: String? = null,
    @SerializedName("body") var body: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("notificationType") var notificationType: String? = null,
    @SerializedName("notification") var notification: String? = null,
    @SerializedName("purchasedAlbum") var purchasedAlbum: PurchasedAlbum? = null
) {
    override fun toString(): String {
        return "NotificationDataClass(title=$title, body=$body, type=$type, notificationType=$notificationType, notification=$notification, purchasedAlbum=$purchasedAlbum)"
    }
}