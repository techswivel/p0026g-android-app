package com.techswivel.qthemusic.helper

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.os.Looper
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.utils.PlayerUtils
import com.techswivel.qthemusic.utils.Utilities

class PlayerHelper(
    context: Context,
    song: Song,
    album: Album?,
    callBack: PlayerCallBack?,
    player: PlayerView?,
    mSongsList: List<Song>
) {
    private var mSongsList = mSongsList
    private var currentSongModel: Song = song
    private var currentAlbum: Album? = album
    private lateinit var dataSourceFactory: DataSource.Factory
    private lateinit var trackSelectionParameters: DefaultTrackSelector.Parameters
    private lateinit var mediaItem: MediaItem
    private lateinit var trackSelector: DefaultTrackSelector
    private lateinit var lastSeenTracksInfo: TracksInfo
    private lateinit var audioPlayer: ExoPlayer
    private var playbackPosition = 0L
    private val playbackStateListener: Player.Listener = playbackStateListener()
    private var mCallBack: PlayerCallBack? = callBack
    private var mContext: Context = context
    private var videoPlayer: PlayerView? = player
    private var isPlayerRelease: Boolean = false
    private lateinit var mHttpDataSourceFactory: HttpDataSource.Factory
    private lateinit var mCacheDataSourceFactory: DataSource.Factory
    private val cache: SimpleCache = QTheMusicApplication.cache
    private val handler: Handler = Handler(Looper.getMainLooper())
    private val updateSongProgress = object : Runnable {
        override fun run() {
            if (::audioPlayer.isInitialized) {
                if (audioPlayer.isPlaying) {
                    mCallBack?.onProgressUpdate((audioPlayer.currentPosition.div(1000)).toInt())
                    mCallBack?.onCurrentDurationUpdate(Utilities.formatSongDuration(audioPlayer.currentPosition))
                    handler.postDelayed(this, Constants.SEEK_BAR_DELAY.toLong())
                }
            }
        }
    }


    constructor(
        context: Context,
        song: Song,
        callBack: PlayerCallBack,
        mSongsList: List<Song>
    ) : this(
        context = context,
        song = song,
        callBack = callBack,
        player = null,
        album = null,
        mSongsList = mSongsList
    )

    constructor() : this(
        context = QTheMusicApplication.getContext(),
        song = Song(),
        callBack = null,
        player = null,
        album = null,
        mSongsList = ArrayList<Song>()
    )

    init {
        initializePlayers()
    }

    private fun initializePlayers() {

        /**
         * Audio Player Initialization
         */
        val mediaItemsListToPlay = mutableListOf<MediaItem>()
        if (currentSongModel.songStatus == SongStatus.PURCHASED) {
            mHttpDataSourceFactory = DefaultHttpDataSource.Factory()
                .setAllowCrossProtocolRedirects(true)

            mCacheDataSourceFactory = CacheDataSource.Factory()
                .setCache(cache)
                .setUpstreamDataSourceFactory(mHttpDataSourceFactory)
                .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

            isPlayerRelease = false
            audioPlayer = ExoPlayer.Builder(mContext)
                .setMediaSourceFactory(DefaultMediaSourceFactory(mCacheDataSourceFactory))
                .build()
                .also { exoPlayer ->
                    if (mSongsList.isEmpty().not()) {
                        for (songs in mSongsList.indices) {
                            mediaItem = MediaItem.fromUri(mSongsList[songs].songAudioUrl ?: "")
                            mediaItemsListToPlay.add(mediaItem)
                        }
                    } else {
                        mediaItem =
                            MediaItem.fromUri(currentSongModel.songAudioUrl ?: "")
                    }
                    if (mSongsList.isEmpty().not()) {
                        exoPlayer.setMediaItems(mediaItemsListToPlay)
                    }

                    val currentSongIndex = mSongsList.indexOf(currentSongModel)
                    exoPlayer.seekTo(currentSongIndex, 0)
                    exoPlayer.setMediaSource(
                        ProgressiveMediaSource.Factory(mCacheDataSourceFactory)
                            .createMediaSource(mediaItem), true
                    )
                    exoPlayer.playWhenReady = true
                    exoPlayer.seekTo(playbackPosition)
                    exoPlayer.addListener(playbackStateListener)
                    exoPlayer.prepare()
                }
        } else {
            isPlayerRelease = false
            audioPlayer = ExoPlayer.Builder(mContext)
                .build()
                .also { exoPlayer ->
                    if (mSongsList.isEmpty().not()) {
                        for (songs in mSongsList.indices) {
                            mediaItem =
                                MediaItem.fromUri(
                                    (if (mSongsList[songs].localPath != null) mSongsList[songs].localPath else mSongsList[songs].songAudioUrl
                                        ?: "")!!
                                )
                            mediaItemsListToPlay.add(mediaItem)
                        }
                    } else {
                        mediaItem =
                            MediaItem.fromUri(
                                (if (currentSongModel.localPath != null) currentSongModel.localPath else currentSongModel.songAudioUrl
                                    ?: "")!!
                            )
                        exoPlayer.setMediaItem(mediaItem)
                    }
                    if (mSongsList.isEmpty().not()) {
                        exoPlayer.setMediaItems(mediaItemsListToPlay)
                        val currentSongIndex = mSongsList.indexOf(currentSongModel)
                        if (mSongsList[currentSongIndex].songStatus != SongStatus.PREMIUM) {
                            exoPlayer.seekTo(currentSongIndex, 0)
                        }
                    } else {
                        exoPlayer.setMediaItem(mediaItem)
                    }
                    exoPlayer.playWhenReady = true
                    exoPlayer.seekTo(playbackPosition)
                    exoPlayer.addListener(playbackStateListener)
                    exoPlayer.prepare()
                }
        }
        /**
         * Video Player Initialization
         */
        initionlizeVideoPlayer()
    }
    private fun initionlizeVideoPlayer() {
        if (currentSongModel.songVideoUrl != null && videoPlayer != null) {
            trackSelectionParameters =
                DefaultTrackSelector.ParametersBuilder(mContext).setMaxVideoSizeSd().build()
            mediaItem = PlayerUtils.prepare(
                Uri.parse(if (currentSongModel.localPath != null) currentSongModel.localPath else currentSongModel.songVideoUrl),
                currentSongModel.songTitle ?: ""
            )
            if (currentSongModel.songStatus == SongStatus.PURCHASED) {
                mHttpDataSourceFactory = DefaultHttpDataSource.Factory()
                    .setAllowCrossProtocolRedirects(true)

                mCacheDataSourceFactory = CacheDataSource.Factory()
                    .setCache(cache)
                    .setUpstreamDataSourceFactory(mHttpDataSourceFactory)
                    .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)
                try {
                    val renderersFactory: RenderersFactory =
                        PlayerUtils.buildRenderersFactory(mContext, false)

                    trackSelector = DefaultTrackSelector(mContext)
                    lastSeenTracksInfo = TracksInfo.EMPTY

                    videoPlayer?.player = ExoPlayer.Builder(mContext)
                        .setRenderersFactory(renderersFactory)
                        .setMediaSourceFactory(DefaultMediaSourceFactory(mCacheDataSourceFactory))
                        .setTrackSelector(trackSelector)
                        .setSeekBackIncrementMs(10000)
                        .setSeekForwardIncrementMs(10000)
                        .build()
                    (videoPlayer?.player as ExoPlayer?)?.trackSelectionParameters =
                        trackSelectionParameters
                    (videoPlayer?.player as ExoPlayer?)?.setAudioAttributes(
                        AudioAttributes.DEFAULT,
                        true
                    )
                    (videoPlayer?.player as ExoPlayer?)?.setMediaSource(
                        ProgressiveMediaSource.Factory(mCacheDataSourceFactory)
                            .createMediaSource(mediaItem), true
                    )
                    (videoPlayer?.player as ExoPlayer?)?.prepare()
                    (videoPlayer?.player as ExoPlayer?)?.playWhenReady = true
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                dataSourceFactory = PlayerUtils.getDataSourceFactory(mContext)
                try {
                    val renderersFactory: RenderersFactory =
                        PlayerUtils.buildRenderersFactory(mContext, false)
                    val mediaSourceFactory: DefaultMediaSourceFactory =
                        DefaultMediaSourceFactory(dataSourceFactory)
                            .setAdViewProvider(videoPlayer)

                    trackSelector = DefaultTrackSelector(mContext)
                    lastSeenTracksInfo = TracksInfo.EMPTY

                    videoPlayer?.player = ExoPlayer.Builder(mContext)
                        .setRenderersFactory(renderersFactory)
                        .setMediaSourceFactory(mediaSourceFactory)
                        .setTrackSelector(trackSelector)
                        .setSeekBackIncrementMs(10000)
                        .setSeekForwardIncrementMs(10000)
                        .build()
                    (videoPlayer?.player as ExoPlayer?)?.trackSelectionParameters =
                        trackSelectionParameters
                    (videoPlayer?.player as ExoPlayer?)?.setAudioAttributes(
                        AudioAttributes.DEFAULT,
                        true
                    )
                    (videoPlayer?.player as ExoPlayer?)?.setMediaItem(mediaItem)
                    (videoPlayer?.player as ExoPlayer?)?.prepare()
                    (videoPlayer?.player as ExoPlayer?)?.playWhenReady = false
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun seekTo(progress: Int) {
        audioPlayer.seekTo((progress).toLong())
    }

    fun play() {
        handler.postDelayed(updateSongProgress, Constants.SEEK_BAR_DELAY.toLong())
        audioPlayer.playWhenReady = true
        audioPlayer.play()
        mCallBack?.onIsPlayingChanged(true)
    }

    fun paused() {
        handler.removeCallbacks(updateSongProgress)
        audioPlayer.playWhenReady = false
        audioPlayer.pause()
        mCallBack?.onIsPlayingChanged(false)
    }

    fun reset() {
        playbackPosition = 0
        handler.removeCallbacks(updateSongProgress)
        audioPlayer.playWhenReady = false
        audioPlayer.isPlaying
    }

    fun forward() {
        audioPlayer.seekTo(
            audioPlayer.currentPosition.plus(10000)
        )
    }

    fun isAudioPlayerPlaying(): Boolean {
        //this is the way around to fix the mini player play icon issue
        return audioPlayer.isPlaying
    }

    fun currentTimeOfSong(): Long {
        return audioPlayer.currentPosition
    }

    fun currentDuration(): Long {
        return audioPlayer.duration
    }

    fun getAudioPlayer(): ExoPlayer {
        return audioPlayer
    }
    fun setVideoPlayer(player: PlayerView?) {
        this.videoPlayer = player
        initionlizeVideoPlayer()
    }

    fun transitionToVideoPlayer() {
        if (audioPlayer.isPlaying) {
            handler.removeCallbacks(updateSongProgress)
            audioPlayer.playWhenReady = false
        }
    }

    fun rewind() {
        audioPlayer.seekTo(
            audioPlayer.currentPosition.minus(10000)
        )
    }

    fun stopPlayer() {
        if ((videoPlayer?.player as ExoPlayer?)?.isPlaying == true) {
            mCallBack?.readyToShowVideoPlayerPlayIcons()
            (videoPlayer?.player as ExoPlayer?)?.playWhenReady = false
        } else if (audioPlayer.isPlaying) {
            handler.removeCallbacks(updateSongProgress)
            audioPlayer.playWhenReady = false
        }
    }

    fun releasePlayers() {
        /**
         * Releasing Audio Player
         */
        audioPlayer.run {
            playbackPosition = this.currentPosition
            release()
        }
        handler.removeCallbacks(updateSongProgress)
        isPlayerRelease = true
        /**
         * Releasing Video Player
         */
        (videoPlayer?.player as ExoPlayer?)?.run {
            playbackPosition = this.currentPosition
            release()
        }
        videoPlayer?.player = null
    }

    fun isPlayerRelease(): Boolean {
        return isPlayerRelease
    }

    private fun playbackStateListener() = object : Player.Listener {
        override fun onPlaybackStateChanged(playbackState: Int) {
            when (playbackState) {
                ExoPlayer.STATE_IDLE -> {
                    mCallBack?.onPlayerStateIdle()
                }
                ExoPlayer.STATE_BUFFERING -> {
                    mCallBack?.onPlayerStateBuffering()

                }
                ExoPlayer.STATE_READY -> {
                    mCallBack?.onPlayerStateReady()
                    mCallBack?.onMaxSongDuration((audioPlayer.duration.div(1000)).toInt())
                    mCallBack?.onCurrentDurationUpdate(
                        Utilities.formatSongDuration(
                            audioPlayer.currentPosition
                        )
                    )
                    handler.postDelayed(updateSongProgress, Constants.SEEK_BAR_DELAY.toLong())
                    mCallBack?.onIsPlayingChanged(true)
                }
                ExoPlayer.STATE_ENDED -> {
                    mCallBack?.onPlayerStateEnded(
                        songStatus = currentSongModel.songStatus ?: SongStatus.FREE,
                        albumStatus = currentAlbum?.albumStatus
                    )
                }
            }
        }

        //Sending callback to handle auto play songs for player from here
        override fun onPositionDiscontinuity(
            oldPosition: Player.PositionInfo,
            newPosition: Player.PositionInfo,
            reason: Int
        ) {
            super.onPositionDiscontinuity(oldPosition, newPosition, reason)
            if (reason == ExoPlayer.DISCONTINUITY_REASON_AUTO_TRANSITION) {
                mCallBack?.onPositionDiscontinuity(oldPosition, newPosition, reason)
            }
        }
    }

    interface PlayerCallBack {
        fun onProgressUpdate(timeInMis: Int)
        fun onCurrentDurationUpdate(time: String) {

        }

        fun onMaxSongDuration(time: Int) {

        }

        fun onMaxSongDuration(time: String) {

        }

        fun onPlayerStateIdle()
        fun onPlayerStateBuffering()
        fun onPlayerStateReady()
        fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?)
        fun readyToShowVideoPlayerPlayIcons() {

        }

        fun onIsPlayingChanged(isPlaying: Boolean) {

        }

        fun onPositionDiscontinuity(
            oldPosition: Player.PositionInfo,
            newPosition: Player.PositionInfo,
            reason: Int
        ) {

        }

        fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

        }

        /**
         * This method is used to show progress bar if played from service class */
        fun showPlayerProgressBar() {

        }

        fun updateUI(song: Song) {

        }
    }

    companion object {
        private const val TAG = "PlayerHelper"
    }
}