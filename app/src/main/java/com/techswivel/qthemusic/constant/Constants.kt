package com.techswivel.qthemusic.constant


object Constants {

    const val NO_OF_COLUMNS_2: Int = 2
    const val NO_OF_COLUMNS_3 = 3
    const val APP_DATABASE_NAME = "QTheMusic"
    const val STAGING = "staging"
    const val PRODUCTION = "production"
    const val ACCEPTANCE = "acceptance"
    const val DEVELOPMENT = "development"
    const val STAGING_SERVER_URL = "https://staging.qthemusic.app/api/v1/"
    const val PRODUCTION_SERVER_URL = "https://production.qthemusic.app/api/v1/"
    const val ACCEPTANCE_SERVER_URL = "https://acceptance.qthemusic.app/api/v1/"
    const val DEVELOPMENT_SERVER_URL = "https://development.qthemusic.app/api/v1/"

    val NO_INTERNET_TITLE = "NO INTERNET"
    val NO_INTERNET_MESSAGE = "Please connect to a stable internet"

    const val PREF_NAME = "_Data_Store_Pref_"
    const val WORK_MANAGER_UNIQUE_NAME = "syncService"

    const val STRIPE_KEY = "abc"
    const val SPLASHDELAY = 1000 * 3 * 1 // in milliSeconds

    const val DEFAULT_DATE_FORMATE = "dd MMMM,yyyy"
    const val DATE_FORMATE = "dd MMM yyyy - HH:mm aa"
    const val FCM_ANDROID_TOPIC = "Android_Users"

    const val DIRECTORY_SUB_PATH = "QthMusic"

    const val CPP_LIBRARY_NAME = "keys"

    const val NOTIFICATION_CHANNEL_ID = 1015
    const val PENDING_INTENT_REQUEST_CODE = 1010
    const val NOTIFICATION_ID = 1
    const val NOTIFICATION_CHANNEL_NAME = "Player_channel"

    const val IMAGE_PICKER_REQUEST_CODE = 1234

    const val CAMERA_PICKER_REQUEST_CODE = 12345

    const val VIDEO_GALLERY_PICKER_REQUEST_CODE = 2234

    const val VIDEO_CAMERA_PICKER_REQUEST_CODE = 22345
    const val GOOGLE_SIGN_IN_REQUEST_CODE = 1001

    const val OTP_RESEND_TIME_IN_MILLI_SECONDS: Long = 30000

    const val OTP_COUNT_DOWN_INTERVAL_IN_MILLI_SECONDS: Long = 1000

    const val SEEK_BAR_DELAY = 100
    const val NUMBER_OF_COLUMN = 3
    const val NUMBER_OF_COLUMN_CATEGORIES_VIEW = 2
    const val ARTIST_COLUMN = 3
    const val TEXTVIEW_LINE_LIMIT = 1
    const val ONE_SECOND: Long = 1000
    const val NEXT_SONG_LIMIT = 3
    const val PAGE: Int = 1
    const val LIMIT = 100
    const val SEARCH_API_DELAY: Long = 2000

    const val HOME_NAV_ID: Int = 0
    const val SEARCH_NAV_ID: Int = 1
    const val CATEGORY_NAV_ID: Int = 2
    const val PROFILE_NAV_ID: Int = 3
    const val PLAY_STORE_PACKAGE = "com.android.vending"
    const val COUNT_MAX_VALUE = 2

}
