package com.techswivel.qthemusic.application

import android.app.Application
import android.app.DownloadManager
import android.content.Context
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.multidex.MultiDex
import com.facebook.appevents.AppEventsLogger
import com.google.android.exoplayer2.database.StandaloneDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.techswivel.qthemusic.BuildConfig
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.helper.StringHelper


class QTheMusicApplication : Application(), LifecycleObserver {

    private val cacheSize: Long = 90 * 1024 * 1024
    private lateinit var cacheEvictor: LeastRecentlyUsedCacheEvictor
    private lateinit var exoplayerDatabaseProvider: StandaloneDatabaseProvider

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        AppEventsLogger.activateApp(this)
        downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        System.loadLibrary(Constants.CPP_LIBRARY_NAME)
        when {
            BuildConfig.FLAVOR.equals(Constants.STAGING) -> {
                mGso =
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)  //google sigin options
                        .requestServerAuthCode(getGoogleClientIdStaging())
                        .requestEmail()
                        .build()
            }
            BuildConfig.FLAVOR.equals(Constants.DEVELOPMENT) -> {
                mGso =
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)  //google sigin options
                        .requestServerAuthCode(getGoogleClientIdDevelopment())
                        .requestEmail()
                        .build()
            }
            BuildConfig.FLAVOR.equals(Constants.ACCEPTANCE) -> {
                mGso =
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)  //google sigin options
                        .requestServerAuthCode(getGoogleClientIdAcceptance())
                        .requestEmail()
                        .build()
            }
            else -> {
                mGso =
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)  //google sigin options
                        .requestServerAuthCode(getGoogleClientIdProduction())
                        .requestEmail()
                        .build()
            }
        }

        mContext = this

        cacheEvictor = LeastRecentlyUsedCacheEvictor(cacheSize)
        exoplayerDatabaseProvider = StandaloneDatabaseProvider(this)
        cache = SimpleCache(cacheDir, cacheEvictor, exoplayerDatabaseProvider)
        subscribeToTopic(Constants.FCM_ANDROID_TOPIC)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        // app moved to foreground
        isInBackground = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        // app moved to background
        isInBackground = true
    }

    private external fun getGoogleClientIdStaging(): String
    private external fun getGoogleClientIdDevelopment(): String
    private external fun getGoogleClientIdAcceptance(): String
    private external fun getGoogleClientIdProduction(): String

    companion object {
        private const val TAG = "QTheMusicApplication"
        lateinit var cache: SimpleCache
        private lateinit var mContext: Context
        private lateinit var mGso: GoogleSignInOptions
        private lateinit var mAuth: FirebaseAuth
        private lateinit var firestoreDB: FirebaseFirestore
        private var downloadManager: DownloadManager? = null
        private var isInBackground = false

        fun getContext(): Context {
            return mContext
        }

        fun getWasInBackground(): Boolean {
            return isInBackground
        }

        fun getGso(): GoogleSignInOptions {
            return mGso
        }

        fun getFirebaseAuthen(): FirebaseAuth {
            return mAuth
        }

        fun getFirebaseFirestore(): FirebaseFirestore {
            return firestoreDB
        }

        fun getDownloadService(): DownloadManager? {
            return downloadManager
        }

        fun subscribeToTopic(topic: String) {
            Firebase.messaging.subscribeToTopic(topic)
                .addOnCompleteListener { task ->
                    var msg = StringHelper.getStringFromId(mContext,R.string.msg_subscribed,topic)
                    if (!task.isSuccessful) {
                        msg = StringHelper.getStringFromId(mContext,R.string.msg_subscribe_failed,topic)
                    }
                    Log.e(TAG, msg.toString())
                }
        }

        fun unSubscribeToTopic(topic: String) {
            Firebase.messaging.unsubscribeFromTopic(topic)
                .addOnCompleteListener { task ->
                    var msg = getContext().getString(R.string.msg_unsubscribed)
                    if (!task.isSuccessful) {
                        msg = getContext().getString(R.string.msg_unsubscribe_failed)
                    }
                    Log.e(TAG, msg)
                }
        }
    }
}