package com.techswivel.qthemusic.services

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.*
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.dataManager.LocalDataManager
import com.techswivel.qthemusic.helper.FileHelper
import com.techswivel.qthemusic.models.DownloadingResponse
import com.techswivel.qthemusic.models.database.DownloadIds
import com.techswivel.qthemusic.models.database.Song
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.channels.FileChannel


class DownloadIngReceiver : BroadcastReceiver() {

    private val mDataManager = LocalDataManager


    override fun onReceive(context: Context?, intent: Intent?) {

        //Fetching the download id received with the broadcast
        val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
        //Checking if the received broadcast is for our enqueued download by matching download id
        GlobalScope.launch {
            if (mDataManager.isDownloadIdExist(id ?: 0)) {
                handleResult(id ?: 0)
            } else {
                Log.e(TAG, "onReceive: no download exist ")
            }
        }
    }

    @SuppressLint("Range")
    private fun handleResult(id: Long) {
        val cursor: Cursor? =
            QTheMusicApplication.getDownloadService()?.query(
                DownloadManager.Query().setFilterById(id)
            )
        if (cursor?.moveToFirst() == true) {
            when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
                DownloadManager.STATUS_FAILED -> {
                    GlobalScope.launch {
                        EventBus.getDefault().post(
                            DownloadingResponse.failed(
                                "Failed with no reason", mDataManager.getDownloadFile(
                                    id
                                ).mSong?.songId ?: 0
                            )
                        )
                    }
                }

                DownloadManager.STATUS_PAUSED -> {
                    GlobalScope.launch {
                        EventBus.getDefault().post(
                            DownloadingResponse.paused(
                                mDataManager.getDownloadFile(
                                    id
                                ).mSong?.songId ?: 0
                            )
                        )
                    }
                }

                DownloadManager.STATUS_PENDING -> {
                    GlobalScope.launch {
                        EventBus.getDefault().post(
                            DownloadingResponse.pending(
                                mDataManager.getDownloadFile(
                                    id
                                ).mSong?.songId ?: 0
                            )
                        )
                    }
                }

                DownloadManager.STATUS_RUNNING -> {
                    GlobalScope.launch {
                        val total =
                            cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                        if (total >= 0) {
                            val downloaded =
                                cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                            val progress = ((downloaded * 100L) / total)
                            Log.d(TAG, "handleResult: now progress is $progress")
                            EventBus.getDefault().post(
                                DownloadingResponse.running(
                                    progress, mDataManager.getDownloadFile(
                                        id
                                    ).mSong?.songId ?: 0
                                )
                            )
                        }
                    }
                }

                DownloadManager.STATUS_SUCCESSFUL -> {
                    GlobalScope.launch {
                        val downloadFile = mDataManager.getDownloadFile(id)
                        mDataManager.updateStatus(
                            DownloadingStatus.INSTALLING,
                            downloadFile.downloadId ?: 0,
                            "Installing"
                        )
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
                            val projectionDownloads = arrayOf(
                                MediaStore.Downloads._ID,
                                MediaStore.Downloads.DISPLAY_NAME
                            )
                            val selectionDownloads = ""
                            val selectionArgsDownloads = emptyArray<String>()
                            val sortOrderDownloads = "${MediaStore.Downloads.DISPLAY_NAME} ASC"
                            var localUri: Uri? = Uri.EMPTY

                            QTheMusicApplication.getContext().applicationContext.contentResolver.query(
                                MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                                projectionDownloads,
                                selectionDownloads,
                                selectionArgsDownloads,
                                sortOrderDownloads
                            )?.use { cursor ->
                                Log.i("SeeMedia", "we got cursor! $cursor")
                                val idColumn =
                                    cursor.getColumnIndex(MediaStore.Files.FileColumns._ID)
                                val nameColumn =
                                    cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME)

                                while (cursor.moveToNext()) { //Here return false
                                    Log.i("SeeMedia", "Move to next!")
                                    val id = cursor.getLong(idColumn)
                                    val name = cursor.getString(nameColumn)

                                    Log.i(
                                        "SeeMedia",
                                        "ID = $id AND NAME= $name AND stored name ${downloadFile.fileName}"
                                    )
                                    Log.i(
                                        TAG,
                                        "handleResult: and local file name = ${downloadFile.fileName.toString()}"
                                    )
                                    val fileStoreNAme = if (name.contains("-")
                                    ) {
                                        name.split("-")[0]
                                    } else {
                                        name
                                    }
                                    val serverName =
                                        downloadFile.fileName?.split(".")?.get(0).toString()
                                    if (fileStoreNAme.contains(serverName, true)) {
                                        localUri = ContentUris.withAppendedId(
                                            MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                                            id
                                        )
                                        Log.i(
                                            TAG,
                                            "handleResult: match name = ${downloadFile.fileName} and URI = ${
                                                ContentUris.withAppendedId(
                                                    MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                                                    id
                                                )
                                            }"
                                        )
                                        localUri = ContentUris.withAppendedId(
                                            MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                                            id
                                        )
                                    }
                                    Log.i(
                                        TAG,
                                        "handleResult: if condition is check now local uri = $localUri"
                                    )
                                }

                                if (localUri.toString().isEmpty()) {
                                    EventBus.getDefault().post(
                                        DownloadingResponse.failed(
                                            "Failed To Download.",
                                            mDataManager.getDownloadFile(id).mSong?.songId ?: 0
                                        )
                                    )
                                    mDataManager.deleteRecord(downloadFile)
                                    return@use
                                }
                                var file = File(
                                    FileHelper.getPathFromUri(
                                        QTheMusicApplication.getContext(),
                                        localUri ?: Uri.EMPTY
                                    )
                                )
                                file = FileHelper.moveFile(
                                    QTheMusicApplication.getContext(),
                                    downloadFile.fileName.toString(), file
                                )
                                Log.e(TAG, "handleResult: file move successfully")
                                if (file.exists()) {
                                    installSong(file, downloadFile)
                                } else {
                                    EventBus.getDefault().post(
                                        DownloadingResponse.failed(
                                            "File not exist",
                                            mDataManager.getDownloadFile(id).mSong?.songId ?: 0
                                        )
                                    )
                                    mDataManager.updateStatus(
                                        DownloadingStatus.FAILED,
                                        downloadFile.downloadId ?: 0,
                                        "File not exist"
                                    )
                                }
                            }
                            Log.i(TAG, "handleResult: out side query local uri = $localUri")

                        } else {
                            val mFile = File(
                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                                downloadFile.fileName ?: ""
                            )
                            if (mFile.exists()) {
                                Log.i("TAG", "handleResult: ${mFile.absolutePath}")
                                installSong(mFile, downloadFile)
                            } else {
                                EventBus.getDefault().post(
                                    DownloadingResponse.failed(
                                        "Failed with no reason",
                                        mDataManager.getDownloadFile(id).mSong?.songId ?: 0
                                    )
                                )
                                mDataManager.deleteRecord(downloadFile)
                                Log.i(TAG, "handleResult: give file path does not exist.")
                            }
                        }
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.R)
    fun moveFileToNewLocation(uri: Uri) {
        //create content values with new name and update
        val newDir = createDirectoryIfNotExist()
        Log.e(TAG, "rename: new path is: ${newDir.path}")
        val contentValues = ContentValues()
        contentValues.put(MediaStore.Downloads.EXTERNAL_CONTENT_URI.path, newDir.path)
        Log.e(
            TAG,
            "rename: file will store at : ${contentValues.get(MediaStore.MediaColumns.DISPLAY_NAME)}"
        )
        QTheMusicApplication.getContext().contentResolver.update(uri, contentValues, null)
    }

    private fun convertToBase64(attachment: File): String {
        return Base64.encodeToString(attachment.readBytes(), Base64.NO_WRAP)
    }

    private fun createMP3(mContext: Context, fileName: String, base64: String?): File {
        var folderPath = ""
        var dwldsPath = File("")
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
                folderPath =
                    mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.path + File.separator.toString() + Constants.DIRECTORY_SUB_PATH
                Log.e(TAG, "createMP3: folder path is $folderPath")
                dwldsPath = File("$folderPath/$fileName")
                val folder = File(folderPath)
                if (folder.exists().not()) {
                    folder.mkdirs()
                }
                val values = ContentValues()
                values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName) // file name
                values.put(
                    MediaStore.MediaColumns.MIME_TYPE,
                    "application/mp3"
                ) // file extension, will automatically add to file
                values.put(
                    MediaStore.DownloadColumns.RELATIVE_PATH,
                    folderPath
                ) // end "/" is not mandatory
                val uriFile = mContext.contentResolver.insert(
                    MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                    values
                ) // important!
                val outputStream = mContext.contentResolver.openOutputStream(uriFile!!)
                outputStream?.write(Base64.decode(base64, 0))
                outputStream?.close()
            } else {
                folderPath =
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path + File.separator.toString() + Constants.DIRECTORY_SUB_PATH
                dwldsPath = File("$folderPath/$fileName")
                val folder = File(folderPath)
                if (folder.exists().not()) {
                    folder.mkdirs()
                }
                val os = FileOutputStream(dwldsPath, false)
                os.write(Base64.decode(base64, 0))
                os.flush()
                os.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e(TAG, "createMP3: exception: ", e)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(mContext, "No PDF Viewer Installed", Toast.LENGTH_LONG).show()
        }
        return dwldsPath
    }


    private fun installSong(file: File, mDownloadFile: DownloadIds) {
        GlobalScope.launch {
            try {
                val downloadFile = mDataManager.getDownloadFile(mDownloadFile.downloadId ?: -1)
                mDataManager.deleteRecord(downloadFile)
                downloadFile.mSong?.localPath = file.absolutePath
                Log.e(TAG, "installSong: file path is: ${file.absolutePath}")
                mDataManager.saveSong(downloadFile.mSong ?: Song())
                EventBus.getDefault()
                    .post(
                        DownloadingResponse.success(
                            downloadFile.mSong?.songId ?: 0
                        )
                    )
            } catch (exception: Exception) {
                Log.e(TAG, "installSong: get exception: ", exception)
                mDataManager.updateStatus(
                    DownloadingStatus.UN_CONFIGURABLE,
                    mDownloadFile.downloadId ?: 0,
                    exception.message ?: "Failed to parse Json"
                )
                EventBus.getDefault()
                    .post(
                        DownloadingResponse.rollBack(
                            exception.localizedMessage?.toString() ?: "Going for rollback",
                            mDownloadFile.mSong?.songId ?: 0
                        )
                    )
            }

        }

    }

    @Throws(IOException::class)
    private fun moveFile(file: File, dir: File): File {
        val newFile = File(dir, file.name)
        Log.e(TAG, "moveFile: absolute path of new file is ${newFile.absolutePath}")
        var outputChannel: FileChannel? = null
        var inputChannel: FileChannel? = null
        try {
            outputChannel = FileOutputStream(newFile).channel
            inputChannel = FileInputStream(file).channel
            inputChannel.transferTo(0, inputChannel.size(), outputChannel)
            if (inputChannel.isOpen) {
                inputChannel.close()
            }
            file.delete()
        } catch (exception: Exception) {
            exception.printStackTrace()
            Log.e(TAG, "moveFile: Exception occure with message ${exception.message}:  ", exception)
        } finally {
            inputChannel?.close()
            outputChannel?.close()
        }
        return newFile
    }

    private fun createDirectoryIfNotExist(): File {
        val filepath: File = Environment.getExternalStorageDirectory()
        val dir = File(filepath.absolutePath + "/${Constants.DIRECTORY_SUB_PATH}")
        if (dir.exists().not()) {
            try {
                dir.mkdirs()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                Log.e(TAG, "createDirectory: failed to create directory with exception: ", e)
            }
        }
        return dir
    }

    companion object {
        private const val TAG = "DownloadIngReceiver"
    }

}