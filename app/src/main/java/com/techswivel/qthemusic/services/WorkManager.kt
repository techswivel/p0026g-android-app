package com.techswivel.qthemusic.services

import androidx.work.*
import androidx.work.WorkManager
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.utils.CommonKeys
import java.util.concurrent.TimeUnit

class WorkManager {

    companion object {
        private val constraint: Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        private val syncAllDataRequest =
            PeriodicWorkRequestBuilder<AllDatabaseSyncWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraint)
                .build()

        fun startService() {
            PrefUtils.setString(
                QTheMusicApplication.getContext(),
                CommonKeys.KEY_SYNC_SERVICE_UUID,
                syncAllDataRequest.id.toString()
            )

            WorkManager
                .getInstance(QTheMusicApplication.getContext())
                .enqueueUniquePeriodicWork(
                    Constants.WORK_MANAGER_UNIQUE_NAME,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    syncAllDataRequest
                )
        }

        fun stopService() {
            WorkManager
                .getInstance(QTheMusicApplication.getContext())
                .cancelAllWork()
        }

        private const val TAG = "WorkManager"

    }

}