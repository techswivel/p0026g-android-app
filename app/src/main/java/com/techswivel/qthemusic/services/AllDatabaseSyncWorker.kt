package com.techswivel.qthemusic.services

import android.annotation.SuppressLint
import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.dataManager.LocalDataManager
import com.techswivel.qthemusic.dataManager.RemoteDataManager
import com.techswivel.qthemusic.models.ResponseMain
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.Utilities
import io.reactivex.Single
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import retrofit2.Response
import java.util.concurrent.TimeUnit

class AllDatabaseSyncWorker(appContext: Context, workerParams: WorkerParameters) :
    RxWorker(appContext, workerParams) {

    val mDataManager = RemoteDataManager
    val mLocalDataManager = LocalDataManager

    @SuppressLint("CheckResult")
    override fun createWork(): Single<Result> {
        return syncedDataFromServer().map { response ->
            when (response.code()) {
                200 -> {
                    val data = response.body()?.response as ResponseModel
                    if (data.status == true) {
                        val recentlyPlayedHistory = data.data.recentlyPlayed
                        val listeningHistory = data.data.listeningHistory

                        runBlocking {
                            mLocalDataManager.deleteListeningHistory()
                            val saveDataInDb = async {

                                //Adding data for listening history
                                if (listeningHistory?.songs != null) {
                                    mLocalDataManager.insertSyncedSongsListToDatabase(
                                        listeningHistory.songs
                                    )
                                }
                                if (listeningHistory?.artists != null) {
                                    mLocalDataManager.insertSyncedArtistsListToDatabase(
                                        listeningHistory.artists
                                    )
                                }

                                if (listeningHistory?.albums != null) {
                                    mLocalDataManager.insertSyncedAlbumsListToDatabase(
                                        listeningHistory.albums
                                    )
                                }

                                //Adding data for recently played
                                if (recentlyPlayedHistory?.songs != null) {
                                    mLocalDataManager.insertSyncedSongsListToDatabase(
                                        recentlyPlayedHistory.songs
                                    )
                                }
                                if (recentlyPlayedHistory?.artists != null) {
                                    mLocalDataManager.insertSyncedArtistsListToDatabase(
                                        recentlyPlayedHistory.artists
                                    )
                                }

                                if (recentlyPlayedHistory?.albums != null) {
                                    mLocalDataManager.insertSyncedAlbumsListToDatabase(
                                        recentlyPlayedHistory.albums
                                    )
                                }
                            }
                            saveDataInDb.await()
                            PrefUtils.setLong(
                                QTheMusicApplication.getContext(),
                                CommonKeys.KEY_LAST_SYNC_TIME,
                                TimeUnit.MILLISECONDS.toSeconds(Utilities.getCurrentTimeInMillis())
                            )
                            Result.success()
                            }
                        } else {
                            Result.retry()
                        }
                    }
                    401 -> {
                        mDataManager.clearAppSession()
                        Result.failure()
                    }
                    403 -> {
                        mDataManager.clearAppSession()
                        Result.failure()
                    }
                    else -> {
                        Result.retry()
                    }
                }

            }
    }

    private fun syncedDataFromServer(): Single<Response<ResponseMain>> {
        val observer = mDataManager.syncRecentlyPlayedData()
        return Single.fromObservable(observer)
    }

    companion object {
        private const val TAG = "AllDatabaseSyncWorker"
    }
}