package com.techswivel.qthemusic.services.exoService

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MainPosition(
    val currentWindowIndex: Int,
    val currentPosition: Long
) : Parcelable {

}