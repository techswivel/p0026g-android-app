package com.techswivel.qthemusic.services.exoService

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

internal class MainStorage private constructor(context: Context) {
    private val mSharedPref: SharedPreferences =
        context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE)

    fun storePosition(currentPosition: MainPosition?) {
        val json = Gson().toJson(currentPosition)
        val editor = mSharedPref.edit()
        editor.putString(CURRENT_POSITION, json)
        editor.apply()
    }

    val position: MainPosition
        get() {
            val json = mSharedPref.getString(CURRENT_POSITION, null)
            return if (json.isNullOrBlank()) {
                MainPosition(0, 0)
            } else {
                Gson().fromJson(json, MainPosition::class.java)
            }
        }

    fun setRestartService(restartService: Boolean) {
        val editor = mSharedPref.edit()
        editor.putBoolean(RESTART_SERVICE, restartService)
        editor.apply()
    }

    fun shouldRestartService(): Boolean {
        return mSharedPref.getBoolean(RESTART_SERVICE, DEFAULT_RESTART_SERVICE)
    }

    companion object {
        private const val MY_PREFERENCES = "MyPrefs"
        private const val CURRENT_POSITION = "position"
        private const val RESTART_SERVICE = "restart_service"
        private const val DEFAULT_RESTART_SERVICE = false
        private var sInstance: MainStorage? = null

        @Synchronized
        fun getInstance(context: Context): MainStorage? {
            if (sInstance == null) {
                //Use application Context to prevent leak.
                sInstance = MainStorage(context.applicationContext)
            }
            return sInstance
        }
    }

}