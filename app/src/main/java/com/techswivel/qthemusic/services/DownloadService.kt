package com.techswivel.qthemusic.services

import android.app.DownloadManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.IBinder
import android.util.Log
import androidx.annotation.NonNull
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.dataManager.LocalDataManager
import com.techswivel.qthemusic.models.database.DownloadIds
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_TYPE


class DownloadService : Service() {

    private val mDataManager = LocalDataManager

    companion object {
        private const val DOWNLOAD_PATH =
            "com.techswivel.QthMusic.android.downloadmanager_DownloadSongService_Download_path"

        fun getDownloadService(
            @NonNull callingClassContext: Context?,
            @NonNull downloadPath: String?,
            @NonNull translation: Song,
            @NonNull type: SongType
        ): Intent {
            val bundle = Bundle()
            bundle.putParcelable(KEY_DATA, translation)
            bundle.putSerializable(KEY_TYPE, type)
            return Intent(callingClassContext, DownloadService::class.java)
                .putExtra(DOWNLOAD_PATH, downloadPath)
                .putExtra(KEY_DATA, bundle)
        }

        private const val TAG = "DownloadService"
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            val downloadPath = intent?.getStringExtra(DOWNLOAD_PATH)
            val bundle = intent?.getBundleExtra(KEY_DATA)
            val data = bundle?.getParcelable<Song>(KEY_DATA) as Song
            val type = bundle?.getSerializable(KEY_TYPE) as SongType
            startDownload(downloadPath, data = data, type)
        } catch (e: Exception) {
            Log.e(TAG, "onStartCommand: ", e)
        }
        return super.onStartCommand(intent, flags, startId)

    }

    private fun startDownload(downloadPath: String?, data: Song, type: SongType) {

        val uri: Uri = Uri.parse(downloadPath) // Path where you want to download file.
        val request: DownloadManager.Request = DownloadManager.Request(uri)
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            if (type == SongType.AUDIO) data.audiofileName else data.videofileName
        )
        request.setAllowedOverMetered(true)
        request.setAllowedOverRoaming(false)
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI) // Tell on which network you want to download file.
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) // This will show notification on top when downloading the file.
        val id =
            QTheMusicApplication.getDownloadService()
                ?.enqueue(request) // This will start downloading
        mDataManager.saveDownloadId(
            DownloadIds(
                id,
                if (type == SongType.AUDIO) data.audiofileName else data.videofileName,
                "Downloading PENDING because its in the queue",
                data.songId ?: -1,
                DownloadingStatus.PENDING,
                data,
                type.value
            )
        )
    }
}