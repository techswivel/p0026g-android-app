package com.techswivel.qthemusic.services.exoService

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.media.session.MediaButtonReceiver
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.constant.Constants.NOTIFICATION_ID
import com.techswivel.qthemusic.constant.Constants.PENDING_INTENT_REQUEST_CODE
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.helper.PlayerHelper
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.activities.buyingHistoryActivity.BuyingHistoryActivity
import com.techswivel.qthemusic.ui.activities.listeningHistoryActivity.ListeningHistoryActivity
import com.techswivel.qthemusic.ui.activities.mainActivity.MainActivity
import com.techswivel.qthemusic.ui.activities.playerActivity.PlayerActivity
import com.techswivel.qthemusic.ui.activities.playlistActivity.PlaylistActivity
import com.techswivel.qthemusic.ui.activities.profileSettingScreen.ProfileSettingActivity
import com.techswivel.qthemusic.ui.wrapper.FrameActivity
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA_MODEL
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_IS_FROM_SERVICE
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_IS_RESTART
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_PLAYLIST_ID
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_SONGS_LIST
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_SONG_TYPE
import org.greenrobot.eventbus.EventBus

class MainService : Service(), PlayerHelper.PlayerCallBack {

    /**
     * This method should be used only for setting the exoplayer instance.
     * If exoplayer's internal are altered or accessed we can not guarantee
     * things will work correctly.
     */

    private lateinit var exoPlayerInstance: PlayerHelper
    private lateinit var mStateBuilder: PlaybackStateCompat.Builder
    private lateinit var mDataBundle: Bundle
    private var mMetadataBuilder: MediaMetadataCompat.Builder? = null
    private var mNotificationManager: NotificationManager? = null
    private val mSongs: MutableList<Song> = mutableListOf()
    private var currentSongIndex = 0
    private var isBuyingHistoryActivity = false
    private var isListeningHistoryActivity = false
    private var isPlayListActiviytActivity = false
    private var isProfileSettingActivity = false
    private var isFrameActivity = false
    private var FrameAvtivityArgs: Bundle = Bundle.EMPTY
    private var fragmentName: String = ""
    private var mPlayerBinder: PlayerHelper.PlayerCallBack? = null
    private var needToInitialize = true

    override fun onCreate() {
        super.onCreate()
        mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        initializeMediaSession()
        updateNotification()
    }

    override fun onBind(intent: Intent): IBinder {
        return MainServiceBinder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            mDataBundle = intent?.getBundleExtra(KEY_DATA) ?: Bundle.EMPTY
            mSongs.clear()
            mDataBundle.getParcelableArrayList<Song>(KEY_SONGS_LIST)?.let { songList ->
                songList.forEach { song ->
                    if (song.songStatus != SongStatus.PREMIUM) {
                        mSongs.add(song)
                    }
                }
            }

            // Initialize the Media Session.
            // Initialize the player

            if (::exoPlayerInstance.isInitialized) {
                if (needToInitialize) {
                    initializePlayer()
                }
            } else {
                initializePlayer()
                if (currentSongIndex == mSongs.size - 1 || currentSongIndex == mSongs.size - 2 || currentSongIndex == mSongs.size - 3) {
                    EventBus.getDefault().post(mSongs.last().songId)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "onStartCommand: ", e)
        }
        return START_NOT_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        EventBus.getDefault().post(PlayerState(isNotificationRemoved = true))
        if (::exoPlayerInstance.isInitialized) {
            if (!exoPlayerInstance.isAudioPlayerPlaying()) {
                mNotificationManager?.cancel(NOTIFICATION_ID)
                stopSelf()
            } else {
                exoPlayerInstance.paused()
            }
        }
    }

    /**
     * Release the player when the service is destroyed.
     */

    override fun onDestroy() {
        super.onDestroy()
        if (::exoPlayerInstance.isInitialized) {
            val mainPosition = MainPosition(
                currentSongIndex,
                exoPlayerInstance.currentTimeOfSong()
            )
            MainStorage.getInstance(this)?.storePosition(mainPosition)
            releasePlayer()
        }
        EventBus.getDefault().post(PlayerState(isNotificationRemoved = true))
        mMediaSession?.isActive = false
    }

    override fun onProgressUpdate(timeInMis: Int) {
    }

    override fun onPlayerStateIdle() {
    }

    override fun onPlayerStateBuffering() {
    }

    override fun onPlayerStateReady() {
        mPlayerBinder?.onPlayerStateReady()
        updateNotification()
    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {
        if (currentSongIndex != mSongs.size - 1) {
            skipToNextSong()
        } else {
            releasePlayer()
            initializePlayer()
        }
    }

    override fun onIsPlayingChanged(isPlaying: Boolean) {
        super.onIsPlayingChanged(isPlaying)
        if (isPlaying) {
            mStateBuilder.setState(
                PlaybackStateCompat.STATE_PLAYING,
                exoPlayerInstance.currentTimeOfSong(), 1f
            )

        } else {
            mStateBuilder.setState(
                PlaybackStateCompat.STATE_PAUSED,
                exoPlayerInstance.currentTimeOfSong(), 1f
            )
        }
        val isRestartService = mDataBundle.getBoolean(KEY_IS_RESTART, false)
        if (isRestartService) {
            mDataBundle.remove(KEY_IS_RESTART)
            exoPlayerInstance.paused()
        }
        EventBus.getDefault().postSticky(
            PlayerState(
                isPlaying = isPlaying,
                currentSong = mSongs[currentSongIndex],
                playerHelper = exoPlayerInstance
            )
        )
        updateNotification()
    }
    override fun onPositionDiscontinuity(
        oldPosition: Player.PositionInfo,
        newPosition: Player.PositionInfo,
        reason: Int
    ) {
        //Auto Play of songs are being handled here in if block
        if (reason == ExoPlayer.DISCONTINUITY_REASON_AUTO_TRANSITION) {
            //Changing the current song index according to the mediaItemList song index
            currentSongIndex = exoPlayerInstance.getAudioPlayer().currentMediaItemIndex
            //Updating the ui according to current song index
            mPlayerBinder?.updateUI(mSongs[currentSongIndex])
            //Giving the current song index to play
            exoPlayerInstance.getAudioPlayer().seekTo(currentSongIndex, 0)
            //changinf the player state to play the current song
            mStateBuilder = PlaybackStateCompat.Builder()
                .setState(
                    PlaybackStateCompat.STATE_PLAYING,
                    currentSongIndex.toLong(),
                    1f
                )
                .setBufferedPosition(exoPlayerInstance.currentDuration())
                .setActions(
                    PlaybackStateCompat.ACTION_PLAY or
                            PlaybackStateCompat.ACTION_PAUSE or
                            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                            PlaybackStateCompat.ACTION_SKIP_TO_NEXT or
                            PlaybackStateCompat.ACTION_PLAY_PAUSE or
                            PlaybackStateCompat.ACTION_SEEK_TO
                )
        } else if (reason == Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT) {
            exoPlayerInstance.play()
        } else if (reason == Player.DISCONTINUITY_REASON_SEEK) {
            mStateBuilder = PlaybackStateCompat.Builder()
                .setActions(
                    PlaybackStateCompat.ACTION_PLAY or
                            PlaybackStateCompat.ACTION_PAUSE or
                            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                            PlaybackStateCompat.ACTION_SKIP_TO_NEXT or
                            PlaybackStateCompat.ACTION_PLAY_PAUSE or
                            PlaybackStateCompat.ACTION_SEEK_TO
                ).setState(
                    PlaybackStateCompat.STATE_PLAYING,
                    newPosition.positionMs,
                    1f
                )
        }
        updateNotification()
    }


    /**
     * Initializes the Media Session to be enabled with media buttons, transport controls, callbacks
     * and media controller.
     */

    private fun initializeMediaSession() {

        // Create a MediaSessionCompat.
        mMediaSession = MediaSessionCompat(this, TAG)

        // Enable callbacks from MediaButtons and TransportControls.
        // noinspection deprecation

        mMediaSession?.setFlags(
            MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or
                    MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS
        )

        // Do not let MediaButtons restart the player when the app is not visible.
        mMediaSession?.setMediaButtonReceiver(null)

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player.
        mStateBuilder = PlaybackStateCompat.Builder()
            .setActions(
                PlaybackStateCompat.ACTION_PLAY or
                        PlaybackStateCompat.ACTION_PAUSE or
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT or
                        PlaybackStateCompat.ACTION_PLAY_PAUSE or
                        PlaybackStateCompat.ACTION_SEEK_TO
            )
        mMediaSession?.setPlaybackState(mStateBuilder.build())
        mMetadataBuilder = MediaMetadataCompat.Builder()

        // MySessionCallback has methods that handle callbacks from a media controller.
        mMediaSession?.setCallback(MySessionCallback())

        // Start the Media Session since the activity is active.
        mMediaSession?.isActive = true
    }

    /**
     * Initialize ExoPlayer.
     */

    private fun initializePlayer() {
        //Starting the mini player here
        if (mSongs.isNotEmpty() && currentSongIndex < mSongs.size) {
            val currentSong = mDataBundle.getParcelable(KEY_DATA_MODEL) as Song?
            currentSongIndex = mSongs.indexOf(currentSong)
            if (currentSongIndex == -1) {
                currentSongIndex = 0
            }
            EventBus.getDefault().post(currentSong)
            exoPlayerInstance = PlayerHelper(
                context = applicationContext,
                mSongs[currentSongIndex],
                this,
                mSongsList = mSongs
            )
        } else {
            val currentSong = mDataBundle.getParcelable(KEY_DATA_MODEL) as Song?
            if (currentSong != null) {
                mSongs.add(currentSong)
                currentSongIndex = 0
                exoPlayerInstance =
                    PlayerHelper(
                        context = applicationContext,
                        mSongs[currentSongIndex],
                        this,
                        mSongsList = mSongs
                    )
            }
        }
    }

    /**
     * Release ExoPlayer.
     */

    private fun releasePlayer() {
        exoPlayerInstance.releasePlayers()
    }

    private fun updateNotification() {
        if (::exoPlayerInstance.isInitialized) {
            if (exoPlayerInstance.currentDuration() != C.TIME_UNSET) {
                mMetadataBuilder?.putLong(
                    MediaMetadataCompat.METADATA_KEY_DURATION,
                    exoPlayerInstance.currentDuration()
                )
                mMediaSession?.setMetadata(mMetadataBuilder?.build())
            }
        }
        val playbackStateCompat = mStateBuilder.build()
        mMediaSession?.setPlaybackState(playbackStateCompat)
        if (mSongs.isNotEmpty()) {
            val currentSong = mSongs[currentSongIndex]
            showNotification(playbackStateCompat, currentSong)
        } else {
            val temp = Song()
            temp.songTitle = "Song Title"
            temp.artist = "Song Artist"
            showNotification(playbackStateCompat, temp)
        }
    }

    /**
     * Shows Media Style notification, with an action that depends on the current MediaSession
     * PlaybackState.
     * @param state  The PlaybackState of the MediaSession.
     * @param currentSong The Sample object to display title and composer on Notification.
     */

    @SuppressLint("InlinedApi")
    private fun showNotification(state: PlaybackStateCompat, currentSong: Song) {
        val builder = NotificationCompat.Builder(
            this,
            java.lang.String.valueOf(Constants.NOTIFICATION_CHANNEL_ID)
        )
        val icon: Int
        val playPause: String
        if (state.state == PlaybackStateCompat.STATE_PLAYING) {
            icon = R.drawable.exo_controls_pause
            playPause = getString(R.string.pause)
        } else {
            icon = R.drawable.exo_controls_play
            playPause = getString(R.string.play)
        }
        val playPauseAction = NotificationCompat.Action(
            icon, playPause,
            MediaButtonReceiver.buildMediaButtonPendingIntent(
                this,
                PlaybackStateCompat.ACTION_PLAY_PAUSE
            )
        )

        val restartAction = NotificationCompat.Action(
            R.drawable.exo_controls_previous, getString(R.string.restart),
            MediaButtonReceiver.buildMediaButtonPendingIntent(
                this,
                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
            )
        )
        val nextAction = NotificationCompat.Action(
            R.drawable.exo_controls_next, getString(R.string.next),
            MediaButtonReceiver.buildMediaButtonPendingIntent(
                this,
                PlaybackStateCompat.ACTION_SKIP_TO_NEXT
            )
        )
        // Create an Intent for the activity you want to start
        val playerIntent = Intent(this, PlayerActivity::class.java)
        val buyingHistoryIntent = Intent(this, BuyingHistoryActivity::class.java)
        buyingHistoryIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val listeningHistoryIntent = Intent(this, ListeningHistoryActivity::class.java)
        listeningHistoryIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val playListIntent = Intent(this, PlaylistActivity::class.java)
        playListIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val profileSetting = Intent(this, ProfileSettingActivity::class.java)
        profileSetting.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val mainIntent = Intent(this, MainActivity::class.java)
        val frameActivityIntent = Intent(this@MainService, FrameActivity::class.java)
        frameActivityIntent.putExtra(CommonKeys.KEY_FRAGMENT, fragmentName)
        frameActivityIntent.putExtra(KEY_DATA, FrameAvtivityArgs)
        frameActivityIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (::mDataBundle.isInitialized) {
            mDataBundle.putBoolean(KEY_IS_FROM_SERVICE, true)
            playerIntent.putExtra(KEY_DATA, mDataBundle)
        }

// Create the TaskStackBuilder
        val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(this).run {
            if (isBuyingHistoryActivity.not().and(isListeningHistoryActivity.not())
                    .and(isPlayListActiviytActivity.not()).and(isProfileSettingActivity.not())
            ) {

            } else {
                addNextIntent(mainIntent)
            }
            if (isFrameActivity) {
                addNextIntent(frameActivityIntent)
            } else {
                this.removeAll {
                    it.filterEquals(frameActivityIntent)
                }
            }
            if (isBuyingHistoryActivity) {
                addNextIntent(buyingHistoryIntent)
            } else {
                this.removeAll {
                    it.filterEquals(buyingHistoryIntent)
                }
            }
            if (isListeningHistoryActivity) {
                addNextIntent(listeningHistoryIntent)
            } else {
                this.removeAll {
                    it.filterEquals(listeningHistoryIntent)
                }
            }
            if (isPlayListActiviytActivity) {
                addNextIntent(playListIntent)
            } else {
                this.removeAll {
                    it.filterEquals(playListIntent)
                }
            }
            if (isProfileSettingActivity) {
                addNextIntent(profileSetting)
            } else {
                this.removeAll {
                    it.filterEquals(profileSetting)
                }
            }

            if (isBuyingHistoryActivity.not().and(isListeningHistoryActivity.not())
                    .and(isPlayListActiviytActivity.not()).and(isProfileSettingActivity.not())
            ) {
                addNextIntentWithParentStack(playerIntent)
            } else {
                addNextIntent(playerIntent)
            }
            // Add the intent, which inflates the back stack
            // addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(
                PENDING_INTENT_REQUEST_CODE,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        }

        val intent = Intent(this, StopServiceBroadcastReceiver::class.java)

        val pendingIntent = PendingIntent.getBroadcast(
            this.applicationContext,
            PENDING_INTENT_REQUEST_CODE,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        builder.setContentTitle(currentSong.songTitle)
            .setContentText(currentSong.artist)
            .setContentIntent(resultPendingIntent)
            .setDeleteIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_audio_song_icon)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .addAction(restartAction)
            .addAction(playPauseAction)
            .addAction(nextAction).setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mMediaSession?.sessionToken)
                    .setShowActionsInCompactView(1, 2)
            )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = java.lang.String.valueOf(Constants.NOTIFICATION_CHANNEL_ID)
            val channel = NotificationChannel(
                channelId,
                Constants.NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_LOW
            )
            channel.setShowBadge(false)
            mNotificationManager?.createNotificationChannel(channel)
            builder.setChannelId(channelId)
        }

        val notificationCompat = builder.build()
        if (state.state == PlaybackStateCompat.STATE_PAUSED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                stopForeground(STOP_FOREGROUND_DETACH)
            } else {
                stopForeground(false)
            }
            mNotificationManager?.notify(NOTIFICATION_ID, notificationCompat)
        } else {
            startForeground(NOTIFICATION_ID, notificationCompat)
        }
    }

    private fun skipToNextSong() {
        exoPlayerInstance.getAudioPlayer().seekToNextMediaItem()
        val currentSongIndex = exoPlayerInstance.getAudioPlayer().currentMediaItemIndex
        mPlayerBinder?.updateUI(mSongs[currentSongIndex])
        if (currentSongIndex != mSongs.size - 1) {
            mPlayerBinder?.showPlayerProgressBar()
            if (currentSongIndex == mSongs.size - 1 || currentSongIndex == mSongs.size - 2 || currentSongIndex == mSongs.size - 3) {
                EventBus.getDefault().post(mSongs.last().songId)
            }
        }
    }

    private fun skipToPreviousSong() {
        exoPlayerInstance.getAudioPlayer().seekToPreviousMediaItem()
        currentSongIndex = exoPlayerInstance.getAudioPlayer().currentMediaItemIndex
        mPlayerBinder?.updateUI(mSongs[currentSongIndex])
        if (currentSongIndex >= 1) {
            exoPlayerInstance.stopPlayer()
            exoPlayerInstance.releasePlayers()
            exoPlayerInstance =
                PlayerHelper(applicationContext, mSongs[currentSongIndex], this@MainService, mSongs)
            mPlayerBinder?.showPlayerProgressBar()
        }
    }

    /**
     * This class will be what is returned when an activity binds to this service.
     * The activity will also use this to know what it can get from our service to know
     * about the video playback.
     */

    inner class MainServiceBinder : Binder() {
        val currentSong: Song
            get() = mSongs[currentSongIndex]

        fun getExoPlayerInstance(): PlayerHelper {
            return if (::exoPlayerInstance.isInitialized) {
                exoPlayerInstance
            } else {
                initializePlayer()
                exoPlayerInstance
            }
        }

        fun getNextPlaySongsList(songList: List<Song>?) {
            songList?.forEach { song ->
                if (song.songStatus != SongStatus.PREMIUM) {
                    mSongs.add(song)
                }
            }
        }

        fun setPlayerBinderCallback(callback: PlayerHelper.PlayerCallBack) {
            mPlayerBinder = callback
        }

        fun getNextSongBuilder(): NextPlaySongBuilder {
            val builder = NextPlaySongBuilder()
            builder.currentSongModel = mSongs[currentSongIndex]
            builder.playedFrom = SongType.from(mDataBundle.getString(KEY_SONG_TYPE) ?: "")
            builder.songsList = mDataBundle.getParcelableArrayList(KEY_SONGS_LIST)
            builder.playListId = mDataBundle.getInt(KEY_PLAYLIST_ID)
            return builder
        }

        fun playNextSong(player: PlayerView? = null) {
            if (currentSongIndex != mSongs.size - 1) {
                skipToNextSong()
            }
        }

        fun playPreviousSong(player: PlayerView? = null) {
            skipToPreviousSong()
        }

        fun isAddBuyingHistory(isAdd: Boolean) {
            isBuyingHistoryActivity = isAdd
            updateNotification()
        }

        fun isAddListeningActivity(isAdd: Boolean) {
            isListeningHistoryActivity = isAdd
            updateNotification()
        }

        fun isAddPlayListAcvitity(isAdd: Boolean) {
            isPlayListActiviytActivity = isAdd
            updateNotification()
        }

        fun isAddProfileSettingActivity(isAdd: Boolean) {
            isProfileSettingActivity = isAdd
            updateNotification()
        }

        fun isAddFrameActivity(
            isAdd: Boolean,
            fragmentName: String?,
            args: Bundle? = Bundle.EMPTY
        ) {
            isFrameActivity = isAdd
            if (args != null) {
                FrameAvtivityArgs = args
            }
            this@MainService.fragmentName = fragmentName.toString()
            updateNotification()
        }

        fun stopService() {
            if (::exoPlayerInstance.isInitialized) {
                if (!exoPlayerInstance.isAudioPlayerPlaying()) {
                    mNotificationManager?.cancel(NOTIFICATION_ID)
                    MainStorage.getInstance(this@MainService)?.setRestartService(false)
                    stopSelf()
                }
            }
        }

        fun checkIfSongAlreadyInTheList(songBuilder: NextPlaySongBuilder) {
            if (exoPlayerInstance.getAudioPlayer().isPlaying) {
                for (i in mSongs.indices) {
                    if (mSongs[i].songId == songBuilder.currentSongModel?.songId) {
                        needToInitialize = false
                        currentSongIndex = i
                        updateNotification()
                        exoPlayerInstance.getAudioPlayer().seekTo(i, 0)
                        break
                    } else {
                        if (i == mSongs.size - 1) {
                            mSongs.clear()
                            songBuilder.songsList?.let { list ->
                                list.forEach { song ->
                                    if (song.songStatus != SongStatus.PREMIUM) {
                                        mSongs.add(song)
                                    }
                                }
                            }
                            exoPlayerInstance.getAudioPlayer().stop()
                            releasePlayer()
                            initializePlayer()
                        }
                    }
                }
            } else {
                for (i in mSongs.indices) {
                    if (mSongs[i].songId == songBuilder.currentSongModel?.songId) {
                        needToInitialize = false
                        currentSongIndex = i
                        updateNotification()
                        exoPlayerInstance.getAudioPlayer().seekTo(i, 0)
                        exoPlayerInstance.getAudioPlayer().play()
                        break
                    } else {
                        if (i == mSongs.size - 1) {
                            mSongs.clear()
                            songBuilder.songsList?.let { list ->
                                list.forEach { song ->
                                    if (song.songStatus != SongStatus.PREMIUM) {
                                        mSongs.add(song)
                                    }
                                }
                            }
                            exoPlayerInstance.getAudioPlayer().stop()
                            releasePlayer()
                            initializePlayer()
                        }
                    }
                }
            }
        }

    }

    /**
     * Media Session Callbacks, where all external clients control the player.
     */

    private inner class MySessionCallback : MediaSessionCompat.Callback() {
        override fun onPlay() {
            exoPlayerInstance.play()
        }

        override fun onPause() {
            exoPlayerInstance.paused()
        }

        override fun onSkipToPrevious() {
            skipToPreviousSong()
        }

        override fun onSkipToNext() {
            skipToNextSong()
        }

        override fun onSeekTo(pos: Long) {
            exoPlayerInstance.seekTo(pos.toInt())
        }
    }

    /**
     * Broadcast Receiver registered to receive the MEDIA_BUTTON intent coming from clients.
     */
    class MediaReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            MediaButtonReceiver.handleIntent(mMediaSession, intent)
        }
    }

    class StopServiceBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            MainStorage.getInstance(context)?.setRestartService(false)
            val stopIntent = Intent(context, MainService::class.java)
            context.stopService(stopIntent)
        }
    }

    class RestartServiceBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            MainStorage.getInstance(context)?.setRestartService(false)
            val startIntent = Intent(context, MainService::class.java)
            val passingBundle = intent.getBundleExtra(KEY_DATA)
            passingBundle?.putBoolean(KEY_IS_RESTART, true)
            startIntent.putExtra(KEY_DATA, passingBundle)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(startIntent)
            } else {
                context.startService(startIntent)
            }
        }
    }

    companion object {
        private val TAG = MainService::class.java.simpleName
        private var mMediaSession: MediaSessionCompat? = null
    }
}