package com.techswivel.qthemusic.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.os.Build
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import coil.load
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.ItemType
import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.models.PurchasedAlbum
import com.techswivel.qthemusic.models.SearchedSongs
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.*
import io.michaelrocks.libphonenumber.android.NumberParseException
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import io.michaelrocks.libphonenumber.android.Phonenumber
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


fun String.isValidPassword(): Boolean {
    val regex =
        ("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\" \"@_{}*!-/`,§±~;\\[\\]:?<>'\"\\\\|()#\$%^&+=+])(?=\\S+).{8,}\$")
    val p = Pattern.compile(regex)
    val m: Matcher = p.matcher(this)
    return m.matches()
}

fun String.isValidEmail(): Boolean {
    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    val p = Pattern.compile(emailPattern)
    val m: Matcher = p.matcher(this.trim())
    return m.matches()
}

fun EditText.isValidPhoneNumber(
    countryCode: String,
    context: Context
): Boolean {
    val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.createInstance(context)
    val isoCode =
        phoneNumberUtil.getRegionCodeForCountryCode(countryCode.toInt())
    val phoneNumber: Phonenumber.PhoneNumber?
    return try { //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
        phoneNumber = phoneNumberUtil.parse(this.editableText.toString(), isoCode)
        val isValid = phoneNumberUtil.isValidNumber(phoneNumber)
        if (isValid) {
            phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
            true
        } else {
            false
        }
    } catch (e: NumberParseException) {
        System.err.println(e)
        false
    }

}

@SuppressLint("SimpleDateFormat")
fun String.toTimeStamp(formatter: String): String {
    val sdf = SimpleDateFormat(formatter)
    val date = Date(this.toLong() * 1000)
    return sdf.format(date)
}

fun String.roundToTwoDecimal(): String {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.DOWN
    return df.format(this.toDouble())
}


fun Int.toHours(): String {
    return if (this % 60 == 0) {
        (this / 60).toString()
    } else {
        String.format("%.2f", this / 60.toDouble())
    }
}

fun Int.toMinutes(): String {
    return String.format("%.2f", (this * 60))
}

fun Double.roundToTwoDecimal(): String {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.DOWN
    return df.format(this)
}

fun Context.toDeviceName(): String {
    return Build.BRAND.plus(" ").plus(Build.MODEL)
}

@Suppress("DEPRECATION")
fun <T> Context.isServiceRunning(service: Class<T>): Boolean {
    return (getSystemService(ACTIVITY_SERVICE) as ActivityManager)
        .getRunningServices(Integer.MAX_VALUE)
        .any { it -> it.service.className == service.name }
}

@SuppressLint("HardwareIds")
fun Context.toDeviceIdentifier(): String {
    return Settings.Secure.getString(
        this.contentResolver,
        Settings.Secure.ANDROID_ID
    )
}

fun ImageView.loadImg(url: String) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    this.load(url) {
        crossfade(true)
        allowHardware(true)
        placeholder(circularProgressDrawable)
        error(R.drawable.no_image_palce_holder)
    }
}

fun EditText.showKeyboard() {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun EditText.closeKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun View.setVisibilityInMotionLayout(visibility: Int) {
    val motionLayout = parent as MotionLayout
    motionLayout.constraintSetIds.forEach {
        val constraintSet = motionLayout.getConstraintSet(it) ?: return@forEach
        constraintSet.setVisibility(this.id, visibility)
        constraintSet.applyTo(motionLayout)
    }
}

fun View.visibilityGone() {
    this.visibility = View.GONE
}

fun View.visibilityVisible() {
    this.visibility = View.VISIBLE
}

fun View.visibilityInVisible() {
    this.visibility = View.INVISIBLE
}

fun TextView.getTextLineCount(text: String?, lineCount: (Int) -> (Unit)) {
    val params: PrecomputedTextCompat.Params = TextViewCompat.getTextMetricsParams(this)
    val ref: WeakReference<TextView>? = WeakReference(this)

    GlobalScope.launch(Dispatchers.Default) {
        val text = text?.let { PrecomputedTextCompat.create(it, params) }
        GlobalScope.launch(Dispatchers.Main) {
            ref?.get()?.let { textView ->
                if (text != null) {
                    TextViewCompat.setPrecomputedText(textView, text)
                }
                lineCount.invoke(textView.lineCount)
            }
        }
    }
}

fun SearchedSongs.toAlbumModel() = Album(
    albumCoverImageUrl,
    albumId,
    AlbumStatus.valueOf(albumStatus.toString()),
    albumTitle,
    albumArtist,
    RecommendedSongsType.ALBUM,
    numberOfSongs,
    albumPrice.toString(),
    "sku"
)

fun Song.toFailedPurchased() = FailedPurchased(
    this.songId, isAcknowledge = false, isFromLogin = true, itemType = ItemType.SONG, sku = this.sku
)

fun Album.toFailedPurchased() = FailedPurchased(
    this.albumId,
    isAcknowledge = false,
    isFromLogin = true,
    itemType = ItemType.ALBUM,
    sku = this.sku
)

fun SearchedSongs.toArtistModel() = Artist(
    artistCoverImageUrl, artistId, artistName, null, null,
)

fun PurchasedAlbum.toAlbum() = Album(
    this.albumCoverImageURL,
    null,
    AlbumStatus.PURCHASED,
    this.albumTitle,
    this.songs?.first()?.artist,
    RecommendedSongsType.ALBUM,
    this.totalSongs,
    this.price.toString(),
    null
)

fun PlayerSavedState.toNextSongBuilder(): NextPlaySongBuilder {
    val nextSong = NextPlaySongBuilder()
    nextSong.currentSongId = this.currentSongId
    nextSong.albumId = this.albumId
    nextSong.currentSongModel = this.currentSongModel
    nextSong.artistId = this.artistId
    nextSong.categoryId = this.categoryId
    nextSong.currentAlbumModel = this.currentAlbumModel
    nextSong.playListId = this.playListId
    nextSong.playedFrom = this.playedFrom
    nextSong.songsList = this.songsList
    return nextSong
}