package com.techswivel.qthemusic.utils

import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.techswivel.qthemusic.BuildConfig
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.models.PurchasedAlbum
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.activities.mainActivity.MainActivity
import com.techswivel.qthemusic.ui.activities.playerActivity.PlayerActivity
import com.techswivel.qthemusic.ui.fragments.purchasedSongFragment.PurchasedSongFragment
import com.techswivel.qthemusic.ui.wrapper.FrameActivity
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA


object NotificationUtils {

    private const val TAG = "NotificationUtils"

    fun generateNotification(
        pContext: Context,
        payload: Any?,
        pTitle: String,
        pMessage: String, notificationType: String?, type: String?
    ) {
        val builder = if (payload != null) {
            val notificationIntent: PendingIntent? =
                getNotificationIntent(pContext, payload, notificationType, type)

            NotificationCompat.Builder(
                pContext,
                pContext.resources.getString(R.string.channelName)
            ).setAutoCancel(true)
                .setContentIntent(notificationIntent)
                .setContentTitle(pTitle)
                .setSmallIcon(getNotificationIcon())
                .setContentText(pMessage)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        } else {
            NotificationCompat.Builder(
                pContext,
                pContext.resources.getString(R.string.channelName)
            ).setAutoCancel(true)
                .setContentTitle(pTitle)
                .setSmallIcon(getNotificationIcon())
                .setContentText(pMessage)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        }


        // Add as notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name =
                pContext.resources.getString(R.string.channelName)
            val description = pContext.resources
                .getString(R.string.channelDescription)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(
                pContext.resources.getString(R.string.channelName),
                name,
                importance
            )
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = pContext.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
            builder.setChannelId(pContext.resources.getString(R.string.channelName))
            notificationManager.notify(0, builder.build())
        }
    }

    private fun getNotificationIcon(): Int {
        return when (BuildConfig.FLAVOR) {
            Constants.STAGING -> {
                R.mipmap.app_icon_staging
            }
            Constants.ACCEPTANCE -> {
                R.mipmap.app_icon_acceptance
            }
            Constants.DEVELOPMENT -> {
                R.mipmap.app_icon_development
            }
            else -> {
                R.mipmap.app_icon
            }
        }

    }

    fun getNotificationIntent(
        pContext: Context,
        payload: Any?,
        notificationType: String?,
        type: String?
    ): PendingIntent? {
        when (type) {
            "PRE_ORDER_SONG" -> {
                val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(pContext).run {
                    val mainIntent = Intent(pContext, MainActivity::class.java)
                    addNextIntent(mainIntent)
                    val frameActivityIntent = Intent(pContext, FrameActivity::class.java)
                    frameActivityIntent.putExtra(
                        CommonKeys.KEY_FRAGMENT,
                        PurchasedSongFragment::class.java.name
                    )
                    frameActivityIntent.putExtra(KEY_DATA, Bundle.EMPTY)
                    frameActivityIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    addNextIntent(frameActivityIntent)
                    val playerIntent = Intent(pContext, PlayerActivity::class.java)
                    val bundle = Bundle().apply {
                        putParcelable(CommonKeys.KEY_DATA_MODEL, payload as Song)
                        putParcelableArrayList(
                            CommonKeys.KEY_SONGS_LIST,
                            arrayListOf(payload)
                        )
                        putString(
                            CommonKeys.KEY_SONG_TYPE,
                            SongType.NOTIFICATION.value
                        )
                    }
                    playerIntent.putExtra(KEY_DATA, bundle)
                    playerIntent.flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    addNextIntent(playerIntent)
                    // Add the intent, which inflates the back stack
                    // addNextIntentWithParentStack(resultIntent)
                    // Get the PendingIntent containing the entire back stack
                    getPendingIntent(
                        Constants.PENDING_INTENT_REQUEST_CODE,
                        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                    )
                }
                return resultPendingIntent
            }
            "PRE_ORDER_ALBUM" -> {
                val notificationIntent = Intent(pContext, PlayerActivity::class.java)
                val bundle = Bundle().apply {
                    putParcelable(
                        CommonKeys.KEY_DATA_MODEL,
                        (payload as PurchasedAlbum).songs?.first()
                    )
                    putParcelableArrayList(
                        CommonKeys.KEY_SONGS_LIST,
                        arrayListOf(payload.songs) as ArrayList<out Song>
                    )
                    putString(
                        CommonKeys.KEY_SONG_TYPE,
                        SongType.NOTIFICATION.value
                    )
                }
                notificationIntent.putExtra(KEY_DATA, bundle)
                notificationIntent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                return PendingIntent.getActivity(
                    pContext,
                    0,
                    notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT or
                            PendingIntent.FLAG_IMMUTABLE
                )
            }
            else -> {
                val notificationIntent = Intent(pContext, MainActivity::class.java)
                notificationIntent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                return PendingIntent.getActivity(
                    pContext,
                    0,
                    notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT or
                            PendingIntent.FLAG_IMMUTABLE
                )
            }
        }
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses =
                am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo!!.packageName == context.packageName) {
                isInBackground = false
            }
        }
        return isInBackground
    }
}