package com.techswivel.qthemusic.customData.enums

enum class AdapterType {
    RECOMMENDED_FOR_YOU,
    WHATS_YOUR_MOOD,
    PLAYLIST,
    SONGS,
    FOLLOWING_ARTIST,
    ALBUM,
    TRENDING_SONGS,
    YOUR_INTERESTS,
    ARTIST,
    PRE_ORDERS,
    RECENT_PLAY,
    SEARCHED_SONGS,
    LANGUAGES,
    ALBUM_SONGS,
    RECENT_SONGS,
    RECENT_ALBUM,
    RECENT_ARTIST,
    ALBUMS,
    ARTISTS,
    PAYMENT
}