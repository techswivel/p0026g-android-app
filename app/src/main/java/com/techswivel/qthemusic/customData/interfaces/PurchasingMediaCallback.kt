package com.techswivel.qthemusic.customData.interfaces

import com.techswivel.qthemusic.models.database.Song

interface PurchasingMediaCallback {
    fun onPurchase(song: Song?, songsList: List<Song>?)
}