package com.techswivel.qthemusic.customData.enums

enum class UnlockType {
    SONG, ALBUM
}