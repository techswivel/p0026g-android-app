package com.techswivel.qthemusic.customData.enums

enum class PreOrderType(val value: String) {
    Song("Song"),
    Album("Album"),
}