package com.techswivel.qthemusic.customData.enums

enum class PurchaseType {
    GOOGLE_PAY, PAYPAL, APPLE_PAY, CARD
}