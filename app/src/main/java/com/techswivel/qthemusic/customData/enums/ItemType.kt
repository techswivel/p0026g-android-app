package com.techswivel.qthemusic.customData.enums

enum class ItemType {
    SONG,
    ALBUM,
    ARTIST,
    PLAN_SUBSCRIPTION,
    PRE_ORDER,
    PRE_ORDER_SONG,
    PRE_ORDER_ALBUM
}