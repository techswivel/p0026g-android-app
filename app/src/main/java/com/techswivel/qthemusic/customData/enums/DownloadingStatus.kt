package com.techswivel.qthemusic.customData.enums

enum class DownloadingStatus {
    FAILED, PAUSED, PENDING, RUNNING, SUCCESS, INSTALLING, CANCEL, UN_CONFIGURABLE
}