package com.techswivel.qthemusic.customData.enums

enum class RecommendedSongsType {
    SONGS,
    ALBUM,
    ARTIST,
    ARTIST_SONGS,
    PURCHASED_ALBUM,
    ARTIST_ALBUM,
    RECOMMENDED,
    HISTORY,
    CATEGORY,
}