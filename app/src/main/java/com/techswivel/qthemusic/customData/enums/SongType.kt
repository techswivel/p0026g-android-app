package com.techswivel.qthemusic.customData.enums

enum class SongType(val value: String) {
    ARTIST_ALBUM("ARTIST_ALBUM"),
    TRENDING("TRENDING"),
    PLAY_LIST("PLAYLIST"),
    PLAYLIST("PLAYLIST"),
    CATEGORY("CATEGORY"),
    FAVOURITES("FAVOURITES"),
    FAVOURITE("FAVOURITE"),
    PURCHASED("PURCHASED"),
    PURCHASED_SONG("PURCHASED_SONG"),
    PURCHASED_ALBUM("PURCHASED_ALBUM"),
    ALBUM("ALBUM"),
    DOWNLOADED("DOWNLOADED"),
    RECOMMENDED("RECOMMENDED"),
    SEARCHED("SEARCHED"),
    AUDIO("Audio"),
    HISTORY("History"),
    EMPTY("EMPTY"),
    NOTIFICATION("Notification"),
    VIDEO("Video"),
    ARTIST("ARTIST");

    companion object {
        fun from(findValue: String): SongType = values().first { it.value == findValue }
    }
}