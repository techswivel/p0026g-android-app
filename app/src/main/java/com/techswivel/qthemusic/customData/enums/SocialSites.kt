package com.techswivel.qthemusic.customData.enums

enum class SocialSites {
    FACEBOOK,
    GMAIL,
    GOOGLE,
    APPLE,
    INSTAGRAM
}