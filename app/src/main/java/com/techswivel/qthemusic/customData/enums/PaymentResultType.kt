package com.techswivel.qthemusic.customData.enums

enum class PaymentResultType {
    SUCCESS, FAILED
}