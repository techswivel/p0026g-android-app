package com.techswivel.qthemusic.source.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.techswivel.qthemusic.constant.Constants.APP_DATABASE_NAME
import com.techswivel.qthemusic.models.ListeningHistory
import com.techswivel.qthemusic.models.database.*
import com.techswivel.qthemusic.source.local.database.converter.AlbumConverter
import com.techswivel.qthemusic.source.local.database.converter.DownloadStatusConverter
import com.techswivel.qthemusic.source.local.database.converter.SongConverter
import com.techswivel.qthemusic.source.local.database.converter.SongListConverter
import com.techswivel.qthemusic.source.local.database.databaseDao.*

@Database(
    entities = [DownloadIds::class, Song::class, Album::class, Artist::class, Category::class, ListeningHistory::class, FailedPurchased::class, PlayerSavedState::class],
    version = 18,
    exportSchema = true,
//    autoMigrations = [
//        AutoMigration(
//            from = 1, to = 2
//        ),
//        AutoMigration(
//            from = 2, to = 3
//        )
//    ]
)
@TypeConverters(
    DownloadStatusConverter::class, SongConverter::class, SongListConverter::class,
    AlbumConverter::class
)
abstract class AppRoomDatabase : RoomDatabase() {

    abstract fun downloadDao(): DownloadDao
    abstract fun mSongsDao(): SongsDao
    abstract fun mAlbumDao(): AlbumDao
    abstract fun mArtistDao(): ArtistDao
    abstract fun mCategoriesDao(): CategoryDao
    abstract fun mSyncDao(): SyncDao
    abstract fun mSubscriptionDao(): SubscriptionDao
    abstract fun savedDao(): SavedStateDao

    companion object {
        private var INSTANCE: RoomDatabase? = null

        fun getDatabaseInstance(context: Context): AppRoomDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    AppRoomDatabase::class.java,
                    APP_DATABASE_NAME
                )
                    .fallbackToDestructiveMigration() // only temporary
                    .build()
            }
            return INSTANCE as AppRoomDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}