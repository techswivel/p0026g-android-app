package com.techswivel.qthemusic.source.local.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.techswivel.qthemusic.models.database.Song

class SongListConverter {
    @TypeConverter
    fun fromBooks(obj: MutableList<Song>?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<MutableList<Song>>() {

        }.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toBooksObj(songString: String?): MutableList<Song>? {
        if (songString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<MutableList<Song>>() {

        }.type
        return gson.fromJson<MutableList<Song>>(songString, type)
    }
}