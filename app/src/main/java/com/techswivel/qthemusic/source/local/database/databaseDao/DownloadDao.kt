package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.models.database.DownloadIds
import com.techswivel.qthemusic.source.local.database.BaseDao

@Dao
abstract class DownloadDao : BaseDao() {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveDownloadId(ids: DownloadIds)

    @Query("UPDATE downloadIds set downloadStatus=:status, reason=:reasonMessage where downloadId = :downloadId")
    abstract suspend fun updateStatus(
        status: DownloadingStatus,
        downloadId: Long,
        reasonMessage: String
    )

    @Query("SELECT EXISTS(SELECT * FROM downloadIds WHERE downloadId = :id)")
    abstract suspend fun isDownloadIdExist(id: Long): Boolean

    @Query("SELECT EXISTS(SELECT * FROM downloadIds WHERE fileId = :translationId)")
    abstract suspend fun isDownloadIdExist(translationId: Int): Boolean

    @Query("Select * from downloadIds")
    abstract fun getAllRunningDownloads(): LiveData<List<DownloadIds>>

    @Query("Select * from downloadIds")
    abstract suspend fun getRunningDownloads(): List<DownloadIds>

    @Query("SELECT * from downloadIds where downloadId = :id")
    abstract fun getDownloadFile(id: Long): DownloadIds

    @Query("SELECT * from downloadIds where fileId = :translationId")
    abstract suspend fun getDownloadByTranslationId(translationId: Int): DownloadIds

    @Delete
    abstract suspend fun deleteRecord(ids: DownloadIds)

    @Transaction
    open suspend fun deleteIfFailed(translationId: Int) {
        if (isDownloadIdExist(translationId)) {
            val download = getDownloadByTranslationId(translationId)
            if (download.downloadStatus == DownloadingStatus.FAILED) {
                deleteRecord(download)
            }
        }
    }
}