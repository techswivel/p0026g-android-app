package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.local.database.BaseDao

@Dao
abstract class SongsDao : BaseDao() {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveSong(song: Song)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveSongs(songs: List<Song>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertSong(song: Song)

    @Query("SELECT * FROM Song ORDER BY recentPlay DESC LIMIT 5")
    abstract fun getRecentPlayedSongs(): LiveData<List<Song>>

    @Query("SELECT * FROM Song ORDER BY recentPlay DESC")
    abstract fun getAllSongs(): LiveData<List<Song>>

    @Query("SELECT * FROM Song WHERE songStatus == 'DOWNLOADED'")
    abstract suspend fun getDownloadedSongs(): List<Song>

    @Query("delete from song")
    abstract suspend fun deleteAllData()

    @Query("delete from song where songId = :songId")
    abstract suspend fun deleteSongById(songId: Int?)
}