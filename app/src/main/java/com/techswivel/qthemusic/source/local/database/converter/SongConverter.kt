package com.techswivel.qthemusic.source.local.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.techswivel.qthemusic.models.database.Song

class SongConverter {
    @TypeConverter
    fun fromBooks(obj: Song?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Song>() {

        }.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toBooksObj(songString: String?): Song? {
        if (songString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Song>() {

        }.type
        return gson.fromJson<Song>(songString, type)
    }
}