package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techswivel.qthemusic.models.ListeningHistory
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song

@Dao
abstract class SyncDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertListeningHistory(listeningHistory: ListeningHistory)

    @Query("SELECT * FROM ListeningHistory")
    abstract suspend fun getListeningHistory(): List<ListeningHistory>

    @Query("SELECT * FROM ListeningHistory ORDER BY id DESC LIMIT 5")
    abstract suspend fun getTopFiveRecentlyPlayedSongs(): List<ListeningHistory>

    @Query("DELETE FROM ListeningHistory")
    abstract suspend fun deleteAllListeningHistory()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertSyncedSongsList(songsList: List<Song>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertSyncedArtistsList(artistsList: List<Artist>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertSyncedAlbumsList(albumsList: List<Album>)
}