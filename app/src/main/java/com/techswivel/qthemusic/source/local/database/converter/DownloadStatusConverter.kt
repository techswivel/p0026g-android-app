package com.techswivel.qthemusic.source.local.database.converter

import androidx.room.TypeConverter
import com.techswivel.qthemusic.customData.enums.DownloadingStatus

class DownloadStatusConverter {
    @TypeConverter
    fun fromNoteType(status: DownloadingStatus) = status.name

    @TypeConverter
    fun toNoteType(value: String) = enumValueOf<DownloadingStatus>(value)
}