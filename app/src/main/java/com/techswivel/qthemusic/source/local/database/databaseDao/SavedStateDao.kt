package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.techswivel.qthemusic.models.database.PlayerSavedState
import com.techswivel.qthemusic.source.local.database.BaseDao

@Dao
abstract class SavedStateDao : BaseDao() {

    @Query("select * from playersavedstate")
    abstract suspend fun getCurrentState(): PlayerSavedState

    @Query("delete from PlayerSavedState")
    abstract suspend fun deleteAllData()

    @Insert
    abstract suspend fun savedData(state: PlayerSavedState)

    @Transaction
    open suspend fun savedState(data: PlayerSavedState) {
        deleteAllData()
        savedData(data)
    }
}