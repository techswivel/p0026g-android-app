package com.techswivel.qthemusic.source.remote.firebase

import com.example.example.NotificationMainDataModel
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.models.SubscriptionPlans
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.NotificationUtils
import org.greenrobot.eventbus.EventBus


class FirebaseNotificationService : FirebaseMessagingService() {
    private val TAG = "FirebaseNotificationService"

    override fun onMessageReceived(p0: RemoteMessage) {
        Log.e(TAG, "service called")
        super.onMessageReceived(p0)
        Log.e(TAG, "Data Payload: " + p0.data.toString())

        p0.notification?.let {
            Log.e(TAG, "Message Notification Body: ${it.body}")
            Log.e(TAG, "Message Notification Title : ${it.title}")
        }

        if (p0.data["notificationType"] == null) {
            val replaceString = p0.data.toString().replaceFirst("=", ":")
            val data =
                convertJsonStringToObject(replaceString, NotificationMainDataModel::class.java)
            if (data.notificationDataClass?.type == "PRE_ORDER_SONG") {
                val preOrderSongData = convertJsonStringToObject(
                    data.notificationDataClass?.notification ?: "",
                    Song::class.java
                )
                NotificationUtils.generateNotification(
                    this,
                    preOrderSongData,
                    p0.notification?.title.toString(),
                    p0.notification?.body.toString(),
                    data.notificationDataClass?.notificationType,
                    data.notificationDataClass?.type
                )
            } else {
                val preOrderAlbumData = data.notificationDataClass?.purchasedAlbum
                NotificationUtils.generateNotification(
                    this,
                    preOrderAlbumData,
                    p0.notification?.title.toString(),
                    p0.notification?.body.toString(),
                    data.notificationDataClass?.notificationType,
                    data.notificationDataClass?.type
                )
            }
            EventBus.getDefault().post(data)
        } else {
            when (p0.data["notificationType"]) {
                "PLAN_SUBSCRIPTION" -> {
                    val data = convertJsonStringToObject(
                        p0.data["notification"].toString(),
                        SubscriptionPlans::class.java
                    )
                    saveSubscriptionData(data)
                    NotificationUtils.generateNotification(
                        this,
                        null,
                        p0.notification?.title.toString(),
                        p0.notification?.body.toString(),
                        p0.data["notificationType"],
                        p0.data["type"]
                    )
                }
                "SUBSCRIPTION_CANCELED" -> {
                    removeSubscription()
                    NotificationUtils.generateNotification(
                        this,
                        null,
                        p0.notification?.title.toString(),
                        p0.notification?.body.toString(),
                        p0.data["notificationType"],
                        p0.data["type"]
                    )
                }
                "SUBSCRIPTION_PURCHASED" -> {
                    val data = convertJsonStringToObject(
                        p0.data["notification"].toString(),
                        SubscriptionPlans::class.java
                    )
                    saveSubscriptionData(data)
                    NotificationUtils.generateNotification(
                        this,
                        null,
                        p0.notification?.title.toString(),
                        p0.notification?.body.toString(),
                        p0.data["notificationType"],
                        p0.data["type"]
                    )
                }
                else -> {
                    NotificationUtils.generateNotification(
                        this,
                        null,
                        p0.notification?.title.toString(),
                        p0.notification?.body.toString(),
                        p0.data["notificationType"],
                        p0.data["type"]
                    )
                }
            }
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.e(TAG, "new token is $p0")
    }

    private fun removeSubscription() {
        QTheMusicApplication.unSubscribeToTopic(
            PrefUtils.getString(
                this, CommonKeys.KEY_USER_PLAN_TOPIC
            ) ?: ""
        )

        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_ID
        )
        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_TITLE
        )
        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_PRIZE
        )
        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_DURATION
        )
        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_TOPIC
        )
        PrefUtils.removeValue(
            this, CommonKeys.KEY_USER_PLAN_SKU
        )
    }

    private fun saveSubscriptionData(data: SubscriptionPlans) {
        QTheMusicApplication.subscribeToTopic(data.planTopic ?: "")

        PrefUtils.setString(
            QTheMusicApplication.getContext(),
            CommonKeys.KEY_USER_PLAN_TITLE,
            data.planTitle
        )
        PrefUtils.setString(
            QTheMusicApplication.getContext(),
            CommonKeys.KEY_USER_PLAN_DURATION,
            data.planDuration
        )
        data.planTopic?.let { planTopic ->
            PrefUtils.setString(
                QTheMusicApplication.getContext(), CommonKeys.KEY_USER_PLAN_TOPIC,
                planTopic
            )
        }

        data.sku?.let { sku ->
            PrefUtils.setString(
                QTheMusicApplication.getContext(), CommonKeys.KEY_USER_PLAN_SKU,
                sku
            )
        }
        data.planId?.let { planId ->
            PrefUtils.setInt(
                QTheMusicApplication.getContext(), CommonKeys.KEY_USER_PLAN_ID,
                planId
            )
        }
        data.planPrice?.let { planPrice ->
            PrefUtils.setString(
                QTheMusicApplication.getContext(), CommonKeys.KEY_USER_PLAN_PRIZE,
                planPrice
            )
        }
    }

    private fun <T> convertJsonStringToObject(
        jsonString: String,
        clazz: Class<T>
    ): T =
        Gson().fromJson(jsonString, clazz)
}