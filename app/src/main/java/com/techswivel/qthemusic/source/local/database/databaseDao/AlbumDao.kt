package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techswivel.qthemusic.models.database.Album

@Dao
abstract class AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAlbum(album: Album)

    @Query("SELECT * FROM album ORDER BY recentPlay DESC")
    abstract fun getAlbumList(): LiveData<List<Album>>

    @Query("SELECT * FROM Album ORDER BY recentPlay DESC LIMIT 5")
    abstract fun getRecentlyPlayedAlbums(): LiveData<List<Album>>

    @Query("delete from album")
    abstract suspend fun deleteAllAlbum()
}