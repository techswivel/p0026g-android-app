package com.techswivel.qthemusic.source.remote.networkViewModel

import androidx.lifecycle.MutableLiveData
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.dataManager.RemoteDataManager
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.models.database.UpdatePlayListModel
import com.techswivel.qthemusic.source.remote.rxjava.CustomError
import com.techswivel.qthemusic.source.remote.rxjava.CustomObserver
import com.techswivel.qthemusic.source.remote.rxjava.ErrorUtils
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.utils.Log
import retrofit2.Response

class SongAndArtistsViewModel : BaseViewModel() {
    var songlistResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var followingArtistResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var artistFollowResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var deleteSongRespomse: MutableLiveData<ApiResponse> = MutableLiveData()
    private var mRecommendedSongsResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var preOrdersResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var mSearchedSongResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var mArtistDetailsResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var mArtistFollowingResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var mSyncedRecentlyPlayed: MutableLiveData<ApiResponse> = MutableLiveData()
    var recommendedSongsResponse: MutableLiveData<ApiResponse>
        get() = mRecommendedSongsResponse
        set(value) {
            mRecommendedSongsResponse = value
        }
    private val TAG = "SongAndArtistsViewModel"
    private var mCategoriesResponse: MutableLiveData<ApiResponse> = MutableLiveData()

    var categoriesResponse: MutableLiveData<ApiResponse>
        get() = mCategoriesResponse
        set(value) {
            mCategoriesResponse = value
        }

    private var mAddSongsToDownloadResponse: MutableLiveData<ApiResponse> = MutableLiveData()

    var addSongToDownload: MutableLiveData<ApiResponse>
        get() = mAddSongsToDownloadResponse
        set(value) {
            mAddSongsToDownloadResponse = value
        }

    private var mNextSongsResponse: MutableLiveData<ApiResponse> = MutableLiveData()

    var nextSongsResponse: MutableLiveData<ApiResponse>
        get() = mNextSongsResponse
        set(value) {
            mNextSongsResponse = value
        }


    fun startDownloadingSong(song: Song, type: SongType) {
        mRemoteDataManager.startDownloadingSong(song, type)
    }

    fun addOrRemoveSongToDownload(actionType: ActionType, song: Song, songType: SongType?) {
        RemoteDataManager.addOrRemoveSongToDownload(actionType, song, songType).doAfterNext {
            if (it.isSuccessful) {
                if ((songType != null) && (actionType == ActionType.ADD)) {
                    mRemoteDataManager.startDownloadingSong(song, songType)
                }
            }
        }.doOnSubscribe {
            mAddSongsToDownloadResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mAddSongsToDownloadResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mAddSongsToDownloadResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mAddSongsToDownloadResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mAddSongsToDownloadResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mAddSongsToDownloadResponse.value = ApiResponse.complete()
            }
        })
    }

    fun getRecommendedSongsDataFromServer(recommendedSongsBodyModel: RecommendedSongsBodyModel) {
        mRemoteDataManager.getRecommendedSongsData(recommendedSongsBodyModel).doOnSubscribe {
            mRecommendedSongsResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mRecommendedSongsResponse.value = ApiResponse.success(t.body()?.response)
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mRecommendedSongsResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mRecommendedSongsResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mRecommendedSongsResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mRecommendedSongsResponse.postValue(ApiResponse.complete())
            }
        })
    }

    fun getCategoriesDataFromServer(categoryType: CategoryType) {
        mRemoteDataManager.getCategoriesData(categoryType).doOnSubscribe {
            mCategoriesResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mCategoriesResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mCategoriesResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mCategoriesResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mCategoriesResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mCategoriesResponse.value = ApiResponse.complete()
            }
        })
    }

    fun getNextSongsData(nextSongParameters: NextPlaySong) {
        mRemoteDataManager.loadNextSongs(nextSongParameters).doOnSubscribe {
            mNextSongsResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mNextSongsResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mNextSongsResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mNextSongsResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mNextSongsResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mNextSongsResponse.value = ApiResponse.complete()
            }
        })
    }

    fun getFollowingArtist() {
        mRemoteDataManager.getFollowingArtist().doOnSubscribe {
            followingArtistResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        followingArtistResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        followingArtistResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        followingArtistResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                followingArtistResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                followingArtistResponse.value = ApiResponse.complete()
            }
        })
    }
    fun getSongs(songsBodyModel: SongsBodyModel) {
        mRemoteDataManager.getSongsFromServer(songsBodyModel).doOnSubscribe {
            songlistResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        songlistResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        songlistResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        songlistResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                songlistResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                songlistResponse.value = ApiResponse.complete()
            }
        })
    }

    fun updatePlayList(updatePlayListModel: UpdatePlayListModel) {
        mRemoteDataManager.updatePlayList(updatePlayListModel).doOnSubscribe {
            deleteSongRespomse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        deleteSongRespomse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        deleteSongRespomse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        deleteSongRespomse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                deleteSongRespomse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                deleteSongRespomse.value = ApiResponse.complete()
            }
        })
    }
    fun getPreOrdersList(artistId: Int?) {
        mRemoteDataManager.getPreOrders(artistId).doOnSubscribe {
            preOrdersResponse.value = ApiResponse.loading()

        }.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        preOrdersResponse.postValue(
                            ApiResponse.success(t.body()?.response)
                        )
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        preOrdersResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        preOrdersResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                preOrdersResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {

            }

        })
    }

    fun getSearchedSongsFromServer(queryRequestModel: QueryRequestModel) {
        mRemoteDataManager.getSearcherSongsList(queryRequestModel).doOnSubscribe {
            mSearchedSongResponse.value = ApiResponse.loading()
        }.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        Log.d(TAG, "success called")
                        mSearchedSongResponse.value = ApiResponse.success(t.body()?.response)
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mSearchedSongResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {

                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        Log.d(TAG, "success else called ${error?.response?.message}")
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mSearchedSongResponse.value = ApiResponse.error(errorData)
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                Log.d(TAG, "onError called")
                mSearchedSongResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {

            }
        })

    }

    fun getArtistDataFromServer(artistId: Int?) {
        mRemoteDataManager.getArtistDetails(artistId).doOnSubscribe {
            mArtistDetailsResponse.value = ApiResponse.loading()
        }.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mArtistDetailsResponse.value =
                            ApiResponse.success(t.body()?.response)
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mArtistDetailsResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain = ErrorUtils.parseError(t)
                        mArtistDetailsResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error.response?.status ?: false,
                                error.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mArtistDetailsResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {

            }
        })

    }

    fun artistFollowingStatus(artistModel: ArtistModel) {
        mRemoteDataManager.getArtistFollowingDetails(artistModel).doOnSubscribe {
            mArtistFollowingResponse.value = ApiResponse.loading()
        }.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mArtistFollowingResponse.value =
                            ApiResponse.success(t.body()?.response)
                    }
                    t.code() == 401 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mArtistFollowingResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain = ErrorUtils.parseError(t)
                        mArtistFollowingResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error.response?.status ?: false,
                                error.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mArtistFollowingResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {

            }
        })

    }
}