package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techswivel.qthemusic.models.database.Artist

@Dao
abstract class ArtistDao {
    @Query("SELECT * FROM Artist ORDER BY recentPlay DESC")
    abstract fun getArtistList(): LiveData<List<Artist>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertArtist(artist: Artist)

    @Query("SELECT * FROM Artist ORDER BY recentPlay DESC LIMIT 5")
    abstract fun getRecentlyPlayedArtists(): LiveData<List<Artist>>

    @Query("delete from Artist")
    abstract suspend fun deleteAllArtists()

}