package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.source.local.database.BaseDao

@Dao
abstract class SubscriptionDao : BaseDao() {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun savedFiledPurchase(purchasedItem: FailedPurchased)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun savedFiledPurchase(purchasedItem: List<FailedPurchased>)

    @Delete
    abstract suspend fun deleteFailedPurchase(purchasedItem: FailedPurchased)

    @Delete
    abstract suspend fun deleteFailedPurchase(purchasedItem: List<FailedPurchased>)

    @Query("delete from FailedPurchased")
    abstract suspend fun deleteAllFailedPurchase()

    @Query("select * from FailedPurchased")
    abstract fun getAllFailedSubscription(): LiveData<List<FailedPurchased>>

    @Query("select * from FailedPurchased where sku == :sku")
    abstract suspend fun getFailedSubscription(sku: String): List<FailedPurchased>

    @Query("select * from FailedPurchased where isFromLogin == 1 and isAcknowledge == 0")
    abstract suspend fun getAllPurchased(): List<FailedPurchased>
}