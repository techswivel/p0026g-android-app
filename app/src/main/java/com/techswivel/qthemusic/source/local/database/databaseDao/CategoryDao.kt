package com.techswivel.qthemusic.source.local.database.databaseDao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techswivel.qthemusic.models.database.Category

@Dao
abstract class CategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertCategoriesList(categoryList: List<Category>)

    @Query("Select * from Category")
    abstract fun getCategoriesList(): LiveData<List<Category>>
}