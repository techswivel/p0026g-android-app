package com.techswivel.qthemusic.source.remote.retrofit

import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.database.UpdatePlayListModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*


interface ApiResponse {
    @POST("token")
    fun getGoogleToken(@Body authModel: GoogleAuthModel): Observable<Response<GoogleResponseModel>>

    @POST("user/login")
    fun userLogin(@Body authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>

    @POST("user/send-otp")
    fun sendOtp(@Body authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>

    @POST("user/verify-otp")
    fun verifyOtpRequest(@Body authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>

    @PUT("user/reset")
    fun setNewPassword(@Body authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>

    @Multipart
    @POST("user/signup")
    fun signUp(
        @Part profile: MultipartBody.Part?,
        @Part("name") name: okhttp3.RequestBody?,
        @Part("email") email: okhttp3.RequestBody?,
        @Part("dOb") dob: Int?,
        @Part("gender") gender: okhttp3.RequestBody?,
        @Part("password") password: okhttp3.RequestBody?,
        @Part("fcmToken") fcmToken: okhttp3.RequestBody?,
        @Part("deviceIdentifier") deviceIdentifier: okhttp3.RequestBody?,
        @Part("completeAddress") completeAddress: okhttp3.RequestBody?,
        @Part("city") city: okhttp3.RequestBody?,
        @Part("state") state: okhttp3.RequestBody?,
        @Part("country") country: okhttp3.RequestBody?,
        @Part("zipcode") zipcode: Int?,
        @Part("deviceName") deviceName: okhttp3.RequestBody?,
        @Part("socialId") socialId: okhttp3.RequestBody?,
        @Part("socialSite") socialSite: okhttp3.RequestBody?,
    ): Observable<Response<ResponseMain>>

    @GET("songs/categories")
    fun getCategories(
        @Query("page") page: Int,
        @Query("limit") limit: Int, @Query("categoryType") categoryType: CategoryType
    ): Observable<Response<ResponseMain>>

    @POST("user/save-interest")
    fun saveInterest(@Body category: Interests): Observable<Response<ResponseMain>>

    @DELETE("user/logout")
    fun logoutUser(@Query("deviceIdentifier") deviceIdentifier: String): Observable<Response<ResponseMain>>

    @POST("songs/set-favourite-song")
    fun setFavoriteSong(@Body favoriteSongBody: FavoriteSongBody): Observable<Response<ResponseMain>>

    @POST("songs/recommended-songs")
    fun getRecommendedSongsData(@Body recommendedSongsBodyModel: RecommendedSongsBodyModel): Observable<Response<ResponseMain>>

    @POST("songs")
    fun getSongsFromServer(
        @Query("page") page: Int,
        @Query("limit") limit: Int, @Body songsBodyModel: SongsBodyModel
    ): Observable<Response<ResponseMain>>

    @POST("songs/next-play-song-list")
    fun loadNextSongs(@Body nextSongParameters: NextPlaySong): Observable<Response<ResponseMain>>

    @POST("songs/download")
    fun addOrRemoveSongToDownload(@Body updateDownloadListBody: UpdateDownloadListBody): Observable<Response<ResponseMain>>

    @GET("artist/artist-detail")
    fun getArtistDetail(
        @Query("artistId") artistId: Int?,
        @Query("page") page: Int?,
        @Query("limit") limit: Int?,
    ): Observable<Response<ResponseMain>>

    @POST("artist/artist-follow")
    fun artistFollowUnFollowRequest(
        @Body artistModel: ArtistModel
    ): Observable<Response<ResponseMain>>

    @GET("card/pre-order-detail")
    fun getPreOrdersDetail(
        @Query("artistId") artistId: Int?,
        @Query("page") page: Int?,
        @Query("limit") limit: Int?,
    ): Observable<Response<ResponseMain>>


    @POST("songs/search")
    fun searchSongs(
        @Body queryRequestModel: QueryRequestModel,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
    ): Observable<Response<ResponseMain>>

    @POST("songs/sync-recently-played")
    fun syncedRecentlyPlayed(
        @Body listeningHistory: SyncHistoryModel,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
    ): Observable<Response<ResponseMain>>

    @POST("card/package-plans")
    fun getSubscriptionPlans(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Observable<Response<ResponseMain>>

    @POST("card/subscribe-to-plan")
    fun subscribeToPlan(
        @Body subscribeToPlanBody: SubscribeToPlanBody
    ): Observable<Response<ResponseMain>>

    @GET("user/playlist")
    fun getPlayList(
        @Query("page") page: Int,
        @Query("limit") limit: Int,
    ): Observable<Response<ResponseMain>>

    @POST("user/save-playlist")
    fun savePlayList(
        @Body playlistModel: PlaylistModel
    ): Observable<Response<ResponseMain>>

    @POST("songs/update-playlist")
    fun updatePlayList(
        @Body updatePlayListModel: UpdatePlayListModel
    ): Observable<Response<ResponseMain>>

    @DELETE("user/delete-playlist/{playlistId}")
    fun deletePlayList(
        @Path(value = "playlistId", encoded = true) playlistId: Int?
    ): Observable<Response<ResponseMain>>

    @GET("artist/get-following-artist")
    fun getFollowingArtistList(
        @Query("page") page: Int,
        @Query("limit") limit: Int,
    ): Observable<Response<ResponseMain>>

    @GET("user/buying-history")
    fun getBuyingHistory(
        @Query("typeOfTransection") typeOfTransection: String?,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
    ): Observable<Response<ResponseMain>>

    @Multipart
    @POST("user/update-profile")
    fun updateProfile(
        @Part profile: MultipartBody.Part?,
        @Part("name") name: okhttp3.RequestBody?,
        @Part("email") email: okhttp3.RequestBody?,
        @Part("dOb") dob: Int?,
        @Part("gender") gender: okhttp3.RequestBody?,
        @Part("completeAddress") completeAddress: okhttp3.RequestBody?,
        @Part("city") city: okhttp3.RequestBody?,
        @Part("state") state: okhttp3.RequestBody?,
        @Part("country") country: okhttp3.RequestBody?,
        @Part("zipCode") zipcode: Int?,
        @Part("isEnableNotification") isEnableNotification: Boolean?,
        @Part("isArtistUpdateEnable") isArtistUpdateEnable: Boolean?,
        @Part("phoneNumber") phoneNumber: okhttp3.RequestBody?,
        @Part("otp") otp: Int?,
    ): Observable<Response<ResponseMain>>


}

