package com.techswivel.qthemusic.source.remote.networkViewModel

import android.app.Activity
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.billingclient.api.*
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.PaymentResultType
import com.techswivel.qthemusic.models.ApiResponse
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseMain
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.rxjava.CustomError
import com.techswivel.qthemusic.source.remote.rxjava.CustomObserver
import com.techswivel.qthemusic.source.remote.rxjava.ErrorUtils
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.ui.dialogFragments.paymentResultDialogFragment.PaymentResultDialogFragment
import com.techswivel.qthemusic.utils.CommonKeys
import kotlinx.coroutines.launch
import retrofit2.Response


class InAppPurchasesViewModel : BaseViewModel() {

    var billingClient: BillingClient? = null
    var purchaseToken: MutableLiveData<Purchase> = MutableLiveData()
    var isCalled = MutableLiveData<Boolean>()
    private var mSubscriptionPlansResponse: MutableLiveData<ApiResponse> = MutableLiveData()
    var subscriptionPlansResponse: MutableLiveData<ApiResponse>
        get() = mSubscriptionPlansResponse
        set(value) {
            mSubscriptionPlansResponse = value
        }

    private var mSubscribeToPlanResponse: MutableLiveData<ApiResponse> = MutableLiveData()

    var subscribeToPlanResponse: MutableLiveData<ApiResponse>
        get() = mSubscribeToPlanResponse
        set(value) {
            mSubscribeToPlanResponse = value
        }

    fun setUpBillingClient(activity: Activity, dialogFragment: DialogFragment?) {
        val fragmentActivity = activity as? FragmentActivity
        billingClient = BillingClient.newBuilder(activity)
            .enablePendingPurchases()
            .setListener { billingResult, list ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK &&
                    list != null
                ) {
                    for (purchase in list) {
                        if (purchase.purchaseState == Purchase.PurchaseState.PENDING &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(
                                TAG,
                                "setUpBillingClient: Payment is pending."
                            )

                            updateFailedPurchased(purchase.skus.first())

                            purchaseToken.postValue(purchase)

                        } else if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(
                                TAG,
                                "setUpBillingClient: Item purchased but not acknowledged"
                            )

                            updateFailedPurchased(purchase.skus.first())

                            purchaseToken.postValue(purchase)

                            Log.d(
                                TAG,
                                "Purchase Token: ${purchase.purchaseToken}"
                            )

                        } else if (purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(
                                TAG,
                                "setUpBillingClient: Unspecified State."
                            )

                        }
                    }
                } else if (billingResult.responseCode == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.DEVELOPER_ERROR ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.ERROR ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_NOT_OWNED ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_UNAVAILABLE ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.SERVICE_DISCONNECTED ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.SERVICE_TIMEOUT ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE ||
                    billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED
                ) {
                    dialogFragment?.dismiss()
                    val paymentResultDialogFragment =
                        PaymentResultDialogFragment.newInstance(
                            PaymentResultType.FAILED
                        )
                    if (fragmentActivity?.supportFragmentManager?.isDestroyed?.not() == true) {
                        val result = kotlin.runCatching {
                            paymentResultDialogFragment.show(
                                fragmentActivity.supportFragmentManager,
                                PaymentResultDialogFragment::class.java.simpleName
                            )
                        }
                    }
                }
            }
            .build()
    }

    fun connectToGooglePlayBilling(productId: String, activity: Activity, skuType: String) {
        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    val productIds: MutableList<String> = ArrayList()
                    productIds.add(productId)
                    Log.d(TAG, "In connectToGooglePlayBilling productId is $productId")
                    val skuDetailsParams = SkuDetailsParams
                        .newBuilder()
                        .setSkusList(productIds)
                        .setType(skuType)
                        .build()
                    billingClient?.querySkuDetailsAsync(
                        skuDetailsParams
                    ) { result, list ->
                        Log.d(
                            TAG,
                            "In connectToGooglePlayBilling list size is ${list?.size} and code is ${result.responseCode} and debug message is ${result.debugMessage}"
                        )
                        if ((result.responseCode == BillingClient.BillingResponseCode.OK) &&
                            (list?.isNotEmpty() == true)
                        ) {
                            val productInfo = list[0]

                            billingClient?.launchBillingFlow(
                                activity,
                                BillingFlowParams
                                    .newBuilder()
                                    .setSkuDetails(productInfo as SkuDetails)
                                    .build()
                            )
                        } else {
                            isCalled.postValue(false)
                            var count = PrefUtils.getInt(activity, CommonKeys.INCREAMENT_VALUE)
                            count++
                            PrefUtils.setInt(activity, CommonKeys.INCREAMENT_VALUE, count)
                            var countNew = PrefUtils.getInt(activity, CommonKeys.INCREAMENT_VALUE)
                            Log.d(TAG, "New Count is $countNew")
                            Log.e(TAG, "onBillingSetupFinished: ${result.debugMessage}")
                        }
                    }
                }
            }

            override fun onBillingServiceDisconnected() {
                connectToGooglePlayBilling(productId, activity, skuType)
            }
        })
    }

    fun checkUnAcknowledgedPurchases(activity: Activity) {
        if (billingClient?.connectionState == BillingClient.ConnectionState.DISCONNECTED) {
            billingClient?.startConnection(object : BillingClientStateListener {
                override fun onBillingServiceDisconnected() {
                    checkUnAcknowledgedPurchases(activity)
                }

                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    launchQueryPurchasesAsync(activity)
                }
            })
        } else {
            launchQueryPurchasesAsync(activity)
        }
    }

    private fun launchQueryPurchasesAsync(activity: Activity) {
        billingClient?.queryPurchasesAsync(
            BillingClient.SkuType.INAPP,
            PurchasesResponseListener { billingResult, list ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    for (purchase in list) {
                        if (purchase.purchaseState == Purchase.PurchaseState.PENDING &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(
                                TAG,
                                "OnResume: Payment is pending."
                            )
                        } else if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(TAG, "OnResume: Item purchased but not acknowledged")
                            viewModelScope.launch {
                                val failedList =
                                    mLocalDataManager.getFailedSubscription(purchase.skus.first())
                                val getAllUnAcknowladgeList = mLocalDataManager.getAllPurchased()
                                if (failedList.isEmpty()) {
                                    acknowledgePurchase(purchase.purchaseToken, activity)
                                } else if (getAllUnAcknowladgeList.isNotEmpty()) {
                                    val purchasedItem = getAllUnAcknowladgeList.filter {
                                        it.sku == purchase.skus.first()
                                    }
                                    acknowledgePurchase(purchase.purchaseToken, activity)

                                    if (purchasedItem.isEmpty().not()) {
                                        purchasedItem.first().isAcknowledge = true
                                        mLocalDataManager.savedFiledPurchase(purchasedItem)
                                    }
                                }
                            }
                        } else if (purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE &&
                            !purchase.isAcknowledged
                        ) {
                            Log.d(
                                TAG,
                                "OnResume: Unspecified State."
                            )
                        }
                    }
                }
            }
        )

        billingClient?.queryPurchasesAsync(
            BillingClient.SkuType.SUBS,
            PurchasesResponseListener { billingResult, list ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    val planId = PrefUtils.getInt(
                        activity,
                        CommonKeys.KEY_USER_PLAN_ID
                    )
                    if (list.isEmpty() && planId != 0
                    ) {
                        QTheMusicApplication.unSubscribeToTopic(
                            PrefUtils.getString(
                                activity, CommonKeys.KEY_USER_PLAN_TOPIC
                            ) ?: ""
                        )

                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_ID
                        )
                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_TITLE
                        )
                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_PRIZE
                        )
                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_DURATION
                        )
                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_TOPIC
                        )
                        PrefUtils.removeValue(
                            activity, CommonKeys.KEY_USER_PLAN_SKU
                        )
                    } else {
                        Log.e(
                            TAG,
                            "launchQueryPurchasesAsync: list is not empty and size is ${list.size}"
                        )
                        for (item in list) {
                            if (item.purchaseState == Purchase.PurchaseState.PURCHASED &&
                                !item.isAcknowledged
                            ) {
                                acknowledgePurchase(item.purchaseToken, activity)
                                Log.e(
                                    TAG,
                                    "launchQueryPurchasesAsync: un acknowledge sku of: ${item.skus}"
                                )
                            }
                        }
                    }
                }
            }
        )
    }

    fun acknowledgePurchase(purchaseToken: String?, activity: Activity) {

        val fragmentActivity = activity as FragmentActivity
        val acknowledgePurchaseParams = purchaseToken?.let {
            AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(it).build()
        }
        Log.d(
            TAG,
            "acknowledgePurchase: Acknowledge Flow"
        )

        if (acknowledgePurchaseParams != null) {
            billingClient?.acknowledgePurchase(
                acknowledgePurchaseParams
            ) { billingResult ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    val paymentResultDialogFragment =
                        PaymentResultDialogFragment.newInstance(
                            PaymentResultType.SUCCESS
                        )
                    paymentResultDialogFragment.show(
                        fragmentActivity.supportFragmentManager,
                        PaymentResultDialogFragment::class.java.simpleName
                    )
                    Log.d(
                        TAG,
                        "acknowledgePurchase: Acknowledged"
                    )
                }
            }
        }
    }

    private fun updateFailedPurchased(failedSkU: String) {
        viewModelScope.launch {
            val failedList = mLocalDataManager.getFailedSubscription(failedSkU)
            if (failedList.isNotEmpty()) {
                mLocalDataManager.deleteFailedPurchase(failedList)
            }
        }
    }

    fun getSubscriptionPlansFromServer() {
        mRemoteDataManager.getSubscriptionPlans().doOnSubscribe {
            mSubscriptionPlansResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mSubscriptionPlansResponse.value = ApiResponse.success(t.body()?.response)
                    }
                    t.code() == 403 -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mSubscriptionPlansResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mSubscriptionPlansResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                mSubscriptionPlansResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mSubscriptionPlansResponse.postValue(ApiResponse.complete())
            }
        })
    }

    fun subscribeToPlan(subscribeToPlanBody: SubscribeToPlanBodyBuilder) {
        mRemoteDataManager.subscribeToPlan(subscribeToPlanBody).doOnSubscribe {
            mSubscribeToPlanResponse.value = ApiResponse.loading()
        }?.subscribe(object : CustomObserver<Response<ResponseMain>>() {
            override fun onSuccess(t: Response<ResponseMain>) {
                when {
                    t.isSuccessful -> {
                        mSubscribeToPlanResponse.value = ApiResponse.success(t.body()?.response)
                        Log.d(TAG, "subscribeToPlan success called")
                    }
                    t.code() == 202 -> {
                        mSubscribeToPlanResponse.value = ApiResponse.error(
                            ErrorResponse(
                                false,
                                QTheMusicApplication.getContext()
                                    .getString(R.string.pending_payment),
                                t.code()
                            )
                        )
                    }
                    t.code() == 403 -> {
                        viewModelScope.launch {
                            Log.e(
                                TAG,
                                "Failed to purchase: data need to saved is:${subscribeToPlanBody.subscriptionPlan.toString()}"
                            )
                            mLocalDataManager.savedFailedPurchase(
                                subscribeToPlanBody.subscriptionPlan ?: FailedPurchased()
                            )
                        }
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        val errorData = ErrorResponse(
                            error?.response?.status ?: false,
                            error?.response?.message ?: QTheMusicApplication.getContext()
                                .getString(R.string.something_wrong),
                            t.code()
                        )
                        mSubscribeToPlanResponse.value = ApiResponse.expire(errorData)
                    }
                    else -> {
                        viewModelScope.launch {
                            Log.e(
                                TAG,
                                "Failed to purchase: data need to saved is:${subscribeToPlanBody.subscriptionPlan.toString()}"
                            )
                            mLocalDataManager.savedFailedPurchase(
                                subscribeToPlanBody.subscriptionPlan ?: FailedPurchased()
                            )
                        }
                        val error: ResponseMain? = ErrorUtils.parseError(t)
                        mSubscribeToPlanResponse.value = ApiResponse.error(
                            ErrorResponse(
                                error?.response?.status ?: false,
                                error?.response?.message ?: QTheMusicApplication.getContext()
                                    .getString(R.string.something_wrong),
                                t.code()
                            )
                        )
                    }
                }
            }

            override fun onError(e: Throwable, isInternetError: Boolean, error: CustomError?) {
                Log.d(TAG, "on Error called ")
                mSubscribeToPlanResponse.value = ApiResponse.error(
                    error?.code?.let { code ->
                        ErrorResponse(
                            false,
                            error.message,
                            code
                        )
                    }
                )
            }

            override fun onRequestComplete() {
                mSubscribeToPlanResponse.postValue(ApiResponse.complete())
            }
        })
    }

    suspend fun getFailedSubscription(sku: String): List<FailedPurchased> {
        return mLocalDataManager.getFailedSubscription(sku)
    }

    companion object {
        private const val TAG = "InAppPurchasesViewModel"
    }
}