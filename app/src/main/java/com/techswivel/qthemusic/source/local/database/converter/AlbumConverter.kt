package com.techswivel.qthemusic.source.local.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.techswivel.qthemusic.models.database.Album

class AlbumConverter {
    @TypeConverter
    fun fromBooks(obj: Album?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Album>() {

        }.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toBooksObj(songString: String?): Album? {
        if (songString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Album>() {

        }.type
        return gson.fromJson<Album>(songString, type)
    }
}