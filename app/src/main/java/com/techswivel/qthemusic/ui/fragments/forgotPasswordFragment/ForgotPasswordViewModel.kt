package com.techswivel.qthemusic.ui.fragments.forgotPasswordFragment

import com.techswivel.qthemusic.customData.enums.OtpType
import com.techswivel.qthemusic.models.AuthRequestBuilder
import com.techswivel.qthemusic.ui.base.BindingValidationViewModel

class ForgotPasswordViewModel : BindingValidationViewModel() {
    lateinit var fragmentFlow: OtpType
    lateinit var mbuilder: AuthRequestBuilder

    init {
        mbuilder = AuthRequestBuilder()
        fragmentFlow = OtpType.FORGET_PASSWORD
    }
}