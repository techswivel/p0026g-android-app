package com.techswivel.qthemusic.ui.fragments.homeFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.databinding.FragmentHomeBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.RecommendedSongsBodyBuilder
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.SongsBodyBuilder
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.mainActivity.MaintActivityImp
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.ArtistDetailFragment
import com.techswivel.qthemusic.ui.fragments.categoriesDetailsFragment.CategoriesDetialsFragment
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.DialogUtils
import org.greenrobot.eventbus.EventBus

class HomeFragment : RecyclerViewBaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
        private val TAG = "HomeFragment"
    }

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var networkViewModel: SongAndArtistsViewModel
    private lateinit var mRecommendedForYouAdapter: RecyclerViewAdapter
    private lateinit var mWhatsYourMoodAdapter: RecyclerViewAdapter
    private lateinit var mTrendingSongsAdapter: RecyclerViewAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        networkViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
        viewModel.recommendedSongsDataList.clear()
        viewModel.selectedTab = RecommendedSongsType.SONGS
        setUpHorizentalRecyclerView(
            binding.recyclerViewRecommendedMedia,
            0,
            AdapterType.RECOMMENDED_FOR_YOU
        )
        setUpHorizentalRecyclerView(
            binding.recyclerViewWhatsYourMood,
            0,
            AdapterType.WHATS_YOUR_MOOD
        )
        setUpRecyclerView(
            binding.recyclerViewTrendingSongs,
            AdapterType.TRENDING_SONGS
        )
        setObserver()
        setListeners()
        swipeToRefresh()
        callApi()
    }

    override fun onResume() {
        super.onResume()
        (mActivityListener as MaintActivityImp).changeSelectedItemToCurrent(
            Constants.HOME_NAV_ID,
            HomeFragment()
        )
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mRecommendedForYouAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return when (adapterType) {
            AdapterType.RECOMMENDED_FOR_YOU -> {
                mRecommendedForYouAdapter =
                    RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return when (viewModel.selectedTab) {
                                RecommendedSongsType.SONGS -> {
                                    R.layout.item_songs
                                }
                                RecommendedSongsType.ALBUM -> {
                                    R.layout.item_albums
                                }
                                else -> {
                                    R.layout.item_artist
                                }
                            }
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            when (viewModel.selectedTab) {
                                RecommendedSongsType.ALBUM -> {
                                    val mAlbum = data as Album
                                    val bundle = Bundle()
                                    bundle.putParcelable(CommonKeys.KEY_ALBUM_DETAILS, mAlbum)
                                    ActivityUtils.launchFragment(
                                        requireContext(),
                                        AlbumDetailsFragment::class.java.name,
                                        bundle
                                    )
                                }
                                RecommendedSongsType.SONGS -> {
                                    val songModel = data as Song
                                    if (songModel.songStatus != SongStatus.PREMIUM) {
                                        viewModel.insertSongInDataBase(songModel)
                                        viewModel.insertDataInDataBaseForSync(
                                            songModel.songId
                                        )
                                        val songBuilder = NextPlaySongBuilder().apply {
                                            this.currentSongModel = songModel
                                            this.songsList =
                                                viewModel.recommendedSongsDataList as MutableList<Song>
                                            this.playedFrom = SongType.RECOMMENDED
                                        }

                                        EventBus.getDefault().post(songBuilder)
                                    } else {
                                        val bundle = Bundle().apply {
                                            putParcelable(CommonKeys.KEY_DATA_MODEL, songModel)
                                            putParcelableArrayList(
                                                CommonKeys.KEY_SONGS_LIST,
                                                viewModel.recommendedSongsDataList as ArrayList<out Song>
                                            )
                                            putString(
                                                CommonKeys.KEY_SONG_TYPE,
                                                SongType.RECOMMENDED.value
                                            )
                                        }
                                        ActivityUtils.startPlayerActivity(activity, bundle)
                                    }
                                }
                                else -> {
                                    val artistData = data as Artist
                                    val bundle = Bundle()
                                    bundle.putParcelable(CommonKeys.KEY_ARTIST_MODEL, artistData)
                                    ActivityUtils.launchFragment(
                                        requireContext(),
                                        ArtistDetailFragment::class.java.name,
                                        bundle
                                    )
                                }
                            }
                        }
                    }, viewModel.recommendedSongsDataList)

                mRecommendedForYouAdapter
            }

            AdapterType.WHATS_YOUR_MOOD -> {
                mWhatsYourMoodAdapter =
                    RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.item_whats_your_mood
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onViewClicked(view: View, data: Any?) {
                            when (view.id) {
                                R.id.cv_main_image -> {
                                    val mCategories = data as Category
                                    val bundle = Bundle()
                                    bundle.putParcelable(CommonKeys.KEY_DATA, mCategories)
                                    ActivityUtils.launchFragment(
                                        requireContext(),
                                        CategoriesDetialsFragment::class.java.name,
                                        bundle
                                    )
                                }
                            }
                        }
                    }, viewModel.categoriesDataList)

                mWhatsYourMoodAdapter
            }

            else -> {
                mTrendingSongsAdapter =
                    RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.item_trending_songs
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            val songModel = data as Song
                            if (songModel.songStatus != SongStatus.PREMIUM) {
                                viewModel.insertSongInDataBase(songModel)
                                viewModel.insertDataInDataBaseForSync(
                                    songModel.songId
                                )
                                val songBuilder = NextPlaySongBuilder().apply {
                                    this.currentSongModel = songModel
                                    this.songsList =
                                        viewModel.trendingSongsDataList as MutableList<Song>
                                    this.playedFrom = SongType.TRENDING
                                }
                                EventBus.getDefault().post(songBuilder)
                            } else {
                                val bundle = Bundle().apply {
                                    putParcelable(CommonKeys.KEY_DATA_MODEL, songModel)
                                    putParcelableArrayList(
                                        CommonKeys.KEY_SONGS_LIST,
                                        viewModel.trendingSongsDataList as ArrayList<out Song>
                                    )
                                    putString(
                                        CommonKeys.KEY_SONG_TYPE,
                                        SongType.TRENDING.value
                                    )
                                }
                                ActivityUtils.startPlayerActivity(activity, bundle)
                            }
                        }
                    }, viewModel.trendingSongsDataList)
                mTrendingSongsAdapter
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObserver() {
        networkViewModel.recommendedSongsResponse.observe(viewLifecycleOwner) { recommendedSongsDataResponse ->
            when (recommendedSongsDataResponse.status) {
                NetworkStatus.LOADING -> {
                    enableAndDisableListeners(
                        songsListener = false,
                        albumListener = false,
                        artistListener = false
                    )
                    startRecommendedDataShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    stopRecommendedDataShimmer()
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )

                    viewModel.recommendedSongsDataList.clear()
                    val response = recommendedSongsDataResponse.t as ResponseModel
                    val songsList = response.data.songList
                    val albumsList = response.data.albumList
                    val artistsList = response.data.artistList

                    if (!albumsList.isNullOrEmpty()) {
                        viewModel.albumList.clear()
                        viewModel.albumList.addAll(albumsList)
                    }
                    if (!songsList.isNullOrEmpty()) {
                        viewModel.songsList.clear()
                        viewModel.songsList.addAll(songsList as MutableList<Song>)
                    }
                    if (!songsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.SONGS) {
                        viewModel.recommendedSongsDataList.addAll(songsList)

                    } else if (!albumsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.ALBUM) {
                        viewModel.recommendedSongsDataList.addAll(albumsList)

                    } else if (!artistsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.ARTIST) {
                        viewModel.recommendedSongsDataList.addAll(artistsList)

                    }
                    if (::mRecommendedForYouAdapter.isInitialized)
                        mRecommendedForYouAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    stopRecommendedDataShimmer()
                }
            }
        }

        networkViewModel.categoriesResponse.observe(viewLifecycleOwner) { categoriesDataResponse ->
            when (categoriesDataResponse.status) {
                NetworkStatus.LOADING -> {
                    startCategoriesDataShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    viewModel.categoriesDataList.clear()
                    val response = categoriesDataResponse.t as ResponseModel
                    val categoriesList = response.data.category

                    if (!categoriesList.isNullOrEmpty()) {
                        viewModel.categoriesDataList.addAll(categoriesList)
                    }
                    if (::mWhatsYourMoodAdapter.isInitialized)
                        mWhatsYourMoodAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    val error = categoriesDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    val error = categoriesDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    stopCategoriesDataShimmer()
                }
            }
        }

        networkViewModel.songlistResponse.observe(viewLifecycleOwner) { songsDataResponse ->
            when (songsDataResponse.status) {
                NetworkStatus.LOADING -> {
                    startTrendingSongsDataShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    viewModel.trendingSongsDataList.clear()
                    val response = songsDataResponse.t as ResponseModel
                    val songsList = response.data.songList

                    if (!songsList.isNullOrEmpty()) {
                        viewModel.trendingSongsDataList.addAll(songsList)
                        binding.tvTotalSongs.text =
                            viewModel.trendingSongsDataList.size.toString().plus(" songs")
                    }
                    if (::mTrendingSongsAdapter.isInitialized)
                        mTrendingSongsAdapter.notifyDataSetChanged()
                    stopTrendingSongsDataShimmer()
                }
                NetworkStatus.ERROR -> {
                    stopTrendingSongsDataShimmer()
                    val error = songsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    stopTrendingSongsDataShimmer()
                    val error = songsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    stopTrendingSongsDataShimmer()
                }
            }
        }
    }

    private fun startRecommendedDataShimmer() {
        if (viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.SONGS || viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.ALBUM) {
            binding.recyclerViewRecommendedMedia.visibility = View.GONE
            binding.slSongsAlbums.visibility = View.VISIBLE
            binding.slSongsAlbums.startShimmer()
        } else if (viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.ARTIST) {
            binding.recyclerViewRecommendedMedia.visibility = View.GONE
            binding.slArtists.visibility = View.VISIBLE
            binding.slArtists.startShimmer()
        }
    }

    private fun stopRecommendedDataShimmer() {
        if (viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.SONGS || viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.ALBUM) {
            binding.recyclerViewRecommendedMedia.visibility = View.VISIBLE
            binding.slSongsAlbums.visibility = View.GONE
            binding.slSongsAlbums.stopShimmer()
        } else if (viewModel.recommendedSongsBodyModel.dataType == RecommendedSongsType.ARTIST) {
            binding.recyclerViewRecommendedMedia.visibility = View.VISIBLE
            binding.slArtists.visibility = View.GONE
            binding.slArtists.stopShimmer()
        }
        if (binding.swipeHomeContainer.isRefreshing) {
            binding.swipeHomeContainer.isRefreshing = false

        }
    }

    private fun startCategoriesDataShimmer() {
        binding.slWhatsYourMood.visibility = View.VISIBLE
        binding.slWhatsYourMood.startShimmer()
    }

    private fun stopCategoriesDataShimmer() {
        binding.slWhatsYourMood.visibility = View.GONE
        binding.slWhatsYourMood.stopShimmer()
    }

    private fun startTrendingSongsDataShimmer() {
        binding.slTrendingSongs.visibility = View.VISIBLE
        binding.slTrendingSongs.startShimmer()
    }

    private fun stopTrendingSongsDataShimmer() {
        binding.slTrendingSongs.visibility = View.GONE
        binding.slTrendingSongs.stopShimmer()
    }

    private fun setListeners() {
        binding.btnSongs.setOnClickListener {
            viewModel.selectedTab = RecommendedSongsType.SONGS
            updateSelectedTabBackground(binding.btnSongs, binding.btnAlbums, binding.btnArtists)
            getRecommendedSongs()
        }

        binding.btnAlbums.setOnClickListener {
            viewModel.selectedTab = RecommendedSongsType.ALBUM
            updateSelectedTabBackground(binding.btnAlbums, binding.btnSongs, binding.btnArtists)
            getRecommendedAlbums()
        }

        binding.btnArtists.setOnClickListener {
            viewModel.selectedTab = RecommendedSongsType.ARTIST
            updateSelectedTabBackground(binding.btnArtists, binding.btnAlbums, binding.btnSongs)
            getRecommendedArtists()
        }
    }

    private fun enableAndDisableListeners(
        songsListener: Boolean,
        albumListener: Boolean,
        artistListener: Boolean
    ) {
        binding.btnSongs.isEnabled = songsListener
        binding.btnAlbums.isEnabled = albumListener
        binding.btnArtists.isEnabled = artistListener

    }

    private fun updateSelectedTabBackground(
        selectedTab: Button,
        unselectedTab1: Button,
        unselectedTab2: Button
    ) {
        selectedTab.setBackgroundResource(R.drawable.selected_tab_background)
        unselectedTab1.setBackgroundResource(R.drawable.unselected_tab_background)
        unselectedTab2.setBackgroundResource(R.drawable.unselected_tab_background)
    }

    private fun callApi() {
        viewModel.recommendedSongsDataList.clear()
        viewModel.categoriesDataList.clear()
        viewModel.trendingSongsDataList.clear()
        when (viewModel.selectedTab) {
            RecommendedSongsType.SONGS -> {
                getRecommendedSongs()
            }
            RecommendedSongsType.ALBUM -> {
                getRecommendedAlbums()
            }
            RecommendedSongsType.ARTIST -> {
                getRecommendedArtists()
            }
            else -> {
                getRecommendedSongs()
            }
        }
        networkViewModel.getCategoriesDataFromServer(CategoryType.RECOMMENDED)
        getSongs()
    }

    private fun getRecommendedSongs() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.RECOMMENDED
        recommendedSongsBuilder.dataType = RecommendedSongsType.SONGS
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        networkViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun getRecommendedAlbums() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.RECOMMENDED
        recommendedSongsBuilder.dataType = RecommendedSongsType.ALBUM
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        networkViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun getRecommendedArtists() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.RECOMMENDED
        recommendedSongsBuilder.dataType = RecommendedSongsType.ARTIST
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        networkViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun getSongs() {
        val songsBuilder = SongsBodyBuilder()
        songsBuilder.type = SongType.TRENDING
        val songsBodyModel = SongsBodyBuilder.build(songsBuilder)
        networkViewModel.getSongs(
            songsBodyModel
        )
    }

    private fun swipeToRefresh() {
        binding.swipeHomeContainer.setOnRefreshListener {
            prepareForDataRefresh()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        callApi()
        binding.tvTotalSongs.text = ""
        mRecommendedForYouAdapter.notifyDataSetChanged()
        mTrendingSongsAdapter.notifyDataSetChanged()
        mWhatsYourMoodAdapter.notifyDataSetChanged()
    }

}