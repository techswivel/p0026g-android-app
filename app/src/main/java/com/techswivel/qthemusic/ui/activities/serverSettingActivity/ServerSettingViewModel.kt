package com.techswivel.qthemusic.ui.activities.serverSettingActivity


import com.techswivel.qthemusic.services.WorkManager
import com.techswivel.qthemusic.ui.base.BaseViewModel

class ServerSettingViewModel : BaseViewModel() {

    fun startService() {
        WorkManager.startService()
    }

    fun stopService() {
        WorkManager.stopService()
    }
}