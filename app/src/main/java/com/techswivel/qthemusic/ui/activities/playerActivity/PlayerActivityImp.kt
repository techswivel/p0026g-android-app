package com.techswivel.qthemusic.ui.activities.playerActivity

import com.techswivel.qthemusic.customData.interfaces.BaseInterface

interface PlayerActivityImp : BaseInterface {
    fun showFavProgressBar()
    fun hideFavProgressBar()
}