package com.techswivel.qthemusic.ui.activities.mainActivity

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.example.NotificationMainDataModel
import com.techswivel.dfaktfahrerapp.ui.fragments.underDevelopmentMessageFragment.UnderDevelopmentMessageFragment
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.databinding.ActivityMainBinding
import com.techswivel.qthemusic.models.DownloadingResponse
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.services.exoService.MainStorage
import com.techswivel.qthemusic.source.remote.networkViewModel.InAppPurchasesViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.ui.base.BaseFragment
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.ui.dialogFragments.progressDialogFragment.ProgressDialogFragment
import com.techswivel.qthemusic.ui.fragments.categoriesListFragment.CategoriesFragment
import com.techswivel.qthemusic.ui.fragments.homeFragment.HomeFragment
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.ui.fragments.profileLandingScreen.ProfileLandingFragment
import com.techswivel.qthemusic.ui.fragments.purchaseAlbumFragment.PurchaseAlbumFragment
import com.techswivel.qthemusic.ui.fragments.purchasedSongFragment.PurchasedSongFragment
import com.techswivel.qthemusic.ui.fragments.searchScreen.SearchScreenFragment
import com.techswivel.qthemusic.utils.*
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MainActivity : PlayerBaseActivity(), MaintActivityImp {
    val TAG = "MainActivity"
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel
    private var mFragment: Fragment? = null
    private lateinit var mProfileNetworkViewModel: ProfileNetworkViewModel
    private var isInternetAvailable = true
    private var lastSelectedFragment: Fragment? = null
    private lateinit var mProgressDialog: ProgressDialogFragment

    private lateinit var inAppPurchasesViewModel: InAppPurchasesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        initView()
        checkCategoriesDataInDatabase()
        processPayLoadData()
        openHomeFragment()
        lastSelectedFragment = HomeFragment()
        setBottomNavigationSelector()
        setOnClickListener()
        observeDownloadStatus()
        networkStateObserver()
        if (MainStorage.getInstance(this)
                ?.shouldRestartService() == true && mPlayerService == null
        ) {
            runBlocking {
                val data = viewModel.getSavedState()
                onStartPlayer(data.toNextSongBuilder())
            }
        } else {
            Log.e(
                TAG,
                "onCreate: one then is not as per consition ...  pref value is: ${
                    MainStorage.getInstance(this)?.shouldRestartService()
                } and binder is:${mPlayerService?.isBinderAlive}"
            )
        }
    }

    override fun onRestart() {
        super.onRestart()
        if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == false)
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
    }

    override fun onBackPressed() {
        when {
            getEntryCount() <= 1 -> {
                mPlayerService?.getExoPlayerInstance()?.paused()
                this.finishAffinity()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mPlayerService?.isBinderAlive == true) {
            MainStorage.getInstance(this)?.setRestartService(true)
        }
        if (inAppPurchasesViewModel.billingClient != null) {
            inAppPurchasesViewModel.billingClient?.endConnection()
        }
    }

    override fun onProgressUpdate(timeInMis: Int) {
        mBinding.playLayout.seekBarPlayer.progress = timeInMis
    }

    override fun onPlayerStateIdle() {
    }

    override fun onPlayerStateBuffering() {

    }

    override fun onPlayerStateReady() {

    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        super.readyToShowVideoPlayerPlayIcons()
        if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_pause
                )
            )
        } else {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_play
                )
            )
        }
    }

    override fun onPlayerServiceStop() {
        mBinding.playLayout.playerParentLayout.visibilityGone()
    }

    override fun onPlayerServiceStart() {
        mBinding.playLayout.playerParentLayout.visibilityVisible()
    }

    override fun onProgressBarUpdate(current: Int, max: Int) {
        mBinding.playLayout.seekBarPlayer.max = max
        mBinding.playLayout.seekBarPlayer.progress = current
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        if (data.isNotificationRemoved) {
            mBinding.playLayout.playerParentLayout.visibilityGone()

        } else if (data.isPlaying) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        } else {

        }
    }
    override fun onCurrentSongUpdate(data: Song) {
        super.onCurrentSongUpdate(data)
        mBinding.playLayout.data = data
        mBinding.playLayout.executePendingBindings()
    }

    override fun onInternetAvailable() {
        if (viewModel.needToReplace) {
            lastSelectedFragment?.let { openFragment(it) }
            viewModel.needToReplace = false
            if (mBinding.bottomNavigation.isVisible.not()) {
                mBinding.bottomNavigation.visibilityVisible()
            }
        }
    }

    override fun onConnectionDisable() {
        if (mFragment is HomeFragment || mFragment is ProfileLandingFragment) {
            openFragmentWithoutAddingBackStack(NoInternetFragment())
            if (viewModel.authModel.subscription?.planTitle.isNullOrEmpty()) {
                mBinding.bottomNavigation.visibilityGone()
            }
            viewModel.needToReplace = true
        }
    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                .isAudioPlayerPlaying()
        ) {
            val animation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top)
            mBinding.playLayout.playerParentLayout.startAnimation(animation)
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onProgressBarUpdate(
                mPlayerService?.getExoPlayerInstance()?.currentTimeOfSong()?.toInt() ?: 0,
                mPlayerService?.getExoPlayerInstance()?.currentDuration()?.toInt() ?: 0
            )
            onCurrentSongUpdate(serviceBinder.currentSong)
        } else if ((mPlayerService?.isBinderAlive == true)
        ) {
            val animation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top)
            mBinding.playLayout.playerParentLayout.startAnimation(animation)
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onProgressBarUpdate(
                mPlayerService?.getExoPlayerInstance()?.currentTimeOfSong()?.toInt() ?: 0,
                mPlayerService?.getExoPlayerInstance()?.currentDuration()?.toInt() ?: 0
            )
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
    }

    override fun changeSelectedItemToCurrent(int: Int, fragment: Fragment) {
        mFragment = fragment
        mBinding.bottomNavigation.menu.getItem(int).isChecked = true
    }

    override fun stopMiniPlayer() {

    }

    override fun showProgressBar() {

    }

    override fun hideProgressBar() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPreOrderPurchasedSuccess(data: NotificationMainDataModel) {
        inAppPurchasesViewModel.checkUnAcknowledgedPurchases(this)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnClickListener() {
        mBinding.playLayout.icPlayPaused.setOnClickListener {
            if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
                mBinding.playLayout.icPlayPaused.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_play
                    )
                )
                mPlayerService?.getExoPlayerInstance()?.paused()
            } else {
                mBinding.playLayout.icPlayPaused.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_pause
                    )
                )
                mPlayerService?.getExoPlayerInstance()?.play()
            }
        }
        setOnSwipeListener(mBinding.playLayout.playerParentLayout)
    }

    private fun processPayLoadData() {

        if (intent.extras?.containsKey(KEY_DATA) == true) {
            val data = intent.extras?.getBundle(KEY_DATA)
            val isForForSongPreOrder = intent.getBooleanExtra(CommonKeys.IS_FOR_SONG_PLAYLOAD, true)
            if (data?.isEmpty == false) {
                if (isForForSongPreOrder) {
                    ActivityUtils.launchFragment(this, PurchasedSongFragment::class.java.name)
                    ActivityUtils.startPlayerActivity(this, data)
                } else {
                    ActivityUtils.launchFragment(this, PurchaseAlbumFragment::class.java.name)
                    ActivityUtils.startPlayerActivity(this, data)
                }
            }
        }
    }

    private fun checkCategoriesDataInDatabase() {
        viewModel.mLocalDataManager.getCategoriesFromDatabase().observe(this, Observer {
            viewModel.isCategoriesAvailable = it.isNotEmpty()
        })
    }

    private fun setBottomNavigationSelector() {
        mBinding.bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.bottom_nav_home -> {
                    lastSelectedFragment = HomeFragment()
                    if (mFragment is HomeFragment) {
                        Log.d(TAG, getString(R.string.already_selected))
                    } else {
                        if (isInternetAvailable) {
                            openHomeFragment()
                        } else {
                            openFragmentWithoutAddingBackStack(NoInternetFragment())
                        }
                    }
                }
                R.id.bottom_nav_search -> {
                    lastSelectedFragment = SearchScreenFragment()
                    if (mFragment is SearchScreenFragment) {
                        if (isInternetAvailable.not()) {
                            openSearchScreenFragment()
                        }
                        Log.d(TAG, getString(R.string.already_selected))
                    } else {
                        openSearchScreenFragment()
                    }
                }
                R.id.bottom_nav_categories -> {
                    lastSelectedFragment = CategoriesFragment()
                    if (mFragment is CategoriesFragment) {
                        checkForCategoryFragmentReplacement()
                    } else {
                        checkForCategoryFragmentReplacement()
                    }
                }
                R.id.bottom_nav_profile -> {
                    lastSelectedFragment = ProfileLandingFragment()
                    if (mFragment is ProfileLandingFragment) {
                        Log.d(TAG, getString(R.string.already_selected))
                    } else {
                        if (isInternetAvailable) {
                            openLandingProfileFragment()
                        } else {
                            openFragmentWithoutAddingBackStack(NoInternetFragment())
                        }
                    }
                }
            }
            return@setOnItemSelectedListener true
        }
    }

    private fun checkForCategoryFragmentReplacement() {
        checkCategoriesDataInDatabase()
        if (isInternetAvailable.not() && viewModel.isCategoriesAvailable.not()) {
            openFragmentWithoutAddingBackStack(NoInternetFragment())
        } else {
            openCategoriesFragment()
        }
    }

    private fun openHomeFragment() {
        popUpAllFragmentIncludeThis(HomeFragment::class.java.name)
        openFragment(HomeFragment.newInstance())
    }

    private fun openCategoriesFragment() {
        openFragment(CategoriesFragment.newInstance())
    }

    private fun initView() {
        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]
        mProfileNetworkViewModel = ViewModelProvider(this)[ProfileNetworkViewModel::class.java]
        inAppPurchasesViewModel = ViewModelProvider(this)[InAppPurchasesViewModel::class.java]
        inAppPurchasesViewModel.setUpBillingClient(this, null)
        inAppPurchasesViewModel.checkUnAcknowledgedPurchases(this)
        viewModel.authModel = viewModel.getPrefrencesData(this)
    }


    private fun openLandingProfileFragment() {
        openFragment(ProfileLandingFragment())
    }

    private fun openSearchScreenFragment() {
        openFragment(SearchScreenFragment())
    }

    private fun openUnderDevelopmentFragment() {
        popUpAllFragmentIncludeThis(UnderDevelopmentMessageFragment::class.java.name)
        openFragment(UnderDevelopmentMessageFragment.newInstance())
    }

    private fun openFragment(fragment: Fragment) {
        ::mFragment.set(fragment)
        mFragment.let { fragmentInstance ->
            fragmentInstance?.let { fragmentToBeReplaced ->
                replaceFragment(mBinding.mainContainer.id, fragmentToBeReplaced)
            }
        }
    }

    private fun openFragmentWithoutAddingBackStack(fragment: Fragment) {
        replaceFragmentWithoutAddingToBackStack(mBinding.mainContainer.id, fragment)
        popUpAllFragmentIncludeThis(fragment.tag)
    }

    fun networkStateObserver() {
        connectionLiveData.observe(this) { isConnected ->
            isConnected?.let {
                isInternetAvailable = it
            }
        }
    }

    private fun observeDownloadStatus() {
        contentResolver?.registerContentObserver(
            Uri.parse("content://downloads/my_downloads"),
            true, object : ContentObserver(null) {

                @SuppressLint("Range")
                override fun onChange(selfChange: Boolean, uri: Uri?) {
                    super.onChange(selfChange, uri)
                    if (uri.toString().matches(".*\\d+$".toRegex())) {
                        val changedId: Long = uri?.lastPathSegment?.toLong() ?: 0
                        GlobalScope.launch {
                            if (viewModel.isDownloadIdExist(changedId)) {
                                val cursor: Cursor? = QTheMusicApplication.getDownloadService()
                                    ?.query(DownloadManager.Query().setFilterById(changedId))
                                try {
                                    if (cursor != null && cursor.moveToFirst()) {
                                        when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
                                            DownloadManager.STATUS_FAILED -> {
                                                val columnReason = cursor.getColumnIndex(
                                                    DownloadManager.COLUMN_REASON
                                                )
                                                val reason = cursor.getInt(columnReason)
                                                viewModel.updateStatus(
                                                    DownloadingStatus.FAILED,
                                                    changedId,
                                                    ""
                                                )
                                                EventBus.getDefault().post(
                                                    DownloadingResponse.failed(
                                                        cursor.getString(
                                                            cursor.getColumnIndex(
                                                                DownloadManager.COLUMN_REASON
                                                            )
                                                        ),
                                                        viewModel.getDownloadFile(changedId).translationId
                                                            ?: 0
                                                    )
                                                )
                                            }

                                            DownloadManager.STATUS_PAUSED -> {

                                                val columnReason = cursor.getColumnIndex(
                                                    DownloadManager.COLUMN_REASON
                                                )
                                                val reason = cursor.getInt(columnReason)
                                                viewModel.updateStatus(
                                                    DownloadingStatus.PAUSED,
                                                    changedId,
                                                    "Download Paused"
                                                )
                                                EventBus.getDefault().post(
                                                    DownloadingResponse.failed(
                                                        cursor.getString(
                                                            cursor.getColumnIndex(
                                                                DownloadManager.COLUMN_REASON
                                                            )
                                                        ),
                                                        viewModel.getDownloadFile(changedId).translationId
                                                            ?: 0
                                                    )
                                                )
                                                EventBus.getDefault()
                                                    .post(
                                                        DownloadingResponse.paused(
                                                            viewModel.getDownloadFile(
                                                                changedId
                                                            ).translationId ?: 0
                                                        )
                                                    )
                                            }

                                            DownloadManager.STATUS_PENDING -> {
                                                val columnReason = cursor.getColumnIndex(
                                                    DownloadManager.COLUMN_REASON
                                                )
                                                val reason = cursor.getInt(columnReason)
                                                viewModel.updateStatus(
                                                    DownloadingStatus.PENDING,
                                                    changedId,
                                                    "Download Pending"
                                                )
                                                EventBus.getDefault()
                                                    .post(
                                                        DownloadingResponse.pending(
                                                            viewModel.getDownloadFile(
                                                                changedId
                                                            ).translationId ?: 0
                                                        )
                                                    )
                                            }

                                            DownloadManager.STATUS_RUNNING -> {
                                                val columnReason = cursor.getColumnIndex(
                                                    DownloadManager.COLUMN_REASON
                                                )
                                                val reason = cursor.getInt(columnReason)
                                                viewModel.updateStatus(
                                                    DownloadingStatus.RUNNING,
                                                    changedId,
                                                    "Download Running"
                                                )
                                                val total =
                                                    cursor.getLong(
                                                        cursor.getColumnIndex(
                                                            DownloadManager.COLUMN_TOTAL_SIZE_BYTES
                                                        )
                                                    )
                                                if (total >= 0) {
                                                    val downloaded =
                                                        cursor.getLong(
                                                            cursor.getColumnIndex(
                                                                DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR
                                                            )
                                                        )
                                                    val progress = ((downloaded * 100L) / total)
                                                    Log.i(
                                                        "TAG",
                                                        "handleResult: now progress is $progress for download id $changedId"
                                                    )

                                                    try {
                                                        EventBus.getDefault().post(
                                                            DownloadingResponse.running(
                                                                progress,
                                                                viewModel.getDownloadFile(changedId).translationId
                                                                    ?: 0
                                                            )
                                                        )
                                                    } catch (e: java.lang.Exception) {
                                                        Log.e(BaseFragment.TAG, "run: ", e)
                                                    }
                                                }

                                            }
                                            DownloadManager.STATUS_SUCCESSFUL -> {
                                                val columnReason = cursor.getColumnIndex(
                                                    DownloadManager.COLUMN_REASON
                                                )
                                                val reason = cursor.getInt(columnReason)
                                                viewModel.updateStatus(
                                                    DownloadingStatus.INSTALLING,
                                                    changedId,
                                                    "Download Successful"
                                                )
                                                EventBus.getDefault()
                                                    .post(
                                                        DownloadingResponse.success(
                                                            viewModel.getDownloadFile(
                                                                changedId
                                                            ).translationId ?: 0
                                                        )
                                                    )
                                                Log.i(
                                                    BaseFragment.TAG,
                                                    "handleResult: download success for id $changedId"
                                                )

                                            }
                                        }
                                    } else {
                                        Log.i(BaseFragment.TAG, "onChange: cancel")
                                        try {
                                            val columnReason = cursor?.getColumnIndex(
                                                DownloadManager.COLUMN_REASON
                                            )
                                            val reason = cursor?.getInt(columnReason ?: 0)
                                            viewModel.updateStatus(
                                                DownloadingStatus.CANCEL,
                                                changedId,
                                                "Downloading"
                                            )
                                            EventBus.getDefault()
                                                .post(
                                                    DownloadingResponse.cancel(
                                                        viewModel.getDownloadFile(
                                                            changedId
                                                        ).translationId ?: 0
                                                    )
                                                )
                                            viewModel.deleteRecord(
                                                viewModel.getDownloadFile(
                                                    changedId
                                                )
                                            )
                                        } catch (e: Exception) {
                                            Log.e(TAG, "onChange: ", e)
                                        }
                                    }
                                } finally {
                                    cursor?.close()
                                }
                            } else {
                                Log.i(TAG, "onChange: id did not match ")
                            }
                        }

                    }
                }
            })
    }
}