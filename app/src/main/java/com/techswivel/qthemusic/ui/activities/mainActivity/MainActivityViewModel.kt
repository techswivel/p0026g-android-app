package com.techswivel.qthemusic.ui.activities.mainActivity

import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.models.AuthModel
import com.techswivel.qthemusic.models.database.DownloadIds
import com.techswivel.qthemusic.ui.base.BaseViewModel

class MainActivityViewModel : BaseViewModel() {
    var isCategoriesAvailable: Boolean = true
    var needToReplace = false
    lateinit var authModel: AuthModel

    suspend fun updateStatus(status: DownloadingStatus, changedId: Long, reasonString: String) {
        mLocalDataManager.updateStatus(status, changedId, reasonString)
    }

    suspend fun getDownloadFile(changedId: Long): DownloadIds {

        return mLocalDataManager.getDownloadFile(changedId)
    }

    fun deleteRecord(id: DownloadIds) {
        mLocalDataManager.deleteRecord(id)
    }

    suspend fun isDownloadIdExist(changedId: Long): Boolean {
        return mLocalDataManager.isDownloadIdExist(changedId)
    }

}