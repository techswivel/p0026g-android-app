package com.techswivel.qthemusic.ui.fragments.albumListScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.databinding.FragmentAlbumListScreenBinding
import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.SharedViewModelForAllMusic
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.CommonKeys

class AlbumListScreen : RecyclerViewBaseFragment() {
    private lateinit var mBinding: FragmentAlbumListScreenBinding
    private lateinit var mViewModel: AlbumListScreenViewModel
    private lateinit var mSharedViewModel: SharedViewModelForAllMusic
    private lateinit var mAlbumListAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        mViewModel.mArtistDetailsList =
            arguments?.getParcelable<ArtistDetails>(CommonKeys.KEY_ARTIST_DETAILS_MODEL) as ArtistDetails
        val album = mViewModel.mArtistDetailsList.albums
        if (album != null) {
            mViewModel.mAlbumList.addAll(album)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentAlbumListScreenBinding.inflate(layoutInflater, container, false)
        bindViewModel()
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setClickListeners()
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mAlbumListAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        mAlbumListAdapter = RecyclerViewAdapter(
            object : RecyclerViewAdapter.CallBack {
                override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                    return R.layout.recview_album_category_detial
                }

                override fun onItemClick(data: Any?, position: Int) {
                    super.onItemClick(data, position)
                    val mAlbum = data as Album
                    val bundle = Bundle()
                    bundle.putParcelable(CommonKeys.KEY_ALBUM_DETAILS, mAlbum)
                    ActivityUtils.launchFragment(
                        requireContext(),
                        AlbumDetailsFragment::class.java.name,
                        bundle
                    )
                }

                override fun onNoDataFound() {

                }
            },
            mViewModel.mAlbumList
        )
        return mAlbumListAdapter
    }

    private fun setUpRecyclerView() {
        setUpGridRecyclerView(
            mBinding.recViewAlbumScreen,
            Constants.NUMBER_OF_COLUMN_CATEGORIES_VIEW,
            resources.getDimensionPixelSize(R.dimen._0dp),
            resources.getDimensionPixelSize(R.dimen._0dp),
            AdapterType.ALBUM
        )
    }

    private fun setClickListeners() {
        mBinding.ivBackFragmentAlbum.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(AlbumListScreenViewModel::class.java)
        mSharedViewModel =
            ViewModelProvider(requireActivity()).get(SharedViewModelForAllMusic::class.java)
    }

    private fun bindViewModel() {
        mBinding.albumList = mViewModel.mArtistDetailsList
        mBinding.executePendingBindings()
    }

    companion object {
        private val TAG = "AlbumListScreen"
        fun newInstance() = AlbumListScreen()
    }
}