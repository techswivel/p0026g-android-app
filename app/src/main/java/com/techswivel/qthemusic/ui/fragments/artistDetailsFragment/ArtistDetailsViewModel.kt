package com.techswivel.qthemusic.ui.fragments.artistDetailsFragment

import androidx.fragment.app.Fragment
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.BaseViewModel

class ArtistDetailsViewModel : BaseViewModel() {
    lateinit var artistData: Artist
    val songsList = ArrayList<Song?>()
    val fragmentsList = ArrayList<Fragment>()
    val fragmentTittlesList = ArrayList<String>()

}