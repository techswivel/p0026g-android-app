package com.techswivel.qthemusic.ui.fragments.searchScreen

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.databinding.FragmentSearchScreenBinding
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.NetworkChangeReceiver
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.mainActivity.MaintActivityImp
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.ArtistDetailFragment
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.ui.fragments.searchQueryFragment.SearchQueryFragment
import com.techswivel.qthemusic.utils.*
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus


class SearchScreenFragment : RecyclerViewBaseFragment() {

    private lateinit var mBinding: FragmentSearchScreenBinding
    private lateinit var mViewModel: SearchScreenViewModel
    private lateinit var mSongsAndArtistsViewModel: SongAndArtistsViewModel
    private lateinit var mRecentPlayAdapter: RecyclerViewAdapter
    private lateinit var mRecentAlbumAdapter: RecyclerViewAdapter
    private lateinit var mRecentArtistAdapter: RecyclerViewAdapter
    private var isInternetAvailable = true
    lateinit var connectionLiveData: NetworkChangeReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        connectionLiveData = NetworkChangeReceiver(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentSearchScreenBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.selectedTab = RecommendedSongsType.SONGS
        callObservers()
        networkStateObserver()
        setCurrentUpRecyclerview(mViewModel.selectedTab)
        setListeners()
        initViews()
    }

    override fun onResume() {
        super.onResume()
        (mActivityListener as MaintActivityImp).changeSelectedItemToCurrent(
            Constants.SEARCH_NAV_ID,
            SearchScreenFragment()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.recentPlayedSongsList.clear()
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mRecentPlayAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return when (adapterType) {
            AdapterType.RECENT_SONGS -> {
                mRecentPlayAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                        return R.layout.recview_recent_play_db_layout
                    }

                    override fun onNoDataFound() {

                    }

                    override fun onItemClick(data: Any?, position: Int) {
                        super.onItemClick(data, position)
                        val song = data as Song
                        mViewModel.recentlyPlayerSingleSong.add(song)
                        runBlocking {
                            try {
                                mViewModel.insertSongInDataBase(song)
                                mViewModel.insertDataInDataBaseForSync(song.songId)
                            } catch (e: Exception) {
                                Log.d(TAG, "exeception is ${e.message}")
                            }
                        }
                        if (song.songStatus != SongStatus.PREMIUM) {
                            val songBuilder = NextPlaySongBuilder().apply {
                                this.currentSongModel = song
                                this.songsList =
                                    mViewModel.recentlyPlayerSingleSong
                                this.playedFrom = SongType.SEARCHED
                            }
                            EventBus.getDefault().post(songBuilder)
                        } else if (song.songStatus == SongStatus.PREMIUM) {
                            val bundle = Bundle().apply {
                                putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                                putParcelableArrayList(
                                    CommonKeys.KEY_SONGS_LIST,
                                    mViewModel.recentlyPlayerSingleSong
                                )
                                putString(
                                    CommonKeys.KEY_SONG_TYPE,
                                    SongType.SEARCHED.value
                                )
                            }
                            ActivityUtils.startPlayerActivity(activity, bundle)
                        }
                    }
                }, mViewModel.recentPlayedSongsList)
                mRecentPlayAdapter
            }
            AdapterType.RECENT_ALBUM -> {
                mRecentAlbumAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                        return R.layout.recview_album_category_detial
                    }

                    override fun onNoDataFound() {

                    }

                    override fun onItemClick(data: Any?, position: Int) {
                        super.onItemClick(data, position)
                        val mAlbum = data as Album
                        val bundle = Bundle()
                        bundle.putParcelable(CommonKeys.KEY_ALBUM_DETAILS, mAlbum)
                        ActivityUtils.launchFragment(
                            requireContext(),
                            AlbumDetailsFragment::class.java.name,
                            bundle
                        )
                    }

                    override fun onViewClicked(view: View, data: Any?) {

                    }
                }, mViewModel.recentPlayedAlbumList)
                mRecentAlbumAdapter
            }

            else -> {
                mRecentArtistAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                        return R.layout.item_artist
                    }

                    override fun onNoDataFound() {

                    }

                    override fun onItemClick(data: Any?, position: Int) {
                        super.onItemClick(data, position)
                        val artistData = data as Artist
                        val bundle = Bundle()
                        bundle.putParcelable(CommonKeys.KEY_ARTIST_MODEL, artistData)
                        ActivityUtils.launchFragment(
                            requireContext(),
                            ArtistDetailFragment::class.java.name,
                            bundle
                        )
                    }
                }, mViewModel.recentPlayedArtistList)
                mRecentArtistAdapter
            }
        }
    }

    private fun initViews() {
        mBinding.noDataFoundInc.noDataFoundBackButton.visibilityInVisible()
    }

    private fun setListeners() {

        mBinding.btnSongs.setOnClickListener {
            mViewModel.selectedTab = RecommendedSongsType.SONGS
            setCurrentUpRecyclerview(mViewModel.selectedTab)
            updateSelectedTabBackground(
                mBinding.btnSongs,
                mBinding.btnAlbums,
                mBinding.btnArtists
            )
            checkForEmptyList(mViewModel.selectedTab)
        }

        mBinding.btnAlbums.setOnClickListener {
            mViewModel.selectedTab = RecommendedSongsType.ALBUM
            setCurrentUpRecyclerview(mViewModel.selectedTab)
            updateSelectedTabBackground(
                mBinding.btnAlbums,
                mBinding.btnSongs,
                mBinding.btnArtists
            )
            checkForEmptyList(mViewModel.selectedTab)
        }

        mBinding.btnArtists.setOnClickListener {
            mViewModel.selectedTab = RecommendedSongsType.ARTIST
            setCurrentUpRecyclerview(mViewModel.selectedTab)
            updateSelectedTabBackground(
                mBinding.btnArtists,
                mBinding.btnAlbums,
                mBinding.btnSongs
            )
            checkForEmptyList(mViewModel.selectedTab)
        }
        mBinding.searchView.setOnClickListener {
            if (isInternetAvailable) {
                ActivityUtils.launchFragment(requireContext(), SearchQueryFragment::class.java.name)
            } else {
                ActivityUtils.launchFragment(requireContext(), NoInternetFragment::class.java.name)
            }
        }
    }

    private fun updateSelectedTabBackground(
        selectedTab: Button,
        unselectedTab1: Button,
        unselectedTab2: Button
    ) {
        selectedTab.setBackgroundResource(R.drawable.selected_tab_background)
        unselectedTab1.setBackgroundResource(R.drawable.unselected_tab_background)
        unselectedTab2.setBackgroundResource(R.drawable.unselected_tab_background)
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(SearchScreenViewModel::class.java)
        mSongsAndArtistsViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    private fun setCurrentUpRecyclerview(recommendedSongsType: RecommendedSongsType?) {
        try {
            if (recommendedSongsType?.equals(RecommendedSongsType.SONGS) == true) {
                setUpRecyclerView(mBinding.searchScreenRecyclerView, AdapterType.RECENT_SONGS)
            } else if (recommendedSongsType?.equals(RecommendedSongsType.ALBUM) == true) {
                setUpGridRecyclerView(
                    mBinding.searchScreenRecyclerView,
                    Constants.NUMBER_OF_COLUMN_CATEGORIES_VIEW,
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    AdapterType.RECENT_ALBUM
                )
            } else {
                setUpGridRecyclerView(
                    mBinding.searchScreenRecyclerView,
                    Constants.NUMBER_OF_COLUMN,
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    AdapterType.RECENT_ARTIST
                )
            }
        } catch (e: Exception) {
            Log.d(TAG, "execption is ${e.message}")
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getLastFivePlayedSongsFromDatabase() {
        mViewModel.mLocalDataManager.getRecentPlayedSongs()
            .observe(viewLifecycleOwner, Observer { dbSongsList ->
                if (dbSongsList.isNotEmpty()) {
                    mViewModel.recentPlayedSongsList.clear()
                    mViewModel.recentPlayedSongsList.addAll(dbSongsList)
                    mRecentPlayAdapter.notifyDataSetChanged()

                } else {
                    if (mViewModel.selectedTab == RecommendedSongsType.SONGS) {
                        mBinding.tvNoDataFound.visibilityVisible()
                    }
                }
            })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getRecentPlayedAlbumFromDatabase() {
        runBlocking {

            mViewModel.mLocalDataManager.getRecentPlayedAlbum()
                .observe(viewLifecycleOwner, Observer { dbAlbumList ->
                    if (dbAlbumList.isNotEmpty()) {
                        mViewModel.recentPlayedAlbumList.clear()
                        mViewModel.recentPlayedAlbumList.addAll(dbAlbumList)
                        if (::mRecentAlbumAdapter.isInitialized)
                            mRecentAlbumAdapter.notifyDataSetChanged()
                    }
                })
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getRecentPlayedArtistFromDatabase() {
        runBlocking {

            mViewModel.mLocalDataManager.getRecentPlayedArtist()
                .observe(viewLifecycleOwner, Observer { dbArtistList ->
                    if (dbArtistList.isNotEmpty()) {
                        mViewModel.recentPlayedArtistList.clear()
                        mViewModel.recentPlayedArtistList.addAll(dbArtistList)
                        if (::mRecentArtistAdapter.isInitialized)
                            mRecentArtistAdapter.notifyDataSetChanged()
                    }
                })
        }
    }

    private fun callObservers() {
        getLastFivePlayedSongsFromDatabase()
        getRecentPlayedAlbumFromDatabase()
        getRecentPlayedArtistFromDatabase()
    }

    private fun networkStateObserver() {
        connectionLiveData.observe(viewLifecycleOwner) { isConnected ->
            isConnected?.let {
                isInternetAvailable = it
            }
        }
    }

    private fun checkForEmptyList(selectedTab: RecommendedSongsType?) {
        if (selectedTab == RecommendedSongsType.SONGS
            && mViewModel.recentPlayedSongsList.isEmpty()
        ) {
            mBinding.tvNoDataFound.visibilityVisible()
        } else if (selectedTab == RecommendedSongsType.ALBUM
            && mViewModel.recentPlayedAlbumList.isEmpty()
        ) {
            mBinding.tvNoDataFound.visibilityVisible()
        } else if (selectedTab == RecommendedSongsType.ARTIST
            && mViewModel.recentPlayedArtistList.isEmpty()
        ) {
            mBinding.tvNoDataFound.visibilityVisible()
        } else {
            mBinding.tvNoDataFound.visibilityGone()
        }
    }


    companion object {
        private val TAG = "SearchScreenFragment"
    }
}