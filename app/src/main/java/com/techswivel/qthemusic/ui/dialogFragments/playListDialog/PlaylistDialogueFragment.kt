package com.techswivel.qthemusic.ui.dialogFragments.playListDialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.PlaylistDialogueFragmentBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.PlaylistModel
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.UpdatePlayListBuilder
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseDialogFragment
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.DialogUtils
import com.techswivel.qthemusic.utils.visibilityInVisible
import com.techswivel.qthemusic.utils.visibilityVisible

class PlaylistDialogueFragment : RecyclerViewBaseDialogFragment(), RecyclerViewAdapter.CallBack,
    BaseInterface {

    companion object {
        fun newInstance(callback: Callback) = PlaylistDialogueFragment().apply {
            this.setCallBack(callback)
        }
    }

    private lateinit var viewModel: PlaylistDialogueViewModel
    private lateinit var mProfileViewModel: ProfileNetworkViewModel
    private lateinit var songAndArtistViewModel: SongAndArtistsViewModel
    private lateinit var mBinding: PlaylistDialogueFragmentBinding
    private lateinit var mCallback: Callback
    private lateinit var mAdapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        mBinding = PlaylistDialogueFragmentBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[PlaylistDialogueViewModel::class.java]
        mProfileViewModel = ViewModelProvider(this)[ProfileNetworkViewModel::class.java]
        songAndArtistViewModel =
            ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
        setObserver()
        setOnClickListener()
        mProfileViewModel.getPlayListFromServer()
        if (arguments?.containsKey(CommonKeys.KEY_DATA) == true) {
            viewModel.songId = arguments?.getInt(CommonKeys.KEY_DATA)
        }
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mAdapter
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_play_list_selection
    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
        val item = data as PlaylistModel
        for (items in viewModel.mData) {
            items.setDownloadButtonVisibility(ObservableField<Int>(View.GONE))
        }
        viewModel.mSelectedPlayListItem = item
        viewModel.mSelectedPlayListItem?.setDownloadButtonVisibility(ObservableField<Int>(View.VISIBLE))
    }

    override fun onNoDataFound() {

    }

    override fun showProgressBar() {
        mBinding.slPlaylist.visibility = View.VISIBLE
        mBinding.slPlaylist.startShimmer()
    }

    override fun hideProgressBar() {
        mBinding.slPlaylist.visibility = View.GONE
        mBinding.slPlaylist.stopShimmer()
    }

    private fun setOnClickListener() {
        mBinding.btnDone.setOnClickListener {
            if (viewModel.mSelectedPlayListItem != null) {
                val updatePlayListBuilder = UpdatePlayListBuilder()
                updatePlayListBuilder.playListId = viewModel.mSelectedPlayListItem?.playListId
                updatePlayListBuilder.songId = viewModel.songId
                updatePlayListBuilder.type = ActionType.ADD
                val song = UpdatePlayListBuilder.builder(updatePlayListBuilder)
                songAndArtistViewModel.updatePlayList(song)
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.select_at_least_one_playlist),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        mBinding.close.setOnClickListener {
            dismiss()
        }
    }

    private fun setObserver() {
        songAndArtistViewModel.deleteSongRespomse.observe(viewLifecycleOwner) { songDeletingResponse ->
            when (songDeletingResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.btnDone.isEnabled = false
                    mBinding.playListProgressBar.visibility = View.VISIBLE
                }
                NetworkStatus.SUCCESS -> {
                    mBinding.btnDone.isEnabled = true
                    mBinding.playListProgressBar.visibility = View.INVISIBLE
                    mCallback.onPlayListSelected(viewModel.mSelectedPlayListItem)
                    dismiss()
                }
                NetworkStatus.ERROR -> {
                    mBinding.btnDone.isEnabled = true
                    mBinding.playListProgressBar.visibility = View.INVISIBLE
                    DialogUtils.runTimeAlert(context ?: requireContext(),
                        getString(R.string.error),
                        songDeletingResponse.error?.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.btnDone.isEnabled = true
                    mBinding.playListProgressBar.visibility = View.INVISIBLE
                    DialogUtils.sessionExpireAlert(requireContext(),
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    mBinding.btnDone.isEnabled = true
                    mBinding.playListProgressBar.visibility = View.INVISIBLE
                }
            }
        }
        mProfileViewModel.playlistResponse.observe(viewLifecycleOwner) { response ->
            when (response.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    val responseMain = response.t as ResponseModel
                    if (responseMain.data.playlistModel?.isEmpty() == true) {
                        mBinding.tvEmptyMessage.visibilityVisible()
                        mBinding.btnDone.visibilityInVisible()
                    }
                    for (item in responseMain.data.playlistModel ?: emptyList()) {
                        item.setDownloadButtonVisibility(ObservableField<Int>(View.GONE))
                        viewModel.mData.add(item)
                    }
                    mAdapter = RecyclerViewAdapter(this, viewModel.mData as MutableList<Any>)
                    setUpRecyclerView(mBinding.recyclerView)
                    hideProgressBar()
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = response.error as ErrorResponse
                    DialogUtils.runTimeAlert(context ?: requireContext(),
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    val error = response.error as ErrorResponse
                    DialogUtils.runTimeAlert(context ?: requireContext(),
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }
    }


    private fun setCallBack(callback: Callback) {
        this.mCallback = callback
    }


    interface Callback {
        fun onPlayListSelected(playlistModel: PlaylistModel?)
    }

}