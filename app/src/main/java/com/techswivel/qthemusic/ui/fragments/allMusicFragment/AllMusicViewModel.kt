package com.techswivel.qthemusic.ui.fragments.allMusicFragment

import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.models.RecommendedSongsBodyBuilder
import com.techswivel.qthemusic.models.RecommendedSongsBodyModel
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class AllMusicViewModel : LocalDatabaseViewModel() {
    val mSongsList: MutableList<Any> = ArrayList()
    val mAlbumList: MutableList<Any> = ArrayList()
    lateinit var mArtist: Artist
    lateinit var mRecommendedSongsBodyBuilder: RecommendedSongsBodyBuilder
    lateinit var recommendedSongsBodyModel: RecommendedSongsBodyModel
    lateinit var mArtistDetails: ArtistDetails
}