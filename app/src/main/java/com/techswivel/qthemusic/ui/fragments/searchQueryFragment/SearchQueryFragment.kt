package com.techswivel.qthemusic.ui.fragments.searchQueryFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentSearchQueryBinding
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.ArtistDetailFragment
import com.techswivel.qthemusic.utils.*
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus


class SearchQueryFragment : RecyclerViewBaseFragment(), BaseInterface {


    private lateinit var mBinding: FragmentSearchQueryBinding
    private lateinit var mSongsAndArtistsViewModel: SongAndArtistsViewModel
    private lateinit var mViewModel: SearchQueryViewModel
    private lateinit var mSearchAdapter: RecyclerViewAdapter
    private lateinit var mLanguagesAdapter: RecyclerViewAdapter
    private lateinit var handler: Handler
    var lastLanguageId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentSearchQueryBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()
        setListeners()
        setObserverForViewModel()
        updateSelectedTabBackground(R.drawable.shape_bg_your_interest_selected)
    }

    override fun onDestroy() {
        super.onDestroy()
        Utilities.hideSoftKeyBoard(requireContext(), mBinding.etSearchBox)
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        TODO("Not yet implemented")
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return when (adapterType) {
            AdapterType.SEARCHED_SONGS -> {
                mSearchAdapter =
                    RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            val myData = data as SearchedSongs
                            return when (myData.type) {
                                ResponseType.SONG -> {
                                    R.layout.rec_view_searched_layout
                                }
                                ResponseType.ARTIST -> {
                                    R.layout.recview_searched_artist
                                }
                                else -> {
                                    R.layout.recview_searched_album
                                }
                            }
                        }

                        override fun onNoDataFound() {
                            Log.e(TAG, "No Data Found")

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            val myData = data as SearchedSongs
                            val song = Song()
                            song.songTitle = myData.songTitle
                            song.songAudioUrl = myData.songAudioUrl
                            song.songStatus = myData.songStatus
                            song.songId = myData.songId
                            song.songPrice = myData.albumPrice.toString()
                            song.sku = myData.sku
                            song.coverImageUrl = myData.coverImageUrl
                            song.artist = myData.artistName
                            song.artistId = myData.artistId
                            if (myData.type == ResponseType.SONG) {
                                runBlocking {
                                    try {
                                        mViewModel.insertSongInDataBase(song)
                                        mViewModel.insertDataInDataBaseForSync(song.songId)
                                    } catch (e: Exception) {
                                        Log.d(TAG, "exeception is ${e.message}")
                                    }
                                }
                                if (song.songStatus != SongStatus.PREMIUM) {
                                    val songBuilder = NextPlaySongBuilder().apply {
                                        this.currentSongModel = song
                                        this.songsList =
                                            mViewModel.albumSongsList as MutableList<Song>
                                        this.playedFrom = SongType.SEARCHED
                                    }
                                    EventBus.getDefault().post(songBuilder)
                                } else if (song.songStatus == SongStatus.PREMIUM) {
                                    val bundle = Bundle().apply {
                                        putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                                        putParcelableArrayList(
                                            CommonKeys.KEY_SONGS_LIST,
                                            mViewModel.albumSongsList as ArrayList<out Song>
                                        )
                                        putString(
                                            CommonKeys.KEY_SONG_TYPE,
                                            SongType.SEARCHED.value
                                        )
                                    }
                                    ActivityUtils.startPlayerActivity(activity, bundle)
                                }
                            } else if (myData.type == ResponseType.ARTIST) {
                                val artist = data as SearchedSongs
                                val bundle = Bundle()
                                bundle.putParcelable(
                                    CommonKeys.KEY_ARTIST_MODEL,
                                    artist.toArtistModel()
                                )

                                ActivityUtils.launchFragment(
                                    requireContext(),
                                    ArtistDetailFragment::class.java.name, bundle
                                )

                            } else {
                                val dataObj = data
                                val bundle = Bundle()
                                bundle.putParcelable(
                                    CommonKeys.KEY_ALBUM_DETAILS,
                                    dataObj.toAlbumModel()
                                )
                                ActivityUtils.launchFragment(
                                    requireContext(),
                                    AlbumDetailsFragment::class.java.name,
                                    bundle
                                )
                            }
                        }
                    }, mViewModel.searchedSongsDataList)

                mSearchAdapter
            }
            else -> {
                mLanguagesAdapter =
                    RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.rec_view_languages
                        }

                        override fun onNoDataFound() {
                            Log.e(TAG, "No Data Found")
                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            updateSelectedTabBackground(R.drawable.shape_bg_your_interest_recview)
                            val mLanguages = data as Language
                            lastLanguageId = mLanguages.languageId
                            setSelectedViewBackground()
                            mLanguages.setDownloadSelectedViewBackground(ObservableField(true))
                            if (!mViewModel.queryToSearch.isNullOrEmpty()) {
                                createRequestOrCallApi(
                                    mViewModel.queryToSearch,
                                    mLanguages.languageId
                                )
                            } else {
                                mViewModel.searchedSongsDataList.clear()
                                mSearchAdapter.notifyDataSetChanged()
                            }
                        }

                        override fun onViewClicked(view: View, data: Any?) {

                        }
                    }, mViewModel.searchedLanguagesForRecyclerView)

                mLanguagesAdapter
            }
        }
    }

    override fun showProgressBar() {
        mViewModel.searchedSongsDataList.clear()
        mBinding.noResultFound.visibility = View.INVISIBLE
        mBinding.slSearchedSongs.visibility = View.VISIBLE
        mBinding.slSearchedSongs.startShimmer()

    }

    override fun hideProgressBar() {
        mBinding.slSearchedSongs.visibility = View.GONE
        mBinding.slSearchedSongs.stopShimmer()
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(SearchQueryViewModel::class.java)
        mSongsAndArtistsViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    private fun initialization() {
        handler = Handler(Looper.getMainLooper())
        setUpRecyclerView(mBinding.recyclerViewSearch, AdapterType.SEARCHED_SONGS)
        setUpHorizentalRecyclerView(
            mBinding.recyclerLanguages,
            resources.getDimensionPixelSize(R.dimen.recycler_language_horizental_spacing_4),
            AdapterType.LANGUAGES
        )
        mBinding.etSearchBox.requestFocus()
        Utilities.showSoftKeyBoard(requireContext(), mBinding.etSearchBox)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setListeners() {
        mBinding.noDataFoundInc.noDataFoundBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
        mBinding.ivBackBtnQuery.setOnClickListener {
            requireActivity().onBackPressed()
        }
        mBinding.etSearchBox.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(query: CharSequence, start: Int, before: Int, count: Int) {
                handler.removeCallbacksAndMessages(null)
                if (query.isNotEmpty()) {
                    if (mViewModel.isSearchBoxEmpty) {
                        setSelectedViewBackground()
                        updateSelectedTabBackground(R.drawable.shape_bg_your_interest_selected)
                        mViewModel.isSearchBoxEmpty = false
                    }

                    mViewModel.queryToSearch = query.toString()
                    handler.postDelayed({
                        createRequestOrCallApi(mViewModel.queryToSearch, null)
                    }, Constants.SEARCH_API_DELAY)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().isEmpty()) {
                    mViewModel.queryToSearch = ""
                    mViewModel.isSearchBoxEmpty = true
                    if (mViewModel.searchedLanguagesForRecyclerView.isNullOrEmpty()) {
                        mBinding.btnAllSongs.visibility = View.INVISIBLE
                    }
                }
            }
        })

        mBinding.etSearchBox.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (v.text.toString().isNotEmpty()) {
                    mViewModel.queryToSearch = v.text.toString()
                    createRequestOrCallApi(mViewModel.queryToSearch, null)
                }
                return@OnEditorActionListener true
            }
            false
        })

        mBinding.btnAllSongs.setOnClickListener {
            lastLanguageId = 0
            setSelectedViewBackground()
            updateSelectedTabBackground(R.drawable.shape_bg_your_interest_selected)
            if (!mViewModel.queryToSearch.isNullOrEmpty()) {
                createRequestOrCallApi(mViewModel.queryToSearch, null)
            } else {
                mViewModel.searchedSongsDataList.clear()
                mSearchAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun updateSelectedTabBackground(background: Int) {
        mBinding.btnAllSongs.setBackgroundResource(background)
    }

    private fun createRequestOrCallApi(query: String?, languagesId: Int?) {
        val queryRequestModel = QueryRequestModel(query, languagesId)
        mSongsAndArtistsViewModel.getSearchedSongsFromServer(queryRequestModel)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObserverForViewModel() {
        mSongsAndArtistsViewModel.mSearchedSongResponse.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    mViewModel.searchedSongsDataList.clear()
                    mBinding.recyclerLanguages.visibility = View.VISIBLE
                    mBinding.recyclerViewSearch.visibility = View.VISIBLE
                    val data = it.t as ResponseModel
                    val mySongs = data.data.searchedSongs
                    val myLanguages = data.data.Languages
                    if (mViewModel.searchedLanguagesForRecyclerView.isNullOrEmpty()) {
                        if (myLanguages != null) {
                            for (items in myLanguages) {
                                mBinding.btnAllSongs.visibility = View.VISIBLE
                                val currentLanguageId = items.languageId
                                if (currentLanguageId == lastLanguageId) {
                                    items.setDownloadSelectedViewBackground(
                                        ObservableField<Boolean>(
                                            true
                                        )
                                    )
                                    mViewModel.searchedLanguagesDataList.add(items)
                                    mViewModel.searchedLanguagesForRecyclerView.add(items)
                                } else {
                                    items.setDownloadSelectedViewBackground(
                                        ObservableField<Boolean>(
                                            false
                                        )
                                    )
                                    mViewModel.searchedLanguagesDataList.add(items)
                                    mViewModel.searchedLanguagesForRecyclerView.add(items)
                                }
                            }
                            mLanguagesAdapter.notifyDataSetChanged()
                        }
                    } else {
                        mBinding.btnAllSongs.visibility = View.VISIBLE
                    }

                    if (!mySongs.isNullOrEmpty()) {
                        mViewModel.searchedSongsDataList.addAll(mySongs)
                        mSearchAdapter.notifyDataSetChanged()
                    } else {
                        mBinding.noResultFound.visibility = View.VISIBLE
                    }
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        })
    }

    private fun setSelectedViewBackground() {
        mViewModel.searchedLanguagesForRecyclerView.clear()
        for (i in mViewModel.searchedLanguagesDataList.indices) {
            mViewModel.searchedLanguagesDataList[i].setDownloadSelectedViewBackground(
                ObservableField(false)
            )
            mViewModel.searchedLanguagesForRecyclerView.add(mViewModel.searchedLanguagesDataList[i])
        }
    }

    companion object {
        private val TAG = "SearchQueryFragment"
    }
}