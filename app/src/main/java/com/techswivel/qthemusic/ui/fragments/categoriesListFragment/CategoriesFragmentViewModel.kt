package com.techswivel.qthemusic.ui.fragments.categoriesListFragment

import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class CategoriesFragmentViewModel : LocalDatabaseViewModel() {
    var categoriesList: MutableList<Any> = ArrayList()
    var categoriesListFromApi: MutableList<Category> = ArrayList()

}