package com.techswivel.qthemusic.ui.activities.mainActivity

import androidx.fragment.app.Fragment
import com.techswivel.qthemusic.customData.interfaces.BaseInterface

interface MaintActivityImp:BaseInterface {
    fun changeSelectedItemToCurrent(int: Int, fragment: Fragment)
    fun stopMiniPlayer()
}