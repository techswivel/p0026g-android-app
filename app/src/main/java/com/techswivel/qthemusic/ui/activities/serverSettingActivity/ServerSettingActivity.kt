package com.techswivel.qthemusic.ui.activities.serverSettingActivity

import android.annotation.SuppressLint
import android.content.pm.ApplicationInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.techswivel.qthemusic.BuildConfig
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.databinding.ActivityServerSettingBinding
import com.techswivel.qthemusic.helper.RemoteConfigrations.RemoteConfigSharePrefrence
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.ui.base.BaseActivity
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.Utilities
import java.util.*

class ServerSettingActivity : BaseActivity() {

    private var remoteConfigSharedPreferences: RemoteConfigSharePrefrence? = null
    private lateinit var mBinding: ActivityServerSettingBinding
    private lateinit var mViewModel: ServerSettingViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityServerSettingBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setUpActionBar(mBinding.activityToolbar.toolbar, "", false, isShowHome = true)
        val latSyncedTime = PrefUtils.getLong(this, CommonKeys.KEY_LAST_SYNC_TIME)
        val syncServiceuuid = PrefUtils.getString(this, CommonKeys.KEY_SYNC_SERVICE_UUID)
        mBinding.activityToolbar.toolbarTitle.visibility = View.VISIBLE
        mBinding.activityToolbar.toolbarTitle.text = resources.getString(R.string.server_setting)
        mBinding.activityToolbar.toolbar.setNavigationOnClickListener {
            this.finish()
        }
        initializeComponents()
        crashingImplementation()
        getAndSetBuildVersion()
        setUrlText()
        setMinimumVersion()
        clickListeners()
        if (latSyncedTime.compareTo(0) == 0) {
            mBinding.textLastSyncTime.text = "No Time Saved"
        } else {
            mBinding.textLastSyncTime.text =
                Utilities.timeStampConverter(latSyncedTime, "EEE, d MMM yyyy HH:mm:ss")
        }

        if (syncServiceuuid != null) {
            Log.e(TAG, "onCreate: sync all data id = $syncServiceuuid")
            WorkManager.getInstance(this).getWorkInfoByIdLiveData(UUID.fromString(syncServiceuuid))
                .observe(this) { workInfo ->
                    Log.e(TAG, "onCreate: sync status observer value ${workInfo.toString()}")
                    when (workInfo.state) {
                        WorkInfo.State.BLOCKED -> {
                            mBinding.textSyncAllDataStatus.text = "Sync Status : BLOCKED"
                            Toast.makeText(this, "Sync Status : BLOCKED", Toast.LENGTH_LONG).show()
                        }
                        WorkInfo.State.CANCELLED -> {
                            Toast.makeText(this, "Sync Status : CANCELLED", Toast.LENGTH_LONG)
                                .show()
                            mBinding.textSyncAllDataStatus.text = "Sync Status : CANCELLED"
                        }
                        WorkInfo.State.ENQUEUED -> {
                            Toast.makeText(this, "Sync Status : ENQUEUED", Toast.LENGTH_LONG).show()
                            mBinding.textSyncAllDataStatus.text =
                                "Sync Status : ENQUEUED"
                        }
                        WorkInfo.State.FAILED -> {
                            Toast.makeText(this, "Sync Status : FAILED", Toast.LENGTH_LONG).show()
                            mBinding.textSyncAllDataStatus.text = "Sync Status : FAILED"
                        }
                        WorkInfo.State.RUNNING -> {
                            Toast.makeText(this, "Sync Status : RUNNING", Toast.LENGTH_LONG).show()
                            mBinding.textSyncAllDataStatus.text = "Sync Status : RUNNING"
                        }
                        WorkInfo.State.SUCCEEDED -> {
                            Toast.makeText(this, "Sync Status : SUCCEEDED ", Toast.LENGTH_LONG)
                                .show()
                            PrefUtils.setString(
                                QTheMusicApplication.getContext(),
                                CommonKeys.KEY_SYNC_SERVICE_UUID,
                                workInfo.id.toString()
                            )
                            Log.e(TAG, "onCreate: new sync data uuid = ${workInfo.id}")
                            mBinding.textSyncAllDataStatus.text = "Sync Status : SUCCEEDED"
                        }
                        else -> {
                            Toast.makeText(this, "Sync Status : Unknown", Toast.LENGTH_LONG).show()
                            mBinding.textSyncAllDataStatus.text = "Sync Status : Unknown"
                        }
                    }
                }
        }
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {
        ActivityUtils.launchFragment(this, NoInternetFragment::class.java.name)
    }

    private fun setMinimumVersion() {
        val appInfo: ApplicationInfo =
            packageManager.getApplicationInfo(this.applicationInfo.packageName, 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mBinding.textMinimumVersion.text =
                getString(R.string.minimun_version).plus(" = ")
                    .plus(appInfo.minSdkVersion.toString()).plus(" (")
                    .plus(Utilities.getAndroidVersion(appInfo.minSdkVersion)).plus(")")
        } else {
            mBinding.textMinimumVersion.text =
                getString(R.string.minimun_version).plus(" = 16 (4.1.0)")
        }
    }

    private fun crashingImplementation() {
        if (BuildConfig.FLAVOR.equals(Constants.STAGING)) {
            mBinding.buttonCrash.visibility = View.VISIBLE
        } else {
            mBinding.buttonCrash.visibility = View.GONE
        }
        mBinding.buttonCrash.setOnClickListener {
            Constants.STAGING.toInt()
        }

    }

    private fun getAndSetBuildVersion() {
        val versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            packageManager.getPackageInfo(packageName, 0).longVersionCode
        } else {
            packageManager.getPackageInfo(packageName, 0).versionCode
        }
        val versionName = packageManager
            .getPackageInfo(packageName, 0).versionName
        val buildVersion =
            "Build Number:$versionCode\nVersion Name :$versionName"
        mBinding.textViewBuildVersion.text = buildVersion
    }

    private fun setUrlText() {
        when {
            BuildConfig.FLAVOR.equals(Constants.STAGING) -> {
                mBinding.textServerName.text = BuildConfig.FLAVOR.toUpperCase()
                mBinding.textServerName.visibility = View.INVISIBLE

            }
            BuildConfig.FLAVOR.equals(Constants.DEVELOPMENT) -> {
                mBinding.textServerName.text = BuildConfig.FLAVOR.toUpperCase()
                mBinding.textServerName.visibility = View.VISIBLE

            }
            BuildConfig.FLAVOR.equals(Constants.ACCEPTANCE) -> {
                mBinding.textServerName.text = BuildConfig.FLAVOR.toUpperCase()
                mBinding.textServerName.visibility = View.VISIBLE

            }
        }
    }

    private fun initializeComponents() {
        mViewModel = ViewModelProvider(this).get(ServerSettingViewModel::class.java)
        remoteConfigSharedPreferences = RemoteConfigSharePrefrence(this)
    }

    private fun clickListeners() {
        mBinding.tvStartService.setOnClickListener {
            mViewModel.startService()
        }
        mBinding.tvStopService.setOnClickListener {
            mViewModel.stopService()
        }
    }

    companion object {
        private val TAG = "ServerSettingActivity"
    }
}

