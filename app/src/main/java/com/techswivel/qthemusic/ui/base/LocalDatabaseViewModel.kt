package com.techswivel.qthemusic.ui.base

import androidx.lifecycle.LiveData
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.models.database.Song

open class LocalDatabaseViewModel : BaseViewModel() {
    //inserting single obj in database
    fun insertSongInDataBase(songs: Song) {
        mLocalDataManager.insertRecentPlayedSongToDatabase(songs)
    }

    fun insertAlbumInDataBase(album: Album) {
        mLocalDataManager.insertRecentPlayedAlbumToDatabase(album)
    }

    fun insertArtistInDataBase(artist: Artist) {
        mLocalDataManager.insertRecentPlayedArtistToDatabase(artist)
    }

    //fun for getting all  database
    fun getAllSongs(): LiveData<List<Song>> {
        return mLocalDataManager.getAllSongs()
    }

    fun getAllAlbums(): LiveData<List<Album>> {
        return mLocalDataManager.getAllAlbums()
    }

    fun getAllArtist(): LiveData<List<Artist>> {
        return mLocalDataManager.getAllArtist()
    }

    //fun for inserting lists in database
    fun insertSongsListInDatabase(songsList: List<Song>) {
        mLocalDataManager.insertSyncedSongsListToDatabase(songsList)
    }

    fun insertAlbumsListInDatabase(albumList: List<Album>) {
        mLocalDataManager.insertSyncedAlbumsListToDatabase(albumList)
    }

    fun insertArtistsListInDatabase(artistList: List<Artist>) {
        mLocalDataManager.insertSyncedArtistsListToDatabase(artistList)
    }

    fun insertCategoriesToDB(list: List<Category>) {
        mLocalDataManager.insertCategoriesToDatabase(list)
    }

    //getting categoryList from db
    fun getCategoriesDataFromDatabase(): LiveData<List<Category>> {
        return mLocalDataManager.getCategoriesFromDatabase()
    }

    fun insertDataInDataBaseForSync(
        songId: Int? = null,
        albumId: Int? = null,
        artistId: Int? = null
    ) {
        mLocalDataManager.insertDataForSync(songId, albumId, artistId)
    }
}