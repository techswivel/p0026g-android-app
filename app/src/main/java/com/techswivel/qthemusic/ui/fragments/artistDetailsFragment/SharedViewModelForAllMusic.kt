package com.techswivel.qthemusic.ui.fragments.artistDetailsFragment

import androidx.lifecycle.MutableLiveData
import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.ui.base.BaseViewModel

class SharedViewModelForAllMusic : BaseViewModel() {
    val mArtistData = MutableLiveData<ArtistDetails>()
    val isForNewApiCall = MutableLiveData<Boolean>(false)

    fun setArtistData(artistDetails: ArtistDetails) {
        mArtistData.value = artistDetails
    }

    fun prepareForSwipeToRefresh(boolean: Boolean) {
        isForNewApiCall.value = boolean
    }
}