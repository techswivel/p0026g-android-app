package com.techswivel.qthemusic.ui.dialogFragments.songType

import android.os.Bundle
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA

class SongTypeViewModel : BaseViewModel() {

    private lateinit var mSong: Song

    var mBundle: Bundle
        set(value) {
            mSong = value.getParcelable<Song>(KEY_DATA) as Song
        }
        get() {
            return Bundle.EMPTY
        }

    var mSongData: Song
        get() {
            return mSong
        }
        set(value) {
            mSong = value
        }
}