package com.techswivel.qthemusic.ui.activities.listeningHistoryActivity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.databinding.ActivityListeningHistoryBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.RecommendedSongsBodyBuilder
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.ui.fragments.listeningHistoryAlbumFragment.ListeningHistoryAlbumFragment
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.utils.*

class ListeningHistoryActivity : PlayerBaseActivity() {

    private lateinit var viewModel: ListeningHistoryActivityViewModel
    private var mFragment: Fragment? = null
    private lateinit var mBinding: ActivityListeningHistoryBinding
    private lateinit var mSongAndArtistsViewModel: SongAndArtistsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityListeningHistoryBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        initViewModel()
        setToolBar()
        viewModel.selectedTab = RecommendedSongsType.SONGS
        getSongsListeningFromDatabase()
        setObserver()
        clickListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerService?.isAddListeningActivity(false)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onProgressUpdate(timeInMis: Int) {
        mBinding.playLayout.seekBarPlayer.progress = timeInMis
    }

    override fun onPlayerStateIdle() {

    }

    override fun onPlayerStateBuffering() {

    }

    override fun onPlayerStateReady() {

    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {

    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                .isAudioPlayerPlaying()
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onCurrentSongUpdate(serviceBinder.currentSong)
        } else if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == false)
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
        serviceBinder?.isAddListeningActivity(true)
        setOnSwipeListener(mBinding.playLayout.playerParentLayout)
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        super.readyToShowVideoPlayerPlayIcons()
        if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_pause
                )
            )
        } else {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_play
                )
            )
        }
    }

    override fun onPlayerServiceStop() {
        mBinding.playLayout.playerParentLayout.visibility = View.GONE
    }

    override fun onPlayerServiceStart() {
        mBinding.playLayout.playerParentLayout.visibility = View.VISIBLE
    }

    override fun onProgressBarUpdate(current: Int, max: Int) {
        mBinding.playLayout.seekBarPlayer.max = max
        mBinding.playLayout.seekBarPlayer.progress = current
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        Log.e(TAG, "onPlayerStatusChanged: from listening activity ")
        if (data.isNotificationRemoved) {
            mBinding.playLayout.playerParentLayout.visibilityGone()
        } else if (data.isPlaying) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        } else {
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        }
    }

    override fun onCurrentSongUpdate(data: Song) {
        super.onCurrentSongUpdate(data)
        mBinding.playLayout.data = data
        mBinding.playLayout.executePendingBindings()
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {
        ActivityUtils.launchFragment(this, NoInternetFragment::class.java.name)
    }

    private fun getSongsListeningFromDatabase() {
        viewModel.getAllSongs().observe(this, Observer { dbSongsList ->
            openAlbumFragment(RecommendedSongsType.SONGS)
            if (dbSongsList.isNotEmpty()) {
                viewModel.mSongsDataList.clear()
                viewModel.mSongsDataList.addAll(dbSongsList)

            } else {
                getRecommendedSongs()
            }
        })
    }

    private fun getAlbumsListeningFromDatabase() {
        viewModel.getAllAlbums().observe(this, Observer { dbAlbumsList ->
            openAlbumFragment(RecommendedSongsType.ALBUM)
            if (dbAlbumsList.isNotEmpty()) {
                viewModel.mAlbumsDataList.clear()
                viewModel.mAlbumsDataList.addAll(dbAlbumsList)
            } else {
                getRecommendedAlbums()
            }
        })
    }

    private fun getArtistsListeningFromDatabase() {
        openAlbumFragment(RecommendedSongsType.ARTIST)
        viewModel.getAllArtist().observe(this, Observer { dbArtistsList ->
            if (dbArtistsList.isNotEmpty()) {
                viewModel.mArtistsDataList.clear()
                viewModel.mArtistsDataList.addAll(dbArtistsList)

            } else {
                getRecommendedArtists()
            }
        })
    }

    private fun setObserver() {
        mSongAndArtistsViewModel.recommendedSongsResponse.observe(this) { recommendedSongsDataResponse ->
            when (recommendedSongsDataResponse.status) {
                NetworkStatus.LOADING -> {
                    Log.v("TAG", "Loading")
                }
                NetworkStatus.SUCCESS -> {
                    viewModel.mSongsDataList.clear()
                    val response = recommendedSongsDataResponse.t as ResponseModel
                    val songsList = response.data.songList
                    val albumsList = response.data.albumList
                    val artistsList = response.data.artistList
                    if (!songsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.SONGS) {
                        viewModel.mSongsDataList.clear()
                        viewModel.mSongsDataList.addAll(songsList)
                        viewModel.insertSongsListInDatabase(songsList)
                        openAlbumFragment(RecommendedSongsType.SONGS)
                    } else if (!albumsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.ALBUM) {
                        viewModel.mAlbumsDataList.clear()
                        viewModel.mAlbumsDataList.addAll(albumsList)
                        viewModel.insertAlbumsListInDatabase(albumsList)
                        openAlbumFragment(RecommendedSongsType.ALBUM)
                    } else if (!artistsList.isNullOrEmpty() && viewModel.selectedTab == RecommendedSongsType.ARTIST) {
                        viewModel.mArtistsDataList.clear()
                        viewModel.mArtistsDataList.addAll(artistsList)
                        viewModel.insertArtistsListInDatabase(artistsList)
                        openAlbumFragment(RecommendedSongsType.ARTIST)
                    } else {
                        openAlbumFragment(viewModel.selectedTab ?: RecommendedSongsType.SONGS)
                    }
                    if (mBinding.swipeListeningHisContainer.isRefreshing) {
                        mBinding.swipeListeningHisContainer.isRefreshing = false
                        when (viewModel.selectedTab) {
                            RecommendedSongsType.SONGS -> {
                                getSongsListeningFromDatabase()

                            }
                            RecommendedSongsType.ALBUM -> {
                                getAlbumsListeningFromDatabase()
                            }
                            else -> {
                                getArtistsListeningFromDatabase()
                            }
                        }
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {

                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@ListeningHistoryActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    Log.v("TAG", "Completed")
                }
            }
        }
    }

    private fun openAlbumFragment(type: RecommendedSongsType) {
        val bundle = Bundle()
        when (type) {
            RecommendedSongsType.ALBUM -> {
                bundle.putParcelableArrayList(
                    CommonKeys.KEY_DATA,
                    viewModel.mAlbumsDataList as ArrayList<out Album>
                )
                bundle.putString(CommonKeys.KEY_TYPE, RecommendedSongsType.ALBUM.toString())
            }
            RecommendedSongsType.ARTIST -> {
                bundle.putParcelableArrayList(
                    CommonKeys.KEY_DATA,
                    viewModel.mArtistsDataList as ArrayList<out Artist>
                )
                bundle.putString(CommonKeys.KEY_TYPE, RecommendedSongsType.ARTIST.toString())
            }
            RecommendedSongsType.SONGS -> {
                bundle.putParcelableArrayList(
                    CommonKeys.KEY_DATA,
                    viewModel.mSongsDataList as ArrayList<out Song>
                )
                bundle.putString(CommonKeys.KEY_TYPE, RecommendedSongsType.SONGS.toString())
            }
        }
        popUpAllFragmentIncludeThis(ListeningHistoryAlbumFragment::class.java.name)
        openFragment(ListeningHistoryAlbumFragment.newInstance(bundle))
    }

    private fun setToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar, "", false, true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.listening_history)
    }

    private fun openFragment(fragment: Fragment) {
        ::mFragment.set(fragment)
        mFragment.let { fragmentInstance ->
            fragmentInstance?.let { fragmentToBeReplaced ->
                replaceFragment(mBinding.mainContainer.id, fragmentToBeReplaced)
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this)[ListeningHistoryActivityViewModel::class.java]
        mSongAndArtistsViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        when (viewModel.selectedTab) {
            RecommendedSongsType.SONGS -> {
                getRecommendedSongs()

            }
            RecommendedSongsType.ALBUM -> {
                getRecommendedAlbums()
            }
            else -> {
                getRecommendedArtists()
            }
        }
    }

    private fun clickListeners() {
        mBinding.swipeListeningHisContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()

        })

        mBinding.btnSongs.setOnClickListener {
            viewModel.cancelServerRequest()
            viewModel.selectedTab = RecommendedSongsType.SONGS
            updateSelectedTabBackground(
                mBinding.btnSongs,
                mBinding.btnAlbums,
                mBinding.btnArtists
            )
            getSongsListeningFromDatabase()
        }

        mBinding.btnAlbums.setOnClickListener {
            viewModel.cancelServerRequest()
            viewModel.selectedTab = RecommendedSongsType.ALBUM
            updateSelectedTabBackground(
                mBinding.btnAlbums,
                mBinding.btnSongs,
                mBinding.btnArtists
            )
            getAlbumsListeningFromDatabase()
        }

        mBinding.btnArtists.setOnClickListener {
            viewModel.cancelServerRequest()
            viewModel.selectedTab = RecommendedSongsType.ARTIST
            updateSelectedTabBackground(
                mBinding.btnArtists,
                mBinding.btnAlbums,
                mBinding.btnSongs
            )
            getArtistsListeningFromDatabase()
        }
    }

    private fun getRecommendedArtists() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.HISTORY
        recommendedSongsBuilder.dataType = RecommendedSongsType.ARTIST
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        mSongAndArtistsViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun getRecommendedAlbums() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.HISTORY
        recommendedSongsBuilder.dataType = RecommendedSongsType.ALBUM
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        mSongAndArtistsViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun getRecommendedSongs() {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.HISTORY
        recommendedSongsBuilder.dataType = RecommendedSongsType.SONGS
        viewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        mSongAndArtistsViewModel.getRecommendedSongsDataFromServer(viewModel.recommendedSongsBodyModel)
    }

    private fun updateSelectedTabBackground(
        selectedTab: Button,
        unselectedTab1: Button,
        unselectedTab2: Button
    ) {
        selectedTab.setBackgroundResource(R.drawable.selected_tab_background)
        unselectedTab1.setBackgroundResource(R.drawable.unselected_tab_background)
        unselectedTab2.setBackgroundResource(R.drawable.unselected_tab_background)
    }

    companion object {
        private const val TAG = "ListeningHistoryActivit"
    }
}