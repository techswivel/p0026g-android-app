package com.techswivel.qthemusic.ui.fragments.artistDetailsFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.dfaktfahrerapp.ui.fragments.underDevelopmentMessageFragment.UnderDevelopmentMessageFragment
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.adapter.ViewPagerAdapter
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.databinding.FragmentArtistDetailBinding
import com.techswivel.qthemusic.models.ArtistBuilder
import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.fragments.aboutArtist.AboutArtist
import com.techswivel.qthemusic.ui.fragments.allMusicFragment.AllMusicFragment
import com.techswivel.qthemusic.utils.*
import org.greenrobot.eventbus.EventBus


class ArtistDetailFragment : Fragment(), ArtistDetailsImp {
    private lateinit var mBinding: FragmentArtistDetailBinding
    private lateinit var mViewModel: ArtistDetailsViewModel
    private lateinit var mNetworkViewModel: SongAndArtistsViewModel
    private lateinit var mShareViewModel: SharedViewModelForAllMusic
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        mViewModel.artistData =
            arguments?.getParcelable<Artist>(CommonKeys.KEY_ARTIST_MODEL) as Artist
        callApi()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentArtistDetailBinding.inflate(layoutInflater, container, false)
        observeArtistDetails()
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewPager()
        clickListeners()
        swipeToRefresh()
    }

    override fun showProgressBar() {
        mBinding.topView.visibilityInVisible()
        mBinding.artistDetailShimmer.visibilityVisible()
        mBinding.artistDetailShimmer.startShimmer()
    }

    override fun hideProgressBar() {
        mBinding.artistDetailShimmer.stopShimmer()
        mBinding.artistDetailShimmer.visibility = View.GONE
        mBinding.topView.visibility = View.VISIBLE
        if (mBinding.swipeArtistDetailsContainer.isRefreshing) {
            mBinding.swipeArtistDetailsContainer.isRefreshing = false
        }

    }

    private fun clickListeners() {
        mBinding.tvPlayAllSongsArtistDetails.setOnClickListener {

            var song: Song = Song()
            for (i in mViewModel.songsList.indices) {
                if (mViewModel.songsList[i]?.songStatus != SongStatus.PREMIUM) {
                    song = mViewModel.songsList[i]!!

                    break
                }
            }
            if (song.songId != null) {
                val songBuilder = NextPlaySongBuilder().apply {
                    this.currentSongModel = song
                    this.songsList = mViewModel.songsList as MutableList<Song>
                    this.playedFrom = SongType.ARTIST_ALBUM
                }
                EventBus.getDefault().post(songBuilder)

            } else {
                Utilities.showToast(requireContext(), getString(R.string.no_free_song_availble))
            }
        }
        mBinding.followClickView.setOnClickListener {
            mBinding.followClickView.visibility = View.INVISIBLE
            if (mBinding.tvFollowTag.visibility == View.VISIBLE) {
                requestToChangeArtistFollowingStatus(mViewModel.artistData.artistId, true)
            } else {
                requestToChangeArtistFollowingStatus(mViewModel.artistData.artistId, false)
            }
        }
        mBinding.btnBackArtistDetails.setOnClickListener {
            requireActivity().onBackPressed()
        }
        mBinding.premiumLayoutArtistDetailsMain.setOnClickListener {
            ActivityUtils.launchFragment(
                requireContext(),
                UnderDevelopmentMessageFragment::class.java.name
            )
        }
    }

    private fun setUpViewPager() {
        val bundle = Bundle()
        bundle.putParcelable(CommonKeys.KEY_DATA, mViewModel.artistData)
        val allMusicFragment = AllMusicFragment()
        allMusicFragment.arguments = bundle

        mViewModel.fragmentsList.add(allMusicFragment)
        mViewModel.fragmentsList.add(AboutArtist.newInstance())
        mViewModel.fragmentTittlesList.add(getString(R.string.all_music_tab_1))
        mViewModel.fragmentTittlesList.add(getString(R.string.about_artist_tab_2))
        val mViewPagerAdapter =
            ViewPagerAdapter(
                childFragmentManager,
                mViewModel.fragmentsList,
                mViewModel.fragmentTittlesList
            )
        mBinding.viewPagerArtistDetails.adapter = mViewPagerAdapter
        mBinding.tabLayoutArtistDetails.setupWithViewPager(mBinding.viewPagerArtistDetails)
    }

    private fun animateViewForFollow() {
        mBinding.tvFollowTag.visibility = View.INVISIBLE
        mBinding.tvFollowingTag.visibility = View.VISIBLE
        val animationUtils =
            AnimationUtils.loadAnimation(requireContext(), R.anim.right_to_left_follow_btn)
        mBinding.tvFollowingTag.animation = animationUtils
        animationUtils.start()
    }

    private fun animateViewForUnFollow() {
        mBinding.tvFollowTag.visibility = View.VISIBLE
        mBinding.tvFollowingTag.visibility = View.INVISIBLE
        val animationUtils =
            AnimationUtils.loadAnimation(requireContext(), R.anim.left_to_right_follow_btn)
        mBinding.tvFollowTag.animation = animationUtils
        animationUtils.start()
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(ArtistDetailsViewModel::class.java)
        mNetworkViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
        mShareViewModel =
            ViewModelProvider(requireActivity()).get(SharedViewModelForAllMusic::class.java)
    }

    private fun observeArtistDetails() {
        mNetworkViewModel.mArtistDetailsResponse.observe(
            viewLifecycleOwner,
            Observer { artistData ->

                when (artistData.status) {
                    NetworkStatus.LOADING -> {
                        showProgressBar()
                    }
                    NetworkStatus.SUCCESS -> {
                        val response = artistData.t as ResponseModel
                        val data = response.data.artistDetails
                        if (data != null) {
                            data.songs?.let { songs ->
                                mViewModel.songsList.addAll(songs)
                            }
                            bindViewModel(data)
                            hideProgressBar()
                            mShareViewModel.setArtistData(data)
                        }
                    }
                    NetworkStatus.ERROR -> {
                        hideProgressBar()
                        val error = artistData.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.EXPIRE -> {
                        hideProgressBar()
                        val error = artistData.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    mViewModel.clearAppSession(requireActivity())
                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                }
            })

        mNetworkViewModel.mArtistFollowingResponse.observe(
            viewLifecycleOwner,
            Observer { followingResponse ->
                when (followingResponse.status) {
                    NetworkStatus.LOADING -> {
                        mBinding.pbFollow.visibility = View.VISIBLE
                    }
                    NetworkStatus.SUCCESS -> {
                        mBinding.pbFollow.visibility = View.INVISIBLE
                        if (mBinding.tvFollowTag.visibility == View.VISIBLE) {
                            animateViewForFollow()
                            mBinding.followClickView.visibility = View.VISIBLE
                        } else {
                            animateViewForUnFollow()
                            mBinding.followClickView.visibility = View.VISIBLE
                        }
                    }
                    NetworkStatus.ERROR -> {
                        mBinding.pbFollow.visibility = View.INVISIBLE
                        DialogUtils.runTimeAlert(context ?: requireContext(),
                            getString(R.string.error),
                            followingResponse.error?.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.EXPIRE -> {
                        mBinding.pbFollow.visibility = View.INVISIBLE
                        DialogUtils.runTimeAlert(context ?: requireContext(),
                            getString(R.string.error),
                            followingResponse.error?.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                }
            })
    }

    private fun requestToChangeArtistFollowingStatus(mArtistId: Int?, followingStatus: Boolean?) {
        val artistBuilder = ArtistBuilder()
        artistBuilder.artistId = mArtistId
        artistBuilder.isFollowed = followingStatus
        val artistModel = ArtistBuilder.build(artistBuilder)
        mNetworkViewModel.artistFollowingStatus(artistModel)
    }

    private fun callApi() {
        mNetworkViewModel.getArtistDataFromServer(mViewModel.artistData.artistId)
    }

    private fun bindViewModel(artistDetails: ArtistDetails) {
        mBinding.artistData = artistDetails
        mBinding.executePendingBindings()
    }

    private fun swipeToRefresh() {
        mBinding.swipeArtistDetailsContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        showProgressBar()
        mShareViewModel.prepareForSwipeToRefresh(true)
        callApi()
    }

    companion object {
        private val TAG = "ArtistDetailFragment"
        fun newInstance() = ArtistDetailFragment()
    }

}