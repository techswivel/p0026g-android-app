package com.techswivel.qthemusic.ui.wrapper

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.databinding.ActivityFrameBinding
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_FRAGMENT
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.visibilityGone
import com.techswivel.qthemusic.utils.visibilityVisible


class FrameActivity : PlayerBaseActivity(), ActivityWrapperCallBacks {

    companion object {
        val TAG: String = FrameActivity::class.java.name
    }

    private lateinit var mFragment: Fragment
    private lateinit var mBinding: ActivityFrameBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityFrameBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        val intent = intent
        val fragmentName = intent.getStringExtra(KEY_FRAGMENT)
        if (TextUtils.isEmpty(fragmentName)) {
            throw IllegalStateException("Fragment Name is Null")
        }
        var bundle = intent.getBundleExtra(KEY_DATA)
        if (bundle == null) {
            bundle = Bundle.EMPTY
        }
        val fragmentManager = supportFragmentManager
        mFragment = fragmentManager.fragmentFactory.instantiate(
            ClassLoader.getSystemClassLoader(),
            fragmentName!!
        )
        mFragment.arguments = bundle
        fragmentManager.beginTransaction().replace(
            mBinding.container.id,
            mFragment
        )
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerService?.isAddFrameActivity(
            false,
            intent.getStringExtra(KEY_FRAGMENT),
            intent.getBundleExtra(KEY_DATA)
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setStatusBarColor(color: Int, statusBarTextColor: Int) {
        // changeStatusBarColorTo(R.color.colorTitle, View.SYSTEM_UI_FLAG_VISIBLE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (::mFragment.isInitialized) {
            mFragment.onActivityResult(requestCode, resultCode, data)
        } else {
            Log.e(TAG, "Fragment is not Initialized")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (::mFragment.isInitialized) {
            mFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        } else {
            Log.e(TAG, "Fragment is not Initialized")
        }
    }

    override fun onProgressUpdate(timeInMis: Int) {
        mBinding.playLayout.seekBarPlayer.progress = timeInMis
    }

    override fun onPlayerStateIdle() {

    }

    override fun onPlayerStateBuffering() {

    }

    override fun onPlayerStateReady() {

    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {

    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                .isAudioPlayerPlaying()
        ) {
            val animation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top)
            mBinding.playLayout.playerParentLayout.startAnimation(animation)
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onCurrentSongUpdate(serviceBinder.currentSong)
        } else if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == false)
        ) {
            val animation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top)
            mBinding.playLayout.playerParentLayout.startAnimation(animation)
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
        serviceBinder?.isAddFrameActivity(
            true,
            intent.getStringExtra(KEY_FRAGMENT),
            intent.getBundleExtra(KEY_DATA)
        )
        setOnSwipeListener(mBinding.playLayout.playerParentLayout)
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        super.readyToShowVideoPlayerPlayIcons()
        if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_pause
                )
            )
        } else {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_play
                )
            )
        }
    }

    override fun onPlayerServiceStop() {
        mBinding.playLayout.playerParentLayout.visibility = View.GONE
    }

    override fun onPlayerServiceStart() {
        mBinding.playLayout.playerParentLayout.visibility = View.VISIBLE
    }


    override fun onProgressBarUpdate(current: Int, max: Int) {
        mBinding.playLayout.seekBarPlayer.max = max
        mBinding.playLayout.seekBarPlayer.progress = current
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        Log.d(TAG, "onPlayerStatusChanged called")
        if (data.isNotificationRemoved) {
            mBinding.playLayout.playerParentLayout.visibilityGone()
        } else if (data.isPlaying) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        } else {
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        }
        Log.e(TAG, "onPlayerStatusChanged: frame activity ie update received ")
    }

    override fun onCurrentSongUpdate(data: Song) {
        super.onCurrentSongUpdate(data)
        Log.d(TAG, "onCurrentSongUpdate called ${data.songTitle}")
        mBinding.playLayout.data = data
        mBinding.playLayout.executePendingBindings()
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {

    }
}
