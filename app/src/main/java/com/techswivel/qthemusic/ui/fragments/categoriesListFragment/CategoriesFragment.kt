package com.techswivel.qthemusic.ui.fragments.categoriesListFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentCategoriesBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.services.NetworkChangeReceiver
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.mainActivity.MaintActivityImp
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.categoriesDetailsFragment.CategoriesDetialsFragment
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.DialogUtils
import com.techswivel.qthemusic.utils.Log

class CategoriesFragment : RecyclerViewBaseFragment(), BaseInterface {
    private lateinit var mBinding: FragmentCategoriesBinding
    private lateinit var mViewModel: CategoriesFragmentViewModel
    private lateinit var mCategoriesAdapter: RecyclerViewAdapter
    private lateinit var mSongsAndArtistsViewModel: SongAndArtistsViewModel
    private var isInternetAvailable = true
    lateinit var connectionLiveData: NetworkChangeReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        connectionLiveData = NetworkChangeReceiver(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentCategoriesBinding.inflate(layoutInflater, container, false)
        mBinding.categoriesShimmer.startShimmer()
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApi()
        setUpRecyclerView()
        networkStateObserver()
        observeCategoriesDataFromApi()
        observeCategoriesDataFromDatabase()
        swipeToRefresh()
    }


    override fun onResume() {
        super.onResume()
        (mActivityListener as MaintActivityImp).changeSelectedItemToCurrent(
            Constants.CATEGORY_NAV_ID,
            CategoriesFragment()
        )
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mCategoriesAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        mCategoriesAdapter =
            RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                    return R.layout.rec_view_categories_all_list
                }

                override fun onItemClick(data: Any?, position: Int) {
                    super.onItemClick(data, position)
                    val mCategories = data as Category
                    val bundle = Bundle()
                    bundle.putParcelable(CommonKeys.KEY_DATA, mCategories)
                    if (isInternetAvailable) {
                        ActivityUtils.launchFragment(
                            requireContext(),
                            CategoriesDetialsFragment::class.java.name,
                            bundle
                        )
                    } else {
                        ActivityUtils.launchFragment(
                            requireContext(),
                            NoInternetFragment::class.java.name
                        )
                    }
                }

                override fun onNoDataFound() {

                }
            }, mViewModel.categoriesList)
        return mCategoriesAdapter
    }

    override fun showProgressBar() {
        mBinding.categoriesShimmer.visibility = View.VISIBLE
        mBinding.categoriesShimmer.startShimmer()
    }

    override fun hideProgressBar() {
        mBinding.categoriesShimmer.visibility = View.GONE
        mBinding.categoriesShimmer.stopShimmer()
    }

    private fun setUpRecyclerView() {
        setUpGridRecyclerView(
            mBinding.recViewCategories,
            Constants.NUMBER_OF_COLUMN_CATEGORIES_VIEW,
            resources.getDimensionPixelSize(R.dimen._4dp),
            resources.getDimensionPixelSize(R.dimen._4dp),
            AdapterType.RECOMMENDED_FOR_YOU
        )
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(CategoriesFragmentViewModel::class.java)
        mSongsAndArtistsViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun observeCategoriesDataFromDatabase() {
        mViewModel.getCategoriesDataFromDatabase()
            .observe(viewLifecycleOwner, Observer { dbCategoriesList ->
                if (dbCategoriesList.isEmpty()) {
                    showProgressBar()
                } else {
                    hideProgressBar()
                    mViewModel.categoriesList.clear()
                    mViewModel.categoriesList.addAll(dbCategoriesList)
                    mCategoriesAdapter.notifyDataSetChanged()
                }
            })
    }

    private fun observeCategoriesDataFromApi() {
        mSongsAndArtistsViewModel.categoriesResponse.observe(
            viewLifecycleOwner,
            Observer { categoriesResponse ->
                when (categoriesResponse.status) {
                    NetworkStatus.LOADING -> {

                    }
                    NetworkStatus.SUCCESS -> {
                        if (mBinding.swipeCategoriesContainer.isRefreshing) {
                            mBinding.swipeCategoriesContainer.isRefreshing = false
                        }
                        val responseModel = categoriesResponse.t as ResponseModel
                        mViewModel.categoriesListFromApi.clear()

                        val data = responseModel.data.category
                        if (data != null) {
                            try {
                                mViewModel.categoriesListFromApi.addAll(data)
                                mViewModel.insertCategoriesToDB(mViewModel.categoriesListFromApi)
                            } catch (e: Exception) {
                                Log.d(TAG, "error is ${e.message}")
                            }
                        }
                    }
                    NetworkStatus.ERROR -> {
                        val error = categoriesResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.EXPIRE -> {
                        val error = categoriesResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    mViewModel.clearAppSession(requireActivity())
                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                }
            })
    }

    private fun callApi() {
        mSongsAndArtistsViewModel.getCategoriesDataFromServer(CategoryType.ALL)
    }

    private fun swipeToRefresh() {
        mBinding.swipeCategoriesContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        mViewModel.categoriesListFromApi.clear()
        mViewModel.categoriesList.clear()
        mCategoriesAdapter.notifyDataSetChanged()
        callApi()
        showProgressBar()
    }

    private fun networkStateObserver() {
        connectionLiveData.observe(viewLifecycleOwner) { isConnected ->
            isConnected?.let {
                isInternetAvailable = it
            }
        }
    }

    companion object {
        val TAG = "CategoriesFragment"

        @JvmStatic
        fun newInstance() = CategoriesFragment()
    }

}