package com.techswivel.qthemusic.ui.dialogFragments.unAuthorizedDialogFragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.techswivel.qthemusic.databinding.UnAuthorizedErrorLayoutBinding
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.ui.activities.authActivity.AuthActivity
import com.techswivel.qthemusic.utils.CommonKeys

class UnAuthorizedDialog(private vararg val unAuthorizedError: String?) :
    BottomSheetDialogFragment() {

    private lateinit var mBinding: UnAuthorizedErrorLayoutBinding

    private lateinit var mViewModel: UnAuthorizedViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        mBinding = UnAuthorizedErrorLayoutBinding.inflate(layoutInflater, container, false)
        mViewModel = ViewModelProvider(this).get(UnAuthorizedViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCancelable(false)
        unAuthorizedError.forEach { errors ->
            mBinding.errorMsgForDialog.text = errors
        }
        clickListeners()
    }

    private fun clickListeners() {
        mBinding.btnErrorDismiss.setOnClickListener {
            PrefUtils.clearAllPrefData(requireContext())
            PrefUtils.setBoolean(requireContext(), CommonKeys.KEY_IS_INTEREST_SET, true)
            val intent = Intent(requireContext(), AuthActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            requireActivity().finish()
            mViewModel.clearAppSession(requireActivity())
            dialog?.dismiss()
        }
    }
}