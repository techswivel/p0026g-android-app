package com.techswivel.qthemusic.ui.fragments.followingArtistFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentFollowingArtistBinding
import com.techswivel.qthemusic.models.ArtistModel
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.ArtistDetailFragment
import com.techswivel.qthemusic.utils.*


class FollowingArtistFragment : RecyclerViewBaseFragment(), BaseInterface,
    RecyclerViewAdapter.CallBack {

    private lateinit var mBinding: FragmentFollowingArtistBinding
    private lateinit var viewModel: FollowingArtistViewModel
    private lateinit var songAndArtistViewModel: SongAndArtistsViewModel
    private lateinit var followingArtistAdapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentFollowingArtistBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setUpToolBar()
        setUpAdapter()
        getFollowingArtistsFromServer()
        swipeToRefresh()
        setObserver()
        clickListeners()
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return followingArtistAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return followingArtistAdapter
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_following_artist
    }

    override fun onNoDataFound() {
    }

    override fun showProgressBar() {
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.progressBar.visibility = View.GONE
    }

    override fun onViewClicked(view: View, data: Any?) {
        super.onViewClicked(view, data)
        val artist = data as Artist
        viewModel.artist = artist
        viewModel.artist?.artistId?.let { artist_id ->
            val artistModel = ArtistModel(artist_id, false)
            songAndArtistViewModel.artistFollowingStatus(artistModel)
        }
    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
        viewModel.artist = data as Artist
        val bundle = Bundle()
        bundle.putParcelable(CommonKeys.KEY_ARTIST_MODEL, viewModel.artist)
        ActivityUtils.launchFragment(
            requireContext(),
            ArtistDetailFragment::class.java.name, bundle
        )
    }


    private fun clickListeners() {
        mBinding.noDataFoundInc.noDataFoundBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObserver() {
        songAndArtistViewModel.mArtistFollowingResponse.observe(viewLifecycleOwner) { artistFollowResponse ->
            when (artistFollowResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    val index = viewModel.followingArtistList.indexOf(viewModel.artist as Artist)
                    viewModel.followingArtistList.remove(viewModel.artist as Artist)
                    if (viewModel.followingArtistList.size == 0) {
                        mBinding.tvNoDataFound.visibility = View.VISIBLE
                    } else {
                        mBinding.tvNoDataFound.visibility = View.GONE
                    }
                    followingArtistAdapter.notifyItemRemoved(index)
                    Toast.makeText(
                        QTheMusicApplication.getContext(),
                        getString(R.string.artist_unfollow),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = artistFollowResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = artistFollowResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }

        songAndArtistViewModel.followingArtistResponse.observe(viewLifecycleOwner) { followingArtistResponse ->
            when (followingArtistResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.tvNoDataFound.visibilityInVisible()
                    mBinding.shimmerLayout.visibilityVisible()
                    mBinding.shimmerLayout.startShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    if (mBinding.swipeFollowingArtistContainer.isRefreshing) {
                        mBinding.swipeFollowingArtistContainer.isRefreshing = false
                    }
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibilityGone()
                    viewModel.followingArtistList.clear()
                    val response = followingArtistResponse.t as ResponseModel
                    val artistList = response.data.artistList

                    if (!artistList.isNullOrEmpty()) {
                        viewModel.followingArtistList.addAll(artistList)
                    } else {
                        mBinding.tvNoDataFound.visibilityVisible()
                    }
                    if (::followingArtistAdapter.isInitialized)
                        followingArtistAdapter.notifyItemRangeInserted(
                            0,
                            viewModel.followingArtistList.size - 1
                        )
                }
                NetworkStatus.ERROR -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                    val error = followingArtistResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                    val error = followingArtistResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                }
            }
        }
    }

    private fun getFollowingArtistsFromServer() {
        songAndArtistViewModel.getFollowingArtist()
    }

    private fun setUpAdapter() {
        followingArtistAdapter = RecyclerViewAdapter(this, viewModel.followingArtistList)
        setUpRecyclerView(
            mBinding.recyclerviewFollowingArtist
        )
    }

    private fun setUpToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar,
            "",
            true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.following_artist)
    }

    private fun swipeToRefresh() {
        mBinding.swipeFollowingArtistContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        viewModel.followingArtistList.clear()
        followingArtistAdapter.notifyDataSetChanged()
        getFollowingArtistsFromServer()
    }

    private fun initViewModel() {
        viewModel =
            ViewModelProvider(this).get(FollowingArtistViewModel::class.java)

        songAndArtistViewModel =
            ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

}