package com.techswivel.qthemusic.ui.activities.playerActivity

import android.app.Activity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.ExoPlayer
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.customData.interfaces.PurchasingMediaCallback
import com.techswivel.qthemusic.databinding.ActivityPlayerBinding
import com.techswivel.qthemusic.helper.PlayerHelper
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.ui.dialogFragments.playListDialog.PlaylistDialogueFragment
import com.techswivel.qthemusic.ui.dialogFragments.progressDialogFragment.ProgressDialogFragment
import com.techswivel.qthemusic.ui.dialogFragments.songType.SongTypeFragment
import com.techswivel.qthemusic.ui.dialogFragments.unlockFragment.UnLockFragment
import com.techswivel.qthemusic.utils.*
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_PLAYLIST_ID
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_SONG_TYPE
import com.techswivel.qthemusic.utils.Utilities.formatSongDuration
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit


/**
 * */
class PlayerActivity : PlayerBaseActivity(), PlayerActivityImp, PurchasingMediaCallback {

    private lateinit var binding: ActivityPlayerBinding
    private lateinit var viewModel: PlayerActivityViewModel
    private lateinit var mSongAndArtistsViewModel: SongAndArtistsViewModel
    private lateinit var mProfileNetworkViewModel: ProfileNetworkViewModel
    private lateinit var videoPlayerPlayIcon: ImageView
    private lateinit var videoPlayerPauseIcon: ImageView
    private lateinit var videoPlayerBigPlayIcon: ImageView
    private lateinit var videoPlayerBigPauseIcon: ImageView
    private lateinit var mPlayer: PlayerHelper
    private lateinit var mProgressDialog: ProgressDialogFragment
    private lateinit var mCurrentAcitvity: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[PlayerActivityViewModel::class.java]
        mSongAndArtistsViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
        mProfileNetworkViewModel = ViewModelProvider(this)[ProfileNetworkViewModel::class.java]
        getDataFromBundles()
        mCurrentAcitvity = this
        if (viewModel.isFromService.not()) {
            initializePlayers()
        }
        initViews()
        setListeners()
        setNetworkObserver()
        getFavoriteSongsFromServer()

    }

    override fun onResume() {
        super.onResume()
        viewModel.isOnForground = true
    }

    override fun onStop() {
        super.onStop()
        stopPlayer()
        viewModel.isOnForground = false

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.null_transition, R.anim.bottom_down)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (viewModel.isFromService.not()) {
            releasePlayers()
            if (viewModel.isFromPurchasedResponse) {
                viewModel.isFromPurchasedResponse = false
                EventBus.getDefault().post(viewModel.mSongBuilder)
            }
        }
    }

    override fun onPlayerServiceStop() {
        mPlayerService?.getExoPlayerInstance()?.stopPlayer()
    }

    override fun onPlayerServiceStart() {
        mPlayerService?.getExoPlayerInstance()?.releasePlayers()
    }

    override fun onProgressBarUpdate(current: Int, max: Int) {
        if (::viewModel.isInitialized && viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {
            binding.sbAudioPlayer.max = max
            binding.sbAudioPlayer.progress = current
            onCurrentDurationUpdate(formatSongDuration(current.toLong()))
            binding.maxSongDuration.text = formatSongDuration(getPlayerInstance().currentDuration())
        }
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        if (::viewModel.isInitialized && viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {

            viewModel.mSongBuilder = mPlayerService?.getNextSongBuilder() ?: NextPlaySongBuilder()

            if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
                binding.ivPlayPause.setImageResource(R.drawable.ic_red_pause)
            } else {
                binding.ivPlayPause.setImageResource(R.drawable.ic_red_play)
            }
            runBlocking {
                val savedState = viewModel.getSavedState()
                savedState.currentSongModel = data.currentSong
                viewModel.savedPlayerState(savedState.toNextSongBuilder())
            }
        }
    }


    override fun showPlayerProgressBar() {
        super.showPlayerProgressBar()
        if (viewModel.isOnForground) {
            showLoadingProgressDialog()
        }

    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (::viewModel.isInitialized) {
            serviceBinder?.setPlayerBinderCallback(this)
            if (viewModel.mSongBuilder.playedFrom == SongType.NOTIFICATION) {
                serviceBinder?.getExoPlayerInstance()?.paused()
                mPlayerService = null
                unbindServiceWithTheActivity()
            } else if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                    .isPlayerRelease().not()
                && viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM
            ) {
                viewModel.mSongBuilder = mPlayerSongs
                getPlayerInstance().setVideoPlayer(binding.videoPlayer)
                readyToShowVideoPlayerPlayIcons()
                onCurrentDurationUpdate(
                    formatSongDuration(
                        serviceBinder.getExoPlayerInstance().currentTimeOfSong()
                    )
                )
                onMaxSongDuration(
                    serviceBinder.getExoPlayerInstance().currentDuration().toInt()
                )
                onProgressUpdate(
                    serviceBinder.getExoPlayerInstance().currentTimeOfSong().toInt()
                )
                viewModel.mSongBuilder = serviceBinder.getNextSongBuilder()
                if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
                    binding.ivPlayPause.setImageResource(R.drawable.ic_red_pause)
                } else {
                    binding.ivPlayPause.setImageResource(R.drawable.ic_red_play)
                }
            } else if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                    .isPlayerRelease().not()
                && viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.PREMIUM
            ) {
                serviceBinder.getExoPlayerInstance().paused()
                mPlayerService = null
            } else {
                mPlayerService = null
            }
        }
    }

    override fun showFavProgressBar() {

        binding.ivEmptyHeart.setVisibilityInMotionLayout(View.GONE)
        binding.ivMenuEmptyHeart.visibility = View.GONE
        binding.ivMenuProgressHeart.visibility = View.VISIBLE
        binding.favoriteProgressBar.setVisibilityInMotionLayout(View.VISIBLE)
    }

    override fun hideFavProgressBar() {

        binding.ivEmptyHeart.setVisibilityInMotionLayout(View.VISIBLE)
        binding.ivMenuEmptyHeart.visibility = View.VISIBLE
        binding.ivMenuProgressHeart.visibility = View.GONE
        binding.favoriteProgressBar.setVisibilityInMotionLayout(View.GONE)
    }

    override fun showProgressBar() {
    }

    override fun hideProgressBar() {
    }

    override fun updateUI(song: Song) {
        super.updateUI(song)
        viewModel.mSongBuilder.currentSongModel = song
        prepareForSongChange()
    }

    override fun onProgressUpdate(timeInMis: Int) {
        binding.sbAudioPlayer.progress = timeInMis
    }

    override fun onCurrentDurationUpdate(time: String) {
        binding.songCurrentDuration.text = time
        if (viewModel.mSongBuilder.currentSongModel?.songVideoUrl == null) {
            binding.toggleButtonGroup.setVisibilityInMotionLayout(View.INVISIBLE)
        } else {
            binding.toggleButtonGroup.setVisibilityInMotionLayout(View.VISIBLE)
        }
    }

    override fun onMaxSongDuration(time: Int) {
        binding.sbAudioPlayer.max = time
    }

    override fun onMaxSongDuration(time: String) {
        binding.maxSongDuration.text = time
    }

    override fun onPlayerStateIdle() {
    }

    override fun onPlayerStateBuffering() {
    }

    override fun onPlayerStateReady() {
        if (::mProgressDialog.isInitialized) {
            mProgressDialog.dismiss()
        }
        if (viewModel.isSongPause) {
            getPlayerInstance().paused()
            resetPlayerDurationAndSeekBar()
        }
    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {
        if (albumStatus == AlbumStatus.PREMIUM) {
            val unLockFragment =
                UnLockFragment.newInstance(
                    type = UnlockType.ALBUM,
                    album = viewModel.mSongBuilder.currentAlbumModel,
                    purchasingMediaCallback = this
                )
            unLockFragment.show(
                supportFragmentManager,
                UnLockFragment::class.java.simpleName
            )
        } else if (songStatus == SongStatus.PREMIUM) {
            val unLockFragment =
                UnLockFragment.newInstance(
                    type = UnlockType.SONG,
                    song = viewModel.mSongBuilder.currentSongModel,
                    purchasingMediaCallback = this
                )
            unLockFragment.show(
                supportFragmentManager,
                UnLockFragment::class.java.simpleName
            )
            releasePlayers()
            initializePlayers()
            binding.ivPlayPause.setImageResource(R.drawable.ic_red_play)
            getPlayerInstance().paused()
            resetPlayerDurationAndSeekBar()
        } else {
            playNextSong()
        }
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        showVideoPlayerPauseIcons()
    }

    override fun onNextSongsList(songsList: List<Song>?) {
        viewModel.mSongBuilder.songsList?.addAll(songsList ?: listOf())
    }

    override fun onInternetAvailable() {
        viewModel.isInternetAvailable = true
    }

    override fun onConnectionDisable() {
        viewModel.isInternetAvailable = false
    }

    override fun onPurchase(song: Song?, songsList: List<Song>?) {
        viewModel.isFromPurchasedResponse = true
        if (song != null) {
            val index =
                viewModel.mSongBuilder.songsList?.indexOf(viewModel.mSongBuilder.currentSongModel)
            if (index != null) {
                viewModel.mSongBuilder.songsList?.set(index, song)
            }
            viewModel.mSongBuilder.currentSongModel = song
            finish()

        } else if (!songsList.isNullOrEmpty()) {
            viewModel.mSongBuilder.songsList?.clear()
            viewModel.mSongBuilder.songsList?.addAll(songsList)
            viewModel.mSongBuilder.currentSongModel = songsList[0]
            finish()
        }
    }


    private fun getDataFromBundles() {
        val bundle = intent.extras?.getBundle(KEY_DATA)
        viewModel.mSongBuilder = NextPlaySongBuilder()
        viewModel.mSongBuilder.currentSongModel = bundle?.getParcelable(CommonKeys.KEY_DATA_MODEL)
        viewModel.mSongBuilder.songsList = bundle?.getParcelableArrayList(CommonKeys.KEY_SONGS_LIST)
        viewModel.mSongBuilder.playListId = bundle?.getInt(KEY_PLAYLIST_ID)
        viewModel.mSongBuilder.playedFrom = SongType.from(
            bundle?.getString(KEY_SONG_TYPE, "")
                .toString()
        )
        viewModel.mSongBuilder.currentAlbumModel = bundle?.getParcelable(CommonKeys.KEY_ALBUM)
        viewModel.isFromService = bundle?.getBoolean(CommonKeys.KEY_IS_FROM_SERVICE, false) ?: false
    }

    private fun initViews() {
        binding.tvSongLayrics.setMovementMethod(ScrollingMovementMethod())
        if (viewModel.mSongBuilder.currentSongModel?.lyrics.isNullOrEmpty().not()) {
            binding.tvSongLayrics.text = viewModel.mSongBuilder.currentSongModel?.lyrics
        } else {
            binding.tvSongLayrics.text = getString(R.string.no_lyrics)
        }
        if (viewModel.mSongBuilder.currentSongModel?.songVideoUrl == null) {
            binding.toggleButtonGroup.visibility = View.GONE
        }

        binding.tvPlayingFrom.text = getString(R.string.str_playing_from).plus(" ").plus(
            viewModel.mSongBuilder.playedFrom?.value
        )
        binding.ivSongCover.loadImg(viewModel.mSongBuilder.currentSongModel?.coverImageUrl ?: "")
        binding.tvAlbumName.text = viewModel.mSongBuilder.currentSongModel?.albumName
        binding.tvSongName.text = viewModel.mSongBuilder.currentSongModel?.songTitle
        if (viewModel.mSongBuilder.currentAlbumModel != null) {
            binding.tvArtistName.text = viewModel.mSongBuilder.currentAlbumModel?.albumTitle
        } else {
            binding.tvArtistName.text = viewModel.mSongBuilder.currentSongModel?.artist
        }
        binding.coverImage.loadImg(viewModel.mSongBuilder.currentSongModel?.coverImageUrl ?: "")
        binding.tvMenuSongName.text = viewModel.mSongBuilder.currentSongModel?.songTitle
        binding.tvMenuArtistName.text = viewModel.mSongBuilder.currentSongModel?.artist
        viewModel.authData = viewModel.getPrefrencesData(this)
        if (viewModel.authData.subscription?.planTitle.isNullOrEmpty().not()) {
            binding.crounIcon.visibility = View.INVISIBLE
        } else {
            binding.crounIcon.visibility = View.VISIBLE
        }

        if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.DOWNLOADED) {
            binding.tvOfflineDownload.text = getString(R.string.str_remove_from_downloads)
        } else {
            binding.tvOfflineDownload.text = getString(R.string.offline_download)
        }
    }


    private fun setListeners() {
        binding.ivArrowDown.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.null_transition, R.anim.bottom_down)
        }
        binding.btnAudio.setOnClickListener {
            if (binding.btnAudio.isSelected.not()) {
                binding.mlParent.transitionToStart()
                if ((binding.videoPlayer.player as ExoPlayer?)?.isPlaying == true) {
                    showVideoPlayerPlayIcons()
                    (binding.videoPlayer.player as ExoPlayer?)?.playWhenReady = false
                }
            }
        }
        binding.btnVideo.setOnClickListener {
            showVideoPlayerPlayIcons()
            binding.videoPlayer.visibility = View.VISIBLE
            binding.mlParent.setTransition(R.id.toggle_start, R.id.toggle_end)
            binding.mlParent.transitionToEnd()
            binding.btnAudio.isSelected = false
            if (getPlayerInstance().isAudioPlayerPlaying()) {
                binding.ivPlayPause.setImageResource(R.drawable.ic_red_play)
                getPlayerInstance().transitionToVideoPlayer()
            }
        }
        videoPlayerPlayIcon = findViewById(R.id.iv_video_player_play)
        videoPlayerPlayIcon.setOnClickListener {
            showVideoPlayerPauseIcons()
            (binding.videoPlayer.player as ExoPlayer?)?.playWhenReady = true
        }
        videoPlayerPauseIcon = findViewById(R.id.iv_video_player_pause)
        videoPlayerPauseIcon.setOnClickListener {
            showVideoPlayerPlayIcons()
            (binding.videoPlayer.player as ExoPlayer?)?.playWhenReady = false
        }
        videoPlayerBigPlayIcon = findViewById(R.id.iv_video_player_big_play)
        videoPlayerBigPlayIcon.setOnClickListener {
            showVideoPlayerPauseIcons()
            (binding.videoPlayer.player as ExoPlayer?)?.playWhenReady = true
        }
        videoPlayerBigPauseIcon = findViewById(R.id.iv_video_player_big_pause)
        videoPlayerBigPauseIcon.setOnClickListener {
            showVideoPlayerPlayIcons()
            (binding.videoPlayer.player as ExoPlayer?)?.playWhenReady = false
        }
        binding.ivPlayPause.setOnClickListener {
            if (getPlayerInstance().isAudioPlayerPlaying()) {
                viewModel.isSongPause = true
                binding.ivPlayPause.setImageResource(R.drawable.ic_red_play)
                getPlayerInstance().paused()
            } else {
                viewModel.isSongPause = false
                binding.ivPlayPause.setImageResource(R.drawable.ic_red_pause)
                getPlayerInstance().play()
            }
            if (mPlayerService != null) {
                EventBus.getDefault().post(
                    PlayerState(
                        isPlaying = getPlayerInstance().isAudioPlayerPlaying(),
                        currentSong = mPlayerService?.currentSong,
                        playerHelper = getPlayerInstance()
                    )
                )
            }
        }

        binding.ivForward.setOnClickListener {
            if (viewModel.isSongPause) {
                val currentProgress = binding.sbAudioPlayer.progress
                val updatedProgress = currentProgress.plus(10)
                binding.sbAudioPlayer.progress = updatedProgress
            }
            getPlayerInstance().forward()
        }

        binding.ivRewind.setOnClickListener {
            if (viewModel.isSongPause) {
                val currentProgress = binding.sbAudioPlayer.progress
                val updatedProgress = currentProgress.minus(10)
                binding.sbAudioPlayer.progress = updatedProgress
            }
            getPlayerInstance().rewind()
        }

        binding.ivNext.setOnClickListener {
            if (viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {
                getFavoriteSongsFromServer()
                if (::mPlayer.isInitialized) {
                    playNextSong()
                } else {
                    mPlayerService?.playNextSong(binding.videoPlayer)
                }
            }
        }
        binding.ivPrevious.setOnClickListener {
            if (viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {
                getFavoriteSongsFromServer()
                if (::mPlayer.isInitialized) {
                    playPreviousSong()
                } else {
                    mPlayerService?.playPreviousSong(binding.videoPlayer)
                }
            }
        }
        binding.sbAudioPlayer.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.PREMIUM) {
                    val millis = TimeUnit.SECONDS.toMillis(progress.toLong())
                    if (fromUser) {
                        getPlayerInstance().seekTo(millis.toInt())
                    }
                } else {
                    if (fromUser) {
                        getPlayerInstance().seekTo(progress)
                    }
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        binding.ivEmptyHeart.setOnClickListener {
            if (viewModel.isInternetAvailable) {
                if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.PREMIUM) {
                    Utilities.showToast(this, getString(R.string.premimu_song_error))
                } else if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.DELETED) {
                    Utilities.showToast(this, getString(R.string.deleted_song_fav_errror))
                } else {
                    val favoriteSongBody = FavoriteSongBody(
                        viewModel.mSongBuilder.currentSongModel?.songId ?: 0,
                        (binding.ivEmptyHeart.tag == getString(
                            R.string.str_empty
                        ))
                    )
                    mProfileNetworkViewModel.setFavoriteSong(
                        favoriteSongBody
                    )
                }
            } else {
                Utilities.showToast(this, getString(R.string.offline_toast))
            }
        }

        binding.addToFavoriteLayout.setOnClickListener {
            binding.ivEmptyHeart.performClick()

        }

        binding.addToPlayListLayout.setOnClickListener {
            if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.DELETED) {
                Utilities.showToast(this, getString(R.string.deleted_song_play_list_errror))
            } else {
                binding.close.performClick()
                if (viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {
                    val fragment = PlaylistDialogueFragment.newInstance(object :
                        PlaylistDialogueFragment.Callback {
                        override fun onPlayListSelected(playlistModel: PlaylistModel?) {
                            Utilities.showToast(
                                this@PlayerActivity,
                                "Song added in ${playlistModel?.playListTitle}"
                            )
                        }
                    })
                    val bundle = Bundle()
                    viewModel.mSongBuilder.currentSongModel?.songId?.let { songId ->
                        bundle.putInt(
                            CommonKeys.KEY_DATA,
                            songId
                        )
                    }
                    fragment.arguments = bundle
                    fragment.show(
                        supportFragmentManager,
                        PlaylistDialogueFragment::class.java.toString()
                    )
                } else {
                    Utilities.showToast(this, getString(R.string.playlist_added_song_msg))
                }
            }
        }

        binding.ivMoreOptions.setOnClickListener {
            if (viewModel.isInternetAvailable) {
                val animation = AnimationUtils.loadAnimation(this, R.anim.option_menu_layout)
                binding.optionMenuLayout.startAnimation(animation)
                binding.mlParent.setTransition(R.id.startOptionMenu, R.id.endOptionMenu)
                binding.mlParent.transitionToEnd()
                binding.ivMoreOptions.isEnabled = false

            } else {
                Utilities.showToast(this, getString(R.string.offline_toast))
            }
        }

        binding.close.setOnClickListener {
            binding.mlParent.setTransition(R.id.startOptionMenu, R.id.endOptionMenu)
            binding.mlParent.transitionToStart()
            if (binding.toggleButtonGroup.position == 0) {
                binding.mlParent.setTransition(R.id.toggle_end, R.id.toggle_start)
                binding.mlParent.transitionToStart()
                binding.btnAudio.isSelected = true
            } else {

                binding.mlParent.setTransition(R.id.toggle_start, R.id.toggle_end)
                binding.mlParent.transitionToEnd()
            }
            binding.ivMoreOptions.isEnabled = true
        }

        binding.layoutForLayrics.setOnClickListener {
            binding.mlParent.setTransition(R.id.startLayrics, R.id.endLayrics)
            binding.mlParent.transitionToEnd()
        }

        binding.lyricsFullView.setOnClickListener {
            binding.mlParent.setTransition(R.id.endLayrics, R.id.startLayrics)
            binding.mlParent.transitionToStart()
            if (binding.toggleButtonGroup.position == 0) {
                binding.mlParent.setTransition(R.id.toggle_end, R.id.toggle_start)
                binding.mlParent.transitionToStart()
            } else {
                binding.mlParent.setTransition(R.id.toggle_start, R.id.toggle_end)
                binding.mlParent.transitionToEnd()
            }

            binding.layoutForLayrics.setVisibilityInMotionLayout(View.VISIBLE)
        }

        binding.arrowDown.setOnClickListener {
            binding.mlParent.setTransition(R.id.endLayrics, R.id.startLayrics)
            binding.mlParent.transitionToStart()
            if (binding.toggleButtonGroup.position == 0) {
                binding.mlParent.setTransition(R.id.toggle_end, R.id.toggle_start)
                binding.mlParent.transitionToStart()
            } else {
                binding.mlParent.setTransition(R.id.toggle_start, R.id.toggle_end)
                binding.mlParent.transitionToEnd()
            }
            binding.layoutForLayrics.setVisibilityInMotionLayout(View.VISIBLE)
        }
        binding.downloadSongLayout.setOnClickListener {
            binding.close.performClick()
            if (viewModel.authData.subscription?.planTitle.isNullOrEmpty().not()) {
                if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.DOWNLOADED) {
                    viewModel.actionType = ActionType.REMOVE
                    viewModel.mSongBuilder.currentSongModel?.songStatus = SongStatus.DELETED
                    viewModel.deleteSongById(viewModel.mSongBuilder.currentSongModel?.songId)
                    mSongAndArtistsViewModel.addOrRemoveSongToDownload(
                        ActionType.REMOVE,
                        viewModel.mSongBuilder.currentSongModel ?: Song(),
                        null
                    )
                } else if (viewModel.mSongBuilder.currentSongModel?.songStatus == SongStatus.DELETED) {
                    Utilities.showToast(this, getString(R.string.deleted_song_error))
                } else {
                    viewModel.mSongBuilder.currentSongModel?.songStatus = SongStatus.DOWNLOADED
                    val fragment = SongTypeFragment.newInstance(
                        viewModel.mSongBuilder.currentSongModel ?: Song(),
                        object : SongTypeFragment.Callback {
                            override fun onDownloadingStart(
                                actionType: ActionType,
                                song: Song,
                                songType: SongType
                            ) {
                                viewModel.actionType = actionType
                                mSongAndArtistsViewModel.addOrRemoveSongToDownload(
                                    actionType,
                                    song,
                                    songType
                                )
                            }

                        })
                    fragment.show(supportFragmentManager, SongTypeFragment::class.java.toString())
                }
            } else {
                DialogUtils.runTimeAlert(this,
                    getString(R.string.offline_downalod_),
                    getString(R.string.offline_download_msg),
                    getString(R.string.ok),
                    "",
                    object : DialogUtils.CallBack {
                        override fun onPositiveCallBack() {

                        }

                        override fun onNegativeCallBack() {

                        }
                    }
                )
            }
        }
    }

    private fun showVideoPlayerPlayIcons() {

        videoPlayerPauseIcon.visibility = View.GONE
        videoPlayerPlayIcon.visibility = View.VISIBLE
        videoPlayerBigPauseIcon.visibility = View.GONE
        videoPlayerBigPlayIcon.visibility = View.VISIBLE
    }

    private fun showVideoPlayerPauseIcons() {
        videoPlayerPauseIcon.visibility = View.VISIBLE
        videoPlayerPlayIcon.visibility = View.GONE
        videoPlayerBigPauseIcon.visibility = View.VISIBLE
        videoPlayerBigPlayIcon.visibility = View.GONE
    }

    private fun initializePlayers() {
        mPlayer = PlayerHelper(
            context = this,
            song = viewModel.mSongBuilder.currentSongModel ?: Song(),
            callBack = this,
            player = binding.videoPlayer,
            album = viewModel.mSongBuilder.currentAlbumModel,
            mSongsList = ArrayList<Song>()
        )
        binding.ivPlayPause.setImageResource(R.drawable.ic_red_pause)
    }

    private fun stopPlayer() {
        if (::mPlayer.isInitialized) {
            getPlayerInstance().stopPlayer()
        }
    }

    private fun releasePlayers() {
        getPlayerInstance().releasePlayers()
        binding.videoPlayer.player = null
    }

    private fun playNextSong() {
        val index =
            viewModel.mSongBuilder.songsList?.indexOf(viewModel.mSongBuilder.currentSongModel)
        if ((index?.let { safeIndex -> viewModel.mSongBuilder.songsList?.lastIndex?.minus(safeIndex) }
                ?: 0) > Constants.NEXT_SONG_LIMIT) {
            playNextSongFromList(index ?: 0)

        } else {

            playNextSongFromList(index ?: 0)
            if (viewModel.mSongBuilder.playedFrom == SongType.PLAYLIST ||
                viewModel.mSongBuilder.playedFrom == SongType.CATEGORY ||
                viewModel.mSongBuilder.playedFrom == SongType.FAVOURITES ||
                viewModel.mSongBuilder.playedFrom == SongType.PURCHASED_SONG ||
                viewModel.mSongBuilder.playedFrom == SongType.PURCHASED_ALBUM ||
                viewModel.mSongBuilder.playedFrom == SongType.DOWNLOADED ||
                viewModel.mSongBuilder.playedFrom == SongType.ALBUM ||
                viewModel.mSongBuilder.playedFrom == SongType.SEARCHED
            ) {
                val nextPlaySongBuilder = NextPlaySongBuilder()
                nextPlaySongBuilder.currentSongId = viewModel.mSongBuilder.currentSongModel?.songId
                nextPlaySongBuilder.categoryId = viewModel.mSongBuilder.currentSongModel?.categoryId
                nextPlaySongBuilder.albumId = viewModel.mSongBuilder.currentSongModel?.albumId
                nextPlaySongBuilder.artistId = viewModel.mSongBuilder.currentSongModel?.artistId
                nextPlaySongBuilder.playListId = viewModel.mSongBuilder.currentSongModel?.playListId
                nextPlaySongBuilder.playedFrom = viewModel.mSongBuilder.playedFrom
                val nextPlaySong = NextPlaySongBuilder.build(nextPlaySongBuilder)
                mSongAndArtistsViewModel.getNextSongsData(nextPlaySong)
            }
        }
    }

    private fun playNextSongFromList(index: Int) {
        stopPlayer()
        if (viewModel.mSongBuilder.songsList?.lastIndex != index) {
            viewModel.mSongBuilder.currentSongModel =
                viewModel.mSongBuilder.songsList?.get(index + 1)
            for ((loopIndex, item) in viewModel.mSongBuilder.songsList?.withIndex()
                ?: emptyList()) {
                if (loopIndex > index && item.songStatus != SongStatus.PREMIUM) {
                    viewModel.mSongBuilder.currentSongModel = item
                    break
                }
            }
            prepareForSongChange()
            initializePlayers()
            releasePlayers()
        }
    }

    private fun playPreviousSong() {
        val index =
            viewModel.mSongBuilder.songsList?.indexOf(viewModel.mSongBuilder.currentSongModel)
        if ((index ?: 0) > -1) {
            for (itemIndex in index?.downTo(0)!!) {
                viewModel.mSongBuilder.currentSongModel = viewModel.mSongBuilder.songsList?.get(
                    itemIndex
                )
                if (viewModel.mSongBuilder.currentSongModel?.songStatus != SongStatus.PREMIUM) {
                    break
                }
            }
            showLoadingProgressDialog()
            releasePlayers()
            prepareForSongChange()
            initializePlayers()
        }
    }

    private fun prepareForSongChange() {
        if (viewModel.mSongBuilder.currentSongModel != null) {
            binding.ivSongCover.loadImg(
                viewModel.mSongBuilder.currentSongModel?.coverImageUrl ?: ""
            )
            binding.tvAlbumName.text = viewModel.mSongBuilder.currentSongModel?.albumName
            binding.tvSongName.text = viewModel.mSongBuilder.currentSongModel?.songTitle

            binding.tvArtistName.text = viewModel.mSongBuilder.currentSongModel?.artist
            if (viewModel.mSongBuilder.currentSongModel?.lyrics.isNullOrEmpty().not()) {
                binding.tvSongLayrics.text = viewModel.mSongBuilder.currentSongModel?.lyrics
            } else {
                binding.tvSongLayrics.text = getString(R.string.no_lyrics)
            }
            if (viewModel.mSongBuilder.currentSongModel?.songVideoUrl == null) {
                binding.toggleButtonGroup.visibility = View.GONE
            }
        }
    }

    private fun getPlayerInstance(): PlayerHelper {
        return if (mPlayerService != null) {
            mPlayerService?.getExoPlayerInstance() ?: PlayerHelper()
        } else {
            if (!::mPlayer.isInitialized)
                initializePlayers()
            mPlayer
        }
    }

    private fun resetPlayerDurationAndSeekBar() {

        binding.sbAudioPlayer.progress = 0
        binding.songCurrentDuration.text = getString(R.string.set_duraton_0)
        binding.maxSongDuration.text = getString(R.string.set_duraton_0)
    }

    private fun getFavoriteSongsFromServer() {

        val songsBuilder = SongsBodyBuilder()
        songsBuilder.type = SongType.FAVOURITES
        val songsBodyModel = SongsBodyBuilder.build(songsBuilder)
        mSongAndArtistsViewModel.getSongs(
            songsBodyModel
        )
    }

    fun showLoadingProgressDialog() {

        mProgressDialog = ProgressDialogFragment()
        mProgressDialog.show(supportFragmentManager, TAG)
    }

    private fun setNetworkObserver() {

        mSongAndArtistsViewModel.songlistResponse.observe(this) { favoriteSongsResponse ->
            when (favoriteSongsResponse.status) {
                NetworkStatus.LOADING -> {

                }
                NetworkStatus.SUCCESS -> {
                    val response = favoriteSongsResponse.t as ResponseModel
                    val songsList = response.data.songList

                    if (!songsList.isNullOrEmpty()) {
                        var isFavorite = false
                        for (song in songsList) {
                            if (song.songId == viewModel.mSongBuilder.currentSongModel?.songId) {
                                isFavorite = true
                                break
                            }
                        }
                        if (isFavorite) {
                            binding.ivEmptyHeart.setImageResource(R.drawable.ic_red_heart)
                            binding.ivMenuEmptyHeart.setImageResource(R.drawable.ic_red_heart)
                            binding.favrSupportIcon.visibilityGone()
                            binding.tvAddToFavorite.text =
                                getString(R.string.remove_from_favorite)
                            binding.ivEmptyHeart.tag = getString(R.string.str_filled)
                            binding.ivMenuEmptyHeart.tag = getString(R.string.str_filled)

                        } else {
                            binding.ivEmptyHeart.setImageResource(R.drawable.ic_empty_heart)
                            binding.ivMenuEmptyHeart.setImageResource(R.drawable.ic_empty_heart)
                            binding.favrSupportIcon.visibilityInVisible()
                            binding.tvAddToFavorite.text = getString(R.string.add_to_favorite)
                            binding.ivEmptyHeart.tag = getString(R.string.str_empty)
                            binding.ivMenuEmptyHeart.tag = getString(R.string.str_empty)

                        }
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = favoriteSongsResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    val error = favoriteSongsResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@PlayerActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }

        mProfileNetworkViewModel.setFavoriteSongResponse.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                    showFavProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    if (binding.ivEmptyHeart.tag == getString(R.string.str_empty)) {
                        binding.ivEmptyHeart.setImageResource(R.drawable.ic_red_heart)
                        binding.ivMenuEmptyHeart.setImageResource(R.drawable.ic_red_heart)
                        binding.favrSupportIcon.visibilityGone()
                        binding.tvAddToFavorite.text = getString(R.string.remove_from_favorite)
                        binding.ivEmptyHeart.tag = getString(R.string.str_filled)
                        binding.ivMenuEmptyHeart.tag = getString(R.string.str_filled)
                    } else {
                        binding.ivEmptyHeart.setImageResource(R.drawable.ic_empty_heart)
                        binding.ivMenuEmptyHeart.setImageResource(R.drawable.ic_empty_heart)
                        binding.favrSupportIcon.visibilityInVisible()
                        binding.tvAddToFavorite.text = getString(R.string.add_to_favorite)
                        binding.ivEmptyHeart.tag = getString(R.string.str_empty)
                        binding.ivMenuEmptyHeart.tag = getString(R.string.str_empty)
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    hideFavProgressBar()
                }
                NetworkStatus.EXPIRE -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@PlayerActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
        mSongAndArtistsViewModel.addSongToDownload.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {

                }
                NetworkStatus.SUCCESS -> {
                    if (viewModel.actionType == ActionType.REMOVE) {
                        Toast.makeText(
                            this,
                            getString(R.string.song_removed),
                            Toast.LENGTH_LONG
                        ).show()
                        binding.tvOfflineDownload.text = getString(R.string.offline_download)
                    } else {
                        Toast.makeText(
                            this,
                            getString(R.string.song_downloading_start),
                            Toast.LENGTH_LONG
                        ).show()
                        binding.tvOfflineDownload.text =
                            getString(R.string.str_remove_from_downloads)
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {

                }
                NetworkStatus.EXPIRE -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@PlayerActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
        mSongAndArtistsViewModel.nextSongsResponse.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                }
                NetworkStatus.SUCCESS -> {

                    val data = apiResponse.t as ResponseModel
                    if (data.data.songList?.isNotEmpty() == true) {
                        viewModel.mSongBuilder.songsList?.addAll(
                            data.data.songList ?: emptyList()
                        )
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {

                }
                NetworkStatus.EXPIRE -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@PlayerActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
    }
    companion object {
        private const val TAG = "PlayerActivity"
    }
}