package com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity

import com.techswivel.qthemusic.customData.enums.FragmentType
import com.techswivel.qthemusic.models.SubscriptionPlans
import com.techswivel.qthemusic.ui.base.BaseViewModel

class SubscriptionPlansActivityViewModel : BaseViewModel() {
    lateinit var subscriptionPlans: SubscriptionPlans
    var subscriptionPlansList: ArrayList<Any> = ArrayList()
    var flowType: FragmentType? = null
}