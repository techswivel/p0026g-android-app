package com.techswivel.qthemusic.ui.fragments.favoriteSongFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentFavoriteSongBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.FavoriteSongBody
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.SongsBodyBuilder
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.utils.*
import org.greenrobot.eventbus.EventBus


class FavoriteSongFragment : RecyclerViewBaseFragment(), BaseInterface,
    RecyclerViewAdapter.CallBack {

    companion object {
        fun newInstance() = FavoriteSongFragment()
        fun newInstance(mBundle: Bundle?) = FavoriteSongFragment().apply {
            arguments = mBundle
        }
    }

    private lateinit var mBinding: FragmentFavoriteSongBinding
    private lateinit var viewModel: FavoriteSongFragmentViewModel
    private lateinit var songAndArtistViewModel: SongAndArtistsViewModel
    private lateinit var mFavoriteSongListAdapter: RecyclerViewAdapter
    private lateinit var profileNetworViewModel: ProfileNetworkViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentFavoriteSongBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setUpToolBar()
        setUpAdapter()
        getFavoriteSongsFromServer()
        setObserver()
        swipeToRefresh()
        mBinding.noDataFoundInc.noDataFoundBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mFavoriteSongListAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return mFavoriteSongListAdapter
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_favorite_song
    }

    override fun onNoDataFound() {
    }

    override fun showProgressBar() {
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.progressBar.visibility = View.GONE
    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
    }

    override fun onViewClicked(view: View, data: Any?) {
        super.onViewClicked(view, data)
        val song = data as Song
        when (view.id) {
            R.id.play_button_id_favorite_song -> {
                if (song.songStatus != SongStatus.PREMIUM) {
                    viewModel.insertDataInDataBaseForSync(song.songId)
                    viewModel.insertSongInDataBase(song)
                    val songBuilder = NextPlaySongBuilder().apply {
                        this.currentSongModel = song
                        this.songsList = viewModel.mFavoriteSongsList as MutableList<Song>
                        this.playedFrom = SongType.FAVOURITE
                    }
                    EventBus.getDefault().post(songBuilder)
                } else if (song.songStatus == SongStatus.PREMIUM) {
                    val bundle = Bundle().apply {
                        putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                        putParcelableArrayList(
                            CommonKeys.KEY_SONGS_LIST,
                            viewModel.mFavoriteSongsList as ArrayList<out Song>
                        )
                        putString(
                            CommonKeys.KEY_SONG_TYPE,
                            SongType.FAVOURITE.value
                        )
                    }
                    ActivityUtils.startPlayerActivity(activity, bundle)
                }
            }
            R.id.privatetextDelete -> {
                viewModel.song = song
                viewModel.mFavoriteSongsList
                viewModel.songId = song.songId
                openBottomSheet()
            }
        }
    }

    private fun openBottomSheet() {
        val dialog = BottomSheetDialog(
            requireContext(),
            R.style.BottomSheetDialog
        )
        val view = layoutInflater.inflate(R.layout.bottomsheetlayout, null)
        val textDelete = view.findViewById<TextView>(R.id.deletePlaylistTextviewBottomSheet)
        textDelete.text = getString(R.string.remove_from_favorite)
        val closeDialogImageview =
            view.findViewById<ImageView>(R.id.imageviewCancelDialogBottomSheet)
        closeDialogImageview.setOnClickListener {
            dialog.dismiss()
        }
        textDelete.setOnClickListener {
            val favoriteSongBody = FavoriteSongBody(
                viewModel.songId,
                false
            )
            profileNetworViewModel.setFavoriteSong(
                favoriteSongBody
            )
            dialog.dismiss()
        }

        dialog.setCancelable(false)
        dialog.setContentView(view)
        dialog.show()
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun setObserver() {
        songAndArtistViewModel.songlistResponse.observe(viewLifecycleOwner) { favDataResponse ->
            when (favDataResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.shimmerLayout.visibilityVisible()
                    mBinding.shimmerLayout.startShimmer()
                    mBinding.tvNoDataFound.visibilityGone()
                }
                NetworkStatus.SUCCESS -> {
                    if (mBinding.swipeFavSongsContainer.isRefreshing) {
                        mBinding.swipeFavSongsContainer.isRefreshing = false
                    }
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibilityGone()
                    viewModel.mFavoriteSongsList.clear()
                    val response = favDataResponse.t as ResponseModel
                    val playlist = response.data.songList

                    if (!playlist.isNullOrEmpty()) {
                        viewModel.mFavoriteSongsList.addAll(playlist)
                    } else {
                        mBinding.tvNoDataFound.visibility = View.VISIBLE
                    }
                    if (::mFavoriteSongListAdapter.isInitialized)
                        mFavoriteSongListAdapter.notifyItemRangeInserted(
                            0,
                            viewModel.mFavoriteSongsList.size - 1
                        )
                }
                NetworkStatus.ERROR -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                    val error = favDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                    val error = favDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    mBinding.shimmerLayout.stopShimmer()
                    mBinding.shimmerLayout.visibility = View.GONE
                }
            }
        }


        profileNetworViewModel.setFavoriteSongResponse.observe(viewLifecycleOwner) { favoriteSongResponse ->
            when (favoriteSongResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    removeItemFromList(viewModel.song)

                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    DialogUtils.runTimeAlert(context ?: requireContext(),
                        getString(R.string.error),
                        favoriteSongResponse.error?.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    DialogUtils.sessionExpireAlert(requireContext(),
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }

    }

    private fun removeItemFromList(song: Song?) {
        val index = viewModel.mFavoriteSongsList.indexOf(song as Song)
        viewModel.mFavoriteSongsList.remove(song)
        if (viewModel.mFavoriteSongsList.size == 0) {
            mBinding.tvNoDataFound.visibility = View.VISIBLE
        } else {
            mBinding.tvNoDataFound.visibility = View.GONE
        }
        mFavoriteSongListAdapter.notifyItemRemoved(index)
    }

    private fun getFavoriteSongsFromServer() {
        val songsBuilder = SongsBodyBuilder()
        songsBuilder.type = SongType.FAVOURITES
        val songsBodyModel = SongsBodyBuilder.build(songsBuilder)
        songAndArtistViewModel.getSongs(songsBodyModel)
    }

    private fun initViewModel() {
        viewModel =
            ViewModelProvider(this).get(FavoriteSongFragmentViewModel::class.java)

        songAndArtistViewModel =
            ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)

        profileNetworViewModel =
            ViewModelProvider(this).get(ProfileNetworkViewModel::class.java)
    }

    private fun setUpAdapter() {
        mFavoriteSongListAdapter = RecyclerViewAdapter(this, viewModel.mFavoriteSongsList)
        setUpRecyclerView(
            mBinding.recyclerviewFavoriteSongs
        )
    }

    private fun setUpToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar,
            "",
            true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.favorite_songs)
    }

    private fun swipeToRefresh() {
        mBinding.swipeFavSongsContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        viewModel.mFavoriteSongsList.clear()
        mFavoriteSongListAdapter.notifyDataSetChanged()
        getFavoriteSongsFromServer()
    }
}