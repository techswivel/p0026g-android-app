package com.techswivel.qthemusic.ui.activities.buyingHistoryActivity

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.databinding.ActivityBuyingHistoryBinding
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.ui.fragments.buyingHistoryFragment.BuyingHistoryFragment
import com.techswivel.qthemusic.ui.fragments.buyingHistoryFragment.BuyingHistoryViewModel
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.ui.fragments.paymentTypesBottomSheetFragment.PaymentTypeBottomSheetFragment
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.visibilityGone
import com.techswivel.qthemusic.utils.visibilityVisible

class BuyingHistoryActivity : PlayerBaseActivity(), BuyingHistoryActivityImpl {

    private var mFragment: Fragment? = null
    private lateinit var mBinding: ActivityBuyingHistoryBinding
    private lateinit var viewModel: BuyingHistoryViewModel
    private lateinit var mCurrentFragment: Fragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityBuyingHistoryBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        initViewModel()
        setToolBar()
        openBuyingHistoryFragment()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerService?.isAddBuyingHistory(false)
    }

    override fun onBackPressed() {
        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
        return super.onSupportNavigateUp()
    }

    override fun openPaymentTypeBottomSheetDialogFragment() {
        mBinding.activityToolbar.toolbar.visibility = View.GONE
        openPaymentTypeFragment()
    }

    override fun onCancelCallBack() {
        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onItemClickCallBack(paymentType: String?) {
        (mCurrentFragment as BuyingHistoryActivityImpl).onItemClickCallBack(
            paymentType
        )
        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onProgressUpdate(timeInMis: Int) {
        mBinding.playLayout.seekBarPlayer.progress = timeInMis
    }

    override fun onPlayerStateIdle() {

    }

    override fun onPlayerStateBuffering() {

    }

    override fun onPlayerStateReady() {

    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {

    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                .isAudioPlayerPlaying()
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onCurrentSongUpdate(serviceBinder.currentSong)
        } else if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == false)
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
        serviceBinder?.isAddBuyingHistory(true)
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        super.readyToShowVideoPlayerPlayIcons()
        if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_pause
                )
            )
        } else {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_play
                )
            )
        }
    }

    override fun onPlayerServiceStop() {
        mBinding.playLayout.playerParentLayout.visibility = View.GONE
    }

    override fun onPlayerServiceStart() {
        mBinding.playLayout.playerParentLayout.visibility = View.VISIBLE
    }


    override fun onProgressBarUpdate(current: Int, max: Int) {
        mBinding.playLayout.seekBarPlayer.max = max
        mBinding.playLayout.seekBarPlayer.progress = current
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        if (data.isNotificationRemoved) {
            mBinding.playLayout.playerParentLayout.visibilityGone()
        } else if (data.isPlaying) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        } else {
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        }
    }

    override fun onCurrentSongUpdate(data: Song) {
        super.onCurrentSongUpdate(data)
        mBinding.playLayout.data = data
        mBinding.playLayout.executePendingBindings()
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {
        ActivityUtils.launchFragment(this, NoInternetFragment::class.java.name)
    }

    private fun setToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar, "", false, true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.buying_history)
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(BuyingHistoryViewModel::class.java)
    }

    private fun openBuyingHistoryFragment() {
        mCurrentFragment = BuyingHistoryFragment.newInstance()
        popUpAllFragmentIncludeThis(BuyingHistoryFragment::class.java.name)
        openFragment(mCurrentFragment)
    }

    private fun openPaymentTypeFragment() {
        popUpAllFragmentIncludeThis(PaymentTypeBottomSheetFragment::class.java.name)
        openFragment(PaymentTypeBottomSheetFragment.newInstance())
    }

    private fun openFragment(fragment: Fragment) {
        ::mFragment.set(fragment)
        mFragment.let { fragmentInstance ->
            fragmentInstance?.let { fragmentToBeReplaced ->
                replaceFragment(mBinding.mainContainer.id, fragmentToBeReplaced)
            }
        }
    }


}