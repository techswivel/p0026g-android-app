package com.techswivel.qthemusic.ui.base

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.C
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.helper.PlayerHelper
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.PlayerState
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.services.exoService.MainStorage
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.customWidgets.OnSwipeTouchListener
import com.techswivel.qthemusic.utils.*
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_IS_RESTART
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_PLAYLIST_ID
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

abstract class PlayerBaseActivity : BaseActivity(), PlayerHelper.PlayerCallBack {

    private lateinit var mSongAndArtistsViewModel: SongAndArtistsViewModel
    private val UPDATE_PROGRESS_DELAY: Long = Constants.ONE_SECOND shr 3
    private val mHandler = Handler(Looper.getMainLooper())
    protected var isPlaying = true
    protected var mPlayerSongs: NextPlaySongBuilder = NextPlaySongBuilder()
    protected var mPlayerService: MainService.MainServiceBinder? = null
    private val updateSongProgress = object : Runnable {
        override fun run() {
            val duration: Long? = mPlayerService?.getExoPlayerInstance()?.currentDuration()
            if (duration != C.TIME_UNSET) {
                    onProgressBarUpdate(
                        mPlayerService?.getExoPlayerInstance()?.currentTimeOfSong()?.toInt() ?: 0,
                        duration?.toInt() ?: 0
                    )
            }
            mHandler.postDelayed(
                this,
                if (isPlaying) 0 else UPDATE_PROGRESS_DELAY
            )
        }
    }

    /**
     * Create our connection to the service to be used in our bindService call.
     */
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            //We expect the service binder to be the main services binder.
            //As such we cast.
            if (service is MainService.MainServiceBinder) {
                mPlayerService = service
                //Then we simply set the exoplayer instance on this view.
                onServiceBind(mPlayerService)
                isPlaying = mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() ?: false
                mPlayerSongs = mPlayerService?.getNextSongBuilder() ?: NextPlaySongBuilder()
                readyToShowVideoPlayerPlayIcons()
                mHandler.post(updateSongProgress)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSongAndArtistsViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
        setObserver()
    }

    override fun onRestart() {
        super.onRestart()
        if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == true)
        ) {
            onPlayerServiceStop()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this@PlayerBaseActivity)) {
            EventBus.getDefault().register(this@PlayerBaseActivity)
        }
        if (this.isServiceRunning(MainService::class.java)) {
            val mIntent = Intent(this, MainService::class.java)
            bindService(mIntent, mConnection, BIND_AUTO_CREATE)
        }
    }

    override fun onPause() {
        super.onPause()
        if (EventBus.getDefault().isRegistered(this@PlayerBaseActivity)) {
            EventBus.getDefault().unregister(this@PlayerBaseActivity)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onStartPlayer(songData: NextPlaySongBuilder) {
        mPlayerSongs = songData
        mSongAndArtistsViewModel.savedPlayerState(songData)
        if (this.isServiceRunning(MainService::class.java)) {
            mPlayerService?.checkIfSongAlreadyInTheList(songData)
            onCurrentSongUpdate(songData.currentSongModel ?: Song())
        } else {
            startPlayerService()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    fun onPlayerStateChanged(data: PlayerState) {
        onPlayerStatusChanged(data)
    }
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    fun onEventNextSong(songId: Int?) {
        if (mPlayerSongs.playedFrom == SongType.PLAYLIST ||
            mPlayerSongs.playedFrom == SongType.CATEGORY ||
            mPlayerSongs.playedFrom == SongType.FAVOURITES ||
            mPlayerSongs.playedFrom == SongType.PURCHASED_SONG ||
            mPlayerSongs.playedFrom == SongType.PURCHASED_ALBUM ||
            mPlayerSongs.playedFrom == SongType.DOWNLOADED ||
            mPlayerSongs.playedFrom == SongType.ALBUM ||
            mPlayerSongs.playedFrom == SongType.SEARCHED
        ) {
            val nextPlaySongBuilder = NextPlaySongBuilder()
            nextPlaySongBuilder.currentSongId = songId
            nextPlaySongBuilder.categoryId = mPlayerSongs.currentSongModel?.categoryId
            nextPlaySongBuilder.albumId = mPlayerSongs.currentSongModel?.albumId
            nextPlaySongBuilder.artistId = mPlayerSongs.currentSongModel?.artistId
            nextPlaySongBuilder.playListId =
                if (mPlayerSongs.currentSongModel?.playListId == null) mPlayerService?.getNextSongBuilder()?.playListId else mPlayerSongs.currentSongModel?.playListId
            if (mPlayerSongs.playedFrom == SongType.FAVOURITES) {
                nextPlaySongBuilder.playedFrom = SongType.FAVOURITE
            } else {
                nextPlaySongBuilder.playedFrom = mPlayerSongs.playedFrom
            }
            val nextPlaySong = NextPlaySongBuilder.build(nextPlaySongBuilder)
            mSongAndArtistsViewModel.getNextSongsData(nextPlaySong)
        }
    }

    protected fun unbindServiceWithTheActivity() {
        unbindService(mConnection)
    }

    private fun setObserver() {
        mSongAndArtistsViewModel.nextSongsResponse.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                }
                NetworkStatus.SUCCESS -> {
                    val data = apiResponse.t as ResponseModel
                    if (data.data.songList?.isNotEmpty() == true) {
                        if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                                ?.isPlayerRelease() == false)
                        ) {
                            mPlayerService?.getNextPlaySongsList(data.data.songList)
                            runBlocking {
                                val state = mSongAndArtistsViewModel.getSavedState()
                                state.songsList?.addAll(data.data.songList ?: emptyList())
                                mSongAndArtistsViewModel.savedPlayerState(state.toNextSongBuilder())
                            }
                        } else {
                            onNextSongsList(data.data.songList)
                        }
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {

                }
                NetworkStatus.EXPIRE -> {
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mSongAndArtistsViewModel.clearAppSession(this@PlayerBaseActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
    }

    private fun startPlayerService() {
        val result = kotlin.runCatching {
            val bundle = Bundle()
            bundle.putParcelableArrayList(
                CommonKeys.KEY_SONGS_LIST,
                mPlayerSongs.songsList as ArrayList<out Parcelable>
            )
            bundle.putParcelable(CommonKeys.KEY_DATA_MODEL, mPlayerSongs.currentSongModel)
            bundle.putString(CommonKeys.KEY_SONG_TYPE, mPlayerSongs.playedFrom?.value)
            bundle.putInt(KEY_PLAYLIST_ID, mPlayerSongs.playListId ?: -1)
            val isRestart = MainStorage.getInstance(this)?.shouldRestartService()
            bundle.putBoolean(KEY_IS_RESTART, isRestart ?: false)
            val intent = Intent(this, MainService::class.java)
            intent.putExtra(CommonKeys.KEY_DATA, bundle)
            bindService(intent, mConnection, BIND_AUTO_CREATE)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                startService(intent)
            } else if (mPlayerService?.isBinderAlive == true) {
                startService(intent)
            } else {
                startForegroundService(intent)
            }
            onPlayerServiceStart()

        }

        if (result.isSuccess) {
            Log.d(TAG, "startPlayerService: Service Started successfully")
        } else {
            Log.d(TAG, "startPlayerService: Unable to stop service perform some backups.")
        }
    }

    protected fun stopPlayerService() {
        val result = kotlin.runCatching {
            mHandler.removeCallbacksAndMessages(null)
            mHandler.removeCallbacks(updateSongProgress)
            mPlayerService?.getExoPlayerInstance()?.stopPlayer()
            mPlayerService?.getExoPlayerInstance()?.releasePlayers()
            mPlayerService?.stopService()
            unbindService(mConnection)
            onPlayerServiceStop()
        }
        if (result.isSuccess) {
            Log.d(TAG, "stopPlayerService: Service Stop successfully")
        } else {
            Log.d(TAG, "stopPlayerService: Unable to stop service perform some backups.")
        }
    }

    protected fun setOnSwipeListener(view: View) {
        view.setOnTouchListener(object :
            OnSwipeTouchListener(this@PlayerBaseActivity) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                mPlayerService?.getExoPlayerInstance()?.paused()
                val animation: Animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.slide_left_out
                )
                view.animation = animation
                MainStorage.getInstance(context = this@PlayerBaseActivity)?.setRestartService(false)
                mSongAndArtistsViewModel.deleteSavedState()
                stopPlayerService()
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
                mPlayerService?.getExoPlayerInstance()?.paused()
                val animation: Animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.slide_right_out
                )
                view.animation = animation
                MainStorage.getInstance(context = this@PlayerBaseActivity)?.setRestartService(false)
                mSongAndArtistsViewModel.deleteSavedState()
                stopPlayerService()
            }

            override fun onClick() {
                super.onClick()
                ActivityUtils.startPlayerActivity(
                    this@PlayerBaseActivity,
                    getPlayerActivityArguments()
                )
            }
        })
    }

    private fun getPlayerActivityArguments(): Bundle {
        val bundle = Bundle()
        bundle.putParcelable(
            CommonKeys.KEY_DATA_MODEL,
            mPlayerService?.getNextSongBuilder()?.currentSongModel
        )
        bundle.putParcelableArrayList(
            CommonKeys.KEY_SONGS_LIST,
            mPlayerService?.getNextSongBuilder()?.songsList as ArrayList<out Song>
        )
        bundle.putString(
            CommonKeys.KEY_SONG_TYPE,
            mPlayerService?.getNextSongBuilder()?.playedFrom?.value
        )
        bundle.putBoolean(CommonKeys.KEY_IS_FROM_SERVICE, true)
        return bundle
    }

    abstract fun onPlayerServiceStop()
    abstract fun onPlayerServiceStart()
    protected open fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
    }

    abstract fun onProgressBarUpdate(current: Int, max: Int)
    abstract fun onPlayerStatusChanged(data: PlayerState)
    protected open fun onCurrentSongUpdate(data: Song) {

    }

    protected open fun onNextSongsList(songsList: List<Song>?) {

    }

    companion object {
        private const val TAG = "PlayerBaseActivity"
    }

}