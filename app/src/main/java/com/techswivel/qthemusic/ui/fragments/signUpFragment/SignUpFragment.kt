package com.techswivel.qthemusic.ui.fragments.signUpFragment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessaging
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.SocialSites
import com.techswivel.qthemusic.databinding.FragmentSignUpBinding
import com.techswivel.qthemusic.helper.FileHelper
import com.techswivel.qthemusic.models.AuthRequestBuilder
import com.techswivel.qthemusic.ui.activities.authActivity.AuthActivityImp
import com.techswivel.qthemusic.ui.base.BaseFragment
import com.techswivel.qthemusic.ui.dialogFragments.chooserDialogFragment.ChooserDialogFragment
import com.techswivel.qthemusic.ui.dialogFragments.genderDialogFragment.GenderSelectionDialogFragment
import com.techswivel.qthemusic.ui.dialogFragments.whyWeAreAskingDialogFragment.WhyWeAreAskingDialogFragment
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.Utilities
import com.techswivel.qthemusic.utils.toDeviceIdentifier
import okhttp3.MultipartBody
import java.io.File
import java.io.IOException
import java.text.DateFormat
import java.util.*


class SignUpFragment : BaseFragment(), SignUpFragmentImp {
    private lateinit var mViewModel: SignUpViewModel
    private lateinit var mBinding: FragmentSignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentSignUpBinding.inflate(layoutInflater, container, false)
        (mActivityListener as AuthActivityImp).castCurrentFragment(this)
        return mBinding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clickListeners()
        initialization()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mViewModel.mChooserFragment.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun showProgressBar() {
        mBinding.tvLetGoProfileBtn.isEnabled = false
    }

    override fun hideProgressBar() {
        mBinding.tvLetGoProfileBtn.isEnabled = true
    }

    override fun getGender(gender: String?) {
        mBinding.etUserGender.setText(gender)
    }

    private fun initialization() {
        mViewModel.mBuilder =
            arguments?.getSerializable(CommonKeys.AUTH_BUILDER_MODEL) as AuthRequestBuilder
        if (mViewModel.mBuilder.name != null) {
            mBinding.etUserName.setText(mViewModel.mBuilder.name)
        }
        mViewModel.name = mViewModel.mBuilder.name.toString()
        if (mViewModel.name.isNullOrEmpty()) {
            mBinding.etUserName.setText(mViewModel.name)
        }
        val myPhoto = mViewModel.mBuilder.profile
        if (myPhoto != null) {
            Glide.with(requireContext()).load(mViewModel.mBuilder.profile)
                .into(mBinding.ivUserProfilePic)

        }
    }

    private fun createUserAndCallApi(
        name: String?,
        email: String?,
        password: String?,
        dob: Int?,
        gender: String?,
        completeAddress: String?,
        city: String?,
        state: String?,
        country: String?,
        zipCode: Int?,
        profile: MultipartBody.Part?,
        fcmToken: String?,
        deviceIdentifier: String?,
        socialId: String?,
        socialSite: SocialSites?
    ) {

        (mActivityListener as AuthActivityImp).userSignUp(
            profile,
            name,
            email,
            dob,
            gender,
            password,
            fcmToken,
            deviceIdentifier,
            completeAddress,
            city,
            state,
            country,
            zipCode,
            socialId,
            socialSite
        )
    }

    @SuppressLint("SetTextI18n")
    private fun clickListeners() {
        mBinding.tvLetGoProfileBtn.setOnClickListener {
            mViewModel.email = mViewModel.mBuilder.email.toString()
            if (!mBinding.etZipCode.text.isNullOrEmpty()) {
                mViewModel.zipCode = mBinding.etZipCode.text.toString().toInt()
            }
            if (mBinding.etUserGender.text.toString().isNullOrEmpty()) {
                mViewModel.gender = mBinding.etUserGender.text.toString()
            }
            if (mBinding.etUserName.text.toString().isEmpty()) {
                mBinding.etUserName.error = getString(R.string.name_required)
            } else if (mBinding.etUserDob.text.toString().isEmpty()) {
                mBinding.etUserDob.error = getString(R.string.dob_req)
            } else if (mBinding.etUserAddress.text.toString()
                    .isNotEmpty() && !checkIfAddressNotEmpty()
            ) {
                Log.d(TAG, getString(R.string.missing))
            } else if (checkIfAddressRelatedFieldNotEmpty() && mBinding.etUserAddress.text.toString()
                    .isEmpty()
            ) {
                mBinding.etUserAddress.error = getString(R.string.address_required)

            } else {
                if (!mViewModel.mBuilder.socialId.isNullOrEmpty()) {
                    mViewModel.socailId = mViewModel.mBuilder.socialId.toString()
                    mViewModel.socailSite = mViewModel.mBuilder.socialSite?.name
                }

                if (!checkIfAddressNotEmpty()) {
                    Utilities.hideSoftKeyBoard(requireContext(), mBinding.etUserName)
                    val name = mBinding.etUserName.text
                    Utilities.showToast(requireContext(), "cliked")
                    showProgressBar()
                    getFcmToken(
                        name.toString(),
                        mViewModel.email,
                        mViewModel.mBuilder.password,
                        mViewModel.vmDob,
                        mBinding.etUserGender.text.toString(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        mViewModel.mBuilder.socialId.toString(),
                        mViewModel.mBuilder.socialSite
                    )
                } else {
                    Utilities.hideSoftKeyBoard(requireContext(), mBinding.etZipCode)
                    showProgressBar()
                    getFcmToken(
                        mBinding.etUserName.text.toString(),
                        mViewModel.email,
                        mViewModel.mBuilder.password,
                        mViewModel.vmDob,
                        mBinding.etUserGender.text.toString(),
                        mBinding.etUserAddress.text.toString(),
                        mBinding.etUserCity.text.toString(),
                        mBinding.etUserState.text.toString(),
                        mBinding.etUserCountry.text.toString(),
                        mViewModel.zipCode,
                        mViewModel.mBuilder.socialId.toString(),
                        mViewModel.mBuilder.socialSite
                    )
                }
            }
        }

        mBinding.dobView.setOnClickListener {
            mViewModel.calendar = Calendar.getInstance()
            mViewModel.year = mViewModel.calendar.get(Calendar.YEAR)
            mViewModel.month = mViewModel.calendar.get(Calendar.MONTH)
            mViewModel.day = mViewModel.calendar.get(Calendar.DAY_OF_MONTH)
            val datePicker = DatePickerDialog(
                requireContext(), R.style.MyDatePickerStyle,
                { view, year, month, dayOfMonth ->
                    // change date into millis
                    mViewModel.mcurrentTime.set(year, month, dayOfMonth)
                    mViewModel.dateInMillis = mViewModel.mcurrentTime.getTimeInMillis()

                    val millisToLong = mViewModel.dateInMillis?.div(Constants.ONE_SECOND)
                    if (millisToLong != null) {
                        mViewModel.vmDob = Math.abs(millisToLong.toInt())
                    }
                    val dateObj = Date(mViewModel.dateInMillis as Long)
                    mBinding.etUserDob.setText(
                        DateFormat.getDateInstance(DateFormat.LONG).format(dateObj)
                    )

                },
                mViewModel.year,
                mViewModel.month,
                mViewModel.day
            )
            datePicker.datePicker.maxDate = mViewModel.calendar.timeInMillis
            openDatePicker(datePicker)

        }
        mBinding.tvWhyWeAreAskingTag.setOnClickListener {
            WhyWeAreAskingDialogFragment().show(parentFragmentManager, TAG)

        }
        mBinding.genderView.setOnClickListener {
            GenderSelectionDialogFragment.newInstance(this).show(parentFragmentManager, TAG)
        }

        mBinding.profileImgSection.setOnClickListener {
            showPictureDialog()
        }
    }

    private fun getFcmToken(
        name: String?,
        email: String?,
        password: String?,
        dob: Int?,
        gender: String?,
        completeAddress: String?,
        city: String?,
        state: String?,
        country: String?,
        zipCode: Int?,
        socialId: String?,
        socialSite: SocialSites?
    ) {
        mViewModel.uri?.let {
            val path = FileHelper.getPathFromUri(requireContext(), it)
            mViewModel.imageFile = File(path)
        }
        var body: MultipartBody.Part? = null
        if (mViewModel.imageFile != null) {
            val profileImage = okhttp3.RequestBody.create(
                okhttp3.MediaType.parse("multipart/form-data"),
                mViewModel.imageFile
            )
            body = MultipartBody.Part.createFormData(
                "profile",
                mViewModel.imageFile?.getName(),
                profileImage
            )
        }

        FirebaseMessaging.getInstance().token.addOnSuccessListener { fcmToken ->
            createUserAndCallApi(
                name,
                email,
                password,
                dob,
                gender,
                completeAddress,
                city,
                state,
                country,
                zipCode,
                body,
                fcmToken,
                requireContext().toDeviceIdentifier(),
                socialId,
                socialSite
            )

        }.addOnFailureListener {
            Log.d(TAG, "exception is $it")
        }
    }

    private fun openDatePicker(datePicker: DatePickerDialog) {
        datePicker.show()

        // its not changing color by xml style so this is used to change ok and cancel button color.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            datePicker.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(QTheMusicApplication.getContext().getColor(R.color.color_black))
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            datePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(QTheMusicApplication.getContext().getColor(R.color.color_black))
        }
    }

    private fun showPictureDialog() {
        mViewModel.mChooserFragment = ChooserDialogFragment.newInstance(CommonKeys.TYPE_PHOTO,
            object : ChooserDialogFragment.CallBack {
                override fun onActivityResult(mImageUri: List<Uri>?) {
                    mViewModel.uri = mImageUri?.get(0)
                    val contentURI = mViewModel.uri
                    try {
                        if (Build.VERSION.SDK_INT < 28) {
                            val bitmap = MediaStore.Images.Media.getBitmap(
                                requireActivity().contentResolver,
                                contentURI
                            )
                            mBinding.ivUserProfilePic.setImageBitmap(bitmap)
                        } else {
                            val source =
                                contentURI?.let { uri ->
                                    ImageDecoder.createSource(
                                        requireActivity().contentResolver,
                                        uri
                                    )
                                }
                            val bitmap = source?.let { ImageDecoder.decodeBitmap(it) }
                            mBinding.ivUserProfilePic.setImageBitmap(bitmap)
                        }
                        mViewModel.uri = contentURI

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(
                            QTheMusicApplication.getContext(),
                            "Failed!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })
        mViewModel.mChooserFragment.show(
            childFragmentManager,
            ChooserDialogFragment::class.java.toString()
        )
    }

    private fun checkIfAddressNotEmpty(): Boolean {
        if (mBinding.etUserCity.text.toString()
                .isEmpty() && mBinding.etUserAddress.text.isNullOrEmpty().not()
        ) {
            mBinding.etUserCity.error = getString(R.string.city_required)
            return false
        } else if (mBinding.etUserState.text.toString()
                .isEmpty() && mBinding.etUserAddress.text.isNullOrEmpty().not()
        ) {
            mBinding.etUserState.error = getString(R.string.state_required)
            return false
        } else if (mBinding.etUserCountry.text.toString()
                .isEmpty() && mBinding.etUserAddress.text.isNullOrEmpty().not()
        ) {
            mBinding.etUserCountry.error = getString(R.string.country_required)
            return false
        } else if (mBinding.etZipCode.text.toString()
                .isEmpty() && mBinding.etUserAddress.text.isNullOrEmpty().not()
        ) {
            mBinding.etZipCode.error = getString(R.string.zipcode_required)
            return false
        } else
            return true
    }

    private fun checkIfAddressRelatedFieldNotEmpty(): Boolean {
        if (mBinding.etUserCity.text.toString().isNotEmpty()) {
            return true
        } else if (mBinding.etUserState.text.toString().isNotEmpty()) {
            return true
        } else if (mBinding.etUserCountry.text.toString().isNotEmpty()) {
            return true
        } else return mBinding.etZipCode.text.toString().isNotEmpty()
    }

    companion object {
        private const val TAG = "SignUpFragment"
    }
}