package com.techswivel.qthemusic.ui.activities.profileSettingScreen

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.AlbumStatus
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.OtpType
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.databinding.ActivityProfileSettingBinding
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.services.exoService.MainService
import com.techswivel.qthemusic.source.remote.networkViewModel.AuthNetworkViewModel
import com.techswivel.qthemusic.ui.base.PlayerBaseActivity
import com.techswivel.qthemusic.ui.fragments.otpVerificationFragment.OtpVerification
import com.techswivel.qthemusic.ui.fragments.profileSettingFragment.ProfileSettingFragment
import com.techswivel.qthemusic.ui.fragments.profileUpdatingFragment.ProfileUpdatingFragment
import com.techswivel.qthemusic.utils.*


class ProfileSettingActivity : PlayerBaseActivity(), ProfileSettingActivityImpl {

    private lateinit var mBinding: ActivityProfileSettingBinding
    private var mFragment: Fragment? = null
    private lateinit var mFragmentt: Fragment
    private lateinit var viewModel: ProfileSettingViewModel
    private lateinit var mAuthActivityViewModel: AuthNetworkViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityProfileSettingBinding.inflate(layoutInflater)
        initViewModel()
        setContentView(mBinding.root)
        setToolBar()
        getBundleData()
        setObserver()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerService?.isAddProfileSettingActivity(false)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (::mFragmentt.isInitialized) {
            mFragmentt.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onBackPressed() {

        if (getEntryCount() == 3) {
            mBinding.activityToolbar.toolbarTitle.text = getString(R.string.profile_settings)

        }
        if (getEntryCount() == 2) {
            mBinding.activityToolbar.toolbarTitle.text = getString(R.string.settings)

        }
        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        if (getEntryCount() == 3) {
            mBinding.activityToolbar.toolbarTitle.text = getString(R.string.profile_settings)

        }
        if (getEntryCount() == 2) {
            mBinding.activityToolbar.toolbarTitle.text = getString(R.string.settings)

        }

        if (getEntryCount() == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
        return super.onSupportNavigateUp()
    }

    override fun replaceCurrentFragment(fragment: Fragment) {
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.profile_settings)
        mFragmentt = fragment
        replaceFragment(mBinding.mainContainer.id, fragment)
    }

    override fun openAuthActivityWithPhoneNo(phoneNumber: String?, otpType: OtpType) {
        mBinding.activityToolbar.toolbarTitle.text = ""
        viewModel.phone = phoneNumber
        val bundle = Bundle()
        bundle.putString(CommonKeys.KEY_PHONE_NUMBER, phoneNumber)
        bundle.putSerializable(CommonKeys.OTP_TYPE, otpType)
        openFragment(OtpVerification.newInstance(bundle))

    }

    override fun verifyOtpRequest(authRequestBuilder: AuthRequestModel) {
        viewModel.phone = authRequestBuilder.phoneNumber
        viewModel.authRequestModel = authRequestBuilder
        mAuthActivityViewModel.verifyOtpResponse(authRequestBuilder)
    }

    override fun sendOpt(authRequestBuilder: AuthRequestModel) {
        mAuthActivityViewModel.sendOtpRequest(authRequestBuilder)
    }


    override fun showProgressBar() {
        mBinding.settingsProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.settingsProgressBar.visibility = View.GONE
    }

    override fun openProfileSettingFragmentWithPnone(phoneNumber: String?) {
    }

    override fun openProfileSettingFragmentWithName(authModel: AuthModel?) {
    }

    override fun openProfileSettingFragmentWithAddress(authModel: AuthModel?) {
    }

    override fun openProfileSettingFragmentWithGender(authModel: AuthModel?) {
    }

    override fun onProgressUpdate(timeInMis: Int) {
        mBinding.playLayout.seekBarPlayer.progress = timeInMis
    }

    override fun onPlayerStateIdle() {

    }

    override fun onPlayerStateBuffering() {

    }

    override fun onPlayerStateReady() {

    }

    override fun onPlayerStateEnded(songStatus: SongStatus?, albumStatus: AlbumStatus?) {

    }

    override fun onServiceBind(serviceBinder: MainService.MainServiceBinder?) {
        super.onServiceBind(serviceBinder)
        if (serviceBinder?.isBinderAlive == true && serviceBinder.getExoPlayerInstance()
                .isAudioPlayerPlaying()
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            onCurrentSongUpdate(serviceBinder.currentSong)
        } else if ((mPlayerService?.isBinderAlive == true) && (mPlayerService?.getExoPlayerInstance()
                ?.isPlayerRelease() == false)
        ) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            onCurrentSongUpdate(mPlayerService?.currentSong ?: Song())
        }
        serviceBinder?.isAddProfileSettingActivity(true)
    }

    override fun readyToShowVideoPlayerPlayIcons() {
        super.readyToShowVideoPlayerPlayIcons()
        if (mPlayerService?.getExoPlayerInstance()?.isAudioPlayerPlaying() == true) {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_pause
                )
            )
        } else {
            mBinding.playLayout.icPlayPaused.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_play
                )
            )
        }
    }

    override fun onPlayerServiceStop() {
        mBinding.playLayout.playerParentLayout.visibility = View.GONE
    }

    override fun onPlayerServiceStart() {
        mBinding.playLayout.playerParentLayout.visibility = View.VISIBLE
    }


    override fun onProgressBarUpdate(current: Int, max: Int) {
        mBinding.playLayout.seekBarPlayer.max = max
        mBinding.playLayout.seekBarPlayer.progress = current
    }

    override fun onPlayerStatusChanged(data: PlayerState) {
        if (data.isNotificationRemoved) {
            mBinding.playLayout.playerParentLayout.visibilityGone()
        } else if (data.isPlaying) {
            mBinding.playLayout.playerParentLayout.visibilityVisible()
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        } else {
            readyToShowVideoPlayerPlayIcons()
            mBinding.playLayout.data = data.currentSong
        }
    }

    override fun onCurrentSongUpdate(data: Song) {
        super.onCurrentSongUpdate(data)
        mBinding.playLayout.data = data
        mBinding.playLayout.executePendingBindings()
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {
    }

    private fun setObserver() {
        mAuthActivityViewModel.otpObserver.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    (mFragmentt as OtpVerification).hideProgressBar()

                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mFragmentt as OtpVerification).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mFragmentt as OtpVerification).hideProgressBar()
                            }
                        }
                    )
                }
            }
        })


        mAuthActivityViewModel.otpVerificationResponse.observe(
            this,
            Observer { api_response ->
                when (api_response.status) {
                    NetworkStatus.LOADING -> {
                        showProgressBar()
                        (mFragmentt as OtpVerification).showProgressBar()
                    }
                    NetworkStatus.SUCCESS -> {
                        hideProgressBar()
                        (mFragmentt as OtpVerification).hideProgressBar()
                        val authModelBilder = AuthRequestBuilder()
                        authModelBilder.mPhoneNumber =
                            Utilities.toRequestedBody(viewModel.authRequestModel.phoneNumber)
                        authModelBilder.mOtp = viewModel.authRequestModel.otp
                        mAuthActivityViewModel.updateProfile(authModelBilder)
                    }

                    NetworkStatus.EXPIRE -> {
                        hideProgressBar()
                        val error = api_response.error as ErrorResponse
                        DialogUtils.runTimeAlert(this,
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    (mFragmentt as OtpVerification).hideProgressBar()
                                }

                                override fun onNegativeCallBack() {
                                }
                            }
                        )
                    }

                    NetworkStatus.ERROR -> {
                        hideProgressBar()
                        val error = api_response.error as ErrorResponse
                        DialogUtils.runTimeAlert(this,
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    (mFragmentt as OtpVerification).hideProgressBar()
                                }

                                override fun onNegativeCallBack() {
                                }
                            }
                        )
                    }
                }
            })
        mAuthActivityViewModel.profileUpdationResponse.observe(this) { updateProfileResponse ->
            when (updateProfileResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    val bundle = Bundle()
                    bundle.putString(CommonKeys.KEY_DATA_PHONE, viewModel.phone)
                    //this logic needs to improve
                    val profileUpdatingFragment = ProfileUpdatingFragment()
                    profileUpdatingFragment.arguments = bundle
                    replaceFragmentWithoutAddingToBackStack(
                        mBinding.mainContainer.id,
                        profileUpdatingFragment
                    )
                    mFragmentt = profileUpdatingFragment
                    mBinding.activityToolbar.toolbarTitle.text =
                        getString(R.string.profile_settings)

                    Toast.makeText(
                        this,
                        getString(R.string.phone_number_added),
                        Toast.LENGTH_SHORT
                    ).show()

                }

                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = updateProfileResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }

                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = updateProfileResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@ProfileSettingActivity)
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }

                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                    Log.v("Network_status", "completed")
                }

            }
        }
    }

    private fun openSettingFragment() {
        popUpAllFragmentIncludeThis(ProfileSettingFragment::class.java.name)
        openFragment(ProfileSettingFragment.newInstance())
    }

    private fun openUpdatingFragment() {
        var bundle = Bundle()
        bundle.putString(CommonKeys.KEY_PHONE_NUMBER, viewModel.phone)
        viewModel.fragment = ProfileUpdatingFragment.newInstance()
        openFragment(ProfileUpdatingFragment.newInstance(bundle))
    }


    private fun openFragment(fragment: Fragment) {
        ::mFragment.set(fragment)
        mFragment.let { fragmentInstance ->
            fragmentInstance?.let { fragmentToBeReplaced ->
                replaceFragment(mBinding.mainContainer.id, fragmentToBeReplaced)
            }
        }
    }

    private fun openProfileUpdatingFragment(fragment: Fragment) {
        mFragmentt = fragment
        ::mFragmentt.set(fragment)
        mFragment.let { fragmentInstance ->
            fragmentInstance?.let { fragmentToBeReplaced ->
                replaceFragmentWithoutAddingToBackStack(
                    mBinding.mainContainer.id,
                    fragmentToBeReplaced
                )
            }
        }
    }


    private fun setToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar, "", false, true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.settings)
    }


    private fun initViewModel() {
        viewModel =
            ViewModelProvider(this).get(ProfileSettingViewModel::class.java)
        mAuthActivityViewModel =
            ViewModelProvider(this).get(AuthNetworkViewModel::class.java)
    }

    private fun getBundleData() {
        val bundle = intent.extras
        var phone: String? = null
        phone = bundle?.getString(CommonKeys.KEY_PHONE_NUMBER)
        viewModel.phone = phone
        if (phone != null) {
            openUpdatingFragment()
        } else {
            openSettingFragment()
        }

    }

    companion object {
        private const val TAG = "ProfileSettingActivity"
    }
}