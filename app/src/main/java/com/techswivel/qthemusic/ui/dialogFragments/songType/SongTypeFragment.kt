package com.techswivel.qthemusic.ui.dialogFragments.songType

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.databinding.SongTypeFragmentBinding
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.BaseDialogFragment
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.Utilities
import com.techswivel.qthemusic.utils.PermissionUtils

class SongTypeFragment : BaseDialogFragment() {

    companion object {
        private const val TAG = "SongTypeFragment"
        fun newInstance(song: Song, callback: Callback) = SongTypeFragment().apply {
            val bundle = Bundle()
            bundle.putParcelable(KEY_DATA, song)
            this.arguments = bundle
            this.setCallback(callback)
        }
    }

    private lateinit var viewModel: SongTypeViewModel
    private lateinit var mBinding: SongTypeFragmentBinding
    private lateinit var mCallBack: Callback

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        mBinding = SongTypeFragmentBinding.inflate(inflater, container, false)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[SongTypeViewModel::class.java]
        viewModel.mBundle = arguments ?: Bundle.EMPTY
        if (viewModel.mSongData.songVideoUrl.isNullOrEmpty()) {
            mBinding.songDownloadVideo.visibility = View.GONE
            mBinding.songTypeVideo.visibility = View.GONE
        }
        mBinding.close.setOnClickListener {
            dismiss()
        }
        mBinding.songDownloadAudio.setOnClickListener {
            if (PermissionUtils.isStoragePermissionGranted(context)) {
                mCallBack.onDownloadingStart(ActionType.ADD, viewModel.mSongData, SongType.AUDIO)
                mBinding.close.performClick()
            } else {
                PermissionUtils.requestStoragePermission(activity)
            }

        }
        mBinding.songDownloadVideo.setOnClickListener {
            if (PermissionUtils.isStoragePermissionGranted(context)) {
                mCallBack.onDownloadingStart(ActionType.ADD, viewModel.mSongData, SongType.VIDEO)
                mBinding.close.performClick()
            } else {
                PermissionUtils.requestStoragePermission(activity)
            }
        }

    }

    private fun setCallback(callback: Callback) {
        this.mCallBack = callback
    }

    interface Callback {
        fun onDownloadingStart(actionType: ActionType, song: Song, songType: SongType)
    }
}