package com.techswivel.qthemusic.ui.fragments.downloadSongFragment

import com.techswivel.qthemusic.models.AuthModel
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class DownloadSongFragmentViewModel : LocalDatabaseViewModel() {

    var mAuthModel: AuthModel? = null
    var mDownloadedSongsList: ArrayList<Any> = ArrayList()
    var mLocalDownloadedSongsList: ArrayList<Any> = ArrayList()
    var mRemoteDownloadedSongsList: ArrayList<Any> = ArrayList()
    var mSongData: ArrayList<Song> = ArrayList()
    fun processSongsData(): Boolean {
        mDownloadedSongsList.clear()
        mDownloadedSongsList.addAll(mRemoteDownloadedSongsList)
        mLocalDownloadedSongsList.forEach { item ->
            val index = mRemoteDownloadedSongsList.indexOfFirst {
                (it as Song).songId == (item as Song).songId
            }
            if (index != -1) {
                mDownloadedSongsList[index] = item
            } else {
                mDownloadedSongsList.add(item)
            }
        }
        return mDownloadedSongsList.isNotEmpty()
    }

    companion object {
        private const val TAG = "DownloadSongFragmentVie"
    }

}