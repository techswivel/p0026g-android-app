package com.techswivel.qthemusic.ui.activities.playerActivity

import androidx.lifecycle.viewModelScope
import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.models.AuthModel
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class PlayerActivityViewModel : BaseViewModel() {
    var isFromPurchasedResponse = false
    var isOnForground = false
    var isInternetAvailable = true
    lateinit var mSongBuilder: NextPlaySongBuilder
    var isFromService: Boolean = false
    var actionType: ActionType? = null
    lateinit var authData: AuthModel
    var isSongPause = false

    fun deleteSongById(songId: Int?) {
        viewModelScope.launch {
            mLocalDataManager.deleteSongById(songId)
        }
    }
}