package com.techswivel.qthemusic.ui.dialogFragments.unlockFragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.billingclient.api.BillingClient
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.ItemType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.PurchaseType
import com.techswivel.qthemusic.customData.enums.UnlockType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.customData.interfaces.PurchasingMediaCallback
import com.techswivel.qthemusic.databinding.UnLockFragmentBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.InAppPurchasesViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity.SubscriptionPlansActivity
import com.techswivel.qthemusic.ui.base.BaseDialogFragment
import com.techswivel.qthemusic.utils.*
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_ALBUM
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_SONG
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_TYPE

class UnLockFragment : BaseDialogFragment(), BaseInterface {
    protected var mPlayerSongs: NextPlaySongBuilder = NextPlaySongBuilder()
    private lateinit var mSongAndArtistsViewModel: SongAndArtistsViewModel

    companion object {
        private val TAG = "UnLockFragment"

        /**
         * In order to work properly this fragment you need to pass either song instance or album instance
         * because it check if songs is null only then show the album data
         * */
        fun newInstance(
            type: UnlockType,
            song: Song? = null,
            album: Album? = null,
            purchasingMediaCallback: PurchasingMediaCallback
        ) =
            UnLockFragment().apply {
                setCallback(purchasingMediaCallback)
                val bundle = Bundle()
                bundle.putSerializable(KEY_TYPE, type)
                if (song != null)
                    bundle.putParcelable(KEY_SONG, song)
                if (album != null)
                    bundle.putParcelable(KEY_ALBUM, album)
                this.arguments = bundle
            }
    }

    private lateinit var viewModel: UnLockViewModel
    private lateinit var mBinding: UnLockFragmentBinding
    private lateinit var inAppPurchasesViewModel: InAppPurchasesViewModel
    private lateinit var mPurchasingMediaCallback: PurchasingMediaCallback

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        mBinding = UnLockFragmentBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSongAndArtistsViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
        viewModel = ViewModelProvider(this)[UnLockViewModel::class.java]
        inAppPurchasesViewModel = ViewModelProvider(this)[InAppPurchasesViewModel::class.java]
        inAppPurchasesViewModel.setUpBillingClient(activity ?: requireActivity(), this)
        viewModel.bundle = arguments
        mBinding.album = viewModel.albumData
        mBinding.song = viewModel.songData
        setObserver()
        setListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (inAppPurchasesViewModel.billingClient != null) {
            inAppPurchasesViewModel.billingClient?.endConnection()
        }
    }

    override fun showProgressBar() {
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.progressBar.visibility = View.GONE
    }

    private fun setCallback(callBack: PurchasingMediaCallback) {
        mPurchasingMediaCallback = callBack
    }

    private fun setObserver() {
        inAppPurchasesViewModel.isCalled.observe(viewLifecycleOwner, Observer {
            val count = PrefUtils.getInt(requireContext(), CommonKeys.INCREAMENT_VALUE)
            Log.d(TAG, "count is $count")
            if (count > Constants.COUNT_MAX_VALUE) {
                PrefUtils.clearStringData(
                    requireContext(),
                    CommonKeys.INCREAMENT_VALUE
                )
                DialogUtils.runTimeAlert(
                    requireContext(),
                    getString(R.string.clear_cache_tittle),
                    getString(R.string.clear_cache_msg),
                    getString(R.string.go_to_settings),
                    getString(R.string.cancel),
                    object : DialogUtils.CallBack {
                        override fun onPositiveCallBack() {
                            PrefUtils.clearStringData(
                                requireContext(),
                                CommonKeys.INCREAMENT_VALUE
                            )
                            Utilities.openPlayStoreSettings(requireActivity())
                        }

                        override fun onNegativeCallBack() {

                        }
                    })
            }
        })
        inAppPurchasesViewModel.purchaseToken.observe(this) { purchaseToken ->
            val subscribeToPlanBodyBuilder = SubscribeToPlanBodyBuilder()
            if (viewModel.albumData != null) {
                subscribeToPlanBodyBuilder.purchaseToken = purchaseToken.purchaseToken
                subscribeToPlanBodyBuilder.albumId = viewModel.albumData?.albumId
                subscribeToPlanBodyBuilder.itemType = ItemType.ALBUM
                subscribeToPlanBodyBuilder.purchaseType = PurchaseType.GOOGLE_PAY
                subscribeToPlanBodyBuilder.paidAmount = viewModel.albumData?.price?.toFloat()

                subscribeToPlanBodyBuilder.subscriptionPlan = FailedPurchased(
                    viewModel.albumData?.albumId,
                    isAcknowledge = false,
                    isFromLogin = false,
                    itemType = ItemType.ALBUM,
                    sku = viewModel.albumData?.sku
                )

            } else if (viewModel.songData != null) {
                subscribeToPlanBodyBuilder.purchaseToken = purchaseToken.purchaseToken
                subscribeToPlanBodyBuilder.songId = viewModel.songData?.songId
                subscribeToPlanBodyBuilder.itemType = ItemType.SONG
                subscribeToPlanBodyBuilder.purchaseType = PurchaseType.GOOGLE_PAY
                subscribeToPlanBodyBuilder.paidAmount = viewModel.songData?.songPrice?.toFloat()

                subscribeToPlanBodyBuilder.subscriptionPlan = FailedPurchased(
                    viewModel.songData?.songId,
                    isAcknowledge = false,
                    isFromLogin = false,
                    itemType = ItemType.SONG,
                    sku = viewModel.songData?.sku
                )
            }
            inAppPurchasesViewModel.subscribeToPlan(subscribeToPlanBodyBuilder)

        }

        inAppPurchasesViewModel.subscribeToPlanResponse.observe(requireActivity()) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    val response = apiResponse.t as ResponseModel
                    if (response.data.purchasedSong != null) {
                        inAppPurchasesViewModel.acknowledgePurchase(
                            inAppPurchasesViewModel.purchaseToken.value?.purchaseToken,
                            activity ?: requireActivity()
                        )
                        mPurchasingMediaCallback.onPurchase(response.data.purchasedSong, null)
                        dismiss()
                    } else if (response.data.purchasedAlbum != null) {
                        inAppPurchasesViewModel.acknowledgePurchase(
                            inAppPurchasesViewModel.purchaseToken.value?.purchaseToken,
                            activity ?: requireActivity()
                        )
                        mPurchasingMediaCallback.onPurchase(
                            null,
                            response.data.purchasedAlbum?.songs
                        )
                        dismiss()
                    }
                }
                NetworkStatus.ERROR -> {
                    val error = apiResponse.error as ErrorResponse
                    val msg = "${error.code} ${error.message}"
                    Log.d(TAG, "error called ${error.code} ${error.message}")
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        msg,
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        })
                }
                NetworkStatus.EXPIRE -> {
                    DialogUtils.sessionExpireAlert(requireContext(),
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(activity ?: requireActivity())
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }
    }

    private fun setListeners() {
        mBinding.closeLayout.setOnClickListener {
            dismiss()
        }
        mBinding.payNow.setOnClickListener {
            if (viewModel.albumData != null) {
                inAppPurchasesViewModel.connectToGooglePlayBilling(
                    viewModel.albumData?.sku ?: "", activity ?: requireActivity(),
                    BillingClient.SkuType.INAPP
                )
            } else if (viewModel.songData != null) {
                inAppPurchasesViewModel.connectToGooglePlayBilling(
                    viewModel.songData?.sku ?: "", activity ?: requireActivity(),
                    BillingClient.SkuType.INAPP
                )
            }
        }
        mBinding.buyPackage.setOnClickListener {
            ActivityUtils.startNewActivity(
                activity ?: requireActivity(),
                SubscriptionPlansActivity::class.java
            )
        }
    }
}