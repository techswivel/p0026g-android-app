package com.techswivel.qthemusic.ui.fragments.profileSettingFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentProfileSettingBinding
import com.techswivel.qthemusic.models.AuthRequestBuilder
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.source.local.preference.DataStoreUtils
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.AuthNetworkViewModel
import com.techswivel.qthemusic.ui.activities.authActivity.AuthActivity
import com.techswivel.qthemusic.ui.activities.profileSettingScreen.ProfileSettingActivityImpl
import com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity.SubscriptionPlansActivity
import com.techswivel.qthemusic.ui.base.BaseFragment
import com.techswivel.qthemusic.ui.fragments.profileUpdatingFragment.ProfileUpdatingFragment
import com.techswivel.qthemusic.ui.fragments.termAndConditionFragment.TermAndConditionFragment
import com.techswivel.qthemusic.ui.fragments.underDevelopmentMessageFragment.profile_setting_fragment.ProfileSettingFragmentViewModel
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.DialogUtils
import com.techswivel.qthemusic.utils.Log
import kotlinx.coroutines.runBlocking


class ProfileSettingFragment : BaseFragment(), BaseInterface {

    companion object {
        private val TAG = "ProfileSettingFragment"
        fun newInstance() = ProfileSettingFragment()
    }

    private lateinit var netWorkViewModel: AuthNetworkViewModel
    private lateinit var viewModel: ProfileSettingFragmentViewModel
    private lateinit var mBinding: FragmentProfileSettingBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentProfileSettingBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        viewModel.authModel = viewModel.getPrefrencesData(QTheMusicApplication.getContext())
        clickListeners()
        bindViewModelWithView()
        setObserver()
    }

    override fun showProgressBar() {
        mBinding.progressBar.visibility = View.VISIBLE
        mBinding.logoutButton.isEnabled = false
    }

    override fun hideProgressBar() {
        mBinding.progressBar.visibility = View.INVISIBLE
        mBinding.logoutButton.isEnabled = true

    }

    private fun setObserver() {
        netWorkViewModel.profileUpdationResponse.observe(requireActivity()) { updateProfileResponse ->
            when (updateProfileResponse.status) {
                NetworkStatus.LOADING -> {
                }
                NetworkStatus.SUCCESS -> {
                    val mResponseModel = updateProfileResponse.t as ResponseModel
                    mResponseModel.data.authModel?.notification?.isArtistUpdateEnabled?.let { isArtistUpdateEnabled ->
                        PrefUtils.setBoolean(
                            requireContext(), CommonKeys.KEY_ARTIST_UPDATE,
                            isArtistUpdateEnabled
                        )
                    }
                    mResponseModel.data.authModel?.notification?.isNotificationEnabled?.let { isNotificationEnabled ->
                        PrefUtils.setBoolean(
                            requireContext(), CommonKeys.KEY_USER_ENABLE_NOTIFICATION,
                            isNotificationEnabled
                        )
                    }

                }
                NetworkStatus.ERROR -> {
                    val error = updateProfileResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    DialogUtils.sessionExpireAlert(QTheMusicApplication.getContext(),
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                runBlocking {
                                    DataStoreUtils.clearAllPreference()
                                }
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {

                }
            }
        }

        netWorkViewModel.logoutResponse.observe(requireActivity()) { logoutResponse ->
            when (logoutResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    LoginManager.getInstance().logOut()
                    GoogleSignIn.getClient(
                        activity ?: requireActivity(),
                        QTheMusicApplication.getGso()
                    ).signOut()
                    Toast.makeText(
                        QTheMusicApplication.getContext(),
                        getString(R.string.logout_successfully),
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent = Intent(requireContext(), AuthActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = logoutResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    DialogUtils.sessionExpireAlert(QTheMusicApplication.getContext(),
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                runBlocking {
                                    DataStoreUtils.clearAllPreference()
                                }
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    Log.v("Network_status", "Logout completed")
                }
            }
        }
    }


    private fun clickListeners() {

        mBinding.premiumButtonId.setOnClickListener {
            val intent = Intent(requireContext(), SubscriptionPlansActivity::class.java)
            startActivity(intent)
        }

        mBinding.logoutButton.setOnClickListener {
            logoutUser()
        }


        mBinding.userProfileViewID.setOnClickListener {
            (mActivityListener as ProfileSettingActivityImpl).replaceCurrentFragment(
                ProfileUpdatingFragment()
            )
        }

        mBinding.premiumDetailsChangeTvId.setOnClickListener {
            val intent = Intent(requireContext(), SubscriptionPlansActivity::class.java)
            startActivity(intent)
        }

        mBinding.termAndConditionText.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean(CommonKeys.KEY_TERM_AND_CONDITION_PRIVACY, true)
            ActivityUtils.launchFragment(
                requireContext(),
                TermAndConditionFragment::class.java.name,
                bundle
            )
        }

        mBinding.privacyPolicyText.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean(CommonKeys.KEY_TERM_AND_CONDITION_PRIVACY, false)
            ActivityUtils.launchFragment(
                requireContext(),
                TermAndConditionFragment::class.java.name,
                bundle
            )
        }
        var isFirstNotificationBtnEnabled = viewModel.authModel?.notification?.isNotificationEnabled
        mBinding.switchEnableNotification.setOnClickListener(View.OnClickListener {
            if (mBinding.switchEnableNotification.isChecked) {
                isFirstNotificationBtnEnabled = true
                val authRequestBuilder = AuthRequestBuilder()
                authRequestBuilder.mIsEnableNotification = true
                mBinding.customSwitchFollowArtistUpdate.isEnabled = true
                updateProfile(authRequestBuilder)
            } else {
                isFirstNotificationBtnEnabled = false
                mBinding.customSwitchFollowArtistUpdate.isEnabled = false
                mBinding.customSwitchFollowArtistUpdate.isChecked = false
                val authRequestBuilder = AuthRequestBuilder()
                authRequestBuilder.mIsEnableNotification = false
                authRequestBuilder.mIsArtistUpdateEnable = false
                updateProfile(authRequestBuilder)
            }
        })
        mBinding.customSwitchFollowArtistUpdate.isEnabled = isFirstNotificationBtnEnabled == true
        mBinding.customSwitchFollowArtistUpdate.setOnClickListener {
            if (isFirstNotificationBtnEnabled == true) {
                if (mBinding.customSwitchFollowArtistUpdate.isChecked) {
                    val authRequestBuilder = AuthRequestBuilder()
                    authRequestBuilder.mIsArtistUpdateEnable = true
                    updateProfile(authRequestBuilder)
                } else {
                    val authRequestBuilder = AuthRequestBuilder()
                    authRequestBuilder.mIsArtistUpdateEnable = false
                    updateProfile(authRequestBuilder)
                }
            }
        }
    }

    private fun updateProfile(authRequestBuilder: AuthRequestBuilder) {
        netWorkViewModel.updateProfile(authRequestBuilder)
    }

    private fun logoutUser() {
        netWorkViewModel.logoutUser()
    }

    private fun initViewModel() {
        viewModel =
            ViewModelProvider(this).get(ProfileSettingFragmentViewModel::class.java)

        netWorkViewModel =
            ViewModelProvider(this).get(AuthNetworkViewModel::class.java)
    }

    private fun bindViewModelWithView() {
        mBinding.viewModel = viewModel
        mBinding.executePendingBindings()
    }

}