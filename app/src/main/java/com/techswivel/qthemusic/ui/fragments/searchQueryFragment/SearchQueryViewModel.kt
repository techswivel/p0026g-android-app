package com.techswivel.qthemusic.ui.fragments.searchQueryFragment

import com.techswivel.qthemusic.models.Language
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class SearchQueryViewModel : LocalDatabaseViewModel() {
    var searchedSongsDataList: MutableList<Any> = ArrayList()
    var searchedLanguagesDataList: MutableList<Language> = ArrayList()
    var searchedLanguagesForRecyclerView: MutableList<Any> = ArrayList()
    var recentPlayedSongsList: MutableList<Any> = ArrayList()
    var languageTittle: String? = ""
    var languagesId: Int = 0
    var queryToSearch = ""
    var albumSongsList: MutableList<Any> = ArrayList()
    var isSearchBoxEmpty: Boolean = false
}