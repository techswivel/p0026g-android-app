package com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.billingclient.api.BillingClient
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.FragmentType
import com.techswivel.qthemusic.customData.enums.ItemType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.PurchaseType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.ActivitySubscriptionPlansBinding
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.SubscriptionPlans
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.InAppPurchasesViewModel
import com.techswivel.qthemusic.ui.activities.mainActivity.MainActivity
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseActivity
import com.techswivel.qthemusic.ui.fragments.noInternetFragment.NoInternetFragment
import com.techswivel.qthemusic.utils.*
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA

class SubscriptionPlansActivity : RecyclerViewBaseActivity(),
    RecyclerViewAdapter.CallBack, BaseInterface {

    private lateinit var binding: ActivitySubscriptionPlansBinding
    private lateinit var viewModel: SubscriptionPlansActivityViewModel
    private lateinit var inAppPurchasesViewModel: InAppPurchasesViewModel
    private lateinit var mAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubscriptionPlansBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        callApi()
        setObserver()
        setListeners()
        swipeToRefresh()
    }

    override fun onInternetAvailable() {

    }

    override fun onConnectionDisable() {
        ActivityUtils.launchFragment(this, NoInternetFragment::class.java.name)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (inAppPurchasesViewModel.billingClient != null) {
            inAppPurchasesViewModel.billingClient?.endConnection()
        }
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mAdapter
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_subscription_plan
    }

    override fun onNoDataFound() {
    }

    override fun onViewClicked(view: View, data: Any?) {
        super.onViewClicked(view, data)
        viewModel.subscriptionPlans = data as SubscriptionPlans
        when (view.id) {
            R.id.btn_choose_plan -> {
                val planId = PrefUtils.getInt(
                    this,
                    CommonKeys.KEY_USER_PLAN_ID
                )
                if (planId != 0) {
                    Toast.makeText(this, getString(R.string.subscription_error), Toast.LENGTH_SHORT)
                        .show()
                } else {
                    inAppPurchasesViewModel.connectToGooglePlayBilling(
                        viewModel.subscriptionPlans.sku ?: "", this,
                        BillingClient.SkuType.SUBS,
                    )
                }
            }
        }
    }


    override fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun initView() {
        viewModel = ViewModelProvider(this)[SubscriptionPlansActivityViewModel::class.java]
        inAppPurchasesViewModel = ViewModelProvider(this)[InAppPurchasesViewModel::class.java]
        mAdapter = RecyclerViewAdapter(this, viewModel.subscriptionPlansList)
        setUpRecyclerView(
            binding.rvSubscriptionPlans,
            LinearLayoutManager.VERTICAL
        )
        inAppPurchasesViewModel.setUpBillingClient(this, null)
        if (intent.hasExtra(KEY_DATA)) {
            viewModel.flowType = intent.getSerializableExtra(KEY_DATA) as FragmentType
        }
    }

    private fun setObserver() {
        inAppPurchasesViewModel.isCalled.observe(this, Observer {
            val count = PrefUtils.getInt(this, CommonKeys.INCREAMENT_VALUE)
            if (count > Constants.COUNT_MAX_VALUE) {
                PrefUtils.clearStringData(
                    this@SubscriptionPlansActivity,
                    CommonKeys.INCREAMENT_VALUE
                )
                DialogUtils.runTimeAlert(
                    this,
                    getString(R.string.clear_cache_tittle),
                    getString(R.string.clear_cache_msg),
                    getString(R.string.go_to_settings),
                    getString(R.string.cancel),
                    object : DialogUtils.CallBack {
                        override fun onPositiveCallBack() {
                            Utilities.openPlayStoreSettings(this@SubscriptionPlansActivity)
                        }

                        override fun onNegativeCallBack() {

                        }
                    })
            }
        })

        inAppPurchasesViewModel.purchaseToken.observe(this) { purchaseToken ->
            val subscribeToPlanBodyBuilder = SubscribeToPlanBodyBuilder()
            subscribeToPlanBodyBuilder.purchaseToken = purchaseToken.purchaseToken
            subscribeToPlanBodyBuilder.planId = viewModel.subscriptionPlans.planId
            subscribeToPlanBodyBuilder.itemType = ItemType.PLAN_SUBSCRIPTION
            subscribeToPlanBodyBuilder.purchaseType = PurchaseType.GOOGLE_PAY
            subscribeToPlanBodyBuilder.paidAmount = viewModel.subscriptionPlans.planPrice?.toFloat()
            subscribeToPlanBodyBuilder.subscriptionPlan = FailedPurchased(
                viewModel.subscriptionPlans.planId,
                isAcknowledge = false,
                isFromLogin = false,
                itemType = ItemType.PLAN_SUBSCRIPTION,
                sku = viewModel.subscriptionPlans.sku
            )
            inAppPurchasesViewModel.subscribeToPlan(
                subscribeToPlanBodyBuilder
            )
        }

        inAppPurchasesViewModel.subscribeToPlanResponse.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    val response = apiResponse.t as ResponseModel
                    val purchasedSubscription = response.data.purchasedSubscription
                    if (purchasedSubscription != null) {
                        inAppPurchasesViewModel.acknowledgePurchase(
                            inAppPurchasesViewModel.purchaseToken.value?.purchaseToken,
                            this
                        )
                        saveDataInPrefs(purchasedSubscription)
                        QTheMusicApplication.subscribeToTopic(purchasedSubscription.planTopic ?: "")
                    }
                }
                NetworkStatus.ERROR -> {
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        apiResponse.error?.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    DialogUtils.sessionExpireAlert(this,
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@SubscriptionPlansActivity)
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }

        inAppPurchasesViewModel.subscriptionPlansResponse.observe(this) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                    binding.tvNoDataFound.visibilityGone()
                    binding.shimmerLayout.visibilityVisible()
                    binding.shimmerLayout.startShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    if (binding.swipeSubPlanContainer.isRefreshing) {
                        binding.swipeSubPlanContainer.isRefreshing = false
                    }
                    binding.shimmerLayout.stopShimmer()
                    binding.shimmerLayout.visibilityGone()
                    viewModel.subscriptionPlansList.clear()
                    val response = apiResponse.t as ResponseModel
                    val subscriptionPlans = response.data.subscriptionPlans

                    if (!subscriptionPlans.isNullOrEmpty()) {
                        viewModel.subscriptionPlansList.addAll(subscriptionPlans)
                    } else {
                        binding.tvNoDataFound.visibilityVisible()
                    }
                    if (::mAdapter.isInitialized)
                        mAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    binding.shimmerLayout.stopShimmer()
                    binding.shimmerLayout.visibility = View.GONE
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error),
                        apiResponse.error?.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    binding.shimmerLayout.stopShimmer()
                    binding.shimmerLayout.visibility = View.GONE
                    DialogUtils.sessionExpireAlert(this,
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(this@SubscriptionPlansActivity)
                            }

                            override fun onNegativeCallBack() {
                            }
                        })
                }
                NetworkStatus.COMPLETED -> {
                    binding.shimmerLayout.stopShimmer()
                    binding.shimmerLayout.visibility = View.GONE
                }
            }
        }
    }

    private fun saveDataInPrefs(purchasedSubscription: SubscriptionPlans) {
        purchasedSubscription.planId?.let { planId ->
            PrefUtils.setInt(
                this, CommonKeys.KEY_USER_PLAN_ID,
                planId
            )
        }
        purchasedSubscription.planTitle?.let {
            PrefUtils.setString(
                this, CommonKeys.KEY_USER_PLAN_TITLE,
                it
            )
        }
        purchasedSubscription.planPrice?.let {
            PrefUtils.setString(
                this, CommonKeys.KEY_USER_PLAN_PRIZE,
                it
            )
        }
        purchasedSubscription.planDuration?.let {
            PrefUtils.setString(
                this, CommonKeys.KEY_USER_PLAN_DURATION,
                it
            )
        }
        purchasedSubscription.planTopic?.let {
            PrefUtils.setString(
                this, CommonKeys.KEY_USER_PLAN_TOPIC,
                it
            )
        }
        purchasedSubscription.sku?.let {
            PrefUtils.setString(
                this, CommonKeys.KEY_USER_PLAN_SKU,
                it
            )
        }
    }

    private fun setListeners() {
        binding.ivClose.setOnClickListener {
            if ((viewModel.flowType != null) && (viewModel.flowType == FragmentType.AUTH_ACTIVITY)) {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            finish()
        }
    }

    private fun callApi() {
        inAppPurchasesViewModel.getSubscriptionPlansFromServer()
    }

    private fun swipeToRefresh() {
        binding.swipeSubPlanContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        viewModel.subscriptionPlansList.clear()
        mAdapter.notifyDataSetChanged()
        callApi()
    }


    companion object {
        private const val TAG = "SubscriptionPlans"
    }
}