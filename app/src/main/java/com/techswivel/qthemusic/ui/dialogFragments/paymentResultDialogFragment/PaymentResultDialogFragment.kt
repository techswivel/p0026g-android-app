package com.techswivel.qthemusic.ui.dialogFragments.paymentResultDialogFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.enums.PaymentResultType
import com.techswivel.qthemusic.databinding.FragmentPaymentResultDialogBinding
import com.techswivel.qthemusic.ui.base.BaseDialogFragment

class PaymentResultDialogFragment : BaseDialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(paymentResultType: PaymentResultType) =
            PaymentResultDialogFragment().apply {
                setPaymentResultType(paymentResultType)
            }
    }

    private lateinit var binding: FragmentPaymentResultDialogBinding
    private lateinit var viewModel: PaymentResultDialogFragmentViewModel
    private lateinit var mPaymentResultType: PaymentResultType

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setDialogStyle()
        binding = FragmentPaymentResultDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[PaymentResultDialogFragmentViewModel::class.java]
        initViews()
        setListeners()
    }

    private fun setPaymentResultType(value: PaymentResultType) {
        mPaymentResultType = value
    }

    private fun initViews() {
        if (mPaymentResultType == PaymentResultType.FAILED) {
            binding.lavGif.setAnimation(R.raw.payment_failed)
            binding.tvPaymentResult.text = getString(R.string.str_payment_failed)
        }
    }

    private fun setListeners() {
        binding.btnDone.setOnClickListener {
            dismiss()
        }
    }
}