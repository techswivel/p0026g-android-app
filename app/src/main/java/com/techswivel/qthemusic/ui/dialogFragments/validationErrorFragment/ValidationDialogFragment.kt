package com.techswivel.qthemusic.ui.dialogFragments.validationErrorFragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.techswivel.qthemusic.databinding.FragmentValidationDialogBinding


class ValidationDialogFragment(
    private vararg val emailError: String?,
) : BottomSheetDialogFragment() {
    private lateinit var mBinding: FragmentValidationDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentValidationDialogBinding.inflate(layoutInflater, container, false)
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
    }

    private fun setView() {
        emailError.forEach { errors ->
            mBinding.tvErrorEmailValidation.text = errors
        }
    }

    companion object {
        private val TAG = "ValidationDialogFragment"
    }
}