package com.techswivel.qthemusic.ui.activities.splashActivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.example.NotificationDataClass
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.models.PurchasedAlbum
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.ui.activities.mainActivity.MainActivity
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.Log

class SplashViewModel : BaseViewModel() {

    var isUserLogin: Boolean = false
    var isInterestSet: Boolean = false

    init {
        isUserLogin =
            PrefUtils.getBoolean(QTheMusicApplication.getContext(), CommonKeys.KEY_IS_LOGGED_IN)
        isInterestSet =
            PrefUtils.getBoolean(QTheMusicApplication.getContext(), CommonKeys.KEY_IS_INTEREST_SET)
    }


    fun getActivityFromNotification(pContext: Context, intent: Intent): Intent {
        try {
            val replaceString = intent.extras?.get("data").toString().replaceFirst("=", ":")
            val data = convertJsonStringToObject(replaceString, NotificationDataClass::class.java)
            val notificationType = data.notificationType
            if (notificationType == "RELEASED") {
                if (data.type == "PRE_ORDER_SONG") {
                    val preOrderSongData = convertJsonStringToObject(
                        data.notification ?: "",
                        Song::class.java
                    )
                    return getActivityIntent(
                        pContext,
                        preOrderSongData,
                        data.type
                    )
                } else {
                    val preOrderAlbumData = data.purchasedAlbum
                    return getActivityIntent(
                        pContext,
                        preOrderAlbumData,
                        data.type
                    )

                }
            } else {
                return Intent(pContext, MainActivity::class.java)
            }
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
            FirebaseCrashlytics.getInstance().recordException(e)
            return Intent(pContext, MainActivity::class.java)
        }
    }

    fun getActivityIntent(
        pContext: Context,
        payload: Any?,
        type: String?
    ): Intent {
        when (type) {
            "PRE_ORDER_SONG" -> {
                val notificationIntent = Intent(pContext, MainActivity::class.java)
                val bundle = Bundle().apply {
                    putParcelable(CommonKeys.KEY_DATA_MODEL, payload as Song)
                    putParcelableArrayList(
                        CommonKeys.KEY_SONGS_LIST,
                        arrayListOf(payload)
                    )
                    putString(
                        CommonKeys.KEY_SONG_TYPE,
                        SongType.NOTIFICATION.value
                    )
                }
                notificationIntent.putExtra(CommonKeys.KEY_DATA, bundle)
                notificationIntent.putExtra(CommonKeys.IS_FOR_SONG_PLAYLOAD, true)
                notificationIntent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                return notificationIntent
            }
            "PRE_ORDER_ALBUM" -> {
                val notificationIntent = Intent(pContext, MainActivity::class.java)
                val bundle = Bundle().apply {
                    putParcelable(
                        CommonKeys.KEY_DATA_MODEL,
                        (payload as PurchasedAlbum).songs?.first()
                    )
                    putParcelableArrayList(
                        CommonKeys.KEY_SONGS_LIST,
                        arrayListOf(payload.songs) as ArrayList<out Song>
                    )
                    putString(
                        CommonKeys.KEY_SONG_TYPE,
                        SongType.NOTIFICATION.value
                    )
                }
                notificationIntent.putExtra(CommonKeys.KEY_DATA, bundle)
                notificationIntent.putExtra(CommonKeys.IS_FOR_SONG_PLAYLOAD, false)
                notificationIntent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                return notificationIntent
            }
            else -> {
                val notificationIntent = Intent(pContext, MainActivity::class.java)
                notificationIntent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                return notificationIntent
            }
        }
    }

    private fun <T> convertJsonStringToObject(
        jsonString: String,
        clazz: Class<T>
    ): T =
        Gson().fromJson(jsonString, clazz)

    companion object {
        private const val TAG = "SplashViewModel"
    }

}