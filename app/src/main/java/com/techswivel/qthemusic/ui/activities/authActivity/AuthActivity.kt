package com.techswivel.qthemusic.ui.activities.authActivity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.databinding.ActivityAuthBinding
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.AuthNetworkViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.ui.activities.mainActivity.MainActivity
import com.techswivel.qthemusic.ui.base.BaseActivity
import com.techswivel.qthemusic.ui.fragments.forgotPasswordFragment.ForgotPassword
import com.techswivel.qthemusic.ui.fragments.forgotPasswordFragment.ForgotPasswordImp
import com.techswivel.qthemusic.ui.fragments.otpVerificationFragment.OtpVerification
import com.techswivel.qthemusic.ui.fragments.otpVerificationFragment.OtpVerificationImp
import com.techswivel.qthemusic.ui.fragments.setPasswordFragmetnt.SetPassword
import com.techswivel.qthemusic.ui.fragments.setPasswordFragmetnt.SetPasswordImp
import com.techswivel.qthemusic.ui.fragments.signInFragment.SignInFragment
import com.techswivel.qthemusic.ui.fragments.signInFragment.SignInFragmentImp
import com.techswivel.qthemusic.ui.fragments.signUpFragment.SignUpFragment
import com.techswivel.qthemusic.ui.fragments.signUpFragment.SignUpFragmentImp
import com.techswivel.qthemusic.ui.fragments.yourInterestFragment.YourInterestFragment
import com.techswivel.qthemusic.utils.*
import okhttp3.MultipartBody
import java.util.*

class AuthActivity : BaseActivity(), AuthActivityImp {
    private lateinit var mAuthBinding: ActivityAuthBinding
    private lateinit var mGoogleSinInClient: GoogleSignInClient
    private lateinit var mAuthNetworkViewModel: AuthNetworkViewModel
    private lateinit var mAuthActivityViewModel: AuthActivityViewModel
    private lateinit var mProfileNetworkViewModel: ProfileNetworkViewModel
    private lateinit var mCurrentFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuthBinding = ActivityAuthBinding.inflate(layoutInflater)
        initViewModel()

        mAuthActivityViewModel.isLogin = PrefUtils.getBoolean(this, CommonKeys.KEY_IS_LOGGED_IN)
        mAuthActivityViewModel.isInterestSelected =
            PrefUtils.getBoolean(this, CommonKeys.KEY_IS_INTEREST_SET)
        if (mAuthActivityViewModel.isLogin && !mAuthActivityViewModel.isInterestSelected) {
            val bundle = Bundle()
            bundle.putString(CommonKeys.KEY_DATA, FragmentType.AUTH_ACTIVITY.toString())
            bundle.putSerializable(CommonKeys.CATEGORIES_TYPE, CategoryType.INTERESTS)
            val yourIntersetFragment = YourInterestFragment()
            yourIntersetFragment.arguments = bundle
            replaceFragmentWithoutAddingToBackStack(R.id.auth_container, yourIntersetFragment)
        } else {
            replaceFragmentWithoutAddingToBackStack(R.id.auth_container, SignInFragment())
            mCurrentFragment = SignInFragment()
        }

        setContentView(mAuthBinding.root)
    }

    override fun onInternetAvailable() {}

    override fun onConnectionDisable() {}

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {

        return super.onCreateView(name, context, attrs)

    }

    override fun onResume() {
        super.onResume()

    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mAuthActivityViewModel.callbackManager.onActivityResult(
            requestCode,
            resultCode,
            data
        )
        if (requestCode == Constants.GOOGLE_SIGN_IN_REQUEST_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGoogleSignInResult(task)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (::mCurrentFragment.isInitialized) {
            mCurrentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun userLoginRequest(
        authRequestBuilder: AuthRequestModel
    ) {
        mAuthNetworkViewModel.userLogin(authRequestBuilder)
    }

    override fun navigateToHomeScreenAfterLogin(authModel: AuthModel?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    override fun forgotPasswordRequest(
        authRequestBuilder: AuthRequestBuilder,
        sharedViews: View?,
        transitionName: String?,
        isResetRequest: Boolean
    ) {
        mAuthActivityViewModel.isResetRequest = isResetRequest
        mAuthActivityViewModel.sharedView = sharedViews
        mAuthActivityViewModel.myTransitionName = transitionName
        mAuthActivityViewModel.authRequestBilder = authRequestBuilder
        mAuthActivityViewModel.myEmail = authRequestBuilder.email.toString()
        mAuthActivityViewModel.otpType = authRequestBuilder.otpType?.name
        val authModel =
            AuthRequestBuilder.builder(mAuthActivityViewModel.authRequestBilder)
        mAuthNetworkViewModel.sendOtpRequest(authModel)
    }

    override fun showProgressBar() {
        mAuthBinding.authProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mAuthBinding.authProgressBar.visibility = View.INVISIBLE
    }

    override fun replaceCurrentFragment(fragment: Fragment) {
        mCurrentFragment = fragment

        replaceFragment(R.id.auth_container, fragment)
    }

    override fun replaceCurrentFragmentWithoutAddingToBackStack(fragment: Fragment) {
        mCurrentFragment = fragment
        replaceFragmentWithoutAddingToBackStack(R.id.auth_container, fragment)
    }

    override fun verifyOtpRequest(
        authRequestBuilder: AuthRequestModel,

        ) {
        mAuthNetworkViewModel.verifyOtpResponse(authRequestBuilder)
        mAuthActivityViewModel.userEmail = authRequestBuilder.email
        mAuthActivityViewModel.authRequestBilder.otp = authRequestBuilder.otp
    }

    override fun setPasswordRequest(
        authRequestBuilder: AuthRequestBuilder
    ) {
        mAuthActivityViewModel.authRequestBilder.password = authRequestBuilder.password
        val authModel =
            AuthRequestBuilder.builder(authRequestBuilder)
        mAuthNetworkViewModel.requestToSetPassword(authModel)
    }

    override fun saveInterests() {
        PrefUtils.setBoolean(
            QTheMusicApplication.getContext(),
            CommonKeys.KEY_IS_INTEREST_SET,
            true
        ) // quick fix for flow
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    override fun clearData() {
        mAuthActivityViewModel.authRequestBilder = AuthRequestBuilder()
    }

    override fun castCurrentFragment(fragment: Fragment) {
        mCurrentFragment = fragment
    }

    override fun popUpToAllFragments(fragment: Fragment) {
        popUpAllFragmentIncludeThis(fragment::class.java.name)
    }

    override fun signInWithGoogle() {
        signInGoogle()
    }

    override fun signInWithFacebook() {
        signInFacebook()
    }

    override fun userSignUp(
        profile: MultipartBody.Part?,
        name: String?,
        email: String?,
        dob: Int?,
        gender: String?,
        password: String?,
        fcmToken: String?,
        deviceIdentifier: String?,
        completeAddress: String?,
        city: String?,
        state: String?,
        country: String?,
        zipCode: Int?,
        socailId: String?,
        socialSite: SocialSites?
    ) {

        if (socialSite == null) {
            mAuthActivityViewModel.socailId = null
            mAuthActivityViewModel.socailSite = null
        } else {
            mAuthActivityViewModel.socailId =
                Utilities.toRequestedBody(socailId)
            mAuthActivityViewModel.socailSite =
                Utilities.toRequestedBody(socialSite?.name)
        }
        val dataToSendBuilder = AuthRequestBuilder()
        dataToSendBuilder.mProfile = profile
        dataToSendBuilder.mName = Utilities.toRequestedBody(name)
        dataToSendBuilder.mEmail = Utilities.toRequestedBody(email)
        dataToSendBuilder.mDob = dob
        dataToSendBuilder.mPassword = Utilities.toRequestedBody(password)
        dataToSendBuilder.mFcmToken = Utilities.toRequestedBody(fcmToken)
        dataToSendBuilder.mDeviceIdentifier = Utilities.toRequestedBody(deviceIdentifier)
        dataToSendBuilder.mSocailId = mAuthActivityViewModel.socailId
        dataToSendBuilder.mSocialSite = mAuthActivityViewModel.socailSite
        if (!gender.isNullOrEmpty()) {
            dataToSendBuilder.mGender = Utilities.toRequestedBody(gender)
        }
        if (!completeAddress.isNullOrEmpty()) {
            dataToSendBuilder.mCompleteAddress = Utilities.toRequestedBody(completeAddress)
            dataToSendBuilder.mCity = Utilities.toRequestedBody(city)
            dataToSendBuilder.mState = Utilities.toRequestedBody(state)
            dataToSendBuilder.mCountry = Utilities.toRequestedBody(country)
            dataToSendBuilder.zipCode = zipCode
        }

        mAuthNetworkViewModel.userSingUp(
            dataToSendBuilder
        )
    }

    override fun getCategories(categoryType: CategoryType) {

    }

    override fun replaceCurrentFragmentWithAnimation(
        fragment: Fragment,
        view: View,
        string: String
    ) {
        mCurrentFragment = fragment
        replaceFragmentWithAnimation(R.id.auth_container, fragment, view, string)
    }

    private fun initViewModel() {
        mAuthNetworkViewModel = ViewModelProvider(this)[AuthNetworkViewModel::class.java]
        mAuthActivityViewModel = ViewModelProvider(this)[AuthActivityViewModel::class.java]
        mProfileNetworkViewModel = ViewModelProvider(this)[ProfileNetworkViewModel::class.java]
        mAuthActivityViewModel.callbackManager = CallbackManager.Factory.create()
        mAuthActivityViewModel.loginManager = LoginManager.getInstance()
        setAutNetworkViewModelObservers()
    }

    private fun handleGoogleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)
            val authCode = account.serverAuthCode

            if (authCode != null) {
                mAuthNetworkViewModel.getGoogleToken(authCode)
            }
        } catch (e: ApiException) {
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun signInGoogle() {
        mGoogleSinInClient = GoogleSignIn.getClient(this, QTheMusicApplication.getGso())
        val signInIntent = mGoogleSinInClient.signInIntent
        startActivityForResult(signInIntent, Constants.GOOGLE_SIGN_IN_REQUEST_CODE)
    }

    private fun signInFacebook() {
        mAuthActivityViewModel.loginManager.logInWithReadPermissions(
            this,
            Arrays.asList(
                getString(R.string.get_email_fb),
                getString(R.string.fb_public_profile_args)
            )
        )
        mAuthActivityViewModel.loginManager.registerCallback(mAuthActivityViewModel.callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(result: LoginResult?) {
                    val accessToken = result?.accessToken?.token
                    FirebaseMessaging.getInstance().token.addOnSuccessListener {
                        mAuthActivityViewModel.authRequestBilder.loginType = LoginType.SOCIAL
                        mAuthActivityViewModel.authRequestBilder.socialSite = SocialSites.FACEBOOK
                        mAuthActivityViewModel.authRequestBilder.fcmToken = it
                        mAuthActivityViewModel.authRequestBilder.accessToken = accessToken
                        mAuthActivityViewModel.authRequestBilder.deviceIdentifier =
                            QTheMusicApplication.getContext().toDeviceIdentifier()
                        mAuthActivityViewModel.authRequestBilder.deviceName =
                            QTheMusicApplication.getContext().toDeviceName()
                        val authModel =
                            AuthRequestBuilder.builder(mAuthActivityViewModel.authRequestBilder)
                        userLoginRequest(authModel)
                    }.addOnFailureListener { exception ->
                        Utilities.showToast(this@AuthActivity, "${exception.message}")
                    }
                }

                override fun onCancel() {

                }

                override fun onError(exception: FacebookException) {
                    Utilities.showToast(this@AuthActivity, "${exception.message}")
                }
            })
    }

    private fun setAutNetworkViewModelObservers() {
        mAuthNetworkViewModel.signinUserResponse.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    val mResponseModel = it.t as ResponseModel
                    if (mResponseModel.status == true) {
                        val userData = mResponseModel.data.authModel
                        val isInterestSet = userData?.isInterestsSet
                        mAuthActivityViewModel.setDataInSharedPrefrence(mResponseModel.data.authModel)
                        if (isInterestSet == false) {
                            val bundle = Bundle()
                            bundle.putSerializable(
                                CommonKeys.CATEGORIES_TYPE,
                                CategoryType.INTERESTS
                            )
                            bundle.putBoolean(CommonKeys.KEY_DATA, true)
                            val yourInterestFragment = YourInterestFragment()
                            yourInterestFragment.arguments = bundle
                            replaceCurrentFragment(yourInterestFragment)

                        } else {
                            val intent = Intent(this@AuthActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                            finish()
                        }

                    } else {
                        val mResponseModel = it.t as ResponseModel
                        val userData = mResponseModel.data.authModel
                        mAuthActivityViewModel.authRequestBilder.name = userData?.name
                        mAuthActivityViewModel.authRequestBilder.email = userData?.email
                        mAuthActivityViewModel.authRequestBilder.socialId = userData?.socialId
                        mAuthActivityViewModel.authRequestBilder.avatar = userData?.avatar
                        mAuthActivityViewModel.authRequestBilder.otpType = OtpType.EMAIL
                        if (mCurrentFragment is ForgotPassword) {
                            (mCurrentFragment as ForgotPasswordImp).accountNotExistsSendOtp(userData?.email)
                        } else {
                            val bundle = Bundle()
                            bundle.putSerializable(
                                CommonKeys.AUTH_BUILDER_MODEL,
                                mAuthActivityViewModel.authRequestBilder
                            )
                            val forgotPassword = ForgotPassword()
                            forgotPassword.arguments = bundle
                            replaceCurrentFragment(forgotPassword)
                        }
                    }
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as SignInFragmentImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SignInFragmentImp).hideProgressBar()
                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    val error = it.error as ErrorResponse
                    hideProgressBar()
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as SignInFragmentImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SignInFragmentImp).hideProgressBar()
                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                }
            }
        })
        mAuthNetworkViewModel.googleSignResponse.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    val data = it.t as GoogleResponseModel
                    if (data.accessToken != null) {

                        FirebaseMessaging.getInstance().token.addOnSuccessListener {
                            mAuthActivityViewModel.authRequestBilder.accessToken = data.accessToken
                            mAuthActivityViewModel.authRequestBilder.fcmToken = it
                            mAuthActivityViewModel.authRequestBilder.loginType = LoginType.SOCIAL
                            mAuthActivityViewModel.authRequestBilder.socialSite = SocialSites.GMAIL
                            mAuthActivityViewModel.authRequestBilder.deviceIdentifier =
                                QTheMusicApplication.getContext().toDeviceIdentifier()
                            mAuthActivityViewModel.authRequestBilder.deviceName =
                                QTheMusicApplication.getContext().toDeviceName()
                            val authModel =
                                AuthRequestBuilder.builder(mAuthActivityViewModel.authRequestBilder)
                            userLoginRequest(authModel)
                        }.addOnFailureListener {
                            Log.d(TAG, "Failure")
                        }
                    } else {
                        DialogUtils.runTimeAlert(this,
                            getString(R.string.error_tittle),
                            getString(R.string.error_tittle),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                }

                                override fun onNegativeCallBack() {
                                }
                            }
                        )
                    }
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        })

        mAuthNetworkViewModel.otpObserver.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    if (mCurrentFragment is ForgotPassword) {
                        (mCurrentFragment as ForgotPasswordImp).hideProgressBar()
                    } else {
                        (mCurrentFragment as OtpVerification).hideProgressBar()
                    }
                    hideProgressBar()
                    val bundle = Bundle()
                    bundle.putSerializable(
                        CommonKeys.AUTH_BUILDER_MODEL,
                        mAuthActivityViewModel.authRequestBilder
                    )
                    val otpVerification = OtpVerification()

                    otpVerification.arguments = bundle
                    if (!mAuthActivityViewModel.isResetRequest) {

                        if (mAuthActivityViewModel.otpType == OtpType.EMAIL.name) {
                            mAuthActivityViewModel.sharedView?.let { sharedView ->
                                mAuthActivityViewModel.myTransitionName?.let { transition ->
                                    replaceFragmentWithAnimation(
                                        R.id.auth_container,
                                        otpVerification,
                                        sharedView,
                                        transition
                                    )
                                }
                            }
                        } else {
                            replaceCurrentFragment(otpVerification)
                        }
                    } else {
                        Log.d(
                            TAG,
                            "is resend else  otp type ${mAuthActivityViewModel.authRequestBilder.otpType} otp  ${mAuthActivityViewModel.authRequestBilder.otp}"
                        )
                    }
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as ForgotPasswordImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SignInFragmentImp).hideProgressBar()
                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as ForgotPasswordImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as ForgotPasswordImp).hideProgressBar()
                            }
                        }
                    )
                }
            }
        })

        mAuthNetworkViewModel.otpVerificationResponse.observe(
            this,
            Observer {
                when (it.status) {
                    NetworkStatus.LOADING -> {
                        showProgressBar()
                    }
                    NetworkStatus.SUCCESS -> {
                        (mCurrentFragment as OtpVerificationImp).hideProgressBar()
                        hideProgressBar()
                        val bundle = Bundle()
                        bundle.putSerializable(
                            CommonKeys.AUTH_BUILDER_MODEL,
                            mAuthActivityViewModel.authRequestBilder
                        )
                        val setPassword = SetPassword()
                        setPassword.arguments = bundle
                        PrefUtils.setBoolean(this, CommonKeys.START_TIMER, false)
                        replaceCurrentFragment(setPassword)

                    }
                    NetworkStatus.EXPIRE -> {
                        hideProgressBar()
                        val error = it.error as ErrorResponse
                        DialogUtils.runTimeAlert(this,
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    (mCurrentFragment as OtpVerificationImp).hideProgressBar()
                                }

                                override fun onNegativeCallBack() {
                                    (mCurrentFragment as OtpVerificationImp).hideProgressBar()
                                }
                            }
                        )
                    }
                    NetworkStatus.ERROR -> {
                        hideProgressBar()
                        val error = it.error as ErrorResponse
                        DialogUtils.runTimeAlert(this,
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    (mCurrentFragment as OtpVerificationImp).hideProgressBar()
                                }

                                override fun onNegativeCallBack() {
                                    (mCurrentFragment as OtpVerificationImp).hideProgressBar()
                                }
                            }
                        )
                    }
                }
            })

        mAuthNetworkViewModel.setPasswordResponse.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    (mCurrentFragment as SetPasswordImp).hideProgressBar()
                    PrefUtils.removeValue(this, CommonKeys.SIGNIN_BTN_ANIMATION)
                    if (mAuthActivityViewModel.authRequestBilder.otpType?.name == OtpType.FORGET_PASSWORD.name) {
                        replaceCurrentFragment(SignInFragment())
                        popUpAllFragmentIncludeThis(null)

                    } else {
                        val signUpFragment = SignUpFragment()
                        val bundle = Bundle()
                        mAuthActivityViewModel.authRequestBilder.name =
                            mAuthActivityViewModel.userName
                        bundle.putSerializable(
                            CommonKeys.AUTH_BUILDER_MODEL,
                            mAuthActivityViewModel.authRequestBilder
                        )
                        signUpFragment.arguments = bundle
                        replaceCurrentFragment(signUpFragment)
                    }
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                replaceCurrentFragment(SignInFragment())
                                popUpAllFragmentIncludeThis(null)
                            }

                            override fun onNegativeCallBack() {
                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as SetPasswordImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SetPasswordImp).hideProgressBar()
                            }
                        }
                    )
                }
            }
        })
        mAuthNetworkViewModel.userSignupResponse.observe(this, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    val responseModel = it.t as ResponseModel
                    val userData = responseModel.data
                    mAuthActivityViewModel.setDataInSharedPrefrence(userData.authModel)
                    val bundle = Bundle()
                    bundle.putSerializable(CommonKeys.CATEGORIES_TYPE, CategoryType.INTERESTS)
                    val yourInterestFragment = YourInterestFragment()
                    yourInterestFragment.arguments = bundle
                    replaceCurrentFragment(yourInterestFragment)
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as SignUpFragmentImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SignUpFragmentImp).hideProgressBar()
                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(this,
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                (mCurrentFragment as SignUpFragmentImp).hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                (mCurrentFragment as SignUpFragmentImp).hideProgressBar()
                            }
                        }
                    )
                }
            }
        })
    }
    companion object {
        private val TAG = "AuthActivity"
    }
}