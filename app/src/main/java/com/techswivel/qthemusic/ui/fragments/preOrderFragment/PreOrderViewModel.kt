package com.techswivel.qthemusic.ui.fragments.preOrderFragment

import com.techswivel.qthemusic.models.PreOrderData
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.ui.base.BaseViewModel
import kotlinx.coroutines.runBlocking

class PreOrderViewModel : BaseViewModel() {
    var preOrderList: MutableList<Any> = ArrayList()
    var selectedItem: PreOrderData? = null
    var artistId: Int? = null
    var requestedData: SubscribeToPlanBodyBuilder? = null

    fun saveFailedPurchasetoDataBase() {
        runBlocking {
            mLocalDataManager.savedFailedPurchase(
                subscriptionPlans = requestedData?.subscriptionPlan ?: FailedPurchased()
            )
        }
    }
}