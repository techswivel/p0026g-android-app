package com.techswivel.qthemusic.ui.fragments.signUpFragment

import android.net.Uri
import com.techswivel.qthemusic.models.AuthRequestBuilder
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.ui.dialogFragments.chooserDialogFragment.ChooserDialogFragment
import java.io.File
import java.util.*

class SignUpViewModel : BaseViewModel() {
    var imageFile: File? = null
    var uri: Uri? = null
    var myToken = ""
    var email = "waseem5@gmial.com"
    var name = "Waseem Ashgar"
    var socailId: String? = null
    var socailSite: String? = null
    var year: Int = 0
    var month: Int = 0
    var day: Int = 0
    lateinit var calendar: Calendar
    var zipCode: Int? = null
    var gender: String? = null
    lateinit var mBuilder: AuthRequestBuilder
    lateinit var mChooserFragment: ChooserDialogFragment
    val mcurrentTime = Calendar.getInstance()
    var dateInMillis: Long? = null
    var vmDob: Int = 0

}