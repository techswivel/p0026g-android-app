package com.techswivel.qthemusic.ui.fragments.albumListScreen

import androidx.lifecycle.ViewModel
import com.techswivel.qthemusic.models.ArtistDetails

class AlbumListScreenViewModel : ViewModel() {
    var mAlbumList: MutableList<Any> = ArrayList()
    lateinit var mArtistDetailsList: ArtistDetails
}