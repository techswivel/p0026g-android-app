package com.techswivel.qthemusic.ui.fragments.categoriesDetailsFragment

import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.models.RecommendedSongsBodyModel
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class CategoriesDetailsViewModel : LocalDatabaseViewModel() {
    var mCategory: Category? = null
    var selectedTab: RecommendedSongsType? = null
    var songsList: MutableList<Any> = ArrayList()
    var albumList: MutableList<Any> = ArrayList()
    var artistList: MutableList<Any> = ArrayList()
    lateinit var recommendedSongsBodyModel: RecommendedSongsBodyModel
}