package com.techswivel.qthemusic.ui.fragments.aboutArtist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.databinding.FragmentAboutArtistBinding
import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.ui.base.BaseFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.SharedViewModelForAllMusic
import com.techswivel.qthemusic.utils.visibilityInVisible
import com.techswivel.qthemusic.utils.visibilityVisible

class AboutArtist : BaseFragment() {
    private lateinit var mBinding: FragmentAboutArtistBinding
    private lateinit var mSharedViewModel: SharedViewModelForAllMusic
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentAboutArtistBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeSharedViewModelData()
        setParamsForLottie()
    }

    private fun observeSharedViewModelData() {
        mSharedViewModel.mArtistData.observe(viewLifecycleOwner, Observer { artistData ->
            if (artistData != null) {
                bindViewModel(artistData)
                mBinding.emptyTag.visibilityInVisible()
            }
            if (artistData.aboutArtist.isNullOrEmpty()) {
                mBinding.emptyTag.visibilityVisible()
            }
        })
    }

    private fun initViewModel() {
        mSharedViewModel =
            ViewModelProvider(requireActivity()).get(SharedViewModelForAllMusic::class.java)
    }

    private fun setParamsForLottie() {
        val prams = mBinding.noDataFoundInc.noDataLottie.layoutParams
        prams.height = resources.getDimensionPixelSize(R.dimen._200dp)
        prams.width = resources.getDimensionPixelSize(R.dimen._200dp)
        mBinding.noDataFoundInc.noDataLottie.layoutParams = prams
        mBinding.noDataFoundInc.noDataFoundBackButton.visibilityInVisible()
    }

    private fun bindViewModel(artistDetails: ArtistDetails) {
        mBinding.artistAbout = artistDetails
        mBinding.executePendingBindings()
    }

    companion object {
        private val TAG = "AboutArtist"
        fun newInstance() = AboutArtist()
    }
}