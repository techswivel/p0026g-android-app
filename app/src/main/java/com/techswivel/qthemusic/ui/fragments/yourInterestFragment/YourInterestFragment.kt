package com.techswivel.qthemusic.ui.fragments.yourInterestFragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.FragmentType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.databinding.FragmentYourInterestBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.Interests
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.ProfileNetworkViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity.SubscriptionPlansActivity
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_DATA
import com.techswivel.qthemusic.utils.DialogUtils
import com.techswivel.qthemusic.utils.Log
import com.techswivel.qthemusic.utils.Utilities

class YourInterestFragment : RecyclerViewBaseFragment(),
    RecyclerViewAdapter.CallBack, YourInterestImp {
    private lateinit var mBinding: FragmentYourInterestBinding
    private lateinit var mViewModel: YourInterestViewModel
    private lateinit var mProfileNetworkViewModel: ProfileNetworkViewModel
    private lateinit var mCategoriesAdapter: RecyclerViewAdapter
    private lateinit var mSongAndArtistViewModel: SongAndArtistsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        getBundleData()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentYourInterestBinding.inflate(layoutInflater, container, false)
        mViewModel.categoriesListForApiRequest = ArrayList()
        onClickListeners()
        onBackPress()
        mViewModel.categoryType =
            arguments?.getSerializable(CommonKeys.CATEGORIES_TYPE) as CategoryType
        callApi()
        if (arguments?.getBoolean(CommonKeys.KEY_DATA) != null) {
            mViewModel.isFromAuthActivity = arguments?.getBoolean(CommonKeys.KEY_DATA)
        }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        setObserver()
        swipeToRefresh()
    }

    override fun showProgressBar() {
        mBinding.btnInterestLetsGo.isEnabled = false
        mBinding.progressBar.visibility = View.VISIBLE

    }

    override fun hideProgressBar() {
        mBinding.btnInterestLetsGo.isEnabled = true
        mBinding.progressBar.visibility = View.INVISIBLE
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.your_interest_resview_layout
    }

    override fun onNoDataFound() {

    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mCategoriesAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return mCategoriesAdapter
    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewClicked(view: View, data: Any?) {
        super.onViewClicked(view, data)
        val mCategory = data as Category
        if (mViewModel.categoriesListForApiRequest.size < 5) {
            if (checkForCategoryIfAlreadyExistes(mCategory.categoryTitle)) {
                mViewModel.selectedCategoryList.remove(mCategory.categoryTitle)
                view.setBackgroundResource(R.drawable.shape_bg_your_interest_recview)
                getSelectedCategories(mViewModel.selectedCategoryList)

            } else {
                mViewModel.selectedCategoryList.add(mCategory.categoryTitle)
                view.setBackgroundResource(R.drawable.shape_bg_your_interest_selected)
                getSelectedCategories(mViewModel.selectedCategoryList)
            }
        } else {

            if (checkForCategoryIfAlreadyExistes(mCategory.categoryTitle)) {
                mViewModel.selectedCategoryList.remove(mCategory.categoryTitle)
                view.setBackgroundResource(R.drawable.shape_bg_your_interest_recview)
                getSelectedCategories(mViewModel.selectedCategoryList)

            } else {
                Utilities.showToast(requireContext(), getString(R.string.maximum_alert))
            }
        }
    }

    private fun onBackPress() {
        val callBack = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callBack)
    }

    private fun onClickListeners() {
        mBinding.btnInterestLetsGo.setOnClickListener {
            when {
                mViewModel.categoriesListForApiRequest.size < 2 -> {
                    Utilities.showToast(requireContext(), getString(R.string.minimum_alert))
                }
                mViewModel.categoriesListForApiRequest.size > 5 -> {
                    Utilities.showToast(requireContext(), getString(R.string.maximum_alert))
                }
                mViewModel.fragmentType == FragmentType.PROFILE_LANDING.toString() -> {
                    mProfileNetworkViewModel.saveUserInterest(Interests(mViewModel.categoriesListForApiRequest))
                }
                else -> {
                    mProfileNetworkViewModel.saveUserInterest(Interests(mViewModel.categoriesListForApiRequest))

                }
            }
        }
    }

    private fun checkForCategoryIfAlreadyExistes(tittle: String?): Boolean {
        return mViewModel.selectedCategoryList.contains(tittle)
    }

    private fun getSelectedCategories(lis: MutableList<String?>) {
        mViewModel.categoriesListForApiRequest.clear()
        for (i in mViewModel.categoryResponseList) {
            val tittle = i?.categoryTitle
            if (lis.contains(tittle)) {
                mViewModel.categoriesListForApiRequest.add(
                    Category(
                        i?.categoryId,
                        null,
                        null,
                        null,
                        null,
                        false
                    )
                )
                Log.d(TAG, "list is now ${mViewModel.categoriesListForApiRequest}")
            }
        }
    }

    private fun setUpAdapter() {
        mCategoriesAdapter = RecyclerViewAdapter(this, mViewModel.mCategoryResponseList)
        setUpFlexBoxRecViewForYourInterest(
            mBinding.recViewYourInterests
        )
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(YourInterestViewModel::class.java)
        mProfileNetworkViewModel = ViewModelProvider(this).get(ProfileNetworkViewModel::class.java)
        mSongAndArtistViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    private fun getBundleData() {
        mViewModel.fragmentType = arguments?.getString(CommonKeys.KEY_DATA)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObserver() {
        mSongAndArtistViewModel.categoriesResponse.observe(viewLifecycleOwner) { categoryDataResponse ->
            when (categoryDataResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.mInterestShimmer.visibility = View.VISIBLE
                    mBinding.mInterestShimmer.startShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    if (mBinding.swipeInterestContainer.isRefreshing) {
                        mBinding.swipeInterestContainer.isRefreshing = false
                    }
                    mBinding.mInterestShimmer.visibility = View.INVISIBLE
                    mBinding.mInterestShimmer.stopShimmer()
                    hideProgressBar()
                    mViewModel.mCategoryResponseList.clear()
                    val response = categoryDataResponse.t as ResponseModel
                    val categorylist = response.data.category
                    if (categorylist != null) {
                        mViewModel.categoryResponseList = categorylist
                    }
                    for (item in categorylist ?: emptyList()) {
                        if (item.isSelected == true) {
                            mViewModel.selectedCategoryList.add(item.categoryTitle)
                        }
                    }
                    Log.d(TAG, "selected list is ${mViewModel.selectedCategoryList.size}")
                    getSelectedCategories(mViewModel.selectedCategoryList)
                    if (!categorylist.isNullOrEmpty()) {
                        mViewModel.mCategoryResponseList.addAll(categorylist)
                    }
                    if (::mCategoriesAdapter.isInitialized)
                        mCategoriesAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    mBinding.mInterestShimmer.visibility = View.INVISIBLE
                    mBinding.mInterestShimmer.stopShimmer()
                    val error = categoryDataResponse.error as ErrorResponse
                    if (error.message == "Unauthorized") {
                        DialogUtils.errorUnAuthenticatedAlert(
                            parentFragmentManager, error.message,
                            TAG
                        )
                    } else {
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.mInterestShimmer.visibility = View.INVISIBLE
                    mBinding.mInterestShimmer.stopShimmer()
                    val error = categoryDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }

        mProfileNetworkViewModel.saveInterestResponse.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    PrefUtils.setBoolean(
                        QTheMusicApplication.getContext(),
                        CommonKeys.KEY_IS_INTEREST_SET,
                        true
                    )
                    if (mViewModel.fragmentType == FragmentType.PROFILE_LANDING.toString()) {
                        requireActivity().finish()
                    } else {
                        val intent = Intent(requireContext(), SubscriptionPlansActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        intent.putExtra(KEY_DATA, FragmentType.AUTH_ACTIVITY)
                        startActivity(intent)
                    }
                }
                NetworkStatus.EXPIRE -> {
                    hideProgressBar()
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                hideProgressBar()
                            }
                        }
                    )
                }
                NetworkStatus.ERROR -> {
                    val error = it.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                hideProgressBar()
                            }

                            override fun onNegativeCallBack() {
                                hideProgressBar()
                            }
                        }
                    )
                }
            }
        })
    }

    private fun callApi() {
        mSongAndArtistViewModel.getCategoriesDataFromServer(mViewModel.categoryType)
    }

    private fun swipeToRefresh() {
        mBinding.swipeInterestContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        mViewModel.mCategoryResponseList.clear()
        mCategoriesAdapter.notifyDataSetChanged()
        callApi()
    }

    companion object {
        private val TAG = "YourInterestFragment"
        fun newInstance() = YourInterestFragment()
        fun newInstance(mBundle: Bundle?) = YourInterestFragment().apply {
            arguments = mBundle
        }
    }
}