package com.techswivel.qthemusic.ui.fragments.downloadSongFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.techswivel.dfaktfahrerapp.ui.fragments.underDevelopmentMessageFragment.UnderDevelopmentMessageFragment
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentDownloadBinding
import com.techswivel.qthemusic.models.DownloadingResponse
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.SongsBodyBuilder
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.dialogFragments.songType.SongTypeFragment
import com.techswivel.qthemusic.utils.*
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.*


class DownloadSongFragment : RecyclerViewBaseFragment(), BaseInterface,
    RecyclerViewAdapter.CallBack {

    companion object {
        private const val TAG = "DownloadSongFragment"
        fun newInstance() = DownloadSongFragment()
        fun newInstance(mBundle: Bundle?) = DownloadSongFragment().apply {
            arguments = mBundle
        }
    }

    private lateinit var mBinding: FragmentDownloadBinding
    private lateinit var viewModel: DownloadSongFragmentViewModel
    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var songAndArtistsViewModel: SongAndArtistsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentDownloadBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        getDownloadedSongsFromServer()
        getBundleData()
        getSongsFromLocalDB()
        setUpAdapter()
        setUpToolBar()
        clickListeners()
        swipeToRefresh()
        setObserver()
    }

    override fun onStart() {
        super.onStart()
        if (EventBus.getDefault().isRegistered(this).not()) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return adapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return adapter
    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
        val song = data as Song
        if (song.songStatus != SongStatus.PREMIUM) {
            if (!song.localPath.isNullOrEmpty()) {
                viewModel.insertDataInDataBaseForSync(song.songId)
                viewModel.insertSongInDataBase(song)
                val songBuilder = NextPlaySongBuilder().apply {
                    this.currentSongModel = song
                    this.songsList = viewModel.mDownloadedSongsList as MutableList<Song>
                    this.playedFrom = SongType.DOWNLOADED
                }
                EventBus.getDefault().post(songBuilder)
            } else {
                if (song.songVideoUrl != null) {
                    val fragment = SongTypeFragment.newInstance(
                        song,
                        object : SongTypeFragment.Callback {
                            override fun onDownloadingStart(
                                actionType: ActionType,
                                song: Song,
                                songType: SongType
                            ) {
                                songAndArtistsViewModel.startDownloadingSong(song, songType)
                            }
                        })
                    viewModel.mSongData.add(song)
                    fragment.show(parentFragmentManager, SongTypeFragment::class.java.toString())
                } else {
                    songAndArtistsViewModel.startDownloadingSong(song, SongType.AUDIO)
                }
            }
        } else if (song.songStatus == SongStatus.PREMIUM) {
            val bundle = Bundle().apply {
                putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                putParcelableArrayList(
                    CommonKeys.KEY_SONGS_LIST,
                    viewModel.mDownloadedSongsList as ArrayList<out Song>
                )
                putString(
                    CommonKeys.KEY_SONG_TYPE,
                    SongType.DOWNLOADED.value
                )
            }
            ActivityUtils.startPlayerActivity(activity, bundle)
        }
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_downloading_song
    }

    override fun onNoDataFound() {
    }

    override fun showProgressBar() {
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.progressBar.visibility = View.GONE
    }

    @SuppressLint("NotifyDataSetChanged")
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun observeDownloadingFiles(status: DownloadingResponse) {
        val data = status.translationID
        when (status.status) {
            DownloadingStatus.SUCCESS -> {
                for (song in viewModel.mSongData.indices) {
                    if (viewModel.mSongData[song].songId == data) {
                        viewModel.mSongData[song].setProgressVisibilityState(ObservableField(false))
                        adapter.notifyDataSetChanged()
                    }
                }
                getSongsFromLocalDB()
            }
            DownloadingStatus.RUNNING -> {
                for (song in viewModel.mSongData.indices) {
                    if (viewModel.mSongData[song].songId == data) {
                        viewModel.mSongData[song].setProgressVisibilityState(ObservableField(true))
                        viewModel.mSongData[song].setProgressVisibilityPercentage(
                            ObservableField(
                                status.progress?.toInt()
                            )
                        )
                        viewModel.mSongData[song].setProgressVisibilityPercentageString(
                            ObservableField(status.progress?.toString())
                        )
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            DownloadingStatus.PAUSED -> {
                for (song in viewModel.mSongData.indices) {
                    if (viewModel.mSongData[song].songId == data) {
                        viewModel.mSongData[song].setProgressVisibilityState(ObservableField(false))
                        viewModel.mSongData[song].setProgressVisibilityPercentageString(
                            ObservableField(getString(R.string.pasued_msg))
                        )
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            DownloadingStatus.PENDING -> {
                for (song in viewModel.mSongData.indices) {
                    if (viewModel.mSongData[song].songId == data) {
                        viewModel.mSongData[song].setProgressVisibilityState(ObservableField(false))
                        viewModel.mSongData[song].setProgressVisibilityPercentageString(
                            ObservableField(getString(R.string.pending_msg))
                        )
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            DownloadingStatus.FAILED -> {
                for (song in viewModel.mSongData.indices) {
                    if (viewModel.mSongData[song].songId == data) {
                        viewModel.mSongData[song].setProgressVisibilityState(ObservableField(false))
                        viewModel.mSongData[song].setProgressVisibilityPercentageString(
                            ObservableField(getString(R.string.failed_msg))
                        )
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObserver() {
        songAndArtistsViewModel.songlistResponse.observe(viewLifecycleOwner) { downloadSongResponse ->
            when (downloadSongResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.tvNoDataFound.visibilityInVisible()
                    mBinding.shimmerLayout.visibilityVisible()
                    mBinding.shimmerLayout.startShimmer()
                }
                NetworkStatus.SUCCESS -> {
                    if (mBinding.swipeDownloadedSongsContainer.isRefreshing) {
                        mBinding.swipeDownloadedSongsContainer.isRefreshing = false
                    }
                    mBinding.shimmerLayout.visibilityGone()
                    mBinding.shimmerLayout.stopShimmer()
                    viewModel.mRemoteDownloadedSongsList.clear()
                    val response = downloadSongResponse.t as ResponseModel
                    val playlist = response.data.songList
                    for (item in playlist ?: emptyList()) {
                        viewModel.mRemoteDownloadedSongsList.add(item)
                    }
                    if (viewModel.mRemoteDownloadedSongsList.isNotEmpty() && viewModel.processSongsData()) {
                        Log.i(TAG, "setObserver: data process successfully")
                    } else {
                        mBinding.tvNoDataFound.visibilityVisible()
                    }

                    if (::adapter.isInitialized)
                        adapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    mBinding.shimmerLayout.visibility = View.GONE
                    mBinding.shimmerLayout.stopShimmer()
                    val error = downloadSongResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.shimmerLayout.visibility = View.GONE
                    mBinding.shimmerLayout.stopShimmer()
                    val error = downloadSongResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                viewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    mBinding.shimmerLayout.visibility = View.GONE
                    mBinding.shimmerLayout.stopShimmer()
                }
            }
        }
    }


    private fun setUpToolBar() {
        setUpActionBar(
            mBinding.activityToolbar.toolbar,
            "",
            true
        )
        mBinding.activityToolbar.toolbarTitle.text = getString(R.string.download_songs)
    }

    private fun initViewModel() {

        viewModel = ViewModelProvider(this).get(DownloadSongFragmentViewModel::class.java)
        songAndArtistsViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)

    }

    private fun setUpAdapter() {
        adapter = RecyclerViewAdapter(this, viewModel.mDownloadedSongsList)
        setUpRecyclerView(
            mBinding.recyclerviewDownloadedSongs
        )
    }

    private fun clickListeners() {
        mBinding.downloadPremiumButton.setOnClickListener {
            ActivityUtils.launchFragment(
                requireContext(),
                UnderDevelopmentMessageFragment::class.java.name,
            )
        }
        mBinding.noDataFoundInc.noDataFoundBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun getBundleData() {
        viewModel.mAuthModel = viewModel.getPrefrencesData(context ?: requireContext())
        if (viewModel.mAuthModel?.subscription != null) {
            mBinding.downloadPremiumButton.visibility = View.GONE
            mBinding.recyclerviewDownloadedSongs.visibility = View.VISIBLE
        } else {
            mBinding.downloadPremiumButton.visibility = View.VISIBLE
            mBinding.recyclerviewDownloadedSongs.visibility = View.GONE
        }
        mBinding.obj = viewModel.mAuthModel
    }


    private fun getDownloadedSongsFromServer() {
        val songsBuilder = SongsBodyBuilder()
        songsBuilder.type = SongType.DOWNLOADED
        val songsBodyModel = SongsBodyBuilder.build(songsBuilder)
        songAndArtistsViewModel.getSongs(songsBodyModel)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getSongsFromLocalDB() {
        runBlocking {
            val dbDownloadedSongsList = viewModel.mLocalDataManager.getDownloadedSongs()
            if (dbDownloadedSongsList.isNotEmpty()) {
                viewModel.mLocalDownloadedSongsList.clear()
                viewModel.mLocalDownloadedSongsList.addAll(dbDownloadedSongsList)
            }
            viewModel.mDownloadedSongsList.clear()
            viewModel.mLocalDownloadedSongsList.forEach {
                viewModel.mDownloadedSongsList.add(it)
            }
            if (viewModel.mDownloadedSongsList.isNotEmpty()) {
                mBinding.tvNoDataFound.visibility = View.GONE
            }
            if (::adapter.isInitialized)
                adapter.notifyDataSetChanged()
        }
    }

    private fun swipeToRefresh() {
        mBinding.swipeDownloadedSongsContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            prepareForDataRefresh()

        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        getSongsFromLocalDB()
        viewModel.mDownloadedSongsList.clear()
        adapter.notifyDataSetChanged()
        getDownloadedSongsFromServer()
    }

    private fun internetConnectionAvailable(): Boolean {
        var inetAddress: InetAddress? = null
        try {
            val future: Future<InetAddress> =
                Executors.newSingleThreadExecutor().submit(Callable<InetAddress> {
                    try {
                        InetAddress.getByName("google.com")
                    } catch (e: UnknownHostException) {
                        null
                    }
                })
            inetAddress = future.get(6000, TimeUnit.MILLISECONDS);
            future.cancel(true);

        } catch (e: InterruptedException) {
        } catch (e: ExecutionException) {
        } catch (e: TimeoutException) {
        }
        return inetAddress != null && !inetAddress.equals("")
    }

}