package com.techswivel.qthemusic.ui.fragments.categoriesDetailsFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentCategoriesDetialsBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.RecommendedSongsBodyBuilder
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.ArtistDetailFragment
import com.techswivel.qthemusic.utils.*
import org.greenrobot.eventbus.EventBus


class CategoriesDetialsFragment : RecyclerViewBaseFragment(), BaseInterface {
    private lateinit var mBinding: FragmentCategoriesDetialsBinding
    private lateinit var mViewModel: CategoriesDetailsViewModel
    private lateinit var mSongsAdapter: RecyclerViewAdapter
    private lateinit var mArtistAdapter: RecyclerViewAdapter
    private lateinit var mAlbumAdapter: RecyclerViewAdapter
    private lateinit var networkViewModel: SongAndArtistsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        mViewModel.mCategory = arguments?.getParcelable(CommonKeys.KEY_DATA)
        mViewModel.selectedTab = RecommendedSongsType.SONGS
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentCategoriesDetialsBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClickListeners()
        bindViewModel()
        getSongsData()
        getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.SONGS)
        setCurrentRecyclerview(mViewModel.selectedTab)
        swipeToRefresh()
        setParamsForLottie()

    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mAlbumAdapter
    }


    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return when (adapterType) {
            AdapterType.SONGS -> {
                mSongsAdapter = RecyclerViewAdapter(
                    object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.item_trending_songs
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            val song = data as Song
                            if (song.songStatus != SongStatus.PREMIUM) {
                                mViewModel.insertSongInDataBase(song)
                                mViewModel.insertDataInDataBaseForSync(song.songId)
                                val songBuilder = NextPlaySongBuilder().apply {
                                    this.currentSongModel = song
                                    this.songsList = mViewModel.songsList as MutableList<Song>
                                    this.playedFrom = SongType.CATEGORY
                                }
                                EventBus.getDefault().post(songBuilder)
                            } else {
                                val bundle = Bundle().apply {
                                    putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                                    putParcelableArrayList(
                                        CommonKeys.KEY_SONGS_LIST,
                                        mViewModel.songsList as ArrayList<out Song>
                                    )
                                    putString(
                                        CommonKeys.KEY_SONG_TYPE,
                                        SongType.CATEGORY.value
                                    )
                                }
                                ActivityUtils.startPlayerActivity(activity, bundle)
                            }
                        }
                    },
                    mViewModel.songsList
                )
                mSongsAdapter
            }
            AdapterType.ALBUM -> {
                mAlbumAdapter = RecyclerViewAdapter(
                    object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.recview_album_category_detial
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            val mAlbum = data as Album
                            val bundle = Bundle()
                            bundle.putParcelable(CommonKeys.KEY_ALBUM_DETAILS, mAlbum)
                            ActivityUtils.launchFragment(
                                requireContext(),
                                AlbumDetailsFragment::class.java.name,
                                bundle
                            )
                        }
                    },
                    mViewModel.albumList
                )
                mAlbumAdapter
            }
            else -> {
                mArtistAdapter = RecyclerViewAdapter(
                    object : RecyclerViewAdapter.CallBack {
                        override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                            return R.layout.item_artist
                        }

                        override fun onNoDataFound() {

                        }

                        override fun onItemClick(data: Any?, position: Int) {
                            super.onItemClick(data, position)
                            val artistData = data as Artist
                            val bundle = Bundle()
                            bundle.putParcelable(CommonKeys.KEY_ARTIST_MODEL, artistData)
                            ActivityUtils.launchFragment(
                                requireContext(),
                                ArtistDetailFragment::class.java.name,
                                bundle
                            )
                        }
                    },
                    mViewModel.artistList
                )
                mArtistAdapter
            }
        }
    }

    override fun showProgressBar() {
        if (mViewModel.selectedTab == RecommendedSongsType.SONGS) {
            mBinding.slSongsCatDetails.visibility = View.VISIBLE
            mBinding.slAlbumCatDetails.visibility = View.GONE
            mBinding.slArtistCatDetails.visibility = View.GONE
            mBinding.slSongsCatDetails.startShimmer()
        } else if (mViewModel.selectedTab == RecommendedSongsType.ALBUM) {
            mBinding.slAlbumCatDetails.visibility = View.VISIBLE
            mBinding.slSongsCatDetails.visibility = View.GONE
            mBinding.slArtistCatDetails.visibility = View.GONE
            mBinding.slAlbumCatDetails.startShimmer()
        } else {
            mBinding.slArtistCatDetails.visibility = View.VISIBLE
            mBinding.slSongsCatDetails.visibility = View.GONE
            mBinding.slAlbumCatDetails.visibility = View.GONE
            mBinding.slArtistCatDetails.startShimmer()
        }
    }

    override fun hideProgressBar() {
        if (mViewModel.selectedTab == RecommendedSongsType.SONGS) {
            mBinding.slSongsCatDetails.visibility = View.GONE
            mBinding.slSongsCatDetails.stopShimmer()
        } else if (mViewModel.selectedTab == RecommendedSongsType.ALBUM) {
            mBinding.slAlbumCatDetails.visibility = View.GONE
            mBinding.slAlbumCatDetails.stopShimmer()
        } else {
            mBinding.slArtistCatDetails.visibility = View.GONE
            mBinding.slArtistCatDetails.stopShimmer()
        }
    }

    private fun setCurrentRecyclerview(recommendedSongsType: RecommendedSongsType?) {
        try {
            if (recommendedSongsType?.equals(RecommendedSongsType.SONGS) == true) {
                setUpRecyclerView(mBinding.recViewCategoriesDetails, AdapterType.SONGS)
            } else if (recommendedSongsType?.equals(RecommendedSongsType.ALBUM) == true) {
                setUpGridRecyclerView(
                    mBinding.recViewCategoriesDetails,
                    Constants.NUMBER_OF_COLUMN_CATEGORIES_VIEW,
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    AdapterType.ALBUM
                )
            } else {
                setUpGridRecyclerView(
                    mBinding.recViewCategoriesDetails,
                    Constants.ARTIST_COLUMN,
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    resources.getDimensionPixelSize(R.dimen._0dp),
                    AdapterType.ARTIST
                )
            }
        } catch (e: Exception) {

        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onClickListeners() {
        mBinding.btnSongs.setOnClickListener {
            if (mViewModel.selectedTab != RecommendedSongsType.SONGS) {
                mBinding.tvNoDataFoundCategoryDetails.visibility = View.INVISIBLE
                mViewModel.songsList.clear()
                mViewModel.selectedTab = RecommendedSongsType.SONGS
                setCurrentRecyclerview(mViewModel.selectedTab)
                updateSelectedTabBackground(
                    mBinding.btnSongs,
                    mBinding.btnAlbums,
                    mBinding.btnArtists
                )
                getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.SONGS)
                getSongsData()
            }
        }

        mBinding.btnAlbums.setOnClickListener {
            if (mViewModel.selectedTab != RecommendedSongsType.ALBUM) {
                mBinding.tvNoDataFoundCategoryDetails.visibility = View.INVISIBLE
                mViewModel.albumList.clear()
                mViewModel.selectedTab = RecommendedSongsType.ALBUM
                setCurrentRecyclerview(mViewModel.selectedTab)
                updateSelectedTabBackground(
                    mBinding.btnAlbums,
                    mBinding.btnSongs,
                    mBinding.btnArtists
                )
                getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.ALBUM)
                getAlbumData()
            }
        }

        mBinding.btnArtists.setOnClickListener {
            if (mViewModel.selectedTab != RecommendedSongsType.ARTIST) {
                mBinding.tvNoDataFoundCategoryDetails.visibility = View.INVISIBLE
                mViewModel.selectedTab = RecommendedSongsType.ARTIST
                mViewModel.artistList.clear()
                setCurrentRecyclerview(mViewModel.selectedTab)
                updateSelectedTabBackground(
                    mBinding.btnArtists,
                    mBinding.btnAlbums,
                    mBinding.btnSongs
                )
                getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.ARTIST)
                getArtistData()
            }
        }
    }

    private fun getAllDataOfCategory(
        categoryId: Int?,
        recommendedSongsType: RecommendedSongsType?
    ) {
        val recommendedSongsBuilder = RecommendedSongsBodyBuilder()
        recommendedSongsBuilder.requestType = SongType.CATEGORY
        recommendedSongsBuilder.categoryId = categoryId
        recommendedSongsBuilder.dataType = recommendedSongsType
        mViewModel.recommendedSongsBodyModel =
            RecommendedSongsBodyBuilder.build(recommendedSongsBuilder)
        networkViewModel.getRecommendedSongsDataFromServer(mViewModel.recommendedSongsBodyModel)
    }

    private fun updateSelectedTabBackground(
        selectedTab: Button,
        unselectedTab1: Button,
        unselectedTab2: Button
    ) {
        selectedTab.setBackgroundResource(R.drawable.selected_tab_background)
        unselectedTab1.setBackgroundResource(R.drawable.unselected_tab_background)
        unselectedTab2.setBackgroundResource(R.drawable.unselected_tab_background)
    }

    private fun enableAndDisableListeners(
        songsListener: Boolean,
        albumListener: Boolean,
        artistListener: Boolean
    ) {
        mBinding.btnSongs.isEnabled = songsListener
        mBinding.btnAlbums.isEnabled = albumListener
        mBinding.btnArtists.isEnabled = artistListener
        if (mBinding.swipeContainer.isRefreshing) {
            mBinding.swipeContainer.isRefreshing = false
        }

    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(CategoriesDetailsViewModel::class.java)
        networkViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getSongsData() {
        networkViewModel.recommendedSongsResponse.observe(viewLifecycleOwner) { recommendedSongsDataResponse ->
            when (recommendedSongsDataResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                    enableAndDisableListeners(
                        songsListener = false,
                        albumListener = false,
                        artistListener = false
                    )
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    mViewModel.songsList.clear()
                    val response = recommendedSongsDataResponse.t as ResponseModel
                    val songsList = response.data.songList
                    if (!songsList.isNullOrEmpty()) {
                        mViewModel.songsList.addAll(songsList)
                    } else {
                        if (mViewModel.selectedTab == RecommendedSongsType.SONGS) {
                            mBinding.tvNoDataFoundCategoryDetails.visibility = View.VISIBLE
                        }
                    }
                    mSongsAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedSongsDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {

                }
            }
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getAlbumData() {
        networkViewModel.recommendedSongsResponse.observe(viewLifecycleOwner) { recommendedAlbumDataResponse ->
            when (recommendedAlbumDataResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                    enableAndDisableListeners(
                        songsListener = false,
                        albumListener = false,
                        artistListener = false
                    )
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    mViewModel.albumList.clear()
                    val response = recommendedAlbumDataResponse.t as ResponseModel
                    val albumList = response.data.albumList
                    if (!albumList.isNullOrEmpty()) {
                        mViewModel.albumList.addAll(albumList)
                    } else {
                        if (mViewModel.selectedTab == RecommendedSongsType.ALBUM) {
                            mBinding.tvNoDataFoundCategoryDetails.visibility = View.VISIBLE
                        }
                    }
                    if (::mAlbumAdapter.isInitialized)
                        mAlbumAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedAlbumDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedAlbumDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getArtistData() {
        networkViewModel.recommendedSongsResponse.observe(viewLifecycleOwner) { recommendedArtistDataResponse ->
            when (recommendedArtistDataResponse.status) {
                NetworkStatus.LOADING -> {
                    showProgressBar()
                    enableAndDisableListeners(
                        songsListener = false,
                        albumListener = false,
                        artistListener = false
                    )
                }
                NetworkStatus.SUCCESS -> {
                    hideProgressBar()
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    mViewModel.artistList.clear()
                    val response = recommendedArtistDataResponse.t as ResponseModel
                    val artistList = response.data.artistList
                    if (!artistList.isNullOrEmpty()) {
                        mViewModel.artistList.addAll(artistList)
                    } else {
                        if (mViewModel.selectedTab == RecommendedSongsType.ARTIST) {
                            mBinding.tvNoDataFoundCategoryDetails.visibility = View.VISIBLE
                        }
                    }
                    if (::mArtistAdapter.isInitialized)
                        mArtistAdapter.notifyDataSetChanged()
                }
                NetworkStatus.ERROR -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedArtistDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    enableAndDisableListeners(
                        songsListener = true,
                        albumListener = true,
                        artistListener = true
                    )
                    val error = recommendedArtistDataResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
            }
        }
    }

    private fun setParamsForLottie() {
        val prams = mBinding.noDataFoundInc.noDataLottie.layoutParams
        prams.height = resources.getDimensionPixelSize(R.dimen._200dp)
        prams.width = resources.getDimensionPixelSize(R.dimen._200dp)
        mBinding.noDataFoundInc.noDataLottie.layoutParams = prams
        mBinding.noDataFoundInc.noDataFoundBackButton.visibilityInVisible()
    }

    private fun swipeToRefresh() {
        mBinding.swipeContainer.setOnRefreshListener(OnRefreshListener { // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        if (mBinding.tvNoDataFoundCategoryDetails.isVisible) {
            mBinding.tvNoDataFoundCategoryDetails.visibilityInVisible()
        }
        if (mViewModel.selectedTab == RecommendedSongsType.SONGS) {
            mViewModel.songsList.clear()
            mSongsAdapter.notifyDataSetChanged()
            getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.SONGS)

        } else if (mViewModel.selectedTab == RecommendedSongsType.ALBUM) {
            mViewModel.albumList.clear()
            mAlbumAdapter.notifyDataSetChanged()
            getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.ALBUM)
        } else {
            mViewModel.artistList.clear()
            mArtistAdapter.notifyDataSetChanged()
            getAllDataOfCategory(mViewModel.mCategory?.categoryId, RecommendedSongsType.ARTIST)
        }
    }

    private fun bindViewModel() {
        mBinding.categoriesObj = mViewModel
        mBinding.executePendingBindings()
    }

    companion object {
        private val TAG = "CategoriesDetialsFragment"
        fun newInstance() = CategoriesDetialsFragment()
    }
}