package com.techswivel.qthemusic.ui.fragments.searchScreen

import com.techswivel.qthemusic.customData.enums.RecommendedSongsType
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class SearchScreenViewModel : LocalDatabaseViewModel() {
    var selectedTab: RecommendedSongsType? = null
    var recentPlayedSongsList: MutableList<Any> = ArrayList()
    var recentPlayedAlbumList: MutableList<Any> = ArrayList()
    var recentPlayedArtistList: MutableList<Any> = ArrayList()
    var recentlyPlayerSingleSong = ArrayList<Song>()
}