package com.techswivel.qthemusic.ui.dialogFragments.unlockFragment

import android.os.Bundle
import com.techswivel.qthemusic.customData.enums.UnlockType
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.BaseViewModel
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_ALBUM
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_SONG
import com.techswivel.qthemusic.utils.CommonKeys.Companion.KEY_TYPE

class UnLockViewModel : BaseViewModel() {

    private lateinit var unLockType: UnlockType
    private lateinit var song: Song
    private lateinit var album: Album

    var bundle: Bundle? = Bundle.EMPTY
        set(value) {
            field = value
            unLockType = value?.getSerializable(KEY_TYPE) as UnlockType
            if (value.containsKey(KEY_SONG))
                song = value.getParcelable<Song>(KEY_SONG) as Song
            if (value.containsKey(KEY_ALBUM))
                album = value.getParcelable<Album>(KEY_ALBUM) as Album

        }

    var type: UnlockType
        set(value) {
            unLockType = value
        }
        get() = unLockType

    var songData: Song?
        set(value) {
            song = value ?: Song()
        }
        get() {
            return if (::song.isInitialized) {
                song
            } else {
                null
            }
        }

    var albumData: Album?
        set(value) {
            album = value ?: Album()
        }
        get() {
            return if (::album.isInitialized) {
                album
            } else {
                null
            }
        }


}