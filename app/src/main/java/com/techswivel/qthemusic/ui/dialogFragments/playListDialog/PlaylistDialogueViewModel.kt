package com.techswivel.qthemusic.ui.dialogFragments.playListDialog

import com.techswivel.qthemusic.models.PlaylistModel
import com.techswivel.qthemusic.ui.base.BaseViewModel

class PlaylistDialogueViewModel : BaseViewModel() {
    private lateinit var mSelectedItem: PlaylistModel
    private lateinit var mDataList: MutableList<PlaylistModel>
    var songId: Int? = null

    var mSelectedPlayListItem: PlaylistModel?
        set(value) {
            mSelectedItem = value ?: PlaylistModel()
        }
        get() {
            return if (::mSelectedItem.isInitialized) {
                mSelectedItem
            } else {
                null
            }
        }

    var mData: MutableList<PlaylistModel>
        set(value) {
            mDataList = value
        }
        get() {
            return if (::mDataList.isInitialized) {
                mDataList
            } else {
                mDataList = mutableListOf()
                return mDataList
            }
        }

}