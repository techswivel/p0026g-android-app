package com.techswivel.qthemusic.ui.fragments.preOrderFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.billingclient.api.BillingClient
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.*
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentPreOrderBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.PreOrderData
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.networkViewModel.InAppPurchasesViewModel
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.utils.CommonKeys
import com.techswivel.qthemusic.utils.DialogUtils
import com.techswivel.qthemusic.utils.Utilities
import kotlinx.coroutines.runBlocking

class PreOrderFragment : RecyclerViewBaseFragment(), BaseInterface {
    private lateinit var mBinding: FragmentPreOrderBinding
    private lateinit var mPreOrderAdapter: RecyclerViewAdapter
    private lateinit var mViewModel: PreOrderViewModel
    private lateinit var networkViewModel: SongAndArtistsViewModel
    private lateinit var inAppPurchasesViewModel: InAppPurchasesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        if (arguments?.containsKey(CommonKeys.KEY_DATA) == true) {
            mViewModel.artistId = arguments?.getInt(CommonKeys.KEY_DATA)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentPreOrderBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpPreOrderRecyclerView()
        setPreOrderObserver()
        setClickListeners()
        callApi()
        swipeToRefresh()
    }


    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mPreOrderAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        mPreOrderAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
            override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                return R.layout.recview_pre_order
            }

            override fun onNoDataFound() {

            }

            override fun onViewClicked(view: View, data: Any?) {
                super.onViewClicked(view, data)
                when (view.id) {
                    R.id.pre_order_prize_tag -> {
                        val address =
                            PrefUtils.getString(requireContext(), CommonKeys.KEY_USER_ADRESS)
                        if (address.isNullOrEmpty()) {
                            DialogUtils.runTimeAlert(requireContext(),
                                getString(R.string.address_missing),
                                getString(R.string.address_missing_error),
                                getString(R.string.ok),
                                "",
                                object : DialogUtils.CallBack {
                                    override fun onPositiveCallBack() {

                                    }

                                    override fun onNegativeCallBack() {

                                    }
                                }
                            )
                        } else {
                            mViewModel.selectedItem = data as PreOrderData
                            inAppPurchasesViewModel.connectToGooglePlayBilling(
                                mViewModel.selectedItem?.sku ?: "", activity ?: requireActivity(),
                                BillingClient.SkuType.INAPP
                            )
                        }
                    }
                }
            }
        }, mViewModel.preOrderList)

        return mPreOrderAdapter
    }

    override fun showProgressBar() {
        mBinding.slPreOrdersDetails.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mBinding.slPreOrdersDetails.visibility = View.GONE
        if (mBinding.swipePreOrderContainer.isRefreshing) {
            mBinding.swipePreOrderContainer.isRefreshing = false
        }
    }

    private fun setUpPreOrderRecyclerView() {
        setUpRecyclerView(mBinding.recviewPreOrder, AdapterType.PRE_ORDERS)
    }

    private fun setClickListeners() {
        mBinding.btnBackPreOrder.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this)[PreOrderViewModel::class.java]
        networkViewModel = ViewModelProvider(this)[SongAndArtistsViewModel::class.java]
        inAppPurchasesViewModel = ViewModelProvider(this)[InAppPurchasesViewModel::class.java]
        inAppPurchasesViewModel.setUpBillingClient(requireActivity(), null)
    }

    private fun setPreOrderObserver() {
        inAppPurchasesViewModel.isCalled.observe(viewLifecycleOwner, Observer {
            val count = PrefUtils.getInt(requireContext(), CommonKeys.INCREAMENT_VALUE)
            if (count > Constants.COUNT_MAX_VALUE) {
                PrefUtils.clearStringData(
                    requireContext(),
                    CommonKeys.INCREAMENT_VALUE
                )
                DialogUtils.runTimeAlert(
                    requireContext(),
                    getString(R.string.clear_cache_tittle),
                    getString(R.string.clear_cache_msg),
                    getString(R.string.go_to_settings),
                    getString(R.string.cancel),
                    object : DialogUtils.CallBack {
                        override fun onPositiveCallBack() {

                            Utilities.openPlayStoreSettings(requireActivity())
                        }

                        override fun onNegativeCallBack() {
                        }
                    })
            }
        })
        networkViewModel.preOrdersResponse.observe(
            viewLifecycleOwner,
            Observer { preOrderResponse ->
                when (preOrderResponse.status) {
                    NetworkStatus.LOADING -> {
                        showProgressBar()
                    }
                    NetworkStatus.SUCCESS -> {
                        hideProgressBar()
                        val response = preOrderResponse.t as ResponseModel
                        val preOrderData = response.data.preOrderData
                        if (preOrderData != null) {
                            mViewModel.preOrderList.clear()
                            mViewModel.preOrderList.addAll(preOrderData)
                        }
                        if (::mPreOrderAdapter.isInitialized)
                            mPreOrderAdapter.notifyDataSetChanged()
                    }

                    NetworkStatus.ERROR -> {
                        hideProgressBar()
                        val error = preOrderResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.EXPIRE -> {
                        hideProgressBar()
                        val error = preOrderResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    mViewModel.clearAppSession(requireActivity())
                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                }
            })
        inAppPurchasesViewModel.purchaseToken.observe(viewLifecycleOwner) { purchaseToken ->
            val subscribeToPlanBodyBuilder = SubscribeToPlanBodyBuilder()
            subscribeToPlanBodyBuilder.purchaseToken = purchaseToken.purchaseToken
            subscribeToPlanBodyBuilder.preOrderId = mViewModel.selectedItem?.preOrderId
            if (mViewModel.selectedItem?.preOrderType == PreOrderType.Song) {
                subscribeToPlanBodyBuilder.itemType = ItemType.PRE_ORDER_SONG
                subscribeToPlanBodyBuilder.preOrderSongId = mViewModel.selectedItem?.preOrderId
            }
            if (mViewModel.selectedItem?.preOrderType == PreOrderType.Album) {
                subscribeToPlanBodyBuilder.itemType = ItemType.PRE_ORDER_ALBUM
                subscribeToPlanBodyBuilder.preOrderAlbumId = mViewModel.selectedItem?.preOrderId
            }
            subscribeToPlanBodyBuilder.purchaseType = PurchaseType.GOOGLE_PAY
            subscribeToPlanBodyBuilder.paidAmount = mViewModel.selectedItem?.price?.toFloat()
            subscribeToPlanBodyBuilder.subscriptionPlan = FailedPurchased(
                mViewModel.selectedItem?.preOrderId,
                isAcknowledge = false,
                isFromLogin = false,
                itemType = if (mViewModel.selectedItem?.preOrderType == PreOrderType.Song) ItemType.PRE_ORDER_SONG else ItemType.PRE_ORDER_ALBUM,
                sku = mViewModel.selectedItem?.sku
            )
            mViewModel.requestedData = subscribeToPlanBodyBuilder
            runBlocking {
                val data = mViewModel.mLocalDataManager.getFailedSubscription(purchaseToken.skus[0])
                if (data.isEmpty()) {
                    inAppPurchasesViewModel.subscribeToPlan(
                        subscribeToPlanBodyBuilder
                    )
                }
            }


        }

        inAppPurchasesViewModel.subscribeToPlanResponse.observe(viewLifecycleOwner) { apiResponse ->
            when (apiResponse.status) {
                NetworkStatus.LOADING -> {
                    mBinding.preOrderProgressBar.visibility = View.VISIBLE
                }
                NetworkStatus.SUCCESS -> {
                    mBinding.preOrderProgressBar.visibility = View.INVISIBLE
                    val response = apiResponse.t as ResponseModel
                    val topic = response.data.purchasedPreOrder?.sku
                    if (topic?.contains("_wod", true) == true) {
                        QTheMusicApplication.subscribeToTopic(topic.dropLast(4))
                    } else if (topic?.contains("_wd", true) == true) {
                        QTheMusicApplication.subscribeToTopic(topic.dropLast(3))
                    } else {
                        QTheMusicApplication.subscribeToTopic(topic.toString())
                    }
                    if (response.data.purchasedPreOrder != null) {
                        inAppPurchasesViewModel.acknowledgePurchase(
                            inAppPurchasesViewModel.purchaseToken.value?.purchaseToken,
                            activity ?: requireActivity()
                        )
                    }
                }
                NetworkStatus.ERROR -> {
                    mBinding.preOrderProgressBar.visibility = View.INVISIBLE
                    val error = apiResponse.error as ErrorResponse
                    mViewModel.saveFailedPurchasetoDataBase()
                    DialogUtils.runTimeAlert(
                        requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {

                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.EXPIRE -> {
                    mBinding.preOrderProgressBar.visibility = View.INVISIBLE
                    val error = apiResponse.error as ErrorResponse
                    DialogUtils.runTimeAlert(requireContext(),
                        getString(R.string.error_tittle),
                        error.message.toString(),
                        getString(R.string.ok),
                        "",
                        object : DialogUtils.CallBack {
                            override fun onPositiveCallBack() {
                                mViewModel.clearAppSession(requireActivity())
                            }

                            override fun onNegativeCallBack() {

                            }
                        }
                    )
                }
                NetworkStatus.COMPLETED -> {
                    hideProgressBar()
                }
            }
        }
    }

    private fun callApi() {
        networkViewModel.getPreOrdersList(mViewModel.artistId)
    }

    private fun swipeToRefresh() {
        mBinding.swipePreOrderContainer.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        mViewModel.preOrderList.clear()
        mPreOrderAdapter.notifyDataSetChanged()
        callApi()
    }

    companion object {
        private val TAG = "PreOrderFragment"
        fun newInstance() = PreOrderFragment()
    }
}