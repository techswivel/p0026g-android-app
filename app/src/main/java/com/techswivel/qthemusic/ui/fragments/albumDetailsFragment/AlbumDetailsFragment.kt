package com.techswivel.qthemusic.ui.fragments.albumDetailsFragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.NetworkStatus
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentAlbumDetailsBinding
import com.techswivel.qthemusic.models.ErrorResponse
import com.techswivel.qthemusic.models.ResponseModel
import com.techswivel.qthemusic.models.SongsBodyBuilder
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.source.remote.networkViewModel.SongAndArtistsViewModel
import com.techswivel.qthemusic.ui.activities.subscriptionPlansActivity.SubscriptionPlansActivity
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.utils.*
import kotlinx.coroutines.runBlocking
import org.greenrobot.eventbus.EventBus

class AlbumDetailsFragment : RecyclerViewBaseFragment(), RecyclerViewAdapter.CallBack,
    BaseInterface {

    private lateinit var mBinding: FragmentAlbumDetailsBinding
    private lateinit var mViewModel: AlbumDetailsViewModel
    private lateinit var mSongsAdapter: RecyclerViewAdapter
    private lateinit var mSongsAndArtistsViewModel: SongAndArtistsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        mViewModel.albumData =
            arguments?.getParcelable<Album>(CommonKeys.KEY_ALBUM_DETAILS) as Album
        if (arguments?.containsKey(CommonKeys.ARTIST_MODEL) == true) {
            mViewModel.mArtist = arguments?.getParcelable<Artist>(CommonKeys.ARTIST_MODEL) as Artist
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentAlbumDetailsBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()
        getDataFromBundle()
        bindViewModel()
        setObserverForSongsList()
        clickListeners()
        swipeToRefresh()
    }

    override fun onResume() {
        super.onResume()
        val authUser = mViewModel.getPrefrencesData(context ?: QTheMusicApplication.getContext())
        if (authUser.subscription?.planTitle != null) {
            mBinding.premiumLayoutMain.visibilityGone()
            mBinding.tvPlayAllSongs.visibilityVisible()
        } else {
            mBinding.premiumLayoutMain.visibilityVisible()
            mBinding.tvPlayAllSongs.visibilityGone()
        }
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        TODO("Not yet implemented")
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return mSongsAdapter
    }

    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
        return R.layout.item_trending_songs
    }

    override fun onNoDataFound() {

    }

    override fun onItemClick(data: Any?, position: Int) {
        super.onItemClick(data, position)
        val song = data as Song
        runBlocking {
            try {
                if (arguments?.containsKey(CommonKeys.ARTIST_MODEL) == true) {
                    mViewModel.insertArtistInDataBase(mViewModel.mArtist)
                    mViewModel.artistId = mViewModel.mArtist.artistId
                }
                mViewModel.insertAlbumInDataBase(mViewModel.albumData)
                mViewModel.insertSongInDataBase(song)
                mViewModel.insertDataInDataBaseForSync(
                    song.songId,
                    mViewModel.albumData.albumId,
                    mViewModel.artistId
                )
            } catch (e: Exception) {
                Log.d(TAG, "exeception is ${e.message}")
            }
        }
        if (song.songStatus != SongStatus.PREMIUM) {
            val songBuilder = NextPlaySongBuilder().apply {
                this.currentSongModel = song
                this.songsList = mViewModel.albumSongsList as MutableList<Song>
                this.playedFrom = SongType.ALBUM
            }
            EventBus.getDefault().post(songBuilder)
        } else if (song.songStatus == SongStatus.PREMIUM) {
            val bundle = Bundle().apply {
                putParcelable(CommonKeys.KEY_DATA_MODEL, song)
                putParcelableArrayList(
                    CommonKeys.KEY_SONGS_LIST,
                    mViewModel.albumSongsList as ArrayList<out Song>
                )
                putString(
                    CommonKeys.KEY_SONG_TYPE,
                    SongType.ALBUM.value
                )
            }
            ActivityUtils.startPlayerActivity(activity, bundle)
        }
    }

    override fun showProgressBar() {
        mBinding.slTrendingSongs.visibility = View.VISIBLE
        mBinding.slTrendingSongs.startShimmer()
    }

    override fun hideProgressBar() {
        mBinding.slTrendingSongs.visibility = View.GONE
        mBinding.slTrendingSongs.stopShimmer()
        if (mBinding.swipeAlbumDetailsContainer.isRefreshing) {
            mBinding.swipeAlbumDetailsContainer.isRefreshing = false
        }
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(AlbumDetailsViewModel::class.java)
        mSongsAndArtistsViewModel = ViewModelProvider(this).get(SongAndArtistsViewModel::class.java)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initialization() {
        mSongsAdapter = RecyclerViewAdapter(this, mViewModel.albumSongsList)
        setUpRecyclerView(mBinding.recViewAlbumSongs, AdapterType.ALBUM_SONGS)
        mSongsAdapter.notifyDataSetChanged()

    }

    private fun clickListeners() {
        mBinding.premiumLayoutMain.setOnClickListener {
            val intent = Intent(requireContext(), SubscriptionPlansActivity::class.java)
            startActivity(intent)
        }
        mBinding.backBtnAlbum.setOnClickListener {

        requireActivity().onBackPressed()
        }
        mBinding.tvPlayAllSongs.setOnClickListener {
            if (arguments?.get(CommonKeys.ARTIST_MODEL) != null) {
                mViewModel.insertArtistInDataBase(mViewModel.mArtist)
            }
            mViewModel.insertAlbumInDataBase(mViewModel.albumData)
            var song: Song = Song()

            for (i in mViewModel.songListForPlayAllButton.indices) {
                val data = mViewModel.songListForPlayAllButton[i].songStatus
                if (data != SongStatus.PREMIUM) {
                    song = mViewModel.songListForPlayAllButton[i]

                    break
                }
            }
            if (song.songId != null) {
                val songBuilder = NextPlaySongBuilder().apply {
                    this.currentSongModel = song
                    this.songsList = mViewModel.albumSongsList as MutableList<Song>
                    this.playedFrom = SongType.ALBUM
                }
                EventBus.getDefault().post(songBuilder)

            } else {
                Utilities.showToast(requireContext(), getString(R.string.no_free_song_availble))
            }
        }
    }

    private fun createRequestAndCallApi() {
        val songsBuilder = SongsBodyBuilder()
        songsBuilder.type = SongType.ALBUM
        songsBuilder.albumId = mViewModel.albumData.albumId
        val songsBodyModel = SongsBodyBuilder.build(songsBuilder)
        mSongsAndArtistsViewModel.getSongs(
            songsBodyModel
        )
    }

    private fun getDataFromBundle() {

        createRequestAndCallApi()
    }

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    private fun setObserverForSongsList() {


        mSongsAndArtistsViewModel.songlistResponse.observe(
            viewLifecycleOwner,
            Observer { songsDataResponse ->
                when (songsDataResponse.status) {
                    NetworkStatus.LOADING -> {
                        showProgressBar()
                    }
                    NetworkStatus.SUCCESS -> {
                        hideProgressBar()
                        mViewModel.albumSongsList.clear()
                        mViewModel.songListForPlayAllButton.clear()
                        val response = songsDataResponse.t as ResponseModel
                        val songsList = response.data.songList


                        if (!songsList.isNullOrEmpty()) {
                            mBinding.tvTotalSongsTopTag.text =
                                songsList.size.toString() + " " + getString(R.string._songs)
                            mBinding.tvTotalSongsTag.text =
                                songsList.size.toString() + " " + getString(R.string._songs)
                            mViewModel.albumSongsList.addAll(songsList)
                            mViewModel.songListForPlayAllButton.addAll(songsList)
                            for (i in songsList.indices) {
                                if (songsList[i].songStatus != SongStatus.PREMIUM) {
                                    mViewModel.isAlbumPremium = true
                                    break
                                } else {
                                    mViewModel.isAlbumPremium = false
                                }
                            }
                            if (mViewModel.isAlbumPremium) {
                                mBinding.ivCron.visibilityInVisible()
                                mBinding.premiumLayoutMain.visibilityInVisible()
                                mBinding.tvPlayAllSongs.visibilityVisible()
                            }
                        }
                        if (::mSongsAdapter.isInitialized)
                            mSongsAdapter.notifyDataSetChanged()
                    }
                    NetworkStatus.ERROR -> {
                        hideProgressBar()
                        val error = songsDataResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {

                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.EXPIRE -> {
                        hideProgressBar()
                        val error = songsDataResponse.error as ErrorResponse
                        DialogUtils.runTimeAlert(requireContext(),
                            getString(R.string.error_tittle),
                            error.message.toString(),
                            getString(R.string.ok),
                            "",
                            object : DialogUtils.CallBack {
                                override fun onPositiveCallBack() {
                                    mViewModel.clearAppSession(requireActivity())
                                }

                                override fun onNegativeCallBack() {

                                }
                            }
                        )
                    }
                    NetworkStatus.COMPLETED -> {}
                }
            })
    }

    private fun swipeToRefresh() {
        mBinding.swipeAlbumDetailsContainer.setOnRefreshListener {
            // Your code to refresh the list here.
            prepareForDataRefresh()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun prepareForDataRefresh() {
        mViewModel.albumSongsList.clear()
        mSongsAdapter.notifyDataSetChanged()
        createRequestAndCallApi()
    }

    private fun bindViewModel() {
        mBinding.obj = mViewModel
        mBinding.executePendingBindings()
    }

    companion object {
        private val TAG = "AlbumDetailsFragment"
    }
}
