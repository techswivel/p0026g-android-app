package com.techswivel.qthemusic.ui.activities.authActivity

import android.view.View
import androidx.fragment.app.Fragment
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.SocialSites
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.models.AuthModel
import com.techswivel.qthemusic.models.AuthRequestBuilder
import com.techswivel.qthemusic.models.AuthRequestModel
import okhttp3.MultipartBody

interface AuthActivityImp : BaseInterface {

    fun userLoginRequest(authRequestBuilder: AuthRequestModel)
    fun navigateToHomeScreenAfterLogin(authModel: AuthModel?)
    fun forgotPasswordRequest(
        authRequestBuilder: AuthRequestBuilder,
        view: View?,
        string: String?,
        isResetRequest: Boolean
    )

    fun replaceCurrentFragment(fragment: Fragment)
    fun verifyOtpRequest(authRequestBuilder: AuthRequestModel)

    fun setPasswordRequest(
        authRequestBuilder: AuthRequestBuilder
    )

    fun popUpToAllFragments(fragment: Fragment)
    fun signInWithGoogle()
    fun signInWithFacebook()
    fun userSignUp(
        profile: MultipartBody.Part?,
        name: String?,
        email: String?,
        dob: Int?,
        gender: String?,
        password: String?,
        fcmToken: String?,
        deviceIdentifier: String?,
        completeAddress: String?,
        city: String?,
        state: String?,
        country: String?,
        zipCode: Int?,
        socailId: String?,
        socialSite: SocialSites?
    )

    fun getCategories(categoryType: CategoryType)
    fun replaceCurrentFragmentWithAnimation(fragment: Fragment, view: View, string: String)
    fun replaceCurrentFragmentWithoutAddingToBackStack(fragment: Fragment)
    fun saveInterests()
    fun clearData()
    fun castCurrentFragment(fragment: Fragment)
}