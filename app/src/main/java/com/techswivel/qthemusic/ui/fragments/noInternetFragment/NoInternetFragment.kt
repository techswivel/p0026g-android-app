package com.techswivel.qthemusic.ui.fragments.noInternetFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.techswivel.qthemusic.databinding.FragmentNoInternetBinding
import com.techswivel.qthemusic.ui.fragments.downloadSongFragment.DownloadSongFragment
import com.techswivel.qthemusic.utils.ActivityUtils
import com.techswivel.qthemusic.utils.visibilityInVisible

class NoInternetFragment : Fragment() {
    private lateinit var binding: FragmentNoInternetBinding
    private lateinit var viewModel: NoInternetViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModels()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentNoInternetBinding.inflate(layoutInflater, container, false)
        initViews()
        getClickListeners()
        return binding.root
    }

    private fun initViews() {
        if (viewModel.authModel.subscription?.planTitle.isNullOrEmpty()) {
            binding.tvGoToDownloads.visibilityInVisible()
        }
    }
    private fun getClickListeners() {
        binding.tvGoToDownloads.setOnClickListener {
            ActivityUtils.launchFragment(requireContext(), DownloadSongFragment::class.java.name)
        }
    }

    private fun initViewModels() {
        viewModel = ViewModelProvider(this).get(NoInternetViewModel::class.java)
        viewModel.authModel = viewModel.getPrefrencesData(requireContext())
    }
}