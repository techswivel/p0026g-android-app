package com.techswivel.qthemusic.ui.fragments.yourInterestFragment

import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.models.database.Category
import com.techswivel.qthemusic.ui.base.BaseViewModel

class YourInterestViewModel : BaseViewModel() {
    var mCategoryResponseList: ArrayList<Any> = ArrayList()
    lateinit var categoryResponseList: List<Category?>
    lateinit var categoriesListForApiRequest: ArrayList<Category>
    val selectedCategoryList = mutableListOf<String?>()
    var fragmentType: String? = null
    lateinit var categoryType: CategoryType
    var isFromAuthActivity: Boolean? = false
}