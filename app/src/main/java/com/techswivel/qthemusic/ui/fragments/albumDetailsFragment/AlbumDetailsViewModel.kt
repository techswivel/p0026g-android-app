package com.techswivel.qthemusic.ui.fragments.albumDetailsFragment

import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.base.LocalDatabaseViewModel

class AlbumDetailsViewModel : LocalDatabaseViewModel() {

    var isAlbumPremium: Boolean = false
    var albumSongsList: MutableList<Any> = ArrayList()
    lateinit var albumData: Album
    lateinit var mArtist: Artist
    var artistId: Int? = null
    var songListForPlayAllButton = ArrayList<Song>()
}