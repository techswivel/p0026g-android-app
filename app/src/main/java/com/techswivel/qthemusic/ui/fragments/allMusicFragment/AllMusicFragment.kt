package com.techswivel.qthemusic.ui.fragments.allMusicFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.techswivel.qthemusic.R
import com.techswivel.qthemusic.customData.adapter.RecyclerViewAdapter
import com.techswivel.qthemusic.customData.enums.AdapterType
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.customData.interfaces.BaseInterface
import com.techswivel.qthemusic.databinding.FragmentAllMusicBinding
import com.techswivel.qthemusic.models.ArtistDetails
import com.techswivel.qthemusic.models.RecommendedSongsBodyBuilder
import com.techswivel.qthemusic.models.builder.NextPlaySongBuilder
import com.techswivel.qthemusic.models.database.Album
import com.techswivel.qthemusic.models.database.Artist
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.ui.activities.playerActivity.PlayerActivity
import com.techswivel.qthemusic.ui.base.RecyclerViewBaseFragment
import com.techswivel.qthemusic.ui.fragments.albumDetailsFragment.AlbumDetailsFragment
import com.techswivel.qthemusic.ui.fragments.albumListScreen.AlbumListScreen
import com.techswivel.qthemusic.ui.fragments.artistDetailsFragment.SharedViewModelForAllMusic
import com.techswivel.qthemusic.ui.fragments.preOrderFragment.PreOrderFragment
import com.techswivel.qthemusic.utils.*
import org.greenrobot.eventbus.EventBus

class AllMusicFragment : RecyclerViewBaseFragment(), BaseInterface {
    private lateinit var mBinding: FragmentAllMusicBinding
    private lateinit var mViewModel: AllMusicViewModel
    private lateinit var mSharedViewModel: SharedViewModelForAllMusic
    private lateinit var mSongsAdapter: RecyclerViewAdapter
    private lateinit var mAlbumAdapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        if (arguments?.containsKey(CommonKeys.KEY_DATA) == true) {
            mViewModel.mArtist = arguments?.getParcelable<Artist>(CommonKeys.KEY_DATA) as Artist
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentAllMusicBinding.inflate(layoutInflater, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.mRecommendedSongsBodyBuilder = RecommendedSongsBodyBuilder()
        setRecyclerView()
        observeSharedViewModel()
        setClickListeners()
        showProgressBar()
        setParamsForLottie()
    }

    override fun onPrepareAdapter(): RecyclerView.Adapter<*> {
        return mSongsAdapter
    }

    override fun onPrepareAdapter(adapterType: AdapterType?): RecyclerView.Adapter<*> {
        return when (adapterType) {
            AdapterType.SONGS -> {
                mSongsAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                        return R.layout.recview_songs_categories_details
                    }

                    override fun onNoDataFound() {

                    }

                    override fun onItemClick(data: Any?, position: Int) {
                        super.onItemClick(data, position)
                        val songModel = data as Song
                        if (songModel.songStatus != SongStatus.PREMIUM) {
                            mViewModel.insertArtistInDataBase(mViewModel.mArtist)
                            mViewModel.insertSongInDataBase(songModel)
                            mViewModel.insertDataInDataBaseForSync(
                                songModel.songId, artistId = mViewModel.mArtist.artistId
                            )
                            val songBuilder = NextPlaySongBuilder().apply {
                                this.currentSongModel = songModel
                                this.songsList =
                                    mViewModel.mSongsList as MutableList<Song>
                                this.playedFrom = SongType.ARTIST
                            }
                            EventBus.getDefault().post(songBuilder)
                        } else {
                            val bundle = Bundle().apply {
                                putParcelable(CommonKeys.KEY_DATA_MODEL, songModel)
                                putParcelableArrayList(
                                    CommonKeys.KEY_SONGS_LIST,
                                    mViewModel.mSongsList as ArrayList<out Song>
                                )
                                putString(
                                    CommonKeys.KEY_SONG_TYPE,
                                    SongType.ARTIST.value
                                )
                            }
                            ActivityUtils.startNewActivity(
                                requireActivity(),
                                PlayerActivity::class.java,
                                bundle
                            )
                            requireActivity().overridePendingTransition(
                                R.anim.bottom_up,
                                R.anim.null_transition
                            )
                        }
                    }
                }, mViewModel.mSongsList)
                mSongsAdapter
            }
            else -> {
                mAlbumAdapter = RecyclerViewAdapter(object : RecyclerViewAdapter.CallBack {
                    override fun inflateLayoutFromId(position: Int, data: Any?): Int {
                        return R.layout.recview_album_category_detial
                    }

                    override fun onNoDataFound() {

                    }

                    override fun onItemClick(data: Any?, position: Int) {
                        super.onItemClick(data, position)
                        val mAlbum = data as Album
                        val bundle = Bundle()
                        bundle.putParcelable(CommonKeys.ARTIST_MODEL, mViewModel.mArtist)
                        bundle.putParcelable(CommonKeys.KEY_ALBUM_DETAILS, mAlbum)
                        ActivityUtils.launchFragment(
                            requireContext(),
                            AlbumDetailsFragment::class.java.name,
                            bundle
                        )
                    }
                }, mViewModel.mAlbumList)
                mAlbumAdapter
            }
        }
    }

    override fun showProgressBar() {
        mBinding.slAlbums.visibility = View.VISIBLE
        mBinding.slSongsArtistDetails.visibility = View.VISIBLE
        mBinding.noDataFoundAllMusic.visibilityInVisible()
    }

    override fun hideProgressBar() {
        if (!mViewModel.mAlbumList.isNullOrEmpty()) {
            mBinding.slAlbums.visibility = View.INVISIBLE
        } else {
            mBinding.slAlbums.visibility = View.GONE
        }
        mBinding.slSongsArtistDetails.visibility = View.GONE
    }

    private fun setClickListeners() {
        mBinding.tvViewAllTag.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable(CommonKeys.KEY_ARTIST_DETAILS_MODEL, mViewModel.mArtistDetails)
            ActivityUtils.launchFragment(requireContext(), AlbumListScreen::class.java.name, bundle)
        }

        mBinding.preOrderLayoutMain.setOnClickListener {
            val bundle = Bundle()
            mViewModel.mArtist.artistId?.let { it1 -> bundle.putInt(CommonKeys.KEY_DATA, it1) }
            ActivityUtils.launchFragment(
                requireContext(),
                PreOrderFragment::class.java.name,
                bundle
            )
        }
    }

    private fun setRecyclerView() {
        setUpHorizentalRecyclerView(mBinding.recviewAlbumAllMusic, 8, AdapterType.ALBUM)
        setUpRecyclerView(mBinding.recviewSongsAllMusic, AdapterType.SONGS)
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this).get(AllMusicViewModel::class.java)
        mSharedViewModel =
            ViewModelProvider(requireActivity()).get(SharedViewModelForAllMusic::class.java)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun observeSharedViewModel() {
        mSharedViewModel.mArtistData.observe(viewLifecycleOwner, Observer { artistData ->
            mViewModel.mArtistDetails = artistData
            val artistAlbums = artistData.albums
            val artistSongs = artistData.songs
            if (artistData != null) {
                bindViewModel(artistData)
            }
            if (!artistAlbums.isNullOrEmpty()) {
                hideProgressBar()
                mViewModel.mAlbumList.clear()
                mViewModel.mAlbumList.addAll(artistAlbums)

            } else {
                mBinding.tvAlbumTagId.visibility = View.GONE
                mBinding.tvViewAllTag.visibility = View.GONE
                mBinding.recviewAlbumAllMusic.visibility = View.GONE
                mBinding.slAlbums.visibility = View.GONE
            }
            if (::mAlbumAdapter.isInitialized)
                mAlbumAdapter.notifyDataSetChanged()
            if (!artistSongs.isNullOrEmpty()) {
                hideProgressBar()
                mViewModel.mSongsList.clear()
                mViewModel.mSongsList.addAll(artistSongs)
                mBinding.tvSongCountTag.text =
                    mViewModel.mSongsList.size.toString().plus(" ").plus(getString(R.string._songs))
            } else {
                mBinding.noDataFoundAllMusic.visibilityVisible()
                hideProgressBar()
            }
            if (::mSongsAdapter.isInitialized)
                mSongsAdapter.notifyDataSetChanged()
        })
        mSharedViewModel.isForNewApiCall.observe(
            viewLifecycleOwner,
            Observer { setUpForNewApiCall ->
                if (setUpForNewApiCall) {
                    if (mBinding.preOrderLayoutMain.isVisible) {
                        mBinding.preOrderLayoutMain.visibilityGone()
                    }
                    mBinding.tvSongCountTag.text = ""
                    mViewModel.mAlbumList.clear()
                    mViewModel.mSongsList.clear()
                    mAlbumAdapter.notifyDataSetChanged()
                    mSongsAdapter.notifyDataSetChanged()
                    showProgressBar()
                }

            })

    }

    private fun setParamsForLottie() {
        val prams = mBinding.noDataFoundInc.noDataLottie.layoutParams
        prams.height = resources.getDimensionPixelSize(R.dimen._200dp)
        prams.width = resources.getDimensionPixelSize(R.dimen._200dp)
        mBinding.noDataFoundInc.noDataLottie.layoutParams = prams
        mBinding.noDataFoundInc.noDataFoundBackButton.visibilityInVisible()
    }

    private fun bindViewModel(artistDetails: ArtistDetails) {
        mBinding.artist = artistDetails
        mBinding.preOrderLayoutIncludeLayout.tvPreOrderMsg.text = artistDetails.artistName
        mBinding.executePendingBindings()
    }

    companion object {
        private val TAG = "AllMusicFragment"
        fun newInstance() = AllMusicFragment()
    }
}