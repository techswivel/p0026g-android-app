package com.techswivel.qthemusic.dataManager

import androidx.lifecycle.LiveData
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.models.database.*

interface LocalDataManagerImp {
    fun deleteAllLocalData()

    //--------------- Downloading status -------------
    fun saveDownloadId(ids: DownloadIds)
    suspend fun isDownloadIdExist(id: Long): Boolean
    suspend fun getDownloadFile(id: Long): DownloadIds
    fun deleteRecord(ids: DownloadIds)
    fun getAllRunningDownloads(): LiveData<List<DownloadIds>>
    suspend fun getRunningDownloads(): List<DownloadIds>
    suspend fun deleteIfFailed(translationId: Int)
    suspend fun updateStatus(status: DownloadingStatus, downloadId: Long, reason: String)

    //--------------- Song queries -------------

    suspend fun saveSong(song: Song)

    fun insertRecentPlayedSongToDatabase(song: Song)
    fun insertRecentPlayedAlbumToDatabase(album: Album)
    fun insertRecentPlayedArtistToDatabase(artist: Artist)
    fun getRecentPlayedSongs(): LiveData<List<Song>>
    suspend fun deleteSongById(songId: Int?)
    suspend fun getDownloadedSongs(): List<Song>
    fun getRecentPlayedAlbum(): LiveData<List<Album>>
    fun getRecentPlayedArtist(): LiveData<List<Artist>>
    fun deleteSongsIfExceed()
    fun deleteAlbumIfExceed()
    fun deleteArtistIfExceed()
    fun insertCategoriesToDatabase(list: List<Category>)
    fun getCategoriesFromDatabase(): LiveData<List<Category>>
    fun insertDataForSync(songId: Int?, albumId: Int?, artistId: Int?)
    fun deleteListeningHistory()
    fun insertSyncedSongsListToDatabase(songsList: List<Song>)
    fun insertSyncedArtistsListToDatabase(artistsList: List<Artist>)
    fun insertSyncedAlbumsListToDatabase(albumsList: List<Album>)
    fun getAllSongs(): LiveData<List<Song>>
    fun getAllAlbums(): LiveData<List<Album>>
    fun getAllArtist(): LiveData<List<Artist>>

    //--------------- failed purchased queries ------------- //

    suspend fun savedFailedPurchase(subscriptionPlans: FailedPurchased)
    suspend fun savedFiledPurchase(purchasedItem: List<FailedPurchased>)
    suspend fun deleteFailedPurchase(subscriptionPlans: FailedPurchased)
    suspend fun deleteFailedPurchase(purchasedItem: List<FailedPurchased>)
    fun getAllFailedPurchased(): LiveData<List<FailedPurchased>>
    suspend fun getFailedSubscription(sku: String): List<FailedPurchased>
    suspend fun getAllPurchased(): List<FailedPurchased>

    //--------------- saved state quries ------------- //

    suspend fun getCurrentState(): PlayerSavedState
    suspend fun savedState(data: PlayerSavedState)
    suspend fun deleteAllSavedStateData()

}