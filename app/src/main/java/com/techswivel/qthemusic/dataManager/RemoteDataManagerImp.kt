package com.techswivel.qthemusic.dataManager

import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.models.database.UpdatePlayListModel
import io.reactivex.Observable
import retrofit2.Response

interface RemoteDataManagerImp {
    fun getRecommendedSongsData(recommendedSongsBodyModel: RecommendedSongsBodyModel): Observable<Response<ResponseMain>>
    fun getSongsFromServer(songsBodyModel: SongsBodyModel): Observable<Response<ResponseMain>>
    fun getFollowingArtist(): Observable<Response<ResponseMain>>
    fun updatePlayList(
        updatePlayListModel: UpdatePlayListModel
    ): Observable<Response<ResponseMain>>

    fun setFavoriteSong(favoriteSongBody: FavoriteSongBody): Observable<Response<ResponseMain>>
    fun deletePlaylist(playlistModel: PlaylistModel): Observable<Response<ResponseMain>>
    fun getBuyingHistory(
        typeOfTransaction: String
    ): Observable<Response<ResponseMain>>

    fun getCategoriesData(categoryType: CategoryType): Observable<Response<ResponseMain>>
    fun getGoogleAccessToken(serverAuthCode: String): Observable<Response<GoogleResponseModel>>
    fun logoutUser(deviceIdentifier: String): Observable<Response<ResponseMain>>
    fun profileUpdate(authRequestBuilder: AuthRequestBuilder): Observable<Response<ResponseMain>>
    fun userLogin(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>
    fun sendOtp(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>
    fun verifyOtpRequest(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>
    fun setNewPassword(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>>
    fun getSearcherSongsList(queryRequestModel: QueryRequestModel): Observable<Response<ResponseMain>>
    fun signUp(
        mAuthRequestBuilder: AuthRequestBuilder
    ): Observable<Response<ResponseMain>>

    fun saveInterest(category: Interests): Observable<Response<ResponseMain>>
    fun getPreOrders(artistId: Int?): Observable<Response<ResponseMain>>
    fun getArtistDetails(artistId: Int?): Observable<Response<ResponseMain>>
    fun getArtistFollowingDetails(artistModel: ArtistModel): Observable<Response<ResponseMain>>


    fun getPlayListFromServer(): Observable<Response<ResponseMain>>
    fun savePlaylist(playlistModel: PlaylistModel): Observable<Response<ResponseMain>>
    fun addOrRemoveSongToDownload(
        type: ActionType,
        song: Song,
        songType: SongType?
    ): Observable<Response<ResponseMain>>

    fun startDownloadingSong(song: Song, type: SongType)

    fun loadNextSongs(
        nextSongParameters: NextPlaySong
    ): Observable<Response<ResponseMain>>

    fun syncRecentlyPlayedData(): Observable<Response<ResponseMain>>

    fun getSubscriptionPlans(): Observable<Response<ResponseMain>>
    fun subscribeToPlan(
        subscribeToPlanBody: SubscribeToPlanBodyBuilder
    ): Observable<Response<ResponseMain>>
}