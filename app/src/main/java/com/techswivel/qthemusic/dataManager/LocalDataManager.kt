package com.techswivel.qthemusic.dataManager

import androidx.lifecycle.LiveData
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.customData.enums.DownloadingStatus
import com.techswivel.qthemusic.models.ListeningHistory
import com.techswivel.qthemusic.models.database.*
import com.techswivel.qthemusic.source.local.database.AppRoomDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


object LocalDataManager : BaseDataManager(), LocalDataManagerImp {

    /** -------------- Database Instance method ---------------------- */

    override fun deleteAllLocalData() {
        getRoomInstance().clearAllTables()
    }

    override fun insertRecentPlayedAlbumToDatabase(album: Album) {
        val unixTime = System.currentTimeMillis() / 1000L
        album.recentPlay = unixTime
        runBlocking {
            getRoomInstance().mAlbumDao().insertAlbum(album)
        }
    }

    override fun insertRecentPlayedArtistToDatabase(artist: Artist) {
        val unixTime = System.currentTimeMillis() / 1000L
        artist.recentPlay = unixTime
        runBlocking {
            getRoomInstance().mArtistDao().insertArtist(artist)
        }
    }

    override fun getRecentPlayedAlbum(): LiveData<List<Album>> {
        return getRoomInstance().mAlbumDao().getRecentlyPlayedAlbums()
    }

    override fun getRecentPlayedArtist(): LiveData<List<Artist>> {
        return getRoomInstance().mArtistDao().getRecentlyPlayedArtists()
    }

    override suspend fun getDownloadedSongs(): List<Song> {
        return getRoomInstance().mSongsDao().getDownloadedSongs()
    }

    override fun deleteAlbumIfExceed() {
        runBlocking {
            getRoomInstance().mAlbumDao().getRecentlyPlayedAlbums()
        }
    }

    override fun deleteArtistIfExceed() {
        runBlocking {
            getRoomInstance().mArtistDao().getRecentlyPlayedArtists()
        }
    }

    override fun insertCategoriesToDatabase(list: List<Category>) {
        runBlocking {
            getRoomInstance().mCategoriesDao().insertCategoriesList(list)
        }

    }

    override fun getCategoriesFromDatabase(): LiveData<List<Category>> {
        return getRoomInstance().mCategoriesDao().getCategoriesList()
    }

    /** -------------- History Synced methods ---------------------- */
    //Inserting recently played data in ListeningHistory table for sync with server...
    override fun insertDataForSync(songId: Int?, albumId: Int?, artistId: Int?) {
        val listeningHistory = ListeningHistory(null, albumId, artistId, songId)
        runBlocking {
            getRoomInstance().mSyncDao().insertListeningHistory(listeningHistory)
        }
    }

    //deleting all data of ListeningHistory table...
    override fun deleteListeningHistory() {
        runBlocking {
            getRoomInstance().mSyncDao().deleteAllListeningHistory()
        }
    }

    override fun insertSyncedSongsListToDatabase(songsList: List<Song>) {
        runBlocking {
            songsList.onEach {
                val unixTime = System.currentTimeMillis() / 1000L
                it.recentPlay = unixTime
            }
            getRoomInstance().mSyncDao().insertSyncedSongsList(songsList)
        }
    }

    override fun insertSyncedArtistsListToDatabase(artistsList: List<Artist>) {
        runBlocking {
            artistsList.onEach {
                val unixTime = System.currentTimeMillis() / 1000L
                it.recentPlay = unixTime
            }
            getRoomInstance().mSyncDao().insertSyncedArtistsList(artistsList)
        }
    }

    override fun insertSyncedAlbumsListToDatabase(albumsList: List<Album>) {
        runBlocking {
            albumsList.onEach {
                val unixTime = System.currentTimeMillis() / 1000L
                it.recentPlay = unixTime
            }
            getRoomInstance().mSyncDao().insertSyncedAlbumsList(albumsList)
        }
    }

    override fun getAllSongs(): LiveData<List<Song>> {
        return getRoomInstance().mSongsDao().getAllSongs()
    }

    override fun getAllAlbums(): LiveData<List<Album>> {
        return getRoomInstance().mAlbumDao().getAlbumList()
    }

    override fun getAllArtist(): LiveData<List<Artist>> {
        return getRoomInstance().mArtistDao().getArtistList()
    }

    /** -------------- Download methods ---------------------- */

    override fun saveDownloadId(ids: DownloadIds) {
        GlobalScope.launch {
            getRoomInstance().downloadDao().saveDownloadId(ids)
        }
    }

    override suspend fun isDownloadIdExist(id: Long): Boolean {
        return getRoomInstance().downloadDao().isDownloadIdExist(id)
    }

    override suspend fun getDownloadFile(id: Long): DownloadIds {
        return getRoomInstance().downloadDao().getDownloadFile(id)
    }

    override fun deleteRecord(ids: DownloadIds) {
        GlobalScope.launch {
            getRoomInstance().downloadDao().deleteRecord(ids)
        }
    }

    override fun getAllRunningDownloads(): LiveData<List<DownloadIds>> {
        return getRoomInstance().downloadDao().getAllRunningDownloads()
    }

    override suspend fun getRunningDownloads(): List<DownloadIds> {
        return getRoomInstance().downloadDao().getRunningDownloads()
    }

    override suspend fun deleteIfFailed(translationId: Int) {
        getRoomInstance().downloadDao().deleteIfFailed(translationId)
    }

    override suspend fun updateStatus(status: DownloadingStatus, downloadId: Long, reason: String) {
        getRoomInstance().downloadDao().updateStatus(status, downloadId, reason)
    }

    /** -------------- purchased method ------------------ */

    override suspend fun savedFailedPurchase(subscriptionPlans: FailedPurchased) {
        getRoomInstance().mSubscriptionDao().savedFiledPurchase(subscriptionPlans)
    }

    override suspend fun savedFiledPurchase(purchasedItem: List<FailedPurchased>) {
        getRoomInstance().mSubscriptionDao().savedFiledPurchase(purchasedItem)
    }

    override suspend fun deleteFailedPurchase(subscriptionPlans: FailedPurchased) {
        getRoomInstance().mSubscriptionDao().deleteFailedPurchase(subscriptionPlans)
    }

    override suspend fun deleteFailedPurchase(purchasedItem: List<FailedPurchased>) {
        getRoomInstance().mSubscriptionDao().deleteFailedPurchase(purchasedItem)
    }

    override fun getAllFailedPurchased(): LiveData<List<FailedPurchased>> {
        return getRoomInstance().mSubscriptionDao().getAllFailedSubscription()
    }

    override suspend fun getFailedSubscription(sku: String): List<FailedPurchased> {
        return getRoomInstance().mSubscriptionDao().getFailedSubscription(sku)
    }

    override suspend fun getAllPurchased(): List<FailedPurchased> {
        return getRoomInstance().mSubscriptionDao().getAllPurchased()
    }

    /** -------------- Songs methods ---------------------- */

    override suspend fun saveSong(song: Song) {
        getRoomInstance().mSongsDao().saveSong(song)
    }

    override fun deleteSongsIfExceed() {

    }

    override fun getRecentPlayedSongs(): LiveData<List<Song>> {
        return getRoomInstance().mSongsDao().getRecentPlayedSongs()
    }

    override fun insertRecentPlayedSongToDatabase(song: Song) {
        val unixTime = System.currentTimeMillis() / 1000L
        song.recentPlay = unixTime
        runBlocking {
            getRoomInstance().mSongsDao().insertSong(song)
        }
    }

    override suspend fun deleteSongById(songId: Int?) {
        getRoomInstance().mSongsDao().deleteSongById(songId)
    }


    /** -------------- Saved State methods ---------------------- */

    override suspend fun getCurrentState(): PlayerSavedState {
        return getRoomInstance().savedDao().getCurrentState()
    }

    override suspend fun savedState(data: PlayerSavedState) {
        getRoomInstance().savedDao().savedState(data)
    }

    override suspend fun deleteAllSavedStateData() {
        getRoomInstance().savedDao().deleteAllData()
    }


    private fun getRoomInstance(): AppRoomDatabase {
        return AppRoomDatabase.getDatabaseInstance(QTheMusicApplication.getContext())
    }

}
