package com.techswivel.qthemusic.dataManager

import android.content.Intent
import com.techswivel.qthemusic.BuildConfig
import com.techswivel.qthemusic.application.QTheMusicApplication
import com.techswivel.qthemusic.constant.Constants
import com.techswivel.qthemusic.customData.enums.ActionType
import com.techswivel.qthemusic.customData.enums.CategoryType
import com.techswivel.qthemusic.customData.enums.SongStatus
import com.techswivel.qthemusic.customData.enums.SongType
import com.techswivel.qthemusic.models.*
import com.techswivel.qthemusic.models.builder.SubscribeToPlanBodyBuilder
import com.techswivel.qthemusic.models.database.FailedPurchased
import com.techswivel.qthemusic.models.database.Song
import com.techswivel.qthemusic.models.database.UpdatePlayListModel
import com.techswivel.qthemusic.services.DownloadService
import com.techswivel.qthemusic.services.WorkManager
import com.techswivel.qthemusic.source.local.database.AppRoomDatabase
import com.techswivel.qthemusic.source.local.preference.PrefUtils
import com.techswivel.qthemusic.source.remote.retrofit.ApiService
import com.techswivel.qthemusic.ui.activities.authActivity.AuthActivity
import com.techswivel.qthemusic.utils.PlayerUtils
import com.techswivel.qthemusic.utils.Utilities
import com.techswivel.qthemusic.utils.toDeviceName
import com.techswivel.qthemusic.utils.toFailedPurchased
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Response


object RemoteDataManager : BaseDataManager(), RemoteDataManagerImp {


    private lateinit var data: GoogleAuthModel
    private var localDtaManager: LocalDataManager = LocalDataManager
    private val TAG = "DataManagerTAG"

    init {
        System.loadLibrary(Constants.CPP_LIBRARY_NAME)
    }
    /** -------------- API methods ---------------------- */


    /** -------------- Next Song method ---------------------- */
    override fun loadNextSongs(nextSongParameters: NextPlaySong): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .loadNextSongs(nextSongParameters)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { response ->
                if (response.isSuccessful) {
                    val data = response.body()?.response
                    data?.data?.songList?.forEach { song ->
                        if (song.songStatus == SongStatus.PURCHASED) {
                            PlayerUtils.preCacheMedia(song.songAudioUrl)
                            if (!song.songVideoUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songVideoUrl)
                            }
                        }
                    }
                }
            }
    }

    /** -------------- Sync method ---------------------- */

    override fun syncRecentlyPlayedData(): Observable<Response<ResponseMain>> {
        val allListeningHistory = ArrayList<ListeningHistory>()
        val recentlyPlayed = ArrayList<ListeningHistory>()
        val syncHistoryModel = SyncHistoryModel(false, false, null, null)
        runBlocking {
            getRoomInstance().mSyncDao().getTopFiveRecentlyPlayedSongs().let {
                recentlyPlayed.addAll(it)
                if (!recentlyPlayed.isNullOrEmpty()) {
                    syncHistoryModel.isRecentlyPlayed = true
                    syncHistoryModel.recentlyPlayed = recentlyPlayed
                }
            }
            getRoomInstance().mSyncDao().getListeningHistory().let {
                allListeningHistory.addAll(it)
                if (!allListeningHistory.isNullOrEmpty()) {
                    syncHistoryModel.isListeningHistory = true
                    syncHistoryModel.listeningHistory = allListeningHistory
                }
            }
        }
        return ApiService.get()
            .syncedRecentlyPlayed(syncHistoryModel, Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /** -------------- RoomDb Instance method ---------------------- */

    private fun getRoomInstance(): AppRoomDatabase {
        return AppRoomDatabase.getDatabaseInstance(QTheMusicApplication.getContext())
    }

    /** -------------- Song And Artist method ---------------------- */

    override fun getRecommendedSongsData(recommendedSongsBodyModel: RecommendedSongsBodyModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getRecommendedSongsData(recommendedSongsBodyModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { response ->
                if (response.isSuccessful) {
                    val data = response.body()?.response
                    data?.data?.songList?.forEach { song ->
                        if (song.songStatus == SongStatus.PURCHASED) {
                            PlayerUtils.preCacheMedia(song.songAudioUrl)
                            if (!song.songVideoUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songVideoUrl)
                            }
                        }
                    }
                }
            }
    }

    override fun getBuyingHistory(
        typeOfTransaction: String
    ): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getBuyingHistory(typeOfTransaction, Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getCategoriesData(categoryType: CategoryType): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getCategories(Constants.PAGE, Constants.LIMIT, categoryType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getSongsFromServer(songsBodyModel: SongsBodyModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getSongsFromServer(Constants.PAGE, Constants.LIMIT, songsBodyModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { response ->
                if (response.isSuccessful) {
                    val data = response.body()?.response
                    data?.data?.songList?.forEach { song ->
                        if (song.songStatus == SongStatus.PURCHASED) {
                            PlayerUtils.preCacheMedia(song.songAudioUrl)
                            if (!song.songVideoUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songVideoUrl)
                            }
                        }
                    }
                }
            }
    }

    override fun getFollowingArtist(): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getFollowingArtistList(Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /** -------------- Profile method ---------------------- */

    /**
     * This method will get profile data , updating playlist
     */


    override fun saveInterest(category: Interests): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .saveInterest(category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun profileUpdate(authRequestBuilder: AuthRequestBuilder): Observable<Response<ResponseMain>> {

        return ApiService.get()
            .updateProfile(
                authRequestBuilder.mProfile,
                authRequestBuilder.mName,
                authRequestBuilder.mEmail,
                authRequestBuilder.mDob,
                authRequestBuilder.mGender,
                authRequestBuilder.mCompleteAddress,
                authRequestBuilder.mCity,
                authRequestBuilder.mState,
                authRequestBuilder.mCountry,
                authRequestBuilder.mZipCode,
                authRequestBuilder.mIsEnableNotification,
                authRequestBuilder.mIsArtistUpdateEnable,
                authRequestBuilder.mPhoneNumber,
                authRequestBuilder.mOtp
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun updatePlayList(
        updatePlayListModel: UpdatePlayListModel
    ): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .updatePlayList(updatePlayListModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getPlayListFromServer(): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getPlayList(Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun savePlaylist(playlistModel: PlaylistModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .savePlayList(playlistModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun setFavoriteSong(favoriteSongBody: FavoriteSongBody): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .setFavoriteSong(favoriteSongBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun deletePlaylist(playlistModel: PlaylistModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .deletePlayList(playlistModel.playListId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /** -------------- Google Api method ---------------------- */

    // Keys Staging
    private external fun getRemoteGoogleClientIdStaging(): String
    private external fun getGoogleClientSecretStaging(): String
    private external fun getGoogleClientRedirectUriStaging(): String

    //  Keys Development
    private external fun getRemoteGoogleClientIdDevelopment(): String
    private external fun getGoogleClientSecretDevelopment(): String
    private external fun getGoogleClientRedirectUriDevelopment(): String

    //  Keys Acceptance
    private external fun getRemoteGoogleClientIdAcceptance(): String
    private external fun getGoogleClientSecretAcceptance(): String
    private external fun getGoogleClientRedirectUriAcceptance(): String

    //  Keys Production
    private external fun getRemoteGoogleClientIdProduction(): String
    private external fun getGoogleClientSecretProduction(): String
    private external fun getGoogleClientRedirectUriProduction(): String

    override fun getGoogleAccessToken(serverAuthCode: String): Observable<Response<GoogleResponseModel>> {

        when {
            BuildConfig.FLAVOR.equals(Constants.STAGING) -> {
                data = GoogleAuthModel(
                    serverAuthCode,
                    "authorization_code",
                    getRemoteGoogleClientIdStaging(),
                    getGoogleClientSecretStaging(),
                    getGoogleClientRedirectUriStaging()
                )
            }
            BuildConfig.FLAVOR.equals(Constants.DEVELOPMENT) -> {
                data = GoogleAuthModel(
                    serverAuthCode,
                    "authorization_code",
                    getRemoteGoogleClientIdDevelopment(),
                    getGoogleClientSecretDevelopment(),
                    getGoogleClientRedirectUriDevelopment()
                )
            }
            BuildConfig.FLAVOR.equals(Constants.ACCEPTANCE) -> {
                data = GoogleAuthModel(
                    serverAuthCode,
                    "authorization_code",
                    getRemoteGoogleClientIdAcceptance(),
                    getGoogleClientSecretAcceptance(),
                    getGoogleClientRedirectUriAcceptance()
                )
            }
            else -> {
                data = GoogleAuthModel(
                    serverAuthCode,
                    "authorization_code",
                    getRemoteGoogleClientIdProduction(),
                    getGoogleClientSecretProduction(),
                    getGoogleClientRedirectUriProduction()
                )
            }
        }
        return ApiService.getGoogleResponse().getGoogleToken(data).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /** -------------- Authorization method ---------------------- */


    override fun userLogin(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .userLogin(authRequestModel)
            .doOnNext { response ->
                val authData = response.body()?.response
                if (authData?.status == true) {
                    WorkManager.startService()
                    val purchasedItemList = mutableListOf<FailedPurchased>()
                    for (songItem in authData.data.authModel?.purchasedsongs ?: emptyList()) {
                        purchasedItemList.add(songItem.toFailedPurchased())
                    }
                    for (albumItem in authData.data.authModel?.purchasedAlbums ?: emptyList()) {
                        purchasedItemList.add(albumItem.toFailedPurchased())
                    }
                    GlobalScope.launch {
                        LocalDataManager.savedFiledPurchase(purchasedItemList as List<FailedPurchased>)
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun sendOtp(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>> {
        return ApiService.get().sendOtp(authRequestModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun verifyOtpRequest(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .verifyOtpRequest(authRequestModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun setNewPassword(authRequestModel: AuthRequestModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .setNewPassword(authRequestModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getSearcherSongsList(queryRequestModel: QueryRequestModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .searchSongs(queryRequestModel, Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { response ->
                if (response.isSuccessful) {
                    val data = response.body()?.response
                    data?.data?.searchedSongs?.forEach { song ->
                        if (song.songStatus == SongStatus.PURCHASED) {
                            PlayerUtils.preCacheMedia(song.songAudioUrl)
                            if (!song.songVideoUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songVideoUrl)
                            }
                        }
                    }
                }

            }
    }

    override fun signUp(
        mAuthRequestBuilder: AuthRequestBuilder
    ): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .signUp(
                mAuthRequestBuilder.mProfile,
                mAuthRequestBuilder.mName,
                mAuthRequestBuilder.mEmail,
                mAuthRequestBuilder.mDob,
                mAuthRequestBuilder.mGender,
                mAuthRequestBuilder.mPassword,
                mAuthRequestBuilder.mFcmToken,
                mAuthRequestBuilder.mDeviceIdentifier,
                mAuthRequestBuilder.mCompleteAddress,
                mAuthRequestBuilder.mCity,
                mAuthRequestBuilder.mState,
                mAuthRequestBuilder.mCountry,
                mAuthRequestBuilder.zipCode,
                Utilities.toRequestedBody(QTheMusicApplication.getContext().toDeviceName()),
                mAuthRequestBuilder.mSocailId,
                mAuthRequestBuilder.mSocialSite
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun addOrRemoveSongToDownload(
        type: ActionType,
        song: Song,
        songType: SongType?
    ): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .addOrRemoveSongToDownload(UpdateDownloadListBody(song.songId, type))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun startDownloadingSong(song: Song, type: SongType) {
        QTheMusicApplication.getContext().startService(
            DownloadService.getDownloadService(
                QTheMusicApplication.getContext(),
                if (type == SongType.AUDIO) song.songAudioUrl else song.songVideoUrl,
                song,
                type
            )
        )
    }

    override fun getPreOrders(artistId: Int?): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getPreOrdersDetail(artistId, Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getArtistDetails(artistId: Int?): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .getArtistDetail(artistId, Constants.PAGE, Constants.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getArtistFollowingDetails(artistModel: ArtistModel): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .artistFollowUnFollowRequest(artistModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun logoutUser(deviceIdentifier: String): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .logoutUser(deviceIdentifier)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                WorkManager.stopService()
                PrefUtils.clearAllPrefData(QTheMusicApplication.getContext())
                GlobalScope.launch {
                    localDtaManager.deleteAllLocalData()
                }
            }
    }

    fun clearAppSession() {
        PrefUtils.clearAllPrefData(QTheMusicApplication.getContext())
        getRoomInstance().clearAllTables()
        val intent = Intent(QTheMusicApplication.getContext(), AuthActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        QTheMusicApplication.getContext().startActivity(intent)
    }

    override fun getSubscriptionPlans(): Observable<Response<ResponseMain>> {
        return ApiService.get().getSubscriptionPlans(1, 300)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun subscribeToPlan(
        subscribeToPlanBody: SubscribeToPlanBodyBuilder
    ): Observable<Response<ResponseMain>> {
        return ApiService.get()
            .subscribeToPlan(SubscribeToPlanBodyBuilder.build(subscribeToPlanBody))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { response ->
                if (response.isSuccessful) {
                    val data = response.body()?.response
                    if (data?.data?.purchasedSong != null) {
                        if (!data.data.purchasedSong?.songAudioUrl.isNullOrEmpty()) {
                            PlayerUtils.preCacheMedia(data.data.purchasedSong?.songAudioUrl)
                        }
                        if (!data.data.purchasedSong?.songVideoUrl.isNullOrEmpty()) {
                            PlayerUtils.preCacheMedia(data.data.purchasedSong?.songVideoUrl)
                        }
                    } else if (data?.data?.purchasedAlbum != null) {
                        data.data.purchasedAlbum?.songs?.forEach { song ->
                            if (!song.songAudioUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songAudioUrl)
                            }
                            if (!song.songVideoUrl.isNullOrEmpty()) {
                                PlayerUtils.preCacheMedia(song.songVideoUrl)
                            }
                        }
                    }
                }

            }
    }
}